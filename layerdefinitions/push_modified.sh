# full path to current script
FULLPATH=$(readlink -f $0)
DIRNAME=$(dirname $FULLPATH)
# find prefix up to /feltgis-config..., i.e. common base path for the feltgit repos
GITROOT=${FULLPATH%%"/feltgis-config/"*}
# look for update script, warn if missing
CONFIGUPDATER="$GITROOT"/feltgis/go/appengine_sync/configupdater/configupdater
if ! [ -x "${CONFIGUPDATER}" ]; then
  echo "$CONFIGUPDATER is missing, building"
  cd "$GITROOT"/feltgis/go/appengine_sync/configupdater/ && go build && cd -
fi

# run script
pushd "$DIRNAME"
export FG_IS_DEV=true 
git status -- . \
    | grep "new file:\|modified:" \
    | cut -f2 -d":"|grep "\.json$" \
    | sed "s/^[ \t]*//" \
    | xargs -I {} "$CONFIGUPDATER" "$DIRNAME"/{}
popd
