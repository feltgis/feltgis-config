#!/bin/bash

# Specify the JSON file
JSON_FILE="$1"

# Get the file name without the extension
FILE_NAME=${JSON_FILE%%.*}


# Update the "name" attribute in the JSON file
jq --arg name "$FILE_NAME" '.name = $name' "$JSON_FILE" > temp.json && mv temp.json "$JSON_FILE"

echo "The name attribute in $JSON_FILE has been set to $FILE_NAME."
