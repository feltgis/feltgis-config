package main

import "strings"

func populatePunktLinjerFlaterForm_linjer(b *FormBuilder, dib *FormDrawingInfoBuilder) {

	//file types
	isRegistreringFile := strings.Contains(b.file.Name, "Registreringer")
	isMiljoFile := strings.Contains(b.file.Name, "Miljo")

	//common
	b.form.GeometryType = "esriGeometryPolyline"
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449391.4824999999,
		XMin: 12479.43200000003,
		YMax: 7182842.207400002,
		YMin: 6487407.353700001,
	}

	//renderer
	renderer := dib.WithRenderer(b.form.DisplayField)

	//uniqueValueInfos
	if isRegistreringFile {
		if b.company.isCompany(company_StoraEnso, company_Fjordtømmer, company_Nortømmer) {

			if b.company.isCompany(company_Nortømmer) {
				renderer.WithUniqueValueInfo("", "01. Sporlogg").
					WithStringValue("Sporlogg").
					WithSolidLineSymbol(
						FormDrawingInfoColor(0, 91, 91, 91),
						2,
					)
			} else {
				renderer.WithUniqueValueInfo("", "01. Sporlogg").
					WithStringValue("Sporlogg").
					WithSolidLineSymbol(
						FormDrawingInfoColor(0, 255, 197, 255),
						2,
					)
			}

			renderer.WithUniqueValueInfo("", "02. Basveg").
				WithStringValue("Basveg").
				WithDashedLineSymbol(
					FormDrawingInfoColor(139, 69, 19, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "03. Bilveg (ikke registrert i grunnkart)").
				WithStringValue("Vegtrase").
				WithSolidLineSymbol(
					FormDrawingInfoColor(244, 164, 96, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "05. Linjetrase").
				WithStringValue("Linjetrase").
				WithSolidLineSymbol(
					FormDrawingInfoColor(255, 0, 0, 255),
					4,
				)
			renderer.WithUniqueValueInfo("", "06. Sti").
				WithStringValue("Sti").
				WithDashedLineSymbol(
					FormDrawingInfoColor(0, 0, 0, 255),
					1,
				)
			renderer.WithUniqueValueInfo("", "08. Sporskader").
				WithStringValue("Sporskader").
				WithDashedLineSymbol(
					FormDrawingInfoColor(126, 35, 26, 255),
					1,
				)
			renderer.WithUniqueValueInfo("", "09. Planlegging").
				WithStringValue("Planlegging").
				WithDashedLineSymbol(
					FormDrawingInfoColor(191, 64, 191, 255),
					3,
				)

			if b.company.isCompany(company_Nortømmer) {
				renderer.WithUniqueValueInfo("", "10. Manuell felling").
					WithStringValue("Manuell felling").
					WithSolidLineSymbol(
						FormDrawingInfoColor(1, 50, 32, 255),
						1,
					)
				renderer.WithUniqueValueInfo("", "11. Gravemaskin").
					WithStringValue("Gravemaskin").
					WithSolidLineSymbol(
						FormDrawingInfoColor(255, 255, 0, 255),
						1,
					)
				renderer.WithUniqueValueInfo("", "12. Markberedning").
					WithStringValue("Markberedning").
					WithSolidLineSymbol(
						FormDrawingInfoColor(255, 255, 0, 255),
						1,
					)
			}

		} else {

			renderer.WithUniqueValueInfo("", "Sporlogg").
				WithStringValue("Sporlogg").
				WithSolidLineSymbol(
					FormDrawingInfoColor(0, 255, 197, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "Basveg").
				WithStringValue("Basveg").
				WithDashedLineSymbol(
					FormDrawingInfoColor(139, 69, 19, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "Basvei uten graving").
				WithStringValue("Basvei uten graving").
				WithDashedLineSymbol(
					FormDrawingInfoColor(0, 0, 255, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "Basvei med graving").
				WithStringValue("Basvei med graving").
				WithDashedLineSymbol(
					FormDrawingInfoColor(255, 0, 0, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "Bilveg (ikke registrert i grunnkart)").
				WithStringValue("Vegtrase").
				WithSolidLineSymbol(
					FormDrawingInfoColor(244, 164, 96, 255),
					2,
				)
			renderer.WithUniqueValueInfo("", "Linjetrase").
				WithStringValue("Linjetrase").
				WithSolidLineSymbol(
					FormDrawingInfoColor(255, 0, 0, 255),
					4,
				)
			if b.IsForestOwner() {
				renderer.WithUniqueValueInfo("", "Sti").
					WithStringValue("Sti").
					WithDashedLineSymbol(
						FormDrawingInfoColor(191, 64, 191, 255),
						2,
					)
			} else {
				renderer.WithUniqueValueInfo("", "Sti").
					WithStringValue("Sti").
					WithDashedLineSymbol(
						FormDrawingInfoColor(0, 0, 0, 255),
						1,
					)
			}
			renderer.WithUniqueValueInfo("", "Sporskader").
				WithStringValue("Sporskader").
				WithDashedLineSymbol(
					FormDrawingInfoColor(126, 35, 26, 255),
					1,
				)
			renderer.WithUniqueValueInfo("", "Planlegging").
				WithStringValue("Planlegging").
				WithDashedLineSymbol(
					FormDrawingInfoColor(191, 64, 191, 255),
					1,
				)
			renderer.WithUniqueValueInfo("", "Kantsone").
				WithStringValue("Kantsone").
				WithSolidLineSymbol(
					FormDrawingInfoColor(0, 255, 20, 255),
					3,
				)
			renderer.WithUniqueValueInfo("", "Bekk").
				WithStringValue("Bekk").
				WithSolidLineSymbol(
					FormDrawingInfoColor(1, 3, 255, 255),
					3,
				)
		}

	} else if isMiljoFile {
		renderer.WithUniqueValueInfo("", "01. Sporlogg").
			WithStringValue("Sporlogg").
			WithSolidLineSymbol(
				FormDrawingInfoColor(0, 255, 197, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "02. Basveg").
			WithStringValue("Basveg").
			WithDashedLineSymbol(
				FormDrawingInfoColor(139, 69, 19, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "03. Bilveg (ikke registrert i grunnkart)").
			WithStringValue("Vegtrase").
			WithSolidLineSymbol(
				FormDrawingInfoColor(244, 164, 96, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "05. Linjetrase").
			WithStringValue("Linjetrase").
			WithSolidLineSymbol(
				FormDrawingInfoColor(255, 0, 0, 255),
				4,
			)
		renderer.WithUniqueValueInfo("", "06. Sti").
			WithStringValue("Sti").
			WithDashedLineSymbol(
				FormDrawingInfoColor(0, 0, 0, 255),
				1,
			)
		renderer.WithUniqueValueInfo("", "08. Sporskader").
			WithStringValue("Sporskader").
			WithDashedLineSymbol(
				FormDrawingInfoColor(126, 35, 26, 255),
				1,
			)

	}
}
