package main

import (
	"log"
	"strings"
)

func findFormType(b *FormBuilder) geomFormTypeType {
	//find form type
	var formType geomFormTypeType
	if strings.Contains(b.file.Name, "Forhandsrydding") {
		formType = geomFormTypeForhandsrydding
	} else if strings.Contains(b.file.Name, "Grofterensk") {
		formType = geomFormTypeGrofterensk
	} else if strings.Contains(b.file.Name, "Markberedning") {
		formType = geomFormTypeMarkberedning
	} else if strings.Contains(b.file.Name, "Planting") {
		formType = geomFormTypePlanting
	} else if strings.Contains(b.file.Name, "Saaing") {
		formType = geomFormTypeSaaing
	} else if strings.Contains(b.file.Name, "Ungskogpleie") {
		formType = geomFormTypeUngskogpleie
	} else if strings.Contains(b.file.Name, "Skogskade") {
		formType = geomFormTypeSkogskade
	} else if strings.Contains(b.file.Name, "Infrastruktur") {
		formType = geomFormTypeInfrastruktur
	} else if strings.Contains(b.file.Name, "Gjodsling") {
		formType = geomFormTypeGjodsling
	} else if strings.Contains(b.file.Name, "Sproyting") {
		formType = geomFormTypeSproyting
	} else if strings.Contains(b.file.Name, "Gravearbeid") {
		formType = geomFormTypeGravearbeid
	} else if strings.Contains(b.file.Name, "Sporskaderetting") {
		formType = geomFormTypeSporskaderetting
	} else if strings.Contains(b.file.Name, "Stammekvisting") {
		formType = geomFormTypeStammekvisting
	} else {
		log.Fatalf("Unknown file %v", b.file.Name)
	}
	return formType
}

func populatePJobTypeGeomFormCommon(b *FormBuilder, formType geomFormTypeType) {
	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_title"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 500000,
		XMin: 0,
		YMax: 8000000,
		YMin: 5000000,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_type")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true
}

func populatePJobTypeGeomFormDrawingInfo(b *FormBuilder, formType geomFormTypeType) {

	var rendererName string
	var rendererColor []int
	clearColor := FormDrawingInfoColor(0, 0, 0, 0)
	whiteColor := FormDrawingInfoColor(255, 255, 255, 255)
	dashTemplate := []float64{12, 12}

	switch formType {
	case geomFormTypeForhandsrydding:
		rendererName = "Forhåndsrydding"
		rendererColor = FormDrawingInfoColor(255, 150, 0, 255)
	case geomFormTypeGrofterensk:
		rendererName = "Grøfterensk"
		rendererColor = FormDrawingInfoColor(0, 200, 200, 255)
	case geomFormTypeMarkberedning:
		rendererName = "Markberedning"
		rendererColor = FormDrawingInfoColor(165, 0, 255, 255)
	case geomFormTypePlanting:
		rendererName = "Planting"
		rendererColor = FormDrawingInfoColor(255, 0, 0, 255)
	case geomFormTypeSaaing:
		rendererName = "Såing"
		rendererColor = FormDrawingInfoColor(128, 0, 0, 255)
	case geomFormTypeUngskogpleie:
		rendererName = "Ungskogpleie"
		rendererColor = FormDrawingInfoColor(0, 0, 255, 255)
	case geomFormTypeSkogskade:
		rendererName = "Skogskade"
		rendererColor = FormDrawingInfoColor(128, 0, 0, 255)
	case geomFormTypeInfrastruktur:
		rendererName = "Infrastruktur"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255)
	case geomFormTypeGjodsling:
		rendererName = "Gjødsling"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255) //TODO
	case geomFormTypeSproyting:
		rendererName = "Sprøyting"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255) //TODO
	case geomFormTypeGravearbeid:
		rendererName = "Gravearbeid"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255) //TODO
	case geomFormTypeSporskaderetting:
		rendererName = "Sporskaderetting"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255) //TODO
	case geomFormTypeStammekvisting:
		rendererName = "Infrastruktur"
		rendererColor = FormDrawingInfoColor(128, 0, 200, 255) //TODO
	default:
		log.Fatal("Unknown formType")
	}

	//DRAWING INFO
	di := b.AddDrawingInfo()
	di.WithLabelingInfo("_title").
		WithCenterAlignedSymbol(
			FormDrawingInfoColor(0, 0, 0, 255), //black color
			9,
			FormDrawingInfoColor(255, 255, 255, 127), //white halo color
			20,
		)
	renderer := di.WithRenderer("_type")
	renderer.WithUniqueValueInfo(rendererName, rendererName).
		WithStringValue(rendererName).
		WithCimDashedLineSymbol(rendererColor, 5, Round, whiteColor, 3, dashTemplate).
		WithDashedOutlineSymbol(clearColor, rendererColor, 3, true)

	if formType != geomFormTypePlanting || b.IsForestOwner() {
		renderer.WithUniqueValueInfo("", "styleOverride").
			WithStringValue("styleOverride").
			WithSolidOutlineSymbol(clearColor, whiteColor, 3)
	} else {

		greenDashColor := FormDrawingInfoColor(0, 255, 0, 255)
		blackColor := FormDrawingInfoColor(0, 0, 0, 255)

		renderer.WithUniqueValueInfo("Plantesalg inkl arbeid", "Plantesalg inkl arbeid").
			WithStringValue("PlantesalgPlanting").
			WithCimDashedLineSymbol(whiteColor, 5, Round, greenDashColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, whiteColor, 3, true)
		renderer.WithUniqueValueInfo("Plantesalg", "Plantesalg").
			WithStringValue("Plantesalg").
			WithCimDashedLineSymbol(blackColor, 5, Round, greenDashColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, blackColor, 3, true)
		renderer.WithUniqueValueInfo("", "styleOverride").
			WithStringValue("styleOverride").
			WithSolidOutlineSymbol(clearColor, greenDashColor, 3)
	}
}

func populatePJobTypeGeomFormHiddenFields(b *FormBuilder, formType geomFormTypeType) {

	var workTeamOrgNumSuffix = ""
	switch formType {
	case geomFormTypeForhandsrydding:
		workTeamOrgNumSuffix = "FR"
	case geomFormTypeGrofterensk:
		workTeamOrgNumSuffix = "GR"
	case geomFormTypeMarkberedning:
		workTeamOrgNumSuffix = "MB"
	case geomFormTypePlanting:
		workTeamOrgNumSuffix = "PL"
	case geomFormTypeSaaing:
		workTeamOrgNumSuffix = "SA"
	case geomFormTypeUngskogpleie:
		workTeamOrgNumSuffix = "UP"
	case geomFormTypeSkogskade:
		workTeamOrgNumSuffix = "SS"
	case geomFormTypeInfrastruktur:
		workTeamOrgNumSuffix = "IS"
	case geomFormTypeGjodsling:
		workTeamOrgNumSuffix = "GJ"
	case geomFormTypeSproyting:
		workTeamOrgNumSuffix = "SP"
	case geomFormTypeGravearbeid:
		workTeamOrgNumSuffix = "GA"
	case geomFormTypeSporskaderetting:
		workTeamOrgNumSuffix = "SR"
	case geomFormTypeStammekvisting:
		workTeamOrgNumSuffix = "SK"

	default:
		log.Fatal("Unknown formType")
	}

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_type", "Type")
	b.AddHiddenStringField("_parentGlobalId", "Tilhørende objekt")
	b.AddHiddenStringField("_title", "Tittel på tiltaket")
	b.AddHiddenIntegerField("_number", "Tiltaksnummer")
	b.AddHiddenIntegerField("_job_nr", "Ordrenummer. Ett nummer per tiltakstype (dvs antall unike nummer er likt antall tiltakstyper i ordren)")
	b.AddHiddenStringField("_headId", "IO")
	b.AddHiddenStringField("_workTeamOrgNum"+workTeamOrgNumSuffix, "Organisasjonsnummer arbeidslag "+strings.ToLower(string(formType)))

	b.SetFieldIdSeries(
		1,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_type", //6
	)

	switch formType {
	case geomFormTypeForhandsrydding:
		b.SetFieldIdSeries(
			35,
			"_parentGlobalId",
			"_title",
			"_number",
			"_job_nr",
			"_headId",
			"_workTeamOrgNum"+workTeamOrgNumSuffix,
		)
	case geomFormTypeGrofterensk:
		b.SetFieldIdSeries(
			35,
			"_parentGlobalId",
		)
		b.SetFieldIdSeries(
			38,
			"_title",
			"_number",
			"_job_nr",
			"_headId",
			"_workTeamOrgNum"+workTeamOrgNumSuffix,
		)
	case geomFormTypeMarkberedning:
		b.SetFieldIdSeries(
			35,
			"_parentGlobalId",
		)
		b.SetFieldIdSeries(
			38,
			"_title",
			"_number",
			"_job_nr",
			"_headId",
		)

		b.SetFieldIdSeries(45, "_workTeamOrgNum"+workTeamOrgNumSuffix)
	case geomFormTypePlanting:
		b.SetFieldIdSeries(35, "_parentGlobalId")
		b.SetFieldIdSeries(
			37,
			"_title",
			"_number",
			"_job_nr",
		)
		b.SetFieldIdSeries(40, "_headId")
		if b.IsNortømmer() || b.IsStoraEnso() || b.IsLøvenskioldFossum() || b.IsGlommen() || b.IsFritzøe() || b.IsCappelen() {
			b.SetFieldIdSeries(52, "_workTeamOrgNum"+workTeamOrgNumSuffix)
		} else {
			b.SetFieldIdSeries(53, "_workTeamOrgNum"+workTeamOrgNumSuffix)
		}

	case geomFormTypeUngskogpleie:
		b.SetFieldIdSeries(
			35,
			"_parentGlobalId",
		)
		b.SetFieldIdSeries(
			36,
			"_title",
			"_number",
			"_job_nr",
			"_headId",
			"_workTeamOrgNum"+workTeamOrgNumSuffix,
		)
	case
		geomFormTypeSaaing,
		geomFormTypeSkogskade,
		geomFormTypeInfrastruktur,
		geomFormTypeGjodsling,
		geomFormTypeSproyting,
		geomFormTypeGravearbeid,
		geomFormTypeSporskaderetting,
		geomFormTypeStammekvisting:
		b.SetFieldIdSeries(
			1000,
			"_parentGlobalId",
			"_title",
			"_number",
			"_job_nr",
			"_headId",
			"_workTeamOrgNum"+workTeamOrgNumSuffix,
		)
	}

	switch formType {
	case geomFormTypePlanting:
		b.AddHiddenIntegerField("_plants_total", "Planter totalt")
		b.SetFieldIdSeries(20, "_plants_total")
	}
}

func populatePJobTypeGeomFormTypes(b *FormBuilder, formType geomFormTypeType) {

	switch formType {
	case geomFormTypePlanting:
		//type
		if b.IsForestOwner() {
			//-- This needs to contain all the types.
			b.AddType(FormType{
				Id:   "Planting",
				Name: "Planting",
				Templates: []FormTypeTemplate{
					{
						Description: strRef("Plantearbeid"),
						DrawingTool: strRef("esriFeatureEditToolPolygon"),
						Name:        "Planting",
						Prototype: FormTypeTemplatePrototype{
							Attributes: map[string]interface{}{
								"_type": "Planting",
							},
						},
					},
				},
			})
		} else {
			//-- Planting er kun plantearbeid. da vet vi ikke noe om plantene.
			//-- Plantesalg er kun slag av planter (uten arbeid).
			//-- PlantesalgPlanting er begge deler.
			b.AddType(FormType{
				Id:   "Planting",
				Name: "Planting",
				Templates: []FormTypeTemplate{
					{
						Description: strRef("Plantearbeid"),
						DrawingTool: strRef("esriFeatureEditToolPolygon"),
						Name:        "Planting",
						Prototype: FormTypeTemplatePrototype{
							Attributes: map[string]interface{}{
								"_type": "Planting",
							},
						},
					},
					{
						Description: strRef("Plantesalg"),
						DrawingTool: strRef("esriFeatureEditToolNone"),
						Name:        "Plantesalg",
						Prototype: FormTypeTemplatePrototype{
							Attributes: map[string]interface{}{
								"_type": "Plantesalg",
							},
						},
					},
					{
						Description: strRef("Plantesalg inkl arbeid"),
						DrawingTool: strRef("esriFeatureEditToolPolygon"),
						Name:        "PlantesalgPlanting",
						Prototype: FormTypeTemplatePrototype{
							Attributes: map[string]interface{}{
								"_type": "PlantesalgPlanting",
							},
						},
					},
				},
			})
		}
		return //planting is special!
	}

	var typeDescription = ""
	var typeStringKey = ""

	switch formType {
	case geomFormTypeForhandsrydding:
		typeStringKey = "Forhåndsrydding"
	case geomFormTypeGrofterensk:
		typeStringKey = "Grøfterensk"
	case geomFormTypeMarkberedning:
		typeStringKey = "Markberedning"
	case geomFormTypeSaaing:
		typeStringKey = "Såing"
		typeDescription = "Såing" //TODO: any reason for this to be special?
	case geomFormTypeUngskogpleie:
		typeStringKey = "Ungskogpleie"
	case geomFormTypeSkogskade:
		typeStringKey = "Skogskade"
	case geomFormTypeInfrastruktur:
		typeStringKey = "Infrastruktur"
	case geomFormTypeGjodsling:
		typeStringKey = "Gjødsling"
	case geomFormTypeSproyting:
		typeStringKey = "Sprøyting"
	case geomFormTypeGravearbeid:
		typeStringKey = "Gravearbeid"
	case geomFormTypeSporskaderetting:
		typeStringKey = "Sporskaderetting"
	case geomFormTypeStammekvisting:
		typeStringKey = "Stammekvisting"
	default:
		log.Fatal("Unknown formType")
	}

	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      typeStringKey,
		Name:    typeStringKey,
		Templates: []FormTypeTemplate{
			{
				Description: strRef(typeDescription),
				DrawingTool: strRef("esriFeatureEditToolPolygon"),
				Name:        typeStringKey,
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{
						"_type": typeStringKey,
					},
				},
			},
		},
	})
}
