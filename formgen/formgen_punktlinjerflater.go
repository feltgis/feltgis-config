package main

import "strings"

func PopulatePunktLinjerFlaterForm(b *FormBuilder) {

	if b.company.hasFeature(isGroupAlma) {
		PopulatePunktLinjerFlaterExtGroupAlmaForm(b)
		return
	}

	//file types
	isRegistreringFile := strings.Contains(b.file.Name, "Registreringer")
	isMiljoFile := strings.Contains(b.file.Name, "Miljo") //Ikke oppdragsspesifikt = alt mulig...
	isPunktFile := strings.Contains(b.file.Name, "Punkt") || strings.Contains(b.file.Name, "Point")
	isLinjeFile := strings.Contains(b.file.Name, "Linjer") || strings.Contains(b.file.Name, "Line")
	isFlateFile := strings.Contains(b.file.Name, "Flater") || strings.Contains(b.file.Name, "Polygon")

	//common
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.GlobalIdField = "_globalId"
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2

	b.form.MaxRecordCount = 100000
	if isPunktFile {
		b.form.MinScale = 100000
	} else {
		b.form.MinScale = 50000
	}
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"

	b.form.Relationships = []string{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"

	b.form.UseStandardizedQueries = true
	dib := b.AddDrawingInfo()

	var typeFieldName string //special field used to decide type!

	//group Registreringer/Miljo/Hen
	if isRegistreringFile {
		typeFieldName = "_tema"
		if b.company.isCompany(company_Fjordtømmer, company_StatSkog, company_StoraEnso) {
			//no comment labeling info
		} else {
			populatePunktLinjerFlaterForm_commentLabelingInfo(b, dib)
		}
		dib.WithTransparency(0)
	} else if isMiljoFile {
		typeFieldName = "tema"
		dib.WithTransparency(0)
		//no labeling info
	} else {
		b.reporter.Error("Unknown type Registreringer/Miljo/Hen")
	}

	b.form.DisplayField = typeFieldName
	b.form.TypeIdField = strRef(typeFieldName)

	//---------------------------
	//FIELDS!

	//common fields
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")

	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy", //4
	)

	if isRegistreringFile {
		b.AddHiddenStringField(typeFieldName, "Tema")
		b.AddHiddenNullableStringField("_orderId", "OrdreId")

		if isLinjeFile {
			b.AddHiddenNullableStringField("_headId", "IO head Id")
		} else {
			b.AddHiddenNullableStringField("_headId", "IO head ID")
		}

		b.AddHiddenNullableStringField("_serviceOrderId", "Service Order Id")
		b.AddHiddenNullableStringField("_parentGlobalId", "Parent Global Id")
		b.SetFieldIdSeries(5,
			typeFieldName,
		)
		b.SetFieldIdSeries(7,
			"_orderId",
			"_headId",
			"_serviceOrderId",
			"_parentGlobalId", //10
		)

		if isPunktFile {
			b.AddHiddenStringField("_referenceCollection", "Skogkultur tiltak")
			b.SetFieldIdSeries(
				11,
				"_referenceCollection",
			)

			b.AddHiddenNullableStringField("_visibilityMode", "Synlighet")
			b.SetFieldIdSeries(12,
				"_visibilityMode",
			)

		} else {
			b.AddHiddenNullableStringField("_visibilityMode", "Synlighet")
			b.SetFieldIdSeries(11,
				"_visibilityMode",
			)
		}

		b.AddHiddenNullableStringField("_workTeamOrgNumUP", "Organisasjonsnummer arbeidslag ungskogpleie")
		b.AddHiddenNullableStringField("_workTeamOrgNumFR", "Organisasjonsnummer arbeidslag forhåndsrydding")
		b.AddHiddenNullableStringField("_workTeamOrgNumMB", "Organisasjonsnummer arbeidslag markberedning")
		b.AddHiddenNullableStringField("_workTeamOrgNumPL", "Organisasjonsnummer arbeidslag planting")
		b.AddHiddenNullableStringField("_workTeamOrgNumGR", "Organisasjonsnummer arbeidslag grøfterensk")
		b.SetFieldIdSeries(13,
			"_workTeamOrgNumUP",
			"_workTeamOrgNumFR",
			"_workTeamOrgNumMB", //15
			"_workTeamOrgNumPL",
			"_workTeamOrgNumGR",
		)
		b.AddHiddenNullableStringField("_workTeamOrgNumSA", "Organisasjonsnummer arbeidslag såing")
		b.AddHiddenNullableStringField("_workTeamOrgNumSS", "Organisasjonsnummer arbeidslag skogskade")
		b.SetFieldIdSeries(18,
			"_workTeamOrgNumSA",
			"_workTeamOrgNumSS",
		)
		b.AddHiddenNullableStringField("_workTeamOrgNumIS", "Organisasjonsnummer arbeidslag infrastruktur")
		b.SetFieldIdSeries(30,
			"_workTeamOrgNumIS",
		)
		b.AddHiddenStringField("_workTeamOrgNumGJ", "Orgnum arbeidslag gjødsling")
		b.AddHiddenStringField("_workTeamOrgNumSP", "Orgnum arbeidslag sprøyting")
		b.AddHiddenStringField("_workTeamOrgNumGA", "Orgnum arbeidslag gravearbeid")
		b.AddHiddenStringField("_workTeamOrgNumSR", "Orgnum arbeidslag sporskaderetting")
		b.AddHiddenStringField("_workTeamOrgNumSK", "Orgnum arbeidslag stammekvisting")
		b.SetFieldIdSeries(40,
			"_workTeamOrgNumGJ",
			"_workTeamOrgNumSP",
			"_workTeamOrgNumGA",
			"_workTeamOrgNumSR",
			"_workTeamOrgNumSK",
		)

		b.AddNullableStringField("comment", "Kommentar").
			WithAliasTranslation("en", "Comment")
		b.SetFieldIdSeries(6,
			"comment",
		)

	} else if isMiljoFile {
		b.AddUneditableRequiredStringField(typeFieldName, "Tema").
			WithAliasTranslation("en", "Theme")
		b.AddNullableStringField("comment", "Kommentar").
			WithAliasTranslation("en", "Comment")
		b.SetFieldIdSeries(5,
			typeFieldName,
			"comment",
		)
	}

	if isLinjeFile {
		if !b.IsGlommen() {
			b.AddUneditableRequiredDoubleField("length", "Lengde").
				WithAliasTranslation("en", "Length")
			b.SetFieldIdSeries(
				20,
				"length")
		}

	}

	b.WithAutoSortex()
	b.VerifyOrderAndSortex()

	//---------------------------
	//GEOMETRY!

	//geometry points/lines/polygons
	if isPunktFile {
		populatePunktLinjerFlaterForm_punkt(b, dib)
	} else if isLinjeFile {
		populatePunktLinjerFlaterForm_linjer(b, dib)
	} else if isFlateFile {
		populatePunktLinjerFlaterForm_flater(b, dib)
	} else {
		b.reporter.Error("Unknown type Punkt/Linjer/Flater")
	}

	//---------------------------
	//TYPES! (we have created a types generator :-D )
	var typeDrawingTool string
	var typeIds []string //array so we can have the order!
	var typeNames map[string]string
	var typeVisibilityMode map[string]string

	if isRegistreringFile {
		if isPunktFile {
			typeDrawingTool = "esriFeatureEditToolPoint"
			typeIds = []string{
				"Ledning",
				"Storfuglleik",
				"Jordkabel",
				"Balplass",
				"Reirtre",
				"Livsløpstre",
				"Velteplass",
				"Annen fare",
				"Kryssningspunkt",
				"Oljesøl",
				"Observasjon",
				"Ikke kryss",
				"Snuplass",
				"Brønn",
				"Kulturminner",
				"Jaktpost",
				"Sporskade",
				"Jakttarn",
				"Livsløpstrær",
				"Parkering",
				"Felling",
				"Graving",
				"Kavellegging",
				"Landingsplass",
				"Høgstubber",
			}
			if b.company.isCompany(company_Fjordtømmer, company_StoraEnso) {
				//haba
			} else if !b.IsForestOwner() {
				typeIds = append(typeIds, "Kartmerknad")
			}
			if b.IsCappelen() {
				typeIds = append(typeIds, "Stikkrenne")
			}
			typeNames = map[string]string{
				"Ledning":         "17 Ledning",
				"Storfuglleik":    "03 Storfuglleik",
				"Jordkabel":       "18 Jordkabel",
				"Balplass":        "11 Bålplass",
				"Reirtre":         "12 Reirtre",
				"Livsløpstre":     "01 Livsløpstre",
				"Velteplass":      "06 Velteplass",
				"Annen fare":      "20 Annen fare",
				"Kryssningspunkt": "05 Kryssningspunkt",
				"Oljesøl":         "14 Oljesøl",
				"Observasjon":     "04 Observasjon",
				"Ikke kryss":      "16 Ikke kryss",
				"Snuplass":        "07 Snuplass",
				"Brønn":           "19 Brønn",
				"Kulturminner":    "15 Kulturminner",
				"Jaktpost":        "09 Jaktpost",
				"Sporskade":       "13 Sporskade",
				"Jakttarn":        "10 Jaktårn", //spelling
				"Livsløpstrær":    "02 Livsløpstrær",
				"Parkering":       "08 Parkering",
				"Felling":         "21 Felling",
				"Graving":         "22 Graving",
				"Kavellegging":    "23 Kavellegging",
				"Landingsplass":   "24 Landingsplass",
				"Høgstubber":      "25 Høgstubber",
				"Kartmerknad":     "26 Kartmerknad",
			}
			if b.IsCappelen() {
				typeNames["Stikkrenne"] = "27 Stikkrenne"

			}
			typeVisibilityMode = map[string]string{
				"Brønn": "always",
			}
		} else if isLinjeFile {
			typeDrawingTool = "esriFeatureEditToolLine"
			if b.company.isCompany(company_Nortømmer) {
				typeIds = []string{
					"Sporlogg",
					"Linjetrase",
					"Sti",
					"Vegtrase",
					"Basveg",
					"Eiendomsgrense",
					"Sporskader",
					"Planlegging",
					"Manuell felling",
					"Gravemaskin",
					"Markberedning",
				}
				typeNames = map[string]string{
					"Sporlogg":        "01. Sporlogg",
					"Linjetrase":      "05. Linjetrase",
					"Sti":             "06. Sti",
					"Vegtrase":        "03. Bilveg (ikke registrert i grunnkart)",
					"Basveg":          "02. Basveg",
					"Eiendomsgrense":  "07. Eiendomsgrense",
					"Sporskader":      "08. Sporskader",
					"Planlegging":     "09. Planlegging",
					"Manuell felling": "10. Manuell felling",
					"Gravemaskin":     "11. Gravemaskin",
					"Markberedning":   "12. Markberedning",
				}
			} else if b.company.isCompany(company_Fjordtømmer, company_StoraEnso) {
				typeIds = []string{
					"Sporlogg",
					"Linjetrase",
					"Sti",
					"Vegtrase",
					"Basveg",
					"Eiendomsgrense",
					"Sporskader",
					"Planlegging",
				}
				typeNames = map[string]string{
					"Sporlogg":       "01. Sporlogg",
					"Linjetrase":     "05. Linjetrase",
					"Sti":            "06. Sti",
					"Vegtrase":       "03. Bilveg (ikke registrert i grunnkart)",
					"Basveg":         "02. Basveg",
					"Eiendomsgrense": "07. Eiendomsgrense",
					"Sporskader":     "08. Sporskader",
					"Planlegging":    "09. Planlegging",
				}
			} else if b.IsForestOwner() {
				typeIds = []string{
					"Sporlogg",
					"Linjetrase",
					"Sti",
					"Basvei uten graving",
					"Basvei med graving",
					"Eiendomsgrense",
					"Sporskader",
					"Kantsone",
					"Bekk",
				}
			} else {
				typeIds = []string{
					"Sporlogg",
					"Linjetrase",
					"Sti",
					"Vegtrase",
					"Basvei uten graving",
					"Basvei med graving",
					"Eiendomsgrense",
					"Sporskader",
					"Planlegging",
					"Kantsone",
					"Bekk",
				}
				typeNames = map[string]string{
					"Vegtrase": "Bilveg (ikke registrert i grunnkart)",
				}
			}
		} else if isFlateFile {
			typeDrawingTool = "esriFeatureEditToolPolygon"
			typeIds = []string{
				"Ny BVO-figur",
				"Annet",
				"Kantsone",
			}
			typeNames = map[string]string{
				"Ny BVO-figur": "01. Ny BVO-figur",
				"Annet":        "02. Annet",
				"Kantsone":     "03. Kantsone",
			}
		}
	} else if isMiljoFile {
		if isPunktFile {
			typeDrawingTool = "esriFeatureEditToolPoint"
			typeIds = []string{
				"Ledning",
				"Storfuglleik",
				"Jordkabel",
				"Balplass",
				"Reirtre",
				"Livsløpstre",
				"Velteplass",
				"Annen fare",
				"Kryssningspunkt",
				"Oljesøl",
				"Observasjon",
				"Ikke kryss",
				"Snuplass",
				"Brønn",
				"Kulturminner",
				"Jaktpost",
				"Sporskade",
				"Jakttarn",
				"Livsløpstrær",
				"Parkering",
				"Felling",
				"Graving",
				"Kavellegging",
				"Landingsplass",
				"Kumlokk",
			}
			typeNames = map[string]string{
				"Ledning":         "17 Ledning",
				"Storfuglleik":    "03 Storfuglleik",
				"Jordkabel":       "18 Jordkabel",
				"Balplass":        "11 Bålplass",
				"Reirtre":         "12 Reirtre",
				"Livsløpstre":     "01 Livsløpstre",
				"Velteplass":      "06 Velteplass",
				"Annen fare":      "20 Annen fare",
				"Kryssningspunkt": "05 Kryssningspunkt",
				"Oljesøl":         "14 Oljesøl",
				"Observasjon":     "04 Observasjon",
				"Ikke kryss":      "16 Ikke kryss",
				"Snuplass":        "07 Snuplass",
				"Brønn":           "19 Brønn",
				"Kulturminner":    "15 Kulturminner",
				"Jaktpost":        "09 Jaktpost",
				"Sporskade":       "13 Sporskade",
				"Jakttarn":        "10 Jaktårn", //spelling
				"Livsløpstrær":    "02 Livsløpstrær",
				"Parkering":       "08 Parkering",
				"Felling":         "21 Felling",
				"Graving":         "22 Graving",
				"Kavellegging":    "23 Kavellegging",
				"Landingsplass":   "24 Landingsplass",
				"Kumlokk":         "25 Kumlokk", //Why 24?
			}
			/*typeVisibilityMode = map[string]string{
				"Brønn": "always",
			}*/
		}
	}

	for _, typeId := range typeIds {

		var typeName = typeId
		if typeNameMapLookup, ok := typeNames[typeId]; ok {
			typeName = typeNameMapLookup
		}
		var attributes = map[string]interface{}{
			typeFieldName: typeId,
		}
		if visibilityMode, ok := typeVisibilityMode[typeId]; ok {
			attributes["_visibilityMode"] = visibilityMode
		}

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      typeId,
			Name:    typeName,
			Templates: []FormTypeTemplate{
				{
					Description: strRef(""),
					DrawingTool: &typeDrawingTool,
					Name:        typeName,
					Prototype: FormTypeTemplatePrototype{
						Attributes: attributes,
					},
				},
			},
		})
	}
}

//--------------------------
// Labeling info

func populatePunktLinjerFlaterForm_commentLabelingInfo(b *FormBuilder, dib *FormDrawingInfoBuilder) {
	//for "Registreringer" excluding Alma

	//labeling info builder
	lib := dib.WithLabelingInfo("comment")

	lib.WithNoMinScale()
	lib.WithUseCodedValues()

	if strings.Contains(b.file.Name, "Linjer") {
		lib.WithCenteredPointPlacement()
	}
	if strings.Contains(b.file.Name, "RegistreringerPunkt") && b.company.isCompany(company_Nortømmer) {
		lib.WithWhereClause("_tema = 'Kartmerknad'")
	}

	//symbol
	lib.WithLeftAlignedSymbol(
		FormDrawingInfoColor(235, 61, 194, 255), //symbol color
		12,
		FormDrawingInfoColor(255, 255, 255, 200), //white halo color
		2,
	)
}
