package main

import "strings"

func populatePunktLinjerFlaterExtGroupAlmaForm_linjer(b *FormBuilder, dib *FormDrawingInfoBuilder) {

	//file types
	isRegistreringOrHenFile := strings.Contains(b.file.Name, "Registreringer") || strings.Contains(b.file.Name, "Hen")
	//isMisFile := strings.Contains(b.file.Name, "Mis")

	//common
	b.form.GeometryType = "esriGeometryPolyline"
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449391.4824999999,
		XMin: 12479.43200000003,
		YMax: 7182842.207400002,
		YMin: 6487407.353700001,
	}

	//renderer
	renderer := dib.WithRenderer(b.form.DisplayField)

	//default symbol
	if isRegistreringOrHenFile {
		renderer.WithDefaultSymbol(
			FormDrawingInfoColor(171, 20, 0, 255),
		)
	}

	//uniqueValueInfos
	if isRegistreringOrHenFile {
		blackColor := FormDrawingInfoColor(0, 0, 0, 255)
		lineColor := FormDrawingInfoColor(171, 20, 0, 255)
		lineWidth := float64(2)
		overrideLineWidth := float64(2)

		renderer.WithUniqueValueInfo("", "styleOverride").
			WithStringValue("styleOverride").
			WithSolidLineSymbol(blackColor, overrideLineWidth)

		renderer.WithUniqueValueInfo("", "Driftsvei, markberedning").
			WithStringValue("Driftsvei, markberedning").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(171, 251, 200, 255)
		renderer.WithUniqueValueInfo("", "Driftsvei").
			WithStringValue("Driftsvei").
			WithCimSymbol(
				Line,
				FormDrawingDashedLineSymbol(lineColor, lineWidth, Round, []float64{4, 6}),
				FormDrawingSolidLineSymbol(blackColor, overrideLineWidth),
			).
			WithDashedLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 0, 0, 200)
		renderer.WithUniqueValueInfo("", "Forslag driftsvei").
			WithStringValue("Forslag driftsvei").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 0, 0, 255)
		renderer.WithUniqueValueInfo("", "Traktorvei- barmark").
			WithStringValue("Traktorvei- barmark").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(255, 255, 255, 255)
		renderer.WithUniqueValueInfo("", "Sporskader").
			WithStringValue("Sporskader").
			WithCimSymbol(
				Line,
				FormDrawingDashedLineSymbol(lineColor, lineWidth, Round, []float64{4, 6, 1, 6}),
				FormDrawingSolidLineSymbol(blackColor, overrideLineWidth),
			).
			WithDashDotedLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(130, 130, 130, 255)
		lineWidth = 3
		renderer.WithUniqueValueInfo("", "Kulturminne").
			WithStringValue("Kulturminne").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(255, 228, 20, 255)
		lineWidth = 1.5
		renderer.WithUniqueValueInfo("", "Bestandsgrense").
			WithStringValue("Bestandsgrense").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(255, 0, 197, 255)
		lineWidth = 2
		renderer.WithUniqueValueInfo("", "Annet").
			WithStringValue("Annet").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 0, 0, 255)
		renderer.WithUniqueValueInfo("", "Traktorvei-vinter").
			WithStringValue("Traktorvei-vinter").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(163, 242, 242, 255)
		lineWidth = 4
		renderer.WithUniqueValueInfo("", "Lysløype").
			WithStringValue("Lysløype").
			WithCimSymbol(
				Line,
				FormDrawingDashedLineSymbol(lineColor, lineWidth, Round, []float64{8, 10}),
				FormDrawingSolidLineSymbol(blackColor, overrideLineWidth),
			).
			WithDashedLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 255, 197, 255)
		lineWidth = 6
		renderer.WithUniqueValueInfo("", "Snøscooter").
			WithStringValue("Snøscooter").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(255, 255, 255, 255)
		lineWidth = 2
		renderer.WithUniqueValueInfo("", "Sti").
			WithStringValue("Sti").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(130, 130, 130, 255)
		lineWidth = 3
		renderer.WithUniqueValueInfo("", "Vei").
			WithStringValue("Vei").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(130, 130, 130, 255)
		renderer.WithUniqueValueInfo("", "Luftkabel").
			WithStringValue("Luftkabel").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 197, 255, 255)
		lineWidth = 1.5
		renderer.WithUniqueValueInfo("", "Vannledning").
			WithStringValue("Vannledning").
			WithCimSymbol(
				Line,
				FormDrawingSolidLineSymbol(lineColor, lineWidth),
				FormDrawingSolidLineSymbol(blackColor, overrideLineWidth),
			).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 197, 255, 255)
		lineWidth = 2
		renderer.WithUniqueValueInfo("", "Bekk").
			WithStringValue("Bekk").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(229, 252, 192, 255)
		lineWidth = 4
		renderer.WithUniqueValueInfo("", "Grøfterensk").
			WithStringValue("Grøfterensk").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 0, 0, 255)
		renderer.WithUniqueValueInfo("", "Merket sti").
			WithStringValue("Merket sti").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Innkjøringsvei, markberedning").
			WithStringValue("Innkjøringsvei, markberedning").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Skiløype").
			WithStringValue("Skiløype").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(100, 100, 100, 255)
		renderer.WithUniqueValueInfo("", "Grøft").
			WithStringValue("Grøft").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)

		lineColor = FormDrawingInfoColor(0, 0, 0, 100)
		renderer.WithUniqueValueInfo("", "NULL").
			WithStringValue("NULL").
			WithCimSymbol(Line, FormDrawingSolidLineSymbol(lineColor, lineWidth)).
			WithSolidLineSymbol(lineColor, lineWidth)
	}
}
