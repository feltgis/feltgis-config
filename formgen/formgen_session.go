package main

func PopulateSessionForm(b *FormBuilder) {
	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "tema"

	//TODO: do this with builder...
	b.form.DrawingInfo = &FormDrawingInfo{
		LabelingInfo: nil,
		Transparency: intRef(0),
		Renderer: &FormDrawingInfoRenderer{
			DefaultLabel:     nil,
			DefaultSymbol:    nil,
			Field1:           strRef("tema"),
			Field2:           nil,
			Field3:           nil,
			FieldDelimiter:   ", ",
			NoteToFutureSelf: "",
			Type:             "uniqueValue",
			UniqueValueInfos: []*FormDrawingInfoRendererUniqueValueInfo{
				{
					Description: "",
					Label:       "Livstegn",
					Symbol: FormDrawingInfoRendererSymbol{
						Color:       nil,
						Outline:     nil,
						Style:       nil,
						Angle:       intRef(0),
						ContentType: strRef("image/png"),
						Height:      f64Ref(24),
						ImageData:   strRef("iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAApgAAAKYB3X3/OAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAANCSURBVEiJtZZPbBtFFMZ/M7ubXdtdb1xSFyeilBapySVU8h8OoFaooFSqiihIVIpQBKci6KEg9Q6H9kovIHoCIVQJJCKE1ENFjnAgcaSGC6rEnxBwA04Tx43t2FnvDAfjkNibxgHxnWb2e/u992bee7tCa00YFsffekFY+nUzFtjW0LrvjRXrCDIAaPLlW0nHL0SsZtVoaF98mLrx3pdhOqLtYPHChahZcYYO7KvPFxvRl5XPp1sN3adWiD1ZAqD6XYK1b/dvE5IWryTt2udLFedwc1+9kLp+vbbpoDh+6TklxBeAi9TL0taeWpdmZzQDry0AcO+jQ12RyohqqoYoo8RDwJrU+qXkjWtfi8Xxt58BdQuwQs9qC/afLwCw8tnQbqYAPsgxE1S6F3EAIXux2oQFKm0ihMsOF71dHYx+f3NND68ghCu1YIoePPQN1pGRABkJ6Bus96CutRZMydTl+TvuiRW1m3n0eDl0vRPcEysqdXn+jsQPsrHMquGeXEaY4Yk4wxWcY5V/9scqOMOVUFthatyTy8QyqwZ+kDURKoMWxNKr2EeqVKcTNOajqKoBgOE28U4tdQl5p5bwCw7BWquaZSzAPlwjlithJtp3pTImSqQRrb2Z8PHGigD4RZuNX6JYj6wj7O4TFLbCO/Mn/m8R+h6rYSUb3ekokRY6f/YukArN979jcW+V/S8g0eT/N3VN3kTqWbQ428m9/8k0P/1aIhF36PccEl6EhOcAUCrXKZXXWS3XKd2vc/TRBG9O5ELC17MmWubD2nKhUKZa26Ba2+D3P+4/MNCFwg59oWVeYhkzgN/JDR8deKBoD7Y+ljEjGZ0sosXVTvbc6RHirr2reNy1OXd6pJsQ+gqjk8VWFYmHrwBzW/n+uMPFiRwHB2I7ih8ciHFxIkd/3Omk5tCDV1t+2nNu5sxxpDFNx+huNhVT3/zMDz8usXC3ddaHBj1GHj/As08fwTS7Kt1HBTmyN29vdwAw+/wbwLVOJ3uAD1wi/dUH7Qei66PfyuRj4Ik9is+hglfbkbfR3cnZm7chlUWLdwmprtCohX4HUtlOcQjLYCu+fzGJH2QRKvP3UNz8bWk1qMxjGTOMThZ3kvgLI5AzFfo379UAAAAASUVORK5CYII="),
						Type:        "esriPMS",
						Url:         strRef("02118a46198679b7824f4459330dfde3"),
						Width:       f64Ref(24),
						XOffset:     intRef(0),
						YOffset:     intRef(0),
					},
					Value: StringOrInt{
						StringValue: strRef("Livstegn"),
					},
				},
			},
		}}
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("tema")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddNullableStringField("tema", "Tema")
	b.AddHiddenNullableStringField("_orgNum", "Siste orgNum")
	b.AddHiddenNullableStringField("_orderId", "Siste ordre")
	b.AddHiddenNullableStringField("_release", "App release")
	b.AddHiddenIntegerField("_buildNum", "App versjon")
	b.AddHiddenBooleanField("_android_Boolean", "Android")
	b.AddHiddenNullableStringField("_headId", "Siste ordre")
	b.AddHiddenNullableStringField("_sensitiveArterLogin", "Sensitive arter login")
	b.AddHiddenNullableStringField("_osVer", "OS ver")
	b.AddHiddenNullableStringField("_sdLogin", "SD login")
	b.AddHiddenNullableStringField("_ssid", "SSID")
	b.AddHiddenNullableStringField("_investigationMessage", "Beskrivelse av feilsituasjon")
	b.AddHiddenNullableStringField("_goOs", "runtime.GOOS")
	b.AddHiddenNullableStringField("_goArch", "runtime.GOARCH")
	b.AddHiddenNullableStringField("_urlRewrites", "URL rewrites")

	//fieldIds
	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)
	b.SetFieldIdSeries(
		6,
		"tema",
		"_orgNum",
		"_orderId",
		"_release",
		"_buildNum",
		"_android_Boolean",
		"_headId",
		"_sensitiveArterLogin",
		"_osVer",
		"_sdLogin",
		"_ssid",
		"_investigationMessage",
		"_goOs",
		"_goArch",
		"_urlRewrites",
	)

	b.WithAutoSortex()
}
