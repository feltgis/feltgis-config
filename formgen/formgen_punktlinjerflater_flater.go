package main

import "strings"

func populatePunktLinjerFlaterForm_flater(b *FormBuilder, dib *FormDrawingInfoBuilder) {

	//file types
	isRegistreringFile := strings.Contains(b.file.Name, "Registreringer")
	isMiljoFile := strings.Contains(b.file.Name, "Miljo")

	//common
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}

	//renderer
	renderer := dib.WithRenderer(b.form.DisplayField)

	//uniqueValueInfos
	if isRegistreringFile {
		renderer.WithUniqueValueInfo("", "01. Ny BVO-figur").
			WithStringValue("Ny BVO-figur").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 0, 197, 150),
				FormDrawingInfoColor(255, 0, 197, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "02. Annet").
			WithStringValue("Annet").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(110, 110, 110, 150),
				FormDrawingInfoColor(0, 0, 0, 255),
				2,
			)
		if b.IsForestOwner() {
			renderer.WithUniqueValueInfo("", "03. Kantsone").
				WithStringValue("Kantsone").
				WithSolidOutlineBackwardDiagonalSymbol(
					FormDrawingInfoColor(0, 255, 20, 255),
					FormDrawingInfoColor(0, 255, 20, 255),
					2,
				)
		} else {
			renderer.WithUniqueValueInfo("", "03. Kantsone").
				WithStringValue("Kantsone").
				WithSolidOutlineBackwardDiagonalSymbol(
					FormDrawingInfoColor(0, 230, 169, 255),
					FormDrawingInfoColor(0, 230, 169, 255),
					2,
				)
		}

	} else if isMiljoFile {
		renderer.WithUniqueValueInfo("", "Storfuglleik").
			WithStringValue("Storfuglleik").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 0, 197, 150),
				FormDrawingInfoColor(255, 0, 197, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "Tiurleik").
			WithStringValue("Tiurleik").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 0, 197, 150),
				FormDrawingInfoColor(255, 0, 197, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "02. Annet").
			WithStringValue("Annet").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(110, 110, 110, 150),
				FormDrawingInfoColor(0, 0, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "03. Mis").
			WithStringValue("Mis").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(110, 110, 110, 150),
				FormDrawingInfoColor(0, 0, 0, 255),
				2,
			)
	}
}
