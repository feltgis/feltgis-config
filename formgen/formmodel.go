package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
)

type FormTranslation struct {
	Key      string `json:"key"`
	Language string `json:"language"`
	Value    string `json:"value"`
}

type FormFieldSuggestedValue struct {
	Name      string `json:"name"`
	Preferred *bool  `json:"preferred,omitempty"`
	Solo      *bool  `json:"solo,omitempty"`
	Value     string `json:"value"`
}

type FormFieldCodedValue struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type FormFieldDomain struct {
	MultipleChoiceSeparator *string                   `json:"multipleChoiceSeparator,omitempty"`
	SuggestedValues         []FormFieldSuggestedValue `json:"suggestedValues,omitempty"`
	CodedValues             []FormFieldCodedValue     `json:"codedValues,omitempty"`
	Type                    string                    `json:"type"`
	//Name                  string                    `json:"name"` //Not used, only noise
}

type FormFieldLogicField struct {
	Operator              string  `json:"operator"`
	OtherFieldIntValue    *int    `json:"otherFieldIntValue,omitempty"`
	OtherFieldName        string  `json:"otherFieldName"`
	OtherFieldStringValue *string `json:"otherFieldStringValue,omitempty"`
}

type FormFieldLogicFieldInArray struct {
	Field FormFieldLogicField `json:"field"`
}

type FormFieldLogicIfValueIs struct {
	AllOf []FormFieldLogicFieldInArray `json:"allOf,omitempty"`
	AnyOf []FormFieldLogicFieldInArray `json:"anyOf,omitempty"`
	Field *FormFieldLogicField         `json:"field,omitempty"`
}

type FormFieldLogicSetValueThenSetValue struct {
	FieldName   string   `json:"fieldName,omitempty"`
	Comment     *string  `json:"comment,omitempty"`
	DoubleValue *float64 `json:"doubleValue,omitempty"`
	StringValue *string  `json:"stringValue,omitempty"`
	IntValue    *int     `json:"intValue,omitempty"`
}

type FormFieldLogicSetValueElseSetValue struct {
	DoubleValue *float64 `json:"doubleValue,omitempty"`
	StringValue *string  `json:"stringValue,omitempty"`
	IntValue    *int     `json:"intValue,omitempty"`
}

type FormFieldLogicSetValue struct {
	IfValueIs    FormFieldLogicIfValueIs             `json:"ifValueIs"`
	ThenSetValue FormFieldLogicSetValueThenSetValue  `json:"thenSetValue"`
	ElseSetValue *FormFieldLogicSetValueElseSetValue `json:"elseSetValue,omitempty"`
}

type FormFieldLogicValidation struct {
	ExpectedIntValue       *int  `json:"expectedIntValue,omitempty"`
	MaxIntValue            *int  `json:"maxIntValue,omitempty"`
	NegativeNumbersAllowed *bool `json:"negativeNumbersAllowed,omitempty"`
}

type FormFieldLogic struct {
	ResetValueOnChange bool                      `json:"resetValueOnChange,omitempty"`
	SetValue           *FormFieldLogicSetValue   `json:"setValue,omitempty"`
	VisibleIf          *FormFieldLogicIfValueIs  `json:"visibleIf,omitempty"`
	RequiredIf         *FormFieldLogicIfValueIs  `json:"requiredIf,omitempty"`
	Validation         *FormFieldLogicValidation `json:"validation,omitempty"`
}

type FormField struct {
	Alias        string                   `json:"alias"`
	Domain       *FormFieldDomain         `json:"domain,omitempty"`
	Editable     bool                     `json:"editable"`
	FieldId      int                      `json:"fieldid"`
	Length       *int                     `json:"length,omitempty"`
	RequiredIf   *FormFieldLogicIfValueIs `json:"requiredIf,omitempty"`
	Logic        *FormFieldLogic          `json:"logic,omitempty"`
	Name         string                   `json:"name"`
	Notes        *string                  `json:"notes,omitempty"`
	Nullable     bool                     `json:"nullable"`
	Section      *int                     `json:"section,omitempty"`
	Sortix       *int                     `json:"sortix,omitempty"`
	Translations []FormTranslation        `json:"translations,omitempty"`
	Type         string                   `json:"type"`
	Comment      *string                  `json:"comment,omitempty"`
}

type FormSection struct {
	Id       int    `json:"id"`
	Subtitle string `json:"subtitle,omitempty"`
	Title    string `json:"title"`
}

type FormExtentSpatialReference struct {
	LatestWkid int `json:"latestWkid"`
	Wkid       int `json:"wkid"`
}

type FormExtent struct {
	SpatialReference FormExtentSpatialReference `json:"spatialReference"`
	XMax             float64                    `json:"xmax"`
	XMin             float64                    `json:"xmin"`
	YMax             float64                    `json:"ymax"`
	YMin             float64                    `json:"ymin"`
}

type FormTemplate struct {
}

type FormTypeDomains struct {
}

type FormTypeTemplatePrototype struct {
	Attributes map[string]interface{} `json:"attributes"`
}
type FormTypeTemplate struct {
	Description *string                   `json:"description,omitempty"`
	DrawingTool *string                   `json:"drawingTool,omitempty"`
	Name        string                    `json:"name"`
	Prototype   FormTypeTemplatePrototype `json:"prototype"`
}

type FormType struct {
	Comment   *string            `json:"comment,omitempty"`
	Domains   FormTypeDomains    `json:"domains"`
	Id        string             `json:"id"`
	Name      string             `json:"name"`
	Templates []FormTypeTemplate `json:"templates"`
}

type Form struct {
	Alias                                  *string           `json:"alias,omitempty"`
	AllowGeometryUpdates                   bool              `json:"allowGeometryUpdates"`
	Capabilities                           string            `json:"capabilities"`
	CopyrightText                          string            `json:"copyrightText"`
	CurrentVersion                         float32           `json:"currentVersion"`
	DefaultVisibility                      bool              `json:"defaultVisibility"`
	Description                            string            `json:"description"`
	DisplayField                           string            `json:"displayField"`
	DrawingInfo                            *FormDrawingInfo  `json:"drawingInfo"`
	EditFieldsInfo                         *string           `json:"editFieldsInfo"`
	Extent                                 FormExtent        `json:"extent"`
	FieldOrder                             []string          `json:"fieldOrder"`
	Fields                                 []*FormField      `json:"fields"`
	GeometryType                           string            `json:"geometryType"`
	GlobalIdField                          string            `json:"globalIdField"`
	HasAttachments                         bool              `json:"hasAttachments"`
	HasM                                   *bool             `json:"hasM,omitempty"`
	HasZ                                   *bool             `json:"hasZ,omitempty"`
	HtmlPopupType                          string            `json:"htmlPopupType"`
	Id                                     int               `json:"id"` //An ESRI concept making it possible to work on groups of layers. We don´t use this feature, all values could be the 0
	IsDataVersioned                        bool              `json:"isDataVersioned"`
	MaxRecordCount                         int               `json:"maxRecordCount"`
	MaxScale                               int               `json:"maxScale"`
	MinScale                               int               `json:"minScale"`
	Name                                   string            `json:"name"`
	ObjectIdField                          string            `json:"objectIdField"`
	OwnershipBasedAccessControlForFeatures *string           `json:"ownershipBasedAccessControlForFeatures"`
	Relationships                          []string          `json:"relationships"`
	Sections                               []*FormSection    `json:"sections"`
	SupportedQueryFormats                  string            `json:"supportedQueryFormats"`
	SupportsAdvancedQueries                bool              `json:"supportsAdvancedQueries"`
	SupportsRollbackOnFailureParameter     bool              `json:"supportsRollbackOnFailureParameter"`
	SupportsStatistics                     bool              `json:"supportsStatistics"`
	SyncCanReturnChanges                   bool              `json:"syncCanReturnChanges"`
	Templates                              []FormTemplate    `json:"templates"`
	Translations                           []FormTranslation `json:"translations,omitempty"`
	Type                                   string            `json:"type"`
	TypeIdField                            *string           `json:"typeIdField"`
	Types                                  []FormType        `json:"types"`
	UseStandardizedQueries                 bool              `json:"useStandardizedQueries"`
	SyncEvents                             []SyncEventConfig `json:"syncEvents,omitempty"`
	DataSource                             *DataSource       `json:"dataSource,omitempty"`
}

type DataSourceType string

const (
	DataSourceSync    DataSourceType = "sync"
	DataSourceVTFront DataSourceType = "vtfront"
)

type DataSource struct {
	Type      DataSourceType `json:"type"`
	Schema    string         `json:"schema,omitempty"`
	TableName string         `json:"tableName,omitempty"`
}

type SyncEventFilter struct {
	Key         string `json:"key"`
	JMESPath    string `json:"jmesPath,omitempty"`
	StringValue string `json:"stringValue,omitempty"`
	BoolValue   bool   `json:"boolValue,omitempty"`
	IntValue    int    `json:"intValue,omitempty"`
}

type SyncEventType string

const (
	SyncEventTypeCopyIncoming         SyncEventType = "copyIncoming"
	SyncEventTypeAddAttributeOutgoing SyncEventType = "addAttributeOutgoing"
)

type SyncEventConfig struct {
	// The kind of event
	Type SyncEventType `json:"type"`
	// The event should only trigger if collection
	SourceCollection string `json:"sourceCollection"`
	// The event output result should be persisted to
	TargetCollection string `json:"targetCollection,omitempty"`
	// The event should only trigger if..
	Filter *SyncEventFilter `json:"filter,omitempty"`
	// Used to create "Etikett": "{{{Presentasj}}}\n{{{BonTresl}}}{{{Bon}}}", etc
	TemplateString string `json:"templateString,omitempty"`
	// For add attributes, which attribute to write to
	TargetField string `json:"targetField,omitempty"`
}

func (f *Form) Json() string {

	jsonBytes := new(bytes.Buffer)
	e := json.NewEncoder(jsonBytes)
	e.SetEscapeHTML(false) //we need this to not get \u003c
	e.SetIndent("", "    ")
	err := e.Encode(f)

	// write our opened jsonFile as a byte array.
	if err != nil {
		fmt.Println(err)
	}

	jsonString := jsonBytes.String()

	return jsonString + "\n" //extra \n in old files
}

func (f *Form) VerifyFieldIdsAreKept(otherForm *Form) bool {
	var result = true
	for _, field := range f.Fields {
		for _, otherField := range otherForm.Fields {
			if field.Name == otherField.Name {
				if field.FieldId != otherField.FieldId {
					var sectionString = ""
					if field.Section != nil {
						sectionString = " in section " + strconv.Itoa(*field.Section)
					}
					fmt.Println("⚠️ Field " + field.Name + sectionString + " did not keep its fieldId " + strconv.Itoa(field.FieldId))
					result = false
				}
			}
		}
	}
	return result
}

func (f *Form) VerifyFieldIdsAreSet() bool {
	var result = true
	for _, field := range f.Fields {
		if field.FieldId == -1 {
			fmt.Println("⚠️ FieldId for " + field.Name + " is missing (=-1)")
			result = false
		}
	}
	return result
}
