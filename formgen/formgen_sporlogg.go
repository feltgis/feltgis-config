package main

func PopulateSporloggForm(b *FormBuilder) {
	// Default values
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""

	if b.IsAllma() {
		b.form.DisplayField = "Type"
	} else {
		b.form.DisplayField = "_source"
	}

	b.form.GeometryType = "esriGeometryPolyline"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 5
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 1000
	b.form.MaxScale = 0
	b.form.MinScale = 50000
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)

	if b.IsAllma() {
		b.form.Name = b.company.Prefix + "Sporlogg"
		b.form.TypeIdField = strRef("Type")
	} else {
		b.form.Name = b.company.Prefix + "Sporlogg"
		b.form.TypeIdField = strRef("_source")
	}

	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.UseStandardizedQueries = true

	// Extent
	if b.IsAllma() {
		b.form.Extent = FormExtent{
			SpatialReference: FormExtentSpatialReference{
				LatestWkid: 25833,
				Wkid:       25833,
			},
			XMax: 2000000,
			XMin: -1000000,
			YMax: 10000000,
			YMin: 5000000,
		}
	} else {
		b.form.Extent = FormExtent{
			SpatialReference: FormExtentSpatialReference{
				LatestWkid: 25833,
				Wkid:       25833,
			},
			XMax: 449391.4824999999,
			XMin: 12479.43200000003,
			YMax: 7182842.207400002,
			YMin: 6487407.353700001,
		}
	}

	// Fields
	b.AddHiddenStringField("_globalId", "GlobalId")      // FieldId: 0
	b.AddHiddenDateField("_created_Date", "Opprettet")   // FieldId: 1
	b.AddHiddenStringField("_createdBy", "Opprettet av") // FieldId: 2
	b.AddHiddenDateField("_modified_Date", "Endret")     // FieldId: 3
	b.AddHiddenStringField("_modifiedBy", "Endret av")   // FieldId: 4

	if !b.IsAllma() {
		b.AddNullableStringField("comment", "Kommentar") // Ikke med i Allma
	}

	b.AddHiddenStringField("_orderId", "OrdreId") // FieldId: 6
	if b.IsAllma() {
		b.AddHiddenNullableStringField("_headId", "OrdreId")                                            // FieldId: 7
		b.AddUneditableRequiredStringField("orderNum", "OrdreId")                                       // FieldId: 8
		b.AddHiddenStringField("_serviceOrderId", "OrdreId")                                            // FieldId: 9
		b.AddUneditableNullableStringField("Type", "Allma: Sporloggstype")                              // FieldId: 10
		b.AddUneditableNullableDoubleField("Length", "Allma: Lengde")                                   // FieldId: 11
		b.AddUneditableNullableStringField("Description", "Allma: Beskrivelse")                         // FieldId: 12
		b.AddUneditableNullableIntegerField("ObjectId", "Allma: ObjektID")                              // FieldId: 13
		b.AddUneditableNullableDoubleField("Distance", "Allma: Avstand")                                // FieldId: 14
		b.AddUneditableNullableStringField("EndretDato", "Allma: Endret dato")                          // FieldId: 15
		b.AddHiddenNullableStringField("_parentGlobalId", "FeltGIS: Tilknyttet skogkulturordre")        // FieldId: 16
		b.AddHiddenNullableStringField("_workTeamOrgNum", "FeltGIS: Org som skapte linja i FeltKultur") // FieldId: 17
		b.AddHiddenNullableStringField("_subCollection", "Tilknyttet datasett")                         // FieldId: 18
	} else {
		b.AddHiddenEditableStringFieldForRendering("_source", "Kilde")             // Ikke med i Allma
		b.AddHiddenNullableStringField("_headId", "IO head ID")                    // FieldId: 7
		b.AddHiddenStringField("_serviceOrderId", "ServiceOrderId")                // FieldId: 9
		b.AddHiddenNullableStringField("_workTeamOrgNum", "Arbeidslag skogkultur") // FieldId: 17
		b.AddHiddenNullableStringField("_parentGlobalId", "Tilknyttet objekt")     // FieldId: 16
		b.AddHiddenNullableStringField("_subCollection", "Tilknyttet datasett")    // FieldId: 18
	}

	// Set FieldId series
	if b.IsAllma() {
		b.SetFieldIdSeries(0,
			"_globalId", "_created_Date", "_createdBy", "_modified_Date", "_modifiedBy",
		)
		b.SetFieldIdSeries(6,
			"_orderId")
		b.SetFieldIdSeries(8,
			"_headId", "orderNum", "_serviceOrderId",
			"Type", "Length")
		b.SetFieldIdSeries(14, "Description", "ObjectId", "Distance", "EndretDato",
			"_parentGlobalId", "_workTeamOrgNum", "_subCollection",
		)
	} else {
		b.SetFieldIdSeries(0,
			"_globalId", "_created_Date", "_createdBy", "_modified_Date", "_modifiedBy",
			"comment", "_orderId", "_source", "_headId", "_serviceOrderId",
			"_workTeamOrgNum", "_parentGlobalId", "_subCollection",
		)
	}

	// Renderer
	dib := b.AddDrawingInfo()
	renderer := dib.WithRenderer(b.form.DisplayField)

	// Adding unique value infos using builder pattern
	if b.IsAllma() {
		lineWidth := 1.5
		hogstmaskinColor := FormDrawingInfoColor(197, 0, 255, 255)
		lassbaererColor := FormDrawingInfoColor(0, 197, 255, 255)
		gjodslingColor := FormDrawingInfoColor(202, 122, 245, 255)
		markberedningColor := FormDrawingInfoColor(158, 215, 194, 255)

		renderer.WithUniqueValueInfo("", "Sporlogg, hogstmaskin").
			WithStringValue("Sporlogg, hogstmaskin").
			WithSolidLineSymbol(hogstmaskinColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Sporlogg, lassbærer").
			WithStringValue("Sporlogg, lassbærer").
			WithSolidLineSymbol(lassbaererColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Sporlogg, gjødsling").
			WithStringValue("Sporlogg, gjødsling").
			WithSolidLineSymbol(gjodslingColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Sporlogg, markberedning").
			WithStringValue("Sporlogg, markberedning").
			WithSolidLineSymbol(markberedningColor, lineWidth)
	} else {
		lineWidth := 2.0
		hogstmaskinColor := FormDrawingInfoColor(0, 255, 197, 255)
		lassbaererColor := FormDrawingInfoColor(0, 197, 255, 255)
		gjodslingColor := FormDrawingInfoColor(202, 122, 245, 255)
		markberedningColor := FormDrawingInfoColor(255, 255, 0, 255)
		planleggingColor := FormDrawingInfoColor(165, 166, 215, 255)

		renderer.WithUniqueValueInfo("", "Hogstmaskin").
			WithStringValue("Hogstmaskin").
			WithSolidLineSymbol(hogstmaskinColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Lassbærer").
			WithStringValue("Lassbærer").
			WithSolidLineSymbol(lassbaererColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Planlegging").
			WithStringValue("Planlegging").
			WithSolidLineSymbol(planleggingColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Gravemaskin/markberedning").
			WithStringValue("Gravemaskin/markberedning").
			WithSolidLineSymbol(markberedningColor, lineWidth)

		renderer.WithUniqueValueInfo("", "Gjødsling").
			WithStringValue("Gjødsling").
			WithSolidLineSymbol(gjodslingColor, lineWidth)
	}

	// Feature types
	if b.IsAllma() {
		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Sporlogg, hogstmaskin",
			Name:    "Sporlogg, hogstmaskin",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Sporlogg, hogstmaskin",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"Type": "Sporlogg, hogstmaskin"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Sporlogg, lassbærer",
			Name:    "Sporlogg, lassbærer",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Sporlogg, lassbærer",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"Type": "Sporlogg, lassbærer"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Sporlogg, gjødsling",
			Name:    "Sporlogg, gjødsling",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Sporlogg, gjødsling",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"Type": "Sporlogg, gjødsling"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Sporlogg, markberedning",
			Name:    "Sporlogg, markberedning",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Sporlogg, markberedning",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"Type": "Sporlogg, markberedning"},
					},
				},
			},
		})
	} else {
		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Hogstmaskin",
			Name:    "Hogstmaskin",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Hogstmaskin",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_source": "Hogstmaskin"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Lassbærer",
			Name:    "Lassbærer",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Lassbærer",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_source": "Lassbærer"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Planlegging",
			Name:    "Planlegging",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Planlegging",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_source": "Planlegging"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Gravemaskin/markberedning",
			Name:    "Gravemaskin/markberedning",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Gravemaskin/markberedning",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_source": "Gravemaskin/markberedning"},
					},
				},
			},
		})

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      "Gjødsling",
			Name:    "Gjødsling",
			Templates: []FormTypeTemplate{
				{
					DrawingTool: strRef("esriFeatureEditToolLine"),
					Name:        "Gjødsling",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_source": "Gjødsling"},
					},
				},
			},
		})
	}

	// Auto sorting
	b.WithAutoSortex()
}
