package main

import (
	"log"
	"strings"
)

func PopulateEgenkontrollForm(b *FormBuilder) {

	if strings.Contains(b.file.Name, "WorkReport") {
		log.Fatalf("Egenkontroll for Alma should not be created here %v", b.file.Name)
	}

	//TYPE
	isHogstFile := strings.Contains(b.file.Name, "Hogst")
	isLassFile := strings.Contains(b.file.Name, "Lass")
	isSingleFile := !isHogstFile && !isLassFile //single egenkontroll for hogst and lass

	//DEVIATION FIELD NAME
	var deviationFieldHidden = false //TODO: Probably just messy?
	if b.company.isCompany(company_Fjordtømmer, company_Nortømmer, company_StoraEnso) {
		deviationFieldHidden = true
	}

	var deviationBoolFiledName = "dev_Boolean"
	if deviationFieldHidden {
		deviationBoolFiledName = "_dev_Boolean"
	}

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = deviationBoolFiledName

	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 32633,
			Wkid:       32633,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_orderId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = &deviationBoolFiledName
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	renderer :=
		b.AddDrawingInfo().
			WithTransparency(0).
			WithRenderer(deviationBoolFiledName)
	renderer.WithUniqueValueInfo("", "Avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgpJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIRozCxR1IntORZmrhOL033nlM0R8eXN75n98958A9Qf6zgj6xgFKq+4BIcjs0hSH0HZam4WtG68fGmJmagVrrtmFjzvWJNG+FWMAVK8Dya2N678HsTbgAzPkCU3B0xJjRbthpu6weQu3Q2gotcYie13rcGPPKCkwolRwTGeuCeNVB/AHXnYLOoONMpOHIaqVuYGhQ5GItMLeOi3R8hquX4bAH2A9D/bD7b2ClSjkEeya1ThhjZsrANuhphIgt6QuQBWLAekt8F0T7HGfwOoyUgRtgow2WARKAAF3AIwu0HoiJRMHV8jposAHflmAAz4FZoMPii5Tyy8AiFG3AZtdZAZtsJqAABQ/QUSqHyBrjXmAa+AC0Y5/LLyAHOQ9wQeRdHlrCFeYAsK/0VVMWijl44AHOw5lncDAFjT65Vk0p9fGSyH0PcAIWtil1JysyunmlsJr0En6IyDgrnXufXlrk9G2IJ6FnSw2wN7CcUerakMiT1X9rlsMJ2H8XJt/DQCeEK2cKYIApMHmtzx4z5pY7Zl1fA5B+Clcewo0G2BGBpjoI/YSlRfiWhxd5GD5pTL4yt+qCTcEnoNenY6v8NvY/6Tfiz5iwRteyOAAAAABJRU5ErkJggg==",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(1)
	renderer.WithUniqueValueInfo("", "Ingen avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgZJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIJorCxR1IntORZmrh2Lyn9z2m6A8XLvd/zo9zDtwT5j8rHOCFlFLdsk/SbKWJKBG+s8A0X3VOPzTGzNQM1Fq3mWFzRvqkmc0kCLnMEovmpenlDrNc5xzwKRiY4bAZMaN0s91adz0R2mmllRaSxPVZPW6MeWEFqpRKy5iM0UXSbw4ucB0n6HTCzgRZDi1X6gZGZFDO1wRzSY5KBx+5zEUOeoH9DNHPzr+BVSqFA+zSkzpljJmpAtvooZGYNekLkAcSwFqLv4O40+cMcpWRKnAd662wHJACBOgCHlig9SAJiYO75TU0WIGvKzCAp8As0GGJiy3lV4FlylZgs+uugA3WKChR8gCVowrypxSXdgPTwDugHayD+QUUKHiAMidvKNJCdEVwCNhTOX7KU6bAPQ+Qz5ziCfvJ0BiQapWaUu/lgtz1AieYU1vULcnLKBs9vzdYz/khIuMsNe79epKVk9wkSZoeNtUAe8WiyqkrMiSPlp9WL4dj7OU2k7xlgE6iq2YKYIApjC7q0+aIueG27PtwgCyPucR9rtHANmI0UUeEnywwzzeKPKPIsDluiitT/Rdshg9Ar6/vo6CN/U/6DbwMmLDH65GJAAAAAElFTkSuQmCC",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(0)

	//FIELDS
	b.AddHiddenStringField("_orderId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.SetFieldIdSeries(0,
		"_orderId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)

	b.AddHiddenStringField("_orderNum", "Ordrenummer")
	b.SetFieldIdSeries(
		5,
		"_orderNum",
	)

	if b.IsFjordtømmer() {

		//assert!
		if !isSingleFile {
			log.Fatalf("Fjordtømmer uses a single Egenkontroll file")
			b.reporter.Error("Fjordtømmer uses a single Egenkontroll file")
			return
		}

		b.AddRequiredBooleanField("asplanned_Boolean", "Er arbeidene gjennomført etter arbeidsinstruks?")
		b.AddRequiredBooleanField("asnorwegianpefc_Boolean", "Er arbeidet gjennomført i samsvar med Norsk PEFC Skogstandard?\n")
		b.AddMultipleChoiceField("tree_mapped_ok", "Er livsløpstre kartfestet?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("padding_ok", "Er det tatt tilfredstillande omsyn til kantsone?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("cult_inheritage_ok", "Er kulturminnar omsyntatt?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("outdoor_life_ok", "Er spesielle omsyn til friluftsliv omsyntatt (stiar, skiløyper etc.)?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")

		b.AddRequiredBooleanField("tracks_Boolean", "Er det oppstått køyrespor med behov for oppretting?")
		b.AddMultipleChoiceField("marked_ok", "Hvis det er oppstått køyrespor, er dette oppretta?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")

		b.AddMultipleChoiceField("tracklogg_documented_ok", "Er sporlogg dokumentert?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")

		b.SetFieldIdSeries(
			100,
			"asplanned_Boolean",
			"asnorwegianpefc_Boolean",
			"tree_mapped_ok",
			"padding_ok",
			"cult_inheritage_ok",
			"outdoor_life_ok",
			"tracks_Boolean",
			"marked_ok",
			"tracklogg_documented_ok",
		)
	} else if isHogstFile {
		b.AddRequiredBooleanField("asplanned_Boolean", "Er arbeidene gjennomført i samsvar med hogstinstruks, produkt- og miljøkrav?")
		b.AddRequiredIntegerField("volume", "Avvirket volum (m3)")
		b.AddMultipleChoiceField("kontrollklaving_ok", "Er det gjennomført kontrollklaving på drifta?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddMultipleChoiceField("prodreport_ok", "Er produsert volum rapportert daglig på drifta?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddMultipleChoiceField("trees_ok", "Er antall og kvalitet på livsløpstrær iht. gjeldende krav?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("tree_mapped_ok", "Er livsløpstre kartfestet?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("elements_marked_ok", "Er nødvendige elementer merket i felt av virkeskjøper (nøkkelbiotoper, naturtyper, grenser etc.)?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddRequiredBooleanField("bio_ok_Boolean", "Ved hogst inn mot biologisk viktig område, er hogsten tilfredsstillende gjennomført?")
		b.AddMultipleChoiceField("padding_ok", "Er det tatt tilfredsstillende hensyn til kantsone/vegetasjonsbelte?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddNullableIntegerField("padding", "Kantsone - gjennomsnittlig bredde på gjensatt kantsone (m)?")
		b.AddMultipleChoiceField("stubbehoyde_ok", "Er stubbehøyde iht. krav: maks 5% av stubbene er høyere enn 20 cm over høyeste marknivå inntil stubben?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddMultipleChoiceField("damaged_trees_ok", "Ved lukket hogst og tynning - er skadeomfanget på gjenstående trær tilfredsstillende?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("cult_inheritage_ok", "Er kulturminner hensyntatt?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("outdoor_life_ok", "Er spesielle hensyn til friluftsliv hensyntatt (stier, skiløyper etc.)?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.SetFieldIdSeries(
			7,
			"asplanned_Boolean",
			"volume",
			"trees_ok",
			"tree_mapped_ok", //10
			"elements_marked_ok",
			"bio_ok_Boolean",
			"padding_ok",
			"padding",
			"damaged_trees_ok", //15
			"cult_inheritage_ok",
			"outdoor_life_ok",
		)
		b.SetFieldIdSeries(100,
			"kontrollklaving_ok",
			"prodreport_ok",
			"stubbehoyde_ok")
	} else if isLassFile {
		b.AddRequiredBooleanField("asplanned_Boolean", "Er arbeidene gjennomført i samsvar med hogstinstruks, produkt- og miljøkrav?")
		b.AddMultipleChoiceField("fw_reported_daily_ok", "Er framkjørt volum rapportert daglig på drifta?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddRequiredBooleanField("basvei_Boolean", "Er basveier anlagt slik at man best mulig unngår kjøreskader?")
		b.AddMultipleChoiceField("prevent_track_damage", "Er det gjort forebyggende tiltak for å redusere kjøreskader?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddRequiredBooleanField("tracks_Boolean", "Er det oppstått kjørespor med behov for oppretting?")

		b.AddNullableBooleanField("marked_Boolean", "Hvis det er oppstått kjørespor, er sted for oppretting avmerket på kartet?").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:       "exists",
				OtherFieldName: "marked_Boolean", //only show field if this field has value; is phased out
			})
		b.AddMultipleChoiceField("marked_ok", "Hvis det er oppstått kjørespor, er sted for oppretting avmerket på kartet?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:       "missing",
				OtherFieldName: "marked_Boolean", //only show field if old field is missing
			})

		b.SetFieldIdSeries(
			6,
			"asplanned_Boolean",
			"basvei_Boolean",
			"prevent_track_damage",
			"tracks_Boolean",
			"marked_Boolean", //10
		)
		b.SetFieldIdSeries(
			100,
			"marked_ok",
		)

		if b.company.hasFeature(isForestOwner) {

		} else {
			b.AddNullableIntegerField("trackdamage", "Sporskader – hvor mange meter sporskader trenger oppretting?")
			b.SetFieldIdSeries(
				11,
				"trackdamage",
			)
		}

		b.AddMultipleChoiceField("cult_inheritage_ok", "Er kulturminner hensyntatt?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("outdoor_life_ok", "Er spesielle hensyn til friluftsliv hensyntatt (stier, skiløyper etc.)?").
			WithCodedOption("Ja").
			WithCodedOption("Nei").
			WithCodedOption("Ikke relevant")
		b.AddMultipleChoiceField("lunne_ok", "Er tømmerlunne lagt opp iht. krav?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddMultipleChoiceField("lev_merke_ok", "Er tømmer merket med leverandørnummer for hver 5m3?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.AddMultipleChoiceField("sort_merke_ok", "Er tømmer merket med sortimentsnummer for hver 100m3?").
			WithCodedOption("Ja").
			WithCodedOption("Nei")
		b.SetFieldIdSeries(
			12,
			"cult_inheritage_ok",
			"outdoor_life_ok",
		)
		b.SetFieldIdSeries(
			200,
			"fw_reported_daily_ok",
			"lunne_ok",
			"lev_merke_ok",
			"sort_merke_ok",
		)
	}

	if isHogstFile {
		if b.company.hasFeature(isForestOwner) {
			b.AddRequiredBooleanField("rotstop_Boolean", "Benyttet rotstop eller tilsvarende?")
			b.AddRequiredIntegerField("driftsveilengde", "Driftsveilengde, meter")
			b.AddRequiredIntegerField("trestorrelse", "Gjennomsnittlig trestørrelse, liter")
			b.SetFieldIdSeries(
				23,
				"rotstop_Boolean",
				"driftsveilengde",
				"trestorrelse",
			)
			b.AddMultipleChoiceField("lunneHasNonDeliverable", "Inneholder lunnene tømmer som etter standardens krav ikke skal leveres?").
				WithCodedOption("Ja").
				WithCodedOption("Nei")
			b.SetFieldIdSeries(
				200,
				"lunneHasNonDeliverable",
			)

		}
	}

	if deviationFieldHidden {
		b.AddHiddenBooleanField(deviationBoolFiledName, "Avvik registrert")
		if isLassFile || isSingleFile {
			b.SetFieldIdSeries(
				14,
				deviationBoolFiledName,
			)
		} else if isHogstFile {
			b.SetFieldIdSeries(
				18,
				deviationBoolFiledName,
			)
		}
	} else {
		b.AddRequiredBooleanField(deviationBoolFiledName, "Avvik registrert")
		if isLassFile || isSingleFile {
			b.SetFieldIdSeries(
				14,
				deviationBoolFiledName,
			)
		} else if isHogstFile {
			b.SetFieldIdSeries(
				18,
				deviationBoolFiledName,
			)
		}
	}

	if b.IsFjordtømmer() {
		b.AddNullableStringField("comment", "Kommentar").
			WithLength(3000)
		b.AddNullableStringField("sign", "Maskinførers signatur").
			WithLength(3000)
		b.SetFieldIdSeries(
			15,
			"comment",
			"sign",
		)
	} else if isHogstFile {
		b.AddRequiredStringField("comment", "Kommentar").
			WithLength(3000)
		b.AddRequiredStringField("sign", "Maskinførers signatur").
			WithLength(3000)
		b.SetFieldIdSeries(
			19,
			"comment",
			"sign",
		)
	} else if isLassFile {
		b.AddRequiredStringField("comment", "Kommentar").
			WithLength(3000)
		b.AddRequiredStringField("sign", "Maskinførers signatur").
			WithLength(3000)
		b.SetFieldIdSeries(
			15,
			"comment",
			"sign",
		)
	}

	b.AddHiddenStringField("_headId", "IO Head Id")
	b.AddHiddenStringField("_serviceOrderId", "Service Order Id")

	if isSingleFile || isLassFile {
		b.SetFieldIdSeries(
			17,
			"_headId",
			"_serviceOrderId",
		)
	} else if isHogstFile {
		b.SetFieldIdSeries(
			21,
			"_headId",
			"_serviceOrderId",
		)
	}

	if isHogstFile {
		if b.company.hasFeature(isForestOwner) {
			b.AddHiddenStringField("_ownershipMark", "Tømmermerke")
			b.SetFieldIdSeries(
				26,
				"_ownershipMark",
			)
		}
	} else if isLassFile {
		if b.company.hasFeature(isForestOwner) {
			b.AddHiddenStringField("_ownershipMark", "Tømmermerke")
			b.SetFieldIdSeries(
				19,
				"_ownershipMark",
			)
		}
	}

	//TYPES
	b.form.Types = []FormType{
		{
			Domains:   FormTypeDomains{},
			Id:        "Egenkontroll",
			Name:      "Egenkontroll",
			Templates: []FormTypeTemplate{},
		},
	}

	b.WithAutoSortex()
}
