package main

func PopulatePTommerdriftForm(b *FormBuilder) {

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	if b.IsForestOwner() {
		b.form.DisplayField = "bestand_str"
	} else {
		b.form.DisplayField = "name"
	}
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("hogstform")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//LOGIC
	logicHogstformIsForyngelseshogst := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "hogstform",
		OtherFieldStringValue: strRef("Foryngelseshogst"),
	}
	logicHogstformIsNotForyngelseshogst := FormFieldLogicField{
		Operator:              "!=",
		OtherFieldName:        "hogstform",
		OtherFieldStringValue: strRef("Foryngelseshogst"),
	}
	logicHogstformIsOmdisponertAreal := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "hogstform",
		OtherFieldStringValue: strRef("Omdisponert areal"),
	}
	logicHogstformIsNotOmdisponertAreal := FormFieldLogicField{
		Operator:              "!=",
		OtherFieldName:        "hogstform",
		OtherFieldStringValue: strRef("Omdisponert areal"),
	}
	logicTypeHogstIsFrøtrestillingshogst := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "type_hogst",
		OtherFieldStringValue: strRef("Frøtrestillingshogst"),
	}
	logicFieldIsAlwaysHidden := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_globalId",
		OtherFieldStringValue: strRef("Dette feltet skal skjules for brukeren"),
	}

	clearColor := FormDrawingInfoColor(0, 0, 0, 0)
	blackColor := FormDrawingInfoColor(0, 0, 0, 255)
	whiteColor := FormDrawingInfoColor(255, 255, 255, 255)
	yellowColor := FormDrawingInfoColor(255, 255, 0, 255)
	redColor := FormDrawingInfoColor(255, 0, 0, 255)

	dashTemplate := []float64{8, 8}

	//DRAWING INFO
	di := b.AddDrawingInfo().
		WithTransparency(0)
	di.WithLabelingInfo(b.form.DisplayField).
		WithCenterAlignedSymbol(
			FormDrawingInfoColor(255, 255, 255, 255), //black color
			10,
			FormDrawingInfoColor(0, 0, 0, 127), //white halo color
			2,
		)

	if !b.IsContractor() {
		renderer := di.WithRenderer("hogstform")
		renderer.WithUniqueValueInfo("Ukjent", "Ukjent").
			WithStringValue("Ukjent").
			WithCimDashedLineSymbol(blackColor, 3, Butt, redColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, blackColor, 3, false)

		// TODO(kristoffer): Field hogstform does not have "Flate" as option, remove this?
		renderer.WithUniqueValueInfo("", "Flate").
			WithStringValue("Flate").
			WithCimDashedLineSymbol(whiteColor, 3, Butt, redColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, whiteColor, 3, false)
		renderer.WithUniqueValueInfo("", "Tynning").
			WithStringValue("Tynning").
			WithCimDashedLineSymbol(yellowColor, 3, Butt, redColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, yellowColor, 3, false)
		renderer.WithUniqueValueInfo("", "Foryngelseshogst").
			WithStringValue("Foryngelseshogst").
			WithCimDashedLineSymbol(whiteColor, 3, Butt, redColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, whiteColor, 3, false)
		renderer.WithUniqueValueInfo("", "Omdisponert areal").
			WithStringValue("Omdisponert areal").
			WithCimDashedLineSymbol(blackColor, 3, Butt, redColor, 3, dashTemplate).
			WithDashedOutlineSymbol(clearColor, blackColor, 3, false)

		renderer.WithUniqueValueInfo("", "styleOverride").
			WithStringValue("styleOverride").
			WithSolidOutlineSymbol(clearColor, redColor, 3)
	}
	if b.IsContractor() {
		// The contractor prefer a slight fill to the body of the shape, making it easier to see what is inside and outside
		renderer := di.WithRenderer("hogstform")
		renderer.WithUniqueValueInfo("Ukjent", "Ukjent").
			WithStringValue("Ukjent").
			WithCimOutlinedSemitransparentFillSymbol(blackColor, 2, 50).
			WithSolidOutlineSolidSymbol(FormDrawingInfoColor(0, 0, 0, 50), FormDrawingInfoColor(0, 0, 0, 255), 2)
		renderer.WithUniqueValueInfo("", "Tynning").
			WithStringValue("Tynning").
			WithCimOutlinedSemitransparentFillSymbol(FormDrawingInfoColor(59, 205, 50, 255), 2, 50).
			WithSolidOutlineSolidSymbol(FormDrawingInfoColor(59, 205, 50, 50), FormDrawingInfoColor(59, 205, 50, 255), 2)
		renderer.WithUniqueValueInfo("", "Foryngelseshogst").
			WithStringValue("Foryngelseshogst").
			WithCimOutlinedSemitransparentFillSymbol(FormDrawingInfoColor(0, 255, 255, 255), 2, 50).
			WithSolidOutlineSolidSymbol(FormDrawingInfoColor(0, 255, 255, 50), FormDrawingInfoColor(0, 255, 255, 255), 2)
		renderer.WithUniqueValueInfo("", "Omdisponert areal").
			WithStringValue("Omdisponert areal").
			WithCimOutlinedSemitransparentFillSymbol(blackColor, 2, 50).
			WithSolidOutlineSolidSymbol(FormDrawingInfoColor(0, 0, 0, 50), FormDrawingInfoColor(0, 0, 0, 255), 2)
	}

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenNullableStringField("_orderId", "Entreprenør ordre ID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_headId", "IO head ID") //TODO: Was editable, but is hidden, so should not matter, please confirm!
	b.AddHiddenNullableStringField("_serviceOrderId", "TjenesteordreID")
	b.AddHiddenIntegerField("_priceForOwner", "Driftspris skogeier")

	sectionName := ""
	if b.IsForestOwner() {
		sectionName = "Hogstordre"
	}
	b.AddSection(sectionName)

	if b.IsFjordtømmer() {
		b.AddNullableStringField("name", "Navn/kjennetegn")
	} else if !b.IsForestOwner() {
		b.AddRequiredStringField("name", "Navn/kjennetegn")
	}

	b.AddMultipleChoiceField("hogstform", "Hogstform").
		WithCodedOption("Foryngelseshogst").
		WithCodedOption("Tynning").
		WithCodedOption("Omdisponert areal")

	b.AddMultipleChoiceField("type_hogst", "Type hogst").
		WithCodedOption("Flatehogst").
		WithCodedOption("Frøtrestillingshogst").
		WithCodedOption("Oppryddingshogst").
		WithCodedOption("Lukket hogst").
		WithCodedOption("Fjellskoghogst").
		WithCodedOption("Hogst av frøtrær").
		WithLogic().
		WithSetValue(FormFieldLogicSetValue{
			IfValueIs: FormFieldLogicIfValueIs{Field: &logicHogstformIsNotForyngelseshogst},
			ThenSetValue: FormFieldLogicSetValueThenSetValue{
				FieldName:   "type_hogst",
				StringValue: strRef(""),
			},
		}).
		WithVisibleIfField(logicHogstformIsForyngelseshogst)
	b.AddMultipleChoiceField("type_lukket_hogst", "Type lukket hogst").
		WithMultipleChoiceSeparator().
		WithCodedOption("Bledningshogst").
		WithCodedOption("Småflater").
		WithCodedOption("Skjermstilling").
		WithCodedOption("Gjennomhogst"). // requested from fritzoeskoger
		WithCodedOption("Stripehogst").  //requested from stora enso
		WithCodedOption("Kanthogst").    //requested from fra stora enso
		WithLogic().
		WithSetValue(FormFieldLogicSetValue{
			IfValueIs: FormFieldLogicIfValueIs{Field: &FormFieldLogicField{
				Operator:              "!=",
				OtherFieldName:        "type_hogst",
				OtherFieldStringValue: strRef("Lukket hogst"),
			}},
			ThenSetValue: FormFieldLogicSetValueThenSetValue{
				FieldName:   "type_lukket_hogst",
				StringValue: strRef(""),
			},
		}).
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "type_hogst",
			OtherFieldStringValue: strRef("Lukket hogst"),
		})

	b.AddYesNoPreferredNoWithoutSoloField("irreversiblyReregulated", "Irreversibelt omregulert tiltak?").
		WithNotes("### Reversibelt:\nOppdyrking og beite\n\n### Irreversibelt:\nMed irreversibel menes noe som er krevende og lite realistisk å tilbakeføre, med tilsvarende produksjonsevne som før det aktuelle tiltaket ble iverksatt\n\nHvis det er et irreversibelt omregulert tiltak så må det inn på en egen Innkjøpsordre.\n\nEksempler: Offentlige veier, Næringstomter, Bolig-/hyttetomter").
		WithLogic().
		WithVisibleIfField(logicHogstformIsOmdisponertAreal)

	if b.IsFjordtømmer() {
		//no teignr / plannr
	} else if b.IsForestOwner() {
		b.AddNullableIntegerField("teignr", "Teignummer").
			WithLogic().WithVisibleIfField(logicFieldIsAlwaysHidden)
		b.AddNullableIntegerField("plannr", "Plannummer").WithLogic().
			WithVisibleIfField(logicFieldIsAlwaysHidden)
	} else {
		b.AddNullableIntegerField("teignr", "Teignummer")
	}

	if b.IsFjordtømmer() {
		//no bestandnr / bestand_str
	} else if b.IsForestOwner() {
		b.AddNullableStringField("bestand_str", "Bestandsnummer")
	} else {
		b.AddNullableIntegerField("bestandnr", "Bestandsnummer")
	}
	if b.IsForestOwner() {
		b.AddNullableStringField("comment", "Merknad til hogstinstruks")
	}

	b.AddNullableBooleanField("frotrestilling_Boolean", "Frøtrestilling").
		WithLogic().
		WithVisibleIfField(logicFieldIsAlwaysHidden)

	b.AddNullableIntegerField("seedTreesPerDaa", "Antall frøtrær/dekar").
		WithLogic().
		WithVisibleIfField(logicTypeHogstIsFrøtrestillingshogst)
	b.AddNullableBooleanField("seedTrees_Boolean", "Skal noen frøtrær være livsløpstrær?").
		WithLogic().
		WithVisibleIfField(logicTypeHogstIsFrøtrestillingshogst)
	b.AddNullableBooleanField("markberedning_Boolean", "Markberedning").
		WithLogic().
		WithVisibleIfField(logicHogstformIsForyngelseshogst)
	b.AddNullableBooleanField("forhandsrydding_Boolean", "Behov for forhåndsrydding").
		WithLogic().
		WithVisibleIfField(logicHogstformIsNotOmdisponertAreal)

	if !b.IsForestOwner() {
		b.AddNullableStringField("forestOwnerComment", "Kommentar til avtale med skogeier")
	}

	b.AddMultipleChoiceField("foryngelsesmetode", "Foryngelsesmetode").
		WithCodedOption("Planting").
		WithCodedOption("Naturlig foryngelse").
		WithCodedOption("Omdisponering").
		WithCodedOption("Såing").
		WithMultipleChoiceSeparator().
		WithLogic().
		WithVisibleIfField(logicHogstformIsForyngelseshogst)
	if b.IsForestOwner() {
		b.AddNullableMultipleChoiceField("tree_kind", "Foryngelsestreslag").
			WithMultipleChoiceSeparator(",").
			WithCodedOption("Gran").
			WithCodedOption("Furu").
			WithCodedOption("Bjørk").
			WithCodedOption("Svartor").
			WithCodedOption("Lerk").
			WithCodedOption("Sitka")
	}

	if b.IsNortømmer() {
		b.AddMultipleChoiceField("planter", "Hvem skal utføre planting?").
			WithCodedOption("Skogeier").
			WithCodedOption("Nortømmer").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:              "contains",
				OtherFieldName:        "foryngelsesmetode",
				OtherFieldStringValue: strRef("Planting"),
			})
	}

	b.AddNullableIntegerField("volumeGran", "Volum gran")
	b.AddNullableIntegerField("volumeFuru", "Volum furu")
	b.AddNullableIntegerField("volumeLauv", "Volum lauv")

	// ENVIRONMENT/MILJØ SECTIONS FOR FOREST OWNERS (MOVED FROM PMILJO-SCHEMA)
	if b.IsForestOwner() {
		b.AddSection("Spesielle hensyn - om driften berører")
		b.AddMultipleChoiceField("mis", "Nøkkelbiotop/MiS-figur").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt MiS-figur.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("MiS-figurer SKAL merkes i felt i forkant av drift. Livsløpstrær kan med fordel settes som buffer rundt MiS-figuren. Evt. tiltak i MiS-figuren skal godkjennes og beskrives av biolog.").
			WithLogic()
		b.AddMultipleChoiceField("naturreservat", "Naturreservat (vernet eller i verneprosess)?").
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturreservat.").
			WithSuggestedPreferredNoOption().
			WithNotes("Naturreservat skal merkes i felt. Statsforvalteren kan kontaktes for merking der grensene er uklare.").
			WithLogic()
		b.AddMultipleChoiceField("naturtype", "Naturtype med A eller B-verdi, eller NiN?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturtype.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Naturtyper SKAL merkes i felt før oppstart av drift. Evt. hogst i naturtype skal godkjennes av biolog som også skal gi føringer for dette.").
			WithLogic()
		b.AddMultipleChoiceField("narin", "Tilsvarende verdisetting i NARIN?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturtype.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Narin-basen må sjekkes ut for mulige registreringer av verneverdier. Kjerneområder med to stjerner eller mer, må unntas hogst eller sjekkes ut med biolog.").
			WithLogic()
		b.AddMultipleChoiceField("kantsone", "Areal med behov for kantsone?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("La kantsone stå urørt.").
			WithSuggestedOption("Kantsone er merket i kart.").
			WithSuggestedOption("Plukk enkelte store ustabile trær i kantsona.").
			WithSuggestedOption("Hogg kantsona for etablering av ny flersjiktet kantsone, etter føringer fra Statsforvalteren/Miljøansvarlig under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Mot vann, bekk/elv og myr over 2 dekar skal det gjensettes en kantsone. Normal bredde på sonen er 10-15 meter.\n\nBredere kantsone (25-30 m) ved:\n* Edellauv-, høgstaude-, storbregne- eller sumpskog.\n\nSmalere kantsone (ned mot 5 m) ved:\n* Tørre vegetasjonstyper eller bratt terreng ned mot myr/vassdrag\n* Ensjikta furuskog.\n* Bekker 1-2 m.\n\nBekk < 1 m med årssikker vannføring:\n* Sett igjen et vegetasjonsbelte (< 5m).\n\nKantsonen skal som hovedregel stå urørt. \nSkal kantsone fjernes helt, må dette søkes om til SF.\n\nDet kan skje plukkhogst av større trær i kantsone. Alt av lauv og stormsterke trær med dype kroner gjensettes. Hvis eksisterende kantsone er lite funksjonell, er det viktig at kantskogen utvikles i neste omløp (ikke planting, hensyn under ungskogpleie og tynning).\n\nHogst i kantsone skal ikke skje i hekketida (mai, juni og juli).")
		b.AddYesNoPreferredNoWithoutSoloField("kjøring_i_bekk", "Kjøring i bekk som krever spesielle hensyn").
			WithNotes("")
		b.AddYesNoPreferredNoWithoutSoloField("andre_formål", "Hogst for andre formål enn skogbruk").
			WithNotes("")
		b.AddMultipleChoiceField("kulturminner", "Berører drift/transport kulturminner eller kulturmiljøer?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Sett igjen trær med merkebånd, som kulturstubber.").
			WithSuggestedOption("Ikke kjør nærmere enn 5 meter fra ytterkant av kulturminnet!").
			WithSuggestedOption("Se føringer fra arkeolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Kulturminner SKAL merkes i Felt i før oppstart av hogst.\n\nVær obs. på dårlig presisjon på enkelte kulturminner i kart. Kontakt arkeolog hos Fylkeskommunen ved usikkerhet om avgrensning eller hensyn til fredet minne.\n\nKulturstubber skal settes igjen rundt kulturminnet, og man kan ikke kjøre nærmere kulturminnet enn 5 meter fra ytterkanten.\n\nKulturminner fra før 1537 og alle samiske kulturminner fra år 1917 eller eldre er automatisk fredet. Skogeier plikter i tillegg å hensynta andre kjente og verdifulle kulturminner.").
			WithLogic()
		b.AddYesNoPreferredNoWithoutSoloField("graving", "Driftsvei: Behov for graving?").
			WithNotes("")
		b.AddRequiredIntegerField("veilengde", "Driftsvei: Veilengde (m)").
			WithNotes("")
		b.AddNullableStringField("otherConserns", "Andre hensyn?").
			WithLogic()

		// FROM SECTION 3 IN FORMGEN_MILJO
		b.AddSection("Truede/sårbare arter")
		b.AddMultipleChoiceField("hensynskrav", "Truede arter med hensynskrav (sårbare-VU, truet-EN eller kritisk truet-CR)?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Hogg et føringer gitt av biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Hogst nærmere enn 50 meter fra artsregistreringen skal avklares med biolog som gir føringer for hvordan dette evt. skal gjennomføres.").
			WithLogic()
		b.AddMultipleChoiceField("truedearter", "Konsentrasjon av minst fire ulike truede (NT) arter innenfor 10 dekar?").
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Hogg etter føringer gitt av biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Hogst nærmere enn 50 meter fra artsregistreringen skal avklares med biolog som gir føringer for hvordan dette evt. skal gjennomføres.").
			WithLogic()

		b.AddMultipleChoiceField("tiur", "Spillplass tiur?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
			WithSuggestedOption("Hogg etter grense på kart mot GPS.").
			WithSuggestedOption("Se føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedOption("Plukkhogst, gjennomhogst etter anvisninger fra biolog. Spar gran med lav kvistsetting og undervegetasjon (skjul).").
			WithSuggestedOption("Gjennomhogst, gruppehogst etter anvisning fra biolog. Spar gran med lav kvistsetting og undervegetasjon.").
			WithSuggestedPreferredNoOption().
			WithNotes("Ved hogst nærmere enn 300 meter fra ytterkant av leiksentrum skal biolog gi føringer for denne. Entreprenør skal være med på befaring med biolog. Tiurleiker med mer enn 12 tiurer skal ha en forvaltningsplan.").
			WithLogic()
		b.AddMultipleChoiceField("rovfugl", "Hekkeplass for rovfugl?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
			WithSuggestedOption("Plukkhogst, gjennomhogst i hensynssone etter anvisning fra biolog.").
			WithSuggestedPreferredNoOption().
			WithNotes("Hogstgrense mot rovfuglreir jfr. standardens krav, skal merkes i terrenget.").
			WithLogic()
		b.AddMultipleChoiceField("andrehekkendefugler", "Andre hekkende fugler som kan bli påvirket?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Unngå hogst og kjøring i Gjengrodde områder som tidligere var åpne landskaper, dyrket mark eller beiteområder.").
			WithSuggestedOption("Unngå hogst og kjøring i kantsoner mot kulturlandskap, vannstrenger og våtmarksområder.").
			WithSuggestedOption("Unngå hogst og kjøring i myrskog og sumpskog.").
			WithSuggestedOption("Unngå hogst og kjøring i lauvtredominert skog.").
			WithSuggestedPreferredNoOption().
			WithNotes("I hekketiden (normalt perioden mai, juni og juli) skal skogsdrift i skog av spesiell betydning for fuglelivet unngås, såfremt det ikke er nødvendig for å komme til bakenforliggende skog. Disse skogtypene er:\n\na) Gjengrodde områder som tidligere var åpne landskaper, dyrket mark eller beiteområder\nb) Kantsoner mot kulturlandskap, vannstrenger og våtmarksområder\nc) Myrskog og sumpskog\nd) Lauvtredominert skog\n \nFor eldre (hogstklasse 4 og 5), flersjiktet, lauvtredominert skog skal skogsdrift i denne perioden unngås. Med skogsdrift menes maskinell hogst av skogsvirke til industriformål av et visst omfang.\n\nBiolog skal kontaktes før drift hvis det finnes artsobservasjoner av:\na) Hvitryggspett (på Sørlandet og Østlandet)\nb) Dvergspurv (VU)\nc) Vierspurv (CR)\nd) Hortulan (CR)\ne) Lappsanger (EN)\nf) Trelerke (NT)\ng) Blåstjert").
			WithLogic()
		b.AddMultipleChoiceField("ansvarsart", "Forekomst av Nasjonal ansvarsart?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
			WithSuggestedOption("Hogstføring etter anvisning fra biolog.").
			WithSuggestedPreferredNoOption().
			WithNotes("De artene som ikke er i kategoriene sårbar, sterkt truet eller kritisk truet, er tilrettelagt i egen liste. 27 arter. Kontakt biolog før hogst. Hogstgrense skal merkes i felt.").
			WithLogic()
	}

	b.AddSection("")
	b.AddNullableBooleanField("environment_cleared_Boolean", "Tiltaket er vurdert ihht. miljøhensyn").
		WithLogic().
		WithResetValueOnChange(1)

	//PREDEFINED VALUES
	if b.IsFjordtømmer() {
		b.AddType(FormType{
			Id:   "Default", //default
			Name: "Default", //default
			Templates: []FormTypeTemplate{
				{
					Description: strRef("Default"),
					//DrawingTool: strRef("esriFeatureEditToolPolygon"),
					Name: "Default",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{
							"hogstform":               "Foryngelseshogst",
							"foryngelsesmetode":       "Planting",
							"type_hogst":              "Flatehogst",
							"markberedning_Boolean":   false,
							"forhandsrydding_Boolean": false,
						},
					},
				},
			},
		})
	}

	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      "Ukjent",
		Name:    "Ukjent",
		Templates: []FormTypeTemplate{
			{
				Description: strRef(""),
				DrawingTool: strRef("esriFeatureEditToolPolygon"),
				Name:        "Ukjent",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{"hogstform": "Ukjent"},
				},
			},
		},
	})

	//fieldIds
	b.SetFieldIdSeries(
		0,
		"_globalId",      // 1
		"_orderId",       // 2
		"_created_Date",  // 3
		"_createdBy",     // 4
		"_modified_Date", // 5
		"_modifiedBy",    // 6
	)

	if !b.IsForestOwner() {
		b.SetFieldIdSeries(6, "name")
	}

	b.SetFieldIdSeries(
		7,

		"hogstform", // 8
		"_headId",   // 9
	)

	if b.IsFjordtømmer() {

	} else {
		b.SetFieldIdSeries(
			9,
			"teignr", //9
		)
	}

	if b.IsFjordtømmer() {

	} else if b.IsForestOwner() {
		b.SetFieldIdSeries(
			11,
			"bestand_str",
		)
	} else {
		b.SetFieldIdSeries(
			10,
			"bestandnr",
		)
	}
	b.SetFieldIdSeries(
		14,
		"foryngelsesmetode",
	)
	b.SetFieldIdSeries(
		16,
		"_serviceOrderId",
		"seedTreesPerDaa",
		"seedTrees_Boolean",
		"_priceForOwner",
		"markberedning_Boolean",
		"frotrestilling_Boolean",
	)
	b.SetFieldIdSeries(
		22,
		"volumeGran",
		"volumeFuru",
		"volumeLauv",
	)
	b.SetFieldIdSeries(
		25,
		"forhandsrydding_Boolean",
	)
	b.SetFieldIdSeries(
		28,
		"type_hogst",
		"type_lukket_hogst",
	)
	if b.IsForestOwner() {
		b.SetFieldIdSeries(
			30,
			"tree_kind",
			"environment_cleared_Boolean",
			"comment",
		)
		b.SetFieldIdSeries(
			56,
			"plannr",
		)
	} else if b.IsNortømmer() {
		b.SetFieldIdSeries(
			27,
			"forestOwnerComment",
		)
		b.SetFieldIdSeries(
			30,
			"environment_cleared_Boolean",
		)
	} else {
		b.SetFieldIdSeries(
			27,
			"forestOwnerComment",
		)
		b.SetFieldIdSeries(
			31,
			"environment_cleared_Boolean",
		)
	}
	if b.IsNortømmer() {
		b.SetFieldIdSeries(26, "planter")
	}
	b.SetFieldIdSeries(33, "irreversiblyReregulated")

	if b.IsForestOwner() {
		b.SetFieldIdSeries(100,
			"kulturminner",
			"kjøring_i_bekk",
			"andre_formål",
			"mis",
			"naturreservat",
			"naturtype",
			"narin",
			"kantsone",
			"otherConserns",
			"graving",
			"veilengde")

		b.SetFieldIdSeries(200,
			"hensynskrav",
			"truedearter",
			"ansvarsart",
			"tiur",
			"rovfugl",
			"andrehekkendefugler")
	}
	b.WithAutoSortex()
}
