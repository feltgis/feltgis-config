package main

func PopulateMiljoForm(b *FormBuilder) {
	_true := 1
	_false := 0
	_emptyString := ""

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "name"
	b.form.DrawingInfo = nil
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_headId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//COMMON LOGIC
	var visibleIfIrreversible = FormFieldLogicIfValueIs{
		Field: &FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: &_true,
			OtherFieldName:     "_irreversiblyReregulated",
		},
	}
	var visibleIfNotIrreversible = FormFieldLogicIfValueIs{
		Field: &FormFieldLogicField{
			Operator:           "!=",
			OtherFieldIntValue: &_true,
			OtherFieldName:     "_irreversiblyReregulated",
		},
	}
	var visibleIfIrreversibleAndGivesGuidelinesForMeasure = FormFieldLogicIfValueIs{
		AllOf: []FormFieldLogicFieldInArray{
			{Field: *(visibleIfIrreversible.Field)},
			{Field: FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: &_false,
				OtherFieldName:     "regulationGivesGuidelinesForMeasure_Boolean",
			}},
		},
	}

	//FIELDS
	b.AddHiddenStringField("_headId", "HeadID")

	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenNullableStringField("_orderId", "Contractor Order Id")
	b.AddHiddenNullableStringField("_serviceOrderId", "Service Order Id")
	b.AddHiddenNullableStringField("_irreversiblyReregulated", "Irreversibelt Omregulert")

	//=== SECTION 1 ===
	b.AddSection("Spesielle formål")
	b.AddYesNoPreferredYesField("sporing", "Alt tømmer kommer fra kartfestet område?").
		WithNotes("Alt virke virkeskjøper omsetter skal kunne spores tilbake til hogstfelt.")

	//=== SECTION 2 ===
	b.AddSection("Spesielle hensyn - om driften berører")
	if b.IsForestOwner() {
		//now on tiltak
	} else {
		b.AddNullableBooleanField("regulationGivesGuidelinesForMeasure_Boolean", "Er det bestemmelser i reguleringsplanen som legger føringer for hogsten? (kulturminner, Kantsoner, friområder  etc.)").
			WithLogic().WithVisibleIf(visibleIfIrreversible)
		b.AddRequiredBooleanField("requirementsForGreenOpenAreas_Boolean", "Krav til grønt/friområder?").
			WithLogic().WithVisibleIf(visibleIfIrreversibleAndGivesGuidelinesForMeasure)
		b.AddRequiredBooleanField("requirementsForSafeguardingCulturalHeritage_Boolean", "Krav til ivaretakelse av kulturminner?").
			WithLogic().WithVisibleIf(visibleIfIrreversibleAndGivesGuidelinesForMeasure)
		b.AddMultipleChoiceField("mis", "Nøkkelbiotop/MiS-figur").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt MiS-figur.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("MiS-figurer SKAL merkes i felt i forkant av drift. Livsløpstrær kan med fordel settes som buffer rundt MiS-figuren. Evt. tiltak i MiS-figuren skal godkjennes og beskrives av biolog.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("naturreservat", "Naturreservat (vernet eller i verneprosess)?").
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturreservat.").
			WithSuggestedPreferredNoOption().
			WithNotes("Naturreservat skal merkes i felt. Statsforvalteren kan kontaktes for merking der grensene er uklare.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("naturtype", "Naturtype med A eller B-verdi, eller NiN?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturtype.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Naturtyper SKAL merkes i felt før oppstart av drift. Evt. hogst i naturtype skal godkjennes av biolog som også skal gi føringer for dette.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("narin", "Tilsvarende verdisetting i NARIN?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Livsløpstrær skal stå igjen som buffer rundt naturtype.").
			WithSuggestedOption("Hogst etter føringer fra biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Narin-basen må sjekkes ut for mulige registreringer av verneverdier. Kjerneområder med to stjerner eller mer, må unntas hogst eller sjekkes ut med biolog.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("kantsone", "Areal med behov for kantsone?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("La kantsone stå urørt.").
			WithSuggestedOption("Kantsone er merket i kart.").
			WithSuggestedOption("Plukk enkelte store ustabile trær i kantsona.").
			WithSuggestedOption("Hogg kantsona for etablering av ny flersjiktet kantsone, etter føringer fra Statsforvalteren/Miljøansvarlig under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Mot vann, bekk/elv og myr over 2 dekar skal det gjensettes en kantsone. Normal bredde på sonen er 10-15 meter.\n\nBredere kantsone (25-30 m) ved:\n* Edellauv-, høgstaude-, storbregne- eller sumpskog.\n\nSmalere kantsone (ned mot 5 m) ved:\n* Tørre vegetasjonstyper eller bratt terreng ned mot myr/vassdrag\n* Ensjikta furuskog.\n* Bekker 1-2 m.\n\nBekk < 1 m med årssikker vannføring:\n* Sett igjen et vegetasjonsbelte (< 5m).\n\nKantsonen skal som hovedregel stå urørt. \nSkal kantsone fjernes helt, må dette søkes om til SF.\n\nDet kan skje plukkhogst av større trær i kantsone. Alt av lauv og stormsterke trær med dype kroner gjensettes. Hvis eksisterende kantsone er lite funksjonell, er det viktig at kantskogen utvikles i neste omløp (ikke planting, hensyn under ungskogpleie og tynning).\n\nHogst i kantsone skal ikke skje i hekketida (mai, juni og juli).").
			WithLogic().
			WithVisibleIfAnyOf(
				*(visibleIfNotIrreversible.Field),
				FormFieldLogicField{
					Operator:           "!=",
					OtherFieldIntValue: &_false,
					OtherFieldName:     "regulationGivesGuidelinesForMeasure_Boolean",
				},
			)
	}

	b.AddYesNoPreferredNoField("skiltplan", "Krav til skiltplan?").
		WithNotes("Arbeid/velteplass ved offentlig vei krever godkjent skiltplan. Tømmeret lunnet langs veg skal ligge minste 3 meter fra vegskulder, målt vannrett.")
	b.AddMultipleChoiceField("offentliggodkjenning", "Berører drift areal med krav om offentlig godkjenning?").
		WithMultipleChoiceSeparator().
		WithSuggestedOption("Hogst i vernskog: Følg anvisning i skogbrukssjefens hogstgodkjenning.").
		WithSuggestedOption("Hogst i marka: Følg plan og vilkår i skogbrukssjefens svar på hogstmelding eller statsforvalterens hogstgodkjenning.").
		WithSuggestedOption("Hogst i områder/kommuner med generell meldeplikt for hogst. Følg kommunalt vedtak med vilkår/føringer for hogst under veglegg i virkeshandel.").
		WithSuggestedOption("Hogst i samisk reinbeitedistrikt med flate på over 100 dekar. Følg plan fra reindriftsavdelingen hos Statsforvalteren under vedlegg i virkeshandel.").
		WithSuggestedPreferredNoOption().
		WithNotes("Områder med krav om offentlig godkjenning krever at melding sendes kommunen. Dette gjelder hogst i vernskog, hogst i 'Bymarka' (Oslo), og hogst i kommuner som har innført meldeplikt etter forskrift til skogloven. Avvirkning kan ikke starte før svar på melding er mottatt.").
		WithLogic().WithVisibleIf(visibleIfNotIrreversible)

	if b.IsForestOwner() {
		//now on tiltak
	} else {
		b.AddMultipleChoiceField("kulturminner", "Berører drift/transport kulturminner eller kulturmiljøer?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Sett igjen trær med merkebånd, som kulturstubber.").
			WithSuggestedOption("Ikke kjør nærmere enn 5 meter fra ytterkant av kulturminnet!").
			WithSuggestedOption("Se føringer fra arkeolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Kulturminner SKAL merkes i Felt i før oppstart av hogst.\n\nVær obs. på dårlig presisjon på enkelte kulturminner i kart. Kontakt arkeolog hos Fylkeskommunen ved usikkerhet om avgrensning eller hensyn til fredet minne.\n\nKulturstubber skal settes igjen rundt kulturminnet, og man kan ikke kjøre nærmere kulturminnet enn 5 meter fra ytterkanten.\n\nKulturminner fra før 1537 og alle samiske kulturminner fra år 1917 eller eldre er automatisk fredet. Skogeier plikter i tillegg å hensynta andre kjente og verdifulle kulturminner.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("reinbeitedistrikt", "Påvirker drifta område definert som samisk reinbeitedistrikt?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Tiltaket overstiger 100 dekar i tilstøtende område innen samme år. Reinbeitemyndighet er varslet. Se vedlegg i virkeshandel.").
			WithSuggestedOption("Se føringer gitt av reindriftsmyndighetene.").
			WithSuggestedPreferredNoOption().
			WithNotes("Skogeier skal anerkjenne, respektere og opprettholde berørte reindriftssamers rettigheter, sedvaner og kultur i samsvar med reindriftslovens bestemmelser.\nSkogeier må ikke utnytte sin eiendom på en slik måte at det er til vesentlig skade for reindriftsutøvelse. I forkant av slike tiltak skal det gis varsel senest tre uker før planlagt iverksetting. Dette gjelder flatehogst, gjødsling og markberedning som overstiger 100 dekar i tilstøtende områder innen samme år. Reindriftsmyndighetene skal varsles ved markberedning uavhengig av arealomfang.\n \nDer reindriftsamene har rettigheter skal det ved hogst og andre skogbrukstiltak tas særskilt hensyn i følgende områder:\na) Viktige trekk- og flytteleder b) Oppsamlingsområder\nc) Vanskelige passasjer\nd) Viktige nødbeiteområder med hengelav i beitehøyde\ne) Beitehager\nf) Kalvingsland\n g) Lavrik mark\nh) Samiske hellige steder, offerplasser, gravplasser, kulturelt viktige stier og andre steder av særskilt kulturhistorisk betydning").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
	}

	b.AddMultipleChoiceField("tidligere", "Finnes det tidligere miljøhensyn, eks. livsløpstrær som må hensyntas?").
		WithMultipleChoiceSeparator().
		WithSuggestedPreferredNoOption().
		WithNotes("Finnes miljøhensyn fra tidligere drifter som ikke vises på kart på eiendommen? Dette legges inn i kartet. Hvis livsløpstrær er satt i bestandsgrense eller nabobestand – legg basvei rundt.").
		WithLogic().WithVisibleIf(visibleIfNotIrreversible)
	b.AddMultipleChoiceField("plasseringllt", "Anbefalt plassering av livsløpstrær").
		WithSuggestedOption("Synlig på flata").
		WithSuggestedOption("I kanten av flata").
		WithSuggestedOption("Virkeskjøper har merket disse i kart").
		WithSuggestedOption("Som buffer rundt BVO").
		WithSuggestedOption("Som en utvidelse av kantsonen").
		WithSuggestedOption("Ikke relevant for tiltakstypen").
		WithNotes("Hvor anbefaler du at maskinfører skal sette igjen livsløpstrærne på denne drifta?\n\nGjensetting av livsløpstrær Ved hogst skal det i gjennomsnitt settes igjen minst 10 livsløpstrær pr. hektar avvirket areal. Livsløpstrær settes igjen enkeltvis eller i grupper i driftsområdet på en måte som bidrar til stabilitet. Kravet til antall livsløpstrær gjelder som gjennomsnitt for det avvirkede område, og kan omfatte flere skogbestand. Livsløpstrærne velges blant de eldste trærne med høyest verdi for naturmangfoldet. Både dominerende treslag og eventuelt sjeldne/uvanlige treslag skal være representert. Der det er fare for stormfelling, kan inntil halvparten av livsløpstrærne av levende gran og osp kappes til høgstubbe (tre som kappes høyere enn 3 meter). For å finne stormsterke grantrær som kan fungere som livsløpstrær, kan også trær med stor barmasse og en diameter ned til ca. 20 cm brukes. Stående død gran kan inngå som livsløpstrær. Summen av stående død gran og høgstubber skal ikke utgjøre mer enn halvparten av antall livsløpstrær.\n\nMinst 2 av livsløpstrærne skal velges blant dominerende treslag. Utover disse skal følgende trær prioriteres:\n\na) Spesielt grove/gamle trær, hule trær og grove trær med utpreget vid, grovkvistet og/eller flat krone\n\nb) Grove/gamle trær med tydelige eldre kulturspor som hagemarkstrær, styvingstrær, beitetrær og barktatte furutrær\n\nc) Trær med hakkespetthull og reirfunksjon for rovfugler\n\nd) Rødlista treslag som ask, alm, barlind, villeple, og ulike asalarter\n\ne) Edelløvtrær i skoglandskapet innen boreal sone\n\nf) Store eksemplarer av osp, selje, rogn, lønn, lind, hegg, hassel, kirsebær, einer og kristtorn\n\ng) Levende trær med brannspor\n\nLauvtrær som livsløpstrær skal prioriteres.").
		WithLogic().WithVisibleIf(visibleIfNotIrreversible)

	if b.IsForestOwner() {
		//now on tiltak
	} else {
		b.AddNullableStringField("otherProvisionsInRegulationPlan", "Andre bestemmelser i reguleringsplanen?").
			WithLogic().WithVisibleIf(visibleIfIrreversibleAndGivesGuidelinesForMeasure)
		b.AddNullableStringField("otherConserns", "Andre hensyn?").
			WithLogic().WithVisibleIf(visibleIfIrreversible)

	}

	//=== SECTION 3 ===
	if !b.IsForestOwner() {
		b.AddSection("Truede/sårbare arter")
		b.AddMultipleChoiceField("hensynskrav", "Truede arter med hensynskrav (sårbare-VU, truet-EN eller kritisk truet-CR)?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Hogg et føringer gitt av biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Hogst nærmere enn 50 meter fra artsregistreringen skal avklares med biolog som gir føringer for hvordan dette evt. skal gjennomføres.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("truedearter", "Konsentrasjon av minst fire ulike truede (NT) arter innenfor 10 dekar?").
			WithSuggestedOption("Hogg inn til merkebånd. Trær med merkebånd skal stå igjen.").
			WithSuggestedOption("Hogg etter føringer gitt av biolog under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Hogst nærmere enn 50 meter fra artsregistreringen skal avklares med biolog som gir føringer for hvordan dette evt. skal gjennomføres.").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)

		var rovfuglNotesLink = ""
		if b.IsNortømmer() {
			rovfuglNotesLink = " \n\n[Regler for sensitive arter i gitt tidsperiode](https://nt.feltgis.no/hensynssonerovfugl2.jpg)"
		}
		if b.IsFjordtømmer() {
			b.AddNullableStringField("hensyn_til_fugleliv", "Hensyn til fugleliv").
				WithNotes(
					"Ved hogst nærmere enn 300 meter fra ytterkant av leiksentrum skal biolog gi føringer for denne. Entreprenør skal være med på befaring med biolog. Tiurleiker med mer enn 12 tiurer skal ha en forvaltningsplan." +
						"\n\n" +
						"Hogstgrense mot rovfuglreir jfr. standardens krav, skal merkes i terrenget." + rovfuglNotesLink +
						"\n\n" +
						"I hekketiden (normalt perioden mai, juni og juli) skal skogsdrift i skog av spesiell betydning for fuglelivet unngås, såfremt det ikke er nødvendig for å komme til bakenforliggende skog. Disse skogtypene er:\n\na) Gjengrodde områder som tidligere var åpne landskaper, dyrket mark eller beiteområder\nb) Kantsoner mot kulturlandskap, vannstrenger og våtmarksområder\nc) Myrskog og sumpskog\nd) Lauvtredominert skog\n \nFor eldre (hogstklasse 4 og 5), flersjiktet, lauvtredominert skog skal skogsdrift i denne perioden unngås. Med skogsdrift menes maskinell hogst av skogsvirke til industriformål av et visst omfang.\n\nBiolog skal kontaktes før drift hvis det finnes artsobservasjoner av:\na) Hvitryggspett (på Sørlandet og Østlandet)\nb) Dvergspurv (VU)\nc) Vierspurv (CR)\nd) Hortulan (CR)\ne) Lappsanger (EN)\nf) Trelerke (NT)\ng) Blåstjert",
				)
		} else {
			b.AddMultipleChoiceField("tiur", "Spillplass tiur?").
				WithMultipleChoiceSeparator().
				WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
				WithSuggestedOption("Hogg etter grense på kart mot GPS.").
				WithSuggestedOption("Se føringer fra biolog under vedlegg i virkeshandel.").
				WithSuggestedOption("Plukkhogst, gjennomhogst etter anvisninger fra biolog. Spar gran med lav kvistsetting og undervegetasjon (skjul).").
				WithSuggestedOption("Gjennomhogst, gruppehogst etter anvisning fra biolog. Spar gran med lav kvistsetting og undervegetasjon.").
				WithSuggestedPreferredNoOption().
				WithNotes("Ved hogst nærmere enn 300 meter fra ytterkant av leiksentrum skal biolog gi føringer for denne. Entreprenør skal være med på befaring med biolog. Tiurleiker med mer enn 12 tiurer skal ha en forvaltningsplan.").
				WithLogic().WithVisibleIf(visibleIfNotIrreversible)
			b.AddMultipleChoiceField("rovfugl", "Hekkeplass for rovfugl?").
				WithMultipleChoiceSeparator().
				WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
				WithSuggestedOption("Plukkhogst, gjennomhogst i hensynssone etter anvisning fra biolog.").
				WithSuggestedPreferredNoOption().
				WithNotes("Hogstgrense mot rovfuglreir jfr. standardens krav, skal merkes i terrenget." + rovfuglNotesLink).
				WithLogic().WithVisibleIf(visibleIfNotIrreversible)
			b.AddMultipleChoiceField("andrehekkendefugler", "Andre hekkende fugler som kan bli påvirket?").
				WithMultipleChoiceSeparator().
				WithSuggestedOption("Unngå hogst og kjøring i Gjengrodde områder som tidligere var åpne landskaper, dyrket mark eller beiteområder.").
				WithSuggestedOption("Unngå hogst og kjøring i kantsoner mot kulturlandskap, vannstrenger og våtmarksområder.").
				WithSuggestedOption("Unngå hogst og kjøring i myrskog og sumpskog.").
				WithSuggestedOption("Unngå hogst og kjøring i lauvtredominert skog.").
				WithSuggestedPreferredNoOption().
				WithNotes("I hekketiden (normalt perioden mai, juni og juli) skal skogsdrift i skog av spesiell betydning for fuglelivet unngås, såfremt det ikke er nødvendig for å komme til bakenforliggende skog. Disse skogtypene er:\n\na) Gjengrodde områder som tidligere var åpne landskaper, dyrket mark eller beiteområder\nb) Kantsoner mot kulturlandskap, vannstrenger og våtmarksområder\nc) Myrskog og sumpskog\nd) Lauvtredominert skog\n \nFor eldre (hogstklasse 4 og 5), flersjiktet, lauvtredominert skog skal skogsdrift i denne perioden unngås. Med skogsdrift menes maskinell hogst av skogsvirke til industriformål av et visst omfang.\n\nBiolog skal kontaktes før drift hvis det finnes artsobservasjoner av:\na) Hvitryggspett (på Sørlandet og Østlandet)\nb) Dvergspurv (VU)\nc) Vierspurv (CR)\nd) Hortulan (CR)\ne) Lappsanger (EN)\nf) Trelerke (NT)\ng) Blåstjert").
				WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		}
		var ansvarsartNotesLink = ""
		if b.IsNortømmer() {
			ansvarsartNotesLink = "  \n\n[Oversikt over nasjonale ansvarsarter](https://nt.feltgis.no/NasjonaleAnsvarsarter.pdf)"
		}
		b.AddMultipleChoiceField("ansvarsart", "Forekomst av Nasjonal ansvarsart?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Hogg inn mot merkebånd. Bånd skal henge igjen.").
			WithSuggestedOption("Hogstføring etter anvisning fra biolog.").
			WithSuggestedPreferredNoOption().
			WithNotes("De artene som ikke er i kategoriene sårbar, sterkt truet eller kritisk truet, er tilrettelagt i egen liste. 27 arter. Kontakt biolog før hogst. Hogstgrense skal merkes i felt." + ansvarsartNotesLink).
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
	}

	//=== SECTION 4 ===
	b.AddSection("Risikoområder")
	b.AddMultipleChoiceField("friluftsverdier", "Berører drift/transport viktige friluftsverdier?").
		WithMultipleChoiceSeparator().
		WithSuggestedOption("Hogsten berører skiløype - ta hensyn. Hogstavfall skal ryddes vekk.").
		WithSuggestedOption("Hogsten berører mye brukt tursti. Hogstavfall skal ryddes vekk og kryssing skal skje uten vesentlig skade.").
		WithSuggestedPreferredNoOption().
		WithNotes("Friluftsinteressene skal tillegges særlig vekst i viktige friluftslivsområder blant annet valg av hogstform og flatestørrelse, og ved å unngå kjøreskader på stier. Man skal legge vekt på å ivareta opplevelseskvalitetene, særlig langs stier og skiløyper. Ved skogsdrift i områder med mye brukte stier eller preparerte skiløyper skal tiltaket varsles gjennom skilting og dialog med foreninger etc.")

	if b.IsFjordtømmer() {
		b.AddNullableStringField("ras_eller_avrenning", "Risiko for ras eller avrenning").
			WithNotes(
				"Gjelder sikkerhet - og beredskapstiltak, lunne nær bolig eller populært turområde. Kontakt med skole/barnehage for informasjon om fare, informer tømmerbilsjåfør om risiko. Vurder sperrebukker med varselplakat og bånding av sikkerhetssone. Hogst nær høyspent. Linjeeier kontaktes før igangsettelse av drift som er 30 meter eller nærmere høyspent. Ved kryssing under høyspent skal linjeeier kontaktes før oppdrag." +
					"\n\n\n" +
					"Lokal skredmyndighet skal konsulteres dersom området hvor det planlegges hogst er definert i NVEs faresonekart for skred og kvikkleire.\n\nI områder for midlertidige driftsveier kan planering ha et samlet omfang på 150 meter eller på areal inntil 450m2, der fylling eller skjæring ikke fører til mer enn 1 meter avvik fra opprinnelig terrengnivå. Prosjekter med omfang ut over dette er søknadspliktige." +
					"\n\n\n" +
					"Finnes det noe utfor driftsområdet som kan bli påvirket av driften?" +
					"\n\n\n" +
					"Finnes det f.eks. bekk med elvemusling, brønn, åpen drikkevannskilde etc. i nedenfor av driftsområdet som kan bli påvirket? Hvis det er tilfellet: informer entreprenør tydelig om nulltoleranse for forurenset/brunt vann i forbindelse med drifta.",
			)
	} else {
		b.AddMultipleChoiceField("beredskap", "Sikkerhet- eller beredskapstiltak?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Lunneplass er nær populært turområde eller der barn og ungdom leker. Husk rasfarelapper og ikke for bratte eller høye lunner.").
			WithSuggestedOption("Hogst nær høyspent").
			WithSuggestedOption("Kryssing under høyspent er nødvendig").
			WithSuggestedOption("Hold 30 meters avstand til høyspent ved lunning").
			WithSuggestedPreferredNoOption().
			WithNotes("Gjelder sikkerhet - og beredskapstiltak, lunne nær bolig eller populært turområde. Kontakt med skole/barnehage for informasjon om fare, informer tømmerbilsjåfør om risiko. Vurder sperrebukker med varselplakat og bånding av sikkerhetssone. Hogst nær høyspent. Linjeeier kontaktes før igangsettelse av drift som er 30 meter eller nærmere høyspent. Ved kryssing under høyspent skal linjeeier kontaktes før oppdrag.")
		b.AddMultipleChoiceField("erosjon", "Område med høy risiko for ras eller skadelig erosjon?").
			WithMultipleChoiceSeparator().
			WithSuggestedOption("Se godkjent søknad på bygging av midlertidig driftsveg under vedlegg i virkeshandel.").
			WithSuggestedPreferredNoOption().
			WithNotes("Lokal skredmyndighet skal konsulteres dersom området hvor det planlegges hogst er definert i NVEs faresonekart for skred og kvikkleire.\n\nI områder for midlertidige driftsveier kan planering ha et samlet omfang på 150 meter eller på areal inntil 450m2, der fylling eller skjæring ikke fører til mer enn 1 meter avvik fra opprinnelig terrengnivå. Prosjekter med omfang ut over dette er søknadspliktige.")
		b.AddYesNoPreferredNoAndMultipleField("risikoutenfordrift", "Finnes det risikoer utenfor driftsområdet?").
			WithNotes("Finnes det noe utfor driftsområdet som kan bli påvirket av driften?").
			WithLogic().WithVisibleIf(visibleIfNotIrreversible)
		b.AddMultipleChoiceField("avrenning", "Risiko for avrenning til vann, vassdrag eller drikkevannskilder?").
			WithSuggestedOption("Vær ekstra varsom og unngå avrenning!").
			WithSuggestedOption("Vær obs på brønn i området. Denne er merket i kart!").
			WithSuggestedOption("Elv med elvemusling som kan bli påvirket. Denne er synlig i kart.").
			WithSuggestedOption("Anadrom lakseelv i området som kan bli påvirket. Denne er synlig i kart.").
			WithSuggestedOption("Åpen drikkevannskilde som kan bli påvirket.").
			WithSuggestedPreferredNoOption().
			WithNotes("Finnes det f.eks. bekk med elvemusling, brønn, åpen drikkevannskilde etc. i nedenfor av driftsområdet som kan bli påvirket? Hvis det er tilfellet: informer entreprenør tydelig om nulltoleranse for forurenset/brunt vann i forbindelse med drifta.")

	}
	b.AddNullableStringField("otherGuides", "Andre føringer") //request from stora enso

	if b.IsForestOwner() {
		//=== SECTION 5 ===
		b.AddSection("Stier, grøfter, driftsveier")
		b.AddNullableBooleanField("driftsveiKeptOpen_Boolean", "Skal driftsvei holdes åpen?")
		b.AddNullableStringField("commentOpenDriftsveiWidth", "Kommentar om åpen bredde på driftsvei").
			WithLogic().
			WithSetValue(
				FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						Field: &FormFieldLogicField{
							Operator:           "==",
							OtherFieldIntValue: &_false,
							OtherFieldName:     "driftsveiKeptOpen_Boolean",
						},
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						FieldName:   "commentOpenDriftsveiWidth",
						StringValue: &_emptyString,
					},
				},
			).
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: &_true,
				OtherFieldName:     "driftsveiKeptOpen_Boolean",
			})
		b.AddNullableBooleanField("areTherePaths_Boolean", "Er det stier?")
		b.AddNullableStringField("commentRequestedPathWidth", "Kommentar om ønsket bredde på stier").
			WithLogic().
			WithSetValue(
				FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						Field: &FormFieldLogicField{
							Operator:           "==",
							OtherFieldIntValue: &_false,
							OtherFieldName:     "areTherePaths_Boolean",
						},
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						FieldName:   "commentRequestedPathWidth",
						StringValue: &_emptyString,
					},
				},
			).
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: &_true,
				OtherFieldName:     "areTherePaths_Boolean",
			})
		b.AddNullableBooleanField("areThereDitches_Boolean", "Finnes det grøfter?")
	}

	//=== SECTION 5/6(fritsoe) ===
	b.AddSection("")
	var checkedDateAllOfFiledsValid = []FormFieldLogicFieldInArray{
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "sporing"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "regulationGivesGuidelinesForMeasure_Boolean"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "requirementsForGreenOpenAreas_Boolean"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "requirementsForSafeguardingCulturalHeritage_Boolean"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "mis"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "naturreservat"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "naturtype"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "narin"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "kantsone"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "skiltplan"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "offentliggodkjenning"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "kulturminner"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "reinbeitedistrikt"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "tidligere"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "plasseringllt"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "otherProvisionsInRegulationPlan"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "otherConserns"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "hensynskrav"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "truedearter"}},

		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "ansvarsart"}},
		{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "friluftsverdier"}},
	}
	if b.IsFjordtømmer() {

		checkedDateAllOfFiledsValid = append(
			checkedDateAllOfFiledsValid,
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "hensyn_til_fugleliv"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "ras_eller_avrenning"}},
		)
	} else {
		checkedDateAllOfFiledsValid = append(
			checkedDateAllOfFiledsValid,
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "tiur"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "rovfugl"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "andrehekkendefugler"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "beredskap"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "erosjon"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "risikoutenfordrift"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "avrenning"}},
		)
	}

	if b.IsForestOwner() {
		checkedDateAllOfFiledsValid = append(
			checkedDateAllOfFiledsValid,
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "kjøring_i_bekk"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "andre_formål"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "driftsveiKeptOpen_Boolean"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "commentOpenDriftsveiWidth"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "areTherePaths_Boolean"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "commentRequestedPathWidth"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "areThereDitches_Boolean"}},
			FormFieldLogicFieldInArray{Field: FormFieldLogicField{Operator: "valid", OtherFieldName: "kommentar"}},
		)
	}
	if !b.IsForestOwner() {
		checkedDateLogicComment := "one means now"
		checkedDateLogicDoubleValue := 1.0
		b.AddUneditableNullableDateField("checked_Date", "Første dato alle miljøforhold ble undersøkt").
			WithLogic().
			WithSetValue(
				FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						AllOf: checkedDateAllOfFiledsValid,
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						Comment:     &checkedDateLogicComment,
						DoubleValue: &checkedDateLogicDoubleValue,
					},
				},
			)
	}
	if b.IsForestOwner() {
		b.AddNullableStringField("kommentar", "Generell kommentar").
			WithNotes("")
	}

	//fieldIds
	//section 0
	b.SetFieldIdSeries(
		0,
		"_headId",
	)
	b.SetFieldIdSeries(
		20,
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_orderId",
		"_serviceOrderId",
	)

	//section 1
	b.SetFieldIdSeries(3, "sporing")

	//section 2
	if !b.IsForestOwner() {
		b.SetFieldIdSeries(
			4,
			"mis", //4
			"naturreservat",
			"naturtype", //6
			"narin",
			"kantsone", //8
		)
		b.SetFieldIdSeries(
			15,
			"kulturminner",
		)
	}
	b.SetFieldIdSeries(9, "tidligere")
	if !b.IsForestOwner() {
		b.SetFieldIdSeries(10, "hensynskrav")
		b.SetFieldIdSeries(11, "truedearter")
		b.SetFieldIdSeries(14, "ansvarsart")

	}
	if b.IsFjordtømmer() {
		b.SetFieldIdSeries(
			12,
			"hensyn_til_fugleliv",
		)
	} else if !b.IsForestOwner() {
		b.SetFieldIdSeries(
			12,
			"tiur", //12
			"rovfugl",
		)
	}

	b.SetFieldIdSeries(19, "offentliggodkjenning")
	b.SetFieldIdSeries(29, "skiltplan")
	b.SetFieldIdSeries(31, "plasseringllt")
	if !b.IsForestOwner() {
		b.SetFieldIdSeries(33, "reinbeitedistrikt")
	}

	b.SetFieldIdSeries(45, "_irreversiblyReregulated")
	if !b.IsForestOwner() {
		b.SetFieldIdSeries(
			46,
			"regulationGivesGuidelinesForMeasure_Boolean",
			"requirementsForGreenOpenAreas_Boolean",
			"requirementsForSafeguardingCulturalHeritage_Boolean",
			"otherProvisionsInRegulationPlan",
			"otherConserns",
		)
	}
	//section 3
	if b.IsFjordtømmer() {
		//not there
	} else if !b.IsForestOwner() {
		b.SetFieldIdSeries(32, "andrehekkendefugler")
	}

	//section 4
	b.SetFieldIdSeries(16, "friluftsverdier")
	if b.IsFjordtømmer() {
		b.SetFieldIdSeries(17, "ras_eller_avrenning")
	} else {
		b.SetFieldIdSeries(17, "beredskap", "erosjon")
		b.SetFieldIdSeries(26, "risikoutenfordrift")
		b.SetFieldIdSeries(30, "avrenning")
	}
	b.SetFieldIdSeries(51, "otherGuides")

	if b.IsForestOwner() {
		//section 5
		b.SetFieldIdSeries(
			38,
			"driftsveiKeptOpen_Boolean",
			"commentOpenDriftsveiWidth",
			"areTherePaths_Boolean",
			"commentRequestedPathWidth",
			"areThereDitches_Boolean",
		)

		//section 6
		b.SetFieldIdSeries(37, "kommentar")
	}

	//section 5/6
	if !b.IsForestOwner() {
		b.SetFieldIdSeries(27, "checked_Date")
	}

	//PREDEFINED VALUES
	if b.IsFjordtømmer() {
		b.AddType(FormType{
			Id:   "Default", //default
			Name: "Default", //default
			Templates: []FormTypeTemplate{
				{
					Description: strRef("Default"),
					//DrawingTool: strRef("esriFeatureEditToolPolygon"),
					Name: "Default",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{
							"reinbeitedistrikt": "Nei",
							"plasseringllt":     "I kanten av flata",
						},
					},
				},
			},
		})
	}

	b.WithAutoSortex()
}
