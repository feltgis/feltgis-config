package main

import (
	"log"
	"strings"
)

// Alma has its own egenkontroll, since it is imported from an Api!
func PopulateEgenkontrollAlmaForm(b *FormBuilder) {

	if !strings.Contains(b.file.Name, "WorkReport") {
		log.Fatalf("Tried to create Egenkontroll for Alma with the wrong postfix %v", b.file.Name)
	}

	//DEVIATION FIELD NAME
	var deviationBoolFiledName = "_dev_Boolean"

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "dev_Boolean"

	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 32633,
			Wkid:       32633,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_orderId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_dev_Boolean")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	renderer :=
		b.AddDrawingInfo().
			WithTransparency(0).
			WithRenderer(deviationBoolFiledName)
	renderer.WithUniqueValueInfo("", "Avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgpJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIRozCxR1IntORZmrhOL033nlM0R8eXN75n98958A9Qf6zgj6xgFKq+4BIcjs0hSH0HZam4WtG68fGmJmagVrrtmFjzvWJNG+FWMAVK8Dya2N678HsTbgAzPkCU3B0xJjRbthpu6weQu3Q2gotcYie13rcGPPKCkwolRwTGeuCeNVB/AHXnYLOoONMpOHIaqVuYGhQ5GItMLeOi3R8hquX4bAH2A9D/bD7b2ClSjkEeya1ThhjZsrANuhphIgt6QuQBWLAekt8F0T7HGfwOoyUgRtgow2WARKAAF3AIwu0HoiJRMHV8jposAHflmAAz4FZoMPii5Tyy8AiFG3AZtdZAZtsJqAABQ/QUSqHyBrjXmAa+AC0Y5/LLyAHOQ9wQeRdHlrCFeYAsK/0VVMWijl44AHOw5lncDAFjT65Vk0p9fGSyH0PcAIWtil1JysyunmlsJr0En6IyDgrnXufXlrk9G2IJ6FnSw2wN7CcUerakMiT1X9rlsMJ2H8XJt/DQCeEK2cKYIApMHmtzx4z5pY7Zl1fA5B+Clcewo0G2BGBpjoI/YSlRfiWhxd5GD5pTL4yt+qCTcEnoNenY6v8NvY/6Tfiz5iwRteyOAAAAABJRU5ErkJggg==",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(1)
	renderer.WithUniqueValueInfo("", "Ingen avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgZJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIJorCxR1IntORZmrh2Lyn9z2m6A8XLvd/zo9zDtwT5j8rHOCFlFLdsk/SbKWJKBG+s8A0X3VOPzTGzNQM1Fq3mWFzRvqkmc0kCLnMEovmpenlDrNc5xzwKRiY4bAZMaN0s91adz0R2mmllRaSxPVZPW6MeWEFqpRKy5iM0UXSbw4ucB0n6HTCzgRZDi1X6gZGZFDO1wRzSY5KBx+5zEUOeoH9DNHPzr+BVSqFA+zSkzpljJmpAtvooZGYNekLkAcSwFqLv4O40+cMcpWRKnAd662wHJACBOgCHlig9SAJiYO75TU0WIGvKzCAp8As0GGJiy3lV4FlylZgs+uugA3WKChR8gCVowrypxSXdgPTwDugHayD+QUUKHiAMidvKNJCdEVwCNhTOX7KU6bAPQ+Qz5ziCfvJ0BiQapWaUu/lgtz1AieYU1vULcnLKBs9vzdYz/khIuMsNe79epKVk9wkSZoeNtUAe8WiyqkrMiSPlp9WL4dj7OU2k7xlgE6iq2YKYIApjC7q0+aIueG27PtwgCyPucR9rtHANmI0UUeEnywwzzeKPKPIsDluiitT/Rdshg9Ar6/vo6CN/U/6DbwMmLDH65GJAAAAAElFTkSuQmCC",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(0)

	//FIELDS
	b.AddHiddenStringField("_orderId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.SetFieldIdSeries(0,
		"_orderId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)

	b.AddUneditableRequiredStringField("orderNum", "Ordrenummer")
	b.SetFieldIdSeries(
		5,
		"orderNum",
	)

	b.AddHiddenStringField("_headId", "IO Head Id")
	b.AddHiddenStringField("_serviceOrderId", "Service Order Id")

	b.SetFieldIdSeries(
		6,
		"_headId",
		"_serviceOrderId",
	)

	b.AddHiddenBooleanField("_dev_Boolean", "Avvik registrert")
	b.SetFieldIdSeries(
		8,
		"_dev_Boolean",
	)

	b.AddSection("Naturtyper").
		WithSubtitle("Naturtyper verdi A og B, trua naturtyper, utvalgte naturtyper, trua arter, prioriterte arter. Føringer / avklaring av hensyn beskrives. Evt vedlegg (se filer).")
	b.AddUneditableNullableBooleanField("NatureTypeAbConsideration_Boolean", "Registrert før hogst")

	isNatureTypeAbConsiderationLogic := FormFieldLogicField{
		Operator:           "==",
		OtherFieldIntValue: intRef(1),
		OtherFieldName:     "NatureTypeAbConsideration_Boolean",
	}

	b.AddUneditableNullableStringField("NatureTypeAbConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "NatureTypeAbConsiderationComments",
		})

	b.AddRequiredBooleanField("NatureTypeAbAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(isNatureTypeAbConsiderationLogic)
	isNotNatureTypeAbAccountedForLogic := FormFieldLogicField{
		Operator:           "!=",
		OtherFieldIntValue: intRef(1),
		OtherFieldName:     "NatureTypeAbAccountedFor_Boolean",
	}

	b.AddNullableStringField("NatureTypeAbAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			isNatureTypeAbConsiderationLogic,
			isNotNatureTypeAbAccountedForLogic,
		)

	b.SetFieldIdSeries(
		100,
		"NatureTypeAbConsideration_Boolean",
	)

	b.SetFieldIdSeries(
		9,
		"NatureTypeAbConsiderationComments",
		"NatureTypeAbAccountedFor_Boolean",
	)

	b.SetFieldIdSeries(
		42,
		"NatureTypeAbAccountedForComments",
	)

	b.AddSection("Vernskog, markaforskrift").
		WithSubtitle("Meldeplikt - godkjenning skal foreligge. Føringer beskrives her, evt vedlegg (se filer)")
	b.AddUneditableNullableBooleanField("ProtectionForestFieldRegulationsConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("ProtectionForestFieldRegulationsConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "ProtectionForestFieldRegulationsConsiderationComments",
		})
	b.AddRequiredBooleanField("ProtectionForestFieldRegulationsAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "ProtectionForestFieldRegulationsConsideration_Boolean",
		})
	b.AddNullableStringField("ProtectionForestFieldRegulationsAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "ProtectionForestFieldRegulationsConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "ProtectionForestFieldRegulationsAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		101,
		"ProtectionForestFieldRegulationsConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		11,
		"ProtectionForestFieldRegulationsConsiderationComments",
		"ProtectionForestFieldRegulationsAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		43,
		"ProtectionForestFieldRegulationsAccountedForComments",
	)

	b.AddSection("Nøkkelbiotop, verneområde").
		WithSubtitle("Statsforvalter skal kontaktes ved hogst mot naturreservat, Grensa skal merkes.")
	b.AddUneditableNullableBooleanField("KeyHabitatsConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("KeyHabitatsConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "KeyHabitatsConsiderationComments",
		})
	b.AddRequiredBooleanField("KeyHabitatsAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "KeyHabitatsConsideration_Boolean",
		})
	b.AddNullableStringField("KeyHabitatsAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "KeyHabitatsConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "KeyHabitatsAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		102,
		"KeyHabitatsConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		13,
		"KeyHabitatsConsiderationComments",
		"KeyHabitatsAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		44,
		"KeyHabitatsAccountedForComments",
	)

	b.AddSection("Kulturminner").
		WithSubtitle("Obs: groper, hauger, murer, gamle tufter og veier. Rydd trær og busker på kulturminnene. Ikke kjør nærmere enn 5 m fra kanten.")
	b.AddUneditableNullableBooleanField("CulturalHeritageConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("CulturalHeritageConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "CulturalHeritageConsiderationComments",
		})
	b.AddRequiredBooleanField("CulturalHeritageAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "CulturalHeritageConsideration_Boolean",
		})
	b.AddNullableStringField("CulturalHeritageAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CulturalHeritageConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CulturalHeritageAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		103,
		"CulturalHeritageConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		15,
		"CulturalHeritageConsiderationComments",
		"CulturalHeritageAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		45,
		"CulturalHeritageAccountedForComments",
	)

	b.AddSection("Myrskog, sumpskog, edellauv").
		WithSubtitle("Skal ha lukket hogst.")
	b.AddUneditableNullableBooleanField("CarrSwampDecidousConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("CarrSwampDecidousConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "CarrSwampDecidousConsiderationComments",
		})
	b.AddRequiredBooleanField("CarrSwampDecidousAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "CarrSwampDecidousConsideration_Boolean",
		})
	b.AddNullableStringField("CarrSwampDecidousAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CarrSwampDecidousConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CarrSwampDecidousAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		104,
		"CarrSwampDecidousConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		17,
		"CarrSwampDecidousConsiderationComments",
		"CarrSwampDecidousAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		46,
		"CarrSwampDecidousAccountedForComments",
	)

	b.AddSection("Kantsoner").
		WithSubtitle("Kantsonebredde er normalt 10-15 meter, bredere på bløt eller rik vegetasjon. Kantsoner skal normalt stå urørt. Evt hogst skal beskrives og dokumenteres. Det skal søkes dispensasjon der det er krav om det etter lovverket. For bekker mindre enn 1 meter skal buskvegetasjon og mindre trær spares.")
	b.AddUneditableNullableBooleanField("BufferzonesConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("BufferzonesConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "BufferzonesConsiderationComments",
		})
	b.AddRequiredBooleanField("BufferzonesAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "BufferzonesConsideration_Boolean",
		})
	b.AddNullableStringField("BufferzonesAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "BufferzonesConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "BufferzonesAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		105,
		"BufferzonesConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		19,
		"BufferzonesConsiderationComments",
		"BufferzonesAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		47,
		"BufferzonesAccountedForComments",
	)

	b.AddSection("Stier, tur- og tettbebygde områder").
		WithSubtitle("Unngå kjøring i stier og løyper. Skriftlig varsling ifbm tiltak skal vurderes. Utbedre raskt sporskader i viktige friluftsområder og ved tettbebyggelse.")
	b.AddUneditableNullableBooleanField("TrailsHikingDensePopAreasConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("TrailsHikingDensePopAreasConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "TrailsHikingDensePopAreasConsiderationComments",
		})
	b.AddRequiredBooleanField("TrailsHikingDensePopAreasAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "TrailsHikingDensePopAreasConsideration_Boolean",
		})
	b.AddNullableStringField("TrailsHikingDensePopAreasAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "TrailsHikingDensePopAreasConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "TrailsHikingDensePopAreasAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		106,
		"TrailsHikingDensePopAreasConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		21,
		"TrailsHikingDensePopAreasConsiderationComments",
		"TrailsHikingDensePopAreasAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		48,
		"TrailsHikingDensePopAreasAccountedForComments",
	)

	b.AddSection("Livsløpstrær").
		WithSubtitle("Spar minst ett tre pr daa av de eldste i driftsområdet. Spar alle aktuelle treslag, gjerne i grupper. Tørrtrær/ høgstubber av gran/osp kan inngå inntil 50 %. Tørrtrær skal spares i sin helhet der det er mulig.")
	b.AddUneditableNullableStringField("RetentionTreesConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "RetentionTreesConsiderationComments",
		})
	b.AddRequiredBooleanField("RetentionTreesAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst")

	b.AddNullableStringField("RetentionTreesAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "!=",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "RetentionTreesAccountedFor_Boolean",
		})

	b.AddRequiredIntegerField("RetentionTreesAccountedForAmount", "Antall trær").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "RetentionTreesAccountedFor_Boolean",
		})

	b.SetFieldIdSeries(
		23,
		"RetentionTreesConsiderationComments",
		"RetentionTreesAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		39,
		"RetentionTreesAccountedForAmount",
	)
	b.SetFieldIdSeries(
		49,
		"RetentionTreesAccountedForComments",
	)

	b.AddSection("Kryssing av bekker, elver, o.l.").
		WithSubtitle("Tiltak ved kryssing av bekker gjelder uavhengig av bredde.")
	b.AddUneditableNullableBooleanField("CrossStreamsRiversConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("CrossStreamsRiversConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "CrossStreamsRiversConsiderationComments",
		})
	b.AddRequiredBooleanField("CrossStreamsRiversAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "CrossStreamsRiversConsideration_Boolean",
		})
	b.AddNullableStringField("CrossStreamsRiversAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CrossStreamsRiversConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "CrossStreamsRiversAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		108,
		"CrossStreamsRiversConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		25,
		"CrossStreamsRiversConsiderationComments",
		"CrossStreamsRiversAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		50,
		"CrossStreamsRiversAccountedForComments",
	)

	b.AddSection("Drikkevannskilder, avfall, visuelt utsatte områder o.l.").
		WithSubtitle("Ved hogst innenfor hensynsområde drikkevann skal eventuelle restriksjoner avklares med vannverk/kommune. Ved hogst i nærheten av hensynspunkt, drikkevann skal det tas kontakt med skogeier for å avklare forhold til drikkevannskilde.")
	b.AddUneditableNullableBooleanField("DrinkSourceWasteConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("DrinkSourceWasteConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "DrinkSourceWasteConsiderationComments",
		})
	b.AddRequiredBooleanField("DrinkSourceWasteAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "DrinkSourceWasteConsideration_Boolean",
		})
	b.AddNullableStringField("DrinkSourceWasteAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "DrinkSourceWasteConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "DrinkSourceWasteAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		109,
		"DrinkSourceWasteConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		27,
		"DrinkSourceWasteConsiderationComments",
		"DrinkSourceWasteAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		51,
		"DrinkSourceWasteAccountedForComments",
	)

	b.AddSection("Sporskader").
		WithSubtitle("Spor dypere enn 30 cm over mer enn 10 m strekning rapporteres, samt andre sporskader som anses som alvorlige.")
	b.AddUneditableNullableBooleanField("TrackDamageConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("TrackDamageConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "TrackDamageConsiderationComments",
		})

	b.AddRequiredIntegerField("TrackDamageAccountedForMeters", "Antall meter")

	b.AddNullableStringField("TrackDamageAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.")
	b.SetFieldIdSeries(
		110,
		"TrackDamageConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		29,
		"TrackDamageConsiderationComments",
	)
	b.SetFieldIdSeries(
		31,
		"TrackDamageAccountedForMeters",
	)
	b.SetFieldIdSeries(
		52,
		"TrackDamageAccountedForComments",
	)

	b.AddSection("Oljeutslipp").
		WithSubtitle("Varsle brannvesen dersom utslippet overskrider 50 liter.")
	b.AddRequiredBooleanField("OilSpillAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst")

	b.AddNullableIntegerField("OilSpillsAccountedForAmount", "Mengde oljssøl håndtert (liter)").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "OilSpillAccountedFor_Boolean",
		})

	b.AddNullableStringField("OilSpillAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.")
	b.SetFieldIdSeries(
		32,
		"OilSpillAccountedFor_Boolean",
		"OilSpillsAccountedForAmount",
	)
	b.SetFieldIdSeries(
		53,
		"OilSpillAccountedForComments",
	)

	b.AddSection("Tiurleik, rovfuglreir, andre aktuelle hekkelokaliteter (se Allma, artskart, kilden)").
		WithSubtitle("Biolog/ornitolog må konsulteres.")
	b.AddUneditableNullableBooleanField("GrouseRaptornestOtherConsideration_Boolean", "Registrert før hogst")
	b.AddUneditableNullableStringField("GrouseRaptornestOtherConsiderationComments", "Skogbruksleders betraktning").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "!=",
			OtherFieldStringValue: strRef(""),
			OtherFieldName:        "GrouseRaptornestOtherConsiderationComments",
		})
	b.AddRequiredBooleanField("GrouseRaptornestOtherAccountedFor_Boolean", "Registrert/ivaretatt i forbindelse med hogst").
		WithLogic().
		WithRequiredIfField(FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "GrouseRaptornestOtherConsideration_Boolean",
		})
	b.AddNullableStringField("GrouseRaptornestOtherAccountedForComments", "Kommentar").
		WithLength(200).
		WithNotes("Føringer / avklaring av hensyn beskrives.").
		WithLogic().
		WithRequiredIfAllOf(
			FormFieldLogicField{
				Operator:           "==",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "GrouseRaptornestOtherConsideration_Boolean",
			}, FormFieldLogicField{
				Operator:           "!=",
				OtherFieldIntValue: intRef(1),
				OtherFieldName:     "GrouseRaptornestOtherAccountedFor_Boolean",
			},
		)
	b.SetFieldIdSeries(
		111,
		"GrouseRaptornestOtherConsideration_Boolean",
	)
	b.SetFieldIdSeries(
		34,
		"GrouseRaptornestOtherConsiderationComments",
		"GrouseRaptornestOtherAccountedFor_Boolean",
	)
	b.SetFieldIdSeries(
		54,
		"GrouseRaptornestOtherAccountedForComments",
	)

	b.AddSection("Intern kommentar fra entreprenør").WithSubtitle("")
	b.AddNullableStringField("InternalNoteFromEntrepreneur", "Intern kommentar fra entreprenør").
		WithLength(200)

	b.AddUneditableNullableStringField("InitialsF", "Initialer entreprenør lassbærer").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "==",
			OtherFieldStringValue: strRef("HACK: Dette feltet vil alltid være skjult siden denne kommentaren aldri vil matche feltet"),
			OtherFieldName:        "InitialsF",
		})

	b.AddUneditableNullableStringField("InitialsP", "Initialer entreprenør hogstmaskin").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:              "==",
			OtherFieldStringValue: strRef("HACK: Dette feltet vil alltid være skjult siden denne kommentaren aldri vil matche feltet"),
			OtherFieldName:        "InitialsP",
		})

	b.SetFieldIdSeries(
		36,
		"InternalNoteFromEntrepreneur",
		"InitialsF",
		"InitialsP",
	)

	b.AddHiddenStringField("_globalId", "EntreprenørordreID")
	b.AddHiddenBooleanField("_validSchema_Boolean", "Gyldig skjema")
	b.SetFieldIdSeries(40, "_globalId")
	b.SetFieldIdSeries(55, "_validSchema_Boolean")

	//TYPES
	b.form.Types = []FormType{
		{
			Domains:   FormTypeDomains{},
			Id:        "Arbeidsrapport",
			Name:      "Arbeidsrapport",
			Templates: []FormTypeTemplate{},
		},
	}

	b.WithAutoSortex()
}
