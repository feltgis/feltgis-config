package main

import "log"

type MapLayerType string

var (
	mapLayerType_Vector  MapLayerType = "vector"
	mapLayerType_Raster  MapLayerType = "raster"
	mapLayerType_Polygon MapLayerType = "polygon"
	mapLayerType_Line    MapLayerType = "line"
	mapLayerType_Point   MapLayerType = "point"
	mapLayerType_Wmts    MapLayerType = "wmts"
)

type MapLayer struct {
	Name                 string
	Type                 MapLayerType
	LayerId              string //this means different things all over....
	DevUrl               string
	DevStatusUrl         string
	DevBodyReplacements  []ConfigMapLayerBodyReplacement
	ProdUrl              string
	ProdStatusUrl        string
	ProdBodyReplacements []ConfigMapLayerBodyReplacement
	CacheWarmupUrls      []ConfigMapLayerCacheWarmupUrls
}

func (l MapLayer) WithBodyReplacements(replacements []ConfigMapLayerBodyReplacement) MapLayer {
	l.ProdBodyReplacements = replacements
	l.DevBodyReplacements = replacements
	return l
}

// ----------------------------------------------
// ----------------------------------------------
// TILE SERVER MAPS

//The black box that does to much, aka. CacheBoxen
//In map context, it tiles up wms (WebMapServices) layers

var (
	mapLayerUrl_TileServerDev  = "http://35.228.152.51:8000/" //test/dev
	mapLayerUrl_TileServerProd = "http://35.228.152.51/"
)

func TileServerMapLayer(Name string, Path string) MapLayer {
	var DevUrl = mapLayerUrl_TileServerDev + Path
	var ProdUrl = mapLayerUrl_TileServerProd + Path
	return MapLayer{
		Name:          Name,
		Type:          mapLayerType_Raster, //only raster left!
		DevUrl:        DevUrl,
		DevStatusUrl:  "",
		ProdUrl:       ProdUrl,
		ProdStatusUrl: "",
	}
}

var (
	mapLayer_BrukerMinner = TileServerMapLayer(
		"Brukerminner",
		"Brukerminner/MapServer",
	)
	mapLayer_TurRuter = TileServerMapLayer(
		"TurRuter",
		"TurRuter/MapServer",
	)
	mapLayer_Aldersklasseriskog = TileServerMapLayer(
		"Aldersklasseriskog",
		"Aldersklasseriskog/MapServer",
	)
	mapLayer_Andre = TileServerMapLayer(
		"Andre",
		"Andre/MapServer",
	)
	mapLayer_BrattTerreng = TileServerMapLayer(
		"BrattTerreng",
		"BrattTerreng/MapServer",
	)
	mapLayer_GMNaturtyper_NiN = TileServerMapLayer(
		"GM_Naturtyper_NiN",
		"GM_Naturtyper_NiN/MapServer",
	)
	mapLayer_GMNokkelbiotoper = TileServerMapLayer(
		"GM_Nokkelbiotoper",
		"GM_Nokkelbiotoper/MapServer",
	)
	mapLayer_Markfuktighetskart = TileServerMapLayer(
		"Markfuktighetskart",
		"Markfuktighetskart/MapServer",
	)
	mapLayer_GMMarkfuktighetskart_Helelandet = TileServerMapLayer(
		"Markfuktighetskart",
		"MarkfuktighetskartHeleLandet/MapServer",
	)
	mapLayer_N5Raster2 = TileServerMapLayer(
		"N5 raster 2",
		"n5raster2/MapServer",
	)
	mapLayer_Reguleringsplaner = TileServerMapLayer(
		"Reguleringsplaner",
		"Reguleringsplaner/MapServer",
	)
	mapLayer_Riksantikvaren = TileServerMapLayer(
		"Riksantikvaren",
		"Riksantikvaren/MapServer",
	)
	mapLayer_Skyggerelieff = TileServerMapLayer(
		"Skyggerelieff",
		"Skyggerelieff/MapServer",
	)
	mapLayer_KulturminnerWMS = TileServerMapLayer(
		"KulturminnerWMS",
		"KulturminnerWMS/MapServer",
	)
	mapLayer_TraktorOgSkogsbilvegWMS = TileServerMapLayer(
		"Traktor og skogsbilveg",
		"TraktorOgSkogsbilveg/MapServer",
	)
	mapLayer_Artsdata = TileServerMapLayer(
		"artsdata",
		"artsdata/MapServer",
	)
	mapLayer_Losmasser = TileServerMapLayer(
		"losmasser",
		"losmasser/MapServer",
	)
	mapLayer_Mis = TileServerMapLayer(
		"mis",
		"mis/MapServer",
	)
	mapLayer_Mis2 = TileServerMapLayer(
		"mis2",
		"mis2/MapServer",
	)
	mapLayer_NtData = TileServerMapLayer(
		"ntdata",
		"ntdata/MapServer",
	)
)

// ----------------------------------------------
// ----------------------------------------------
// VTFRONT

type MapVtServer string

const (
	mapVtServer_Ingress         MapVtServer = "Ingress"
	mapVtServer_IngressCompound MapVtServer = "IngressCompound"
)

//VtFront = vector tile server, the correct one!
//vt = vector tile
//can remove _esri_25833, no longer needed

var (
	mapLayerUrl_VtFrontIngressDev          = "https://feltgis-testing-ingress-fppskc73cq-lz.a.run.app/vtfront/"
	mapLayerUrl_VtFrontIngressProd         = "https://feltgis-prod-ingress-fppskc73cq-lz.a.run.app/vtfront/"
	mapLayerUrl_VtFrontIngressDevCompound  = "https://feltgis-testing-ingress-fppskc73cq-lz.a.run.app/vtfront/compound/"
	mapLayerUrl_VtFrontIngressProdCompound = "https://feltgis-prod-ingress-fppskc73cq-lz.a.run.app/vtfront/compound/"

	// I want to keep this around to make it easier when I test VTFront layers locally. Sincerely, Matti.
	mapLayerUrl_VTFrontLocal = "http://localhost:8350/"
)

func VtFrontMapLayer(Name string, Server MapVtServer, UrlLayerId string, Status bool) MapLayer {

	var DevUrl string
	var ProdUrl string
	var DevStatusUrl = ""
	var ProdStatusUrl = ""

	switch Server {
	case mapVtServer_Ingress:
		DevUrl = mapLayerUrl_VtFrontIngressDev + UrlLayerId
		ProdUrl = mapLayerUrl_VtFrontIngressProd + UrlLayerId
		if Status {
			DevStatusUrl = mapLayerUrl_VtFrontIngressDev + UrlLayerId + "/status"
			ProdStatusUrl = mapLayerUrl_VtFrontIngressProd + UrlLayerId + "/status"
		}
	case mapVtServer_IngressCompound:
		DevUrl = mapLayerUrl_VtFrontIngressDevCompound + UrlLayerId
		ProdUrl = mapLayerUrl_VtFrontIngressProdCompound + UrlLayerId
		if Status {
			DevStatusUrl = mapLayerUrl_VtFrontIngressDevCompound + UrlLayerId + "/status"
			ProdStatusUrl = mapLayerUrl_VtFrontIngressProdCompound + UrlLayerId + "/status"
		}
	default:
		log.Fatal("HOW?")
	}

	return MapLayer{
		Name:          Name,
		Type:          mapLayerType_Vector,
		DevUrl:        DevUrl,
		DevStatusUrl:  DevStatusUrl,
		ProdUrl:       ProdUrl,
		ProdStatusUrl: ProdStatusUrl,
	}.WithBodyReplacements(mapLayer_IngressFonts_BodyReplacements)
}

var (
	//----------------------------------------------------
	//COMMON
	mapLayer_FriluftslivStatligSikra = VtFrontMapLayer(
		"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
		mapVtServer_Ingress,
		"894a1dce-5731-ff71-4af8-40c4aa7a4058",
		true,
	)
	mapLayer_DebugIndexlayer = VtFrontMapLayer(
		"indexlayer", //hardcoded layer that shows tile grid for debugging?
		mapVtServer_Ingress,
		"indexlayer",
		false,
	)
	mapLayer_JordOgFlomskred = VtFrontMapLayer(
		"jord_og_flomskred",
		mapVtServer_Ingress,
		"2e540c0d-b7d3-e932-eb0c-d0763337c778",
		true,
	)
	mapLayer_Markagrensen = VtFrontMapLayer(
		"markagrensen",
		mapVtServer_Ingress,
		"7b58c63f-399b-9fee-cd06-47ec42025d90",
		false,
	)

	mapLayer_Misc = VtFrontMapLayer(
		"misc",
		mapVtServer_Ingress,
		"5a9307ec-28f8-9d3b-8901-6a85cd645501",
		false,
	)
	mapLayer_NaturtyperHb13AlleAB = VtFrontMapLayer(
		"naturtyper_hb13_alle_a_b",
		mapVtServer_Ingress,
		"c61708f5-112f-2eac-1ed2-66b2a36392c9",
		true,
	)
	//naturtyper_hb13_naturtyper_hb13_alle = alle rader i datasettet
	mapLayer_NaturtyperHb13NaturtyperHb13Alle = VtFrontMapLayer(
		"naturtyper_hb13_naturtyper_hb13_alle",
		mapVtServer_Ingress,
		"6bad240f-1b59-9d77-f2c3-a4afc4a44514",
		false,
	)
	mapLayer_NaturtyperUnfiltered = VtFrontMapLayer(
		"naturtyper_unfiltered",
		mapVtServer_Ingress,
		"a1e5b8df-6b52-151a-a7b4-69681300068f",
		true,
	)
	mapLayer_Skredkvikkleire2Kvikkleirefaregrad = VtFrontMapLayer(
		"skredkvikkleire2_kvikkleirefaregrad",
		mapVtServer_Ingress,
		"bfbb39c2-45b2-ee9c-2457-c986f83099fa",
		false,
	)
	//This layer is now part of FGFugl
	mapLayer_TiurleikerStjørdal = VtFrontMapLayer(
		"tiurleiker_stjørdal",
		mapVtServer_Ingress,
		"b2b266dc-47cb-6363-696d-74fd8b47fc48",
		false,
	)
	mapLayer_FGfugl = VtFrontMapLayer(
		"FGFugl", //This is a layer with birds reported to FeltGIS that must be taken into account in forest measures. Contains both polygons and points
		mapVtServer_Ingress,
		"0aa39fba-cf92-748a-018c-d7d150996777",
		false,
	)
	mapLayer_VernNaturvernOmrade = VtFrontMapLayer(
		"vern_naturvern_omrade",
		mapVtServer_Ingress,
		"1f1aace9-82dc-b429-ffd9-b578fce18e63",
		true,
	)
	mapLayer_Vernskog = VtFrontMapLayer(
		"vernskog",
		mapVtServer_Ingress,
		"763000db-5503-0166-a585-9b5ccce9a8ac",
		true,
	)
	mapLayer_Teig = VtFrontMapLayer(
		"teig",
		mapVtServer_Ingress,
		"acc14f4e-4aac-7bf0-558a-1700a0fafaf4",
		true,
	)
	mapLayer_Grensepunkter = VtFrontMapLayer(
		"grensepunkter",
		mapVtServer_Ingress,
		"2cda7982-5089-763e-f37a-bc53f566e37f",
		false,
	)
	mapLayer_Kulturminner = VtFrontMapLayer(
		"kulturminner",
		mapVtServer_IngressCompound,
		"dbkulturminner",
		false,
	)
	mapLayer_Narin = VtFrontMapLayer(
		"narin",
		mapVtServer_IngressCompound,
		"dbnarin",
		false,
	)

	mapLayer_VegBruksklasser = VtFrontMapLayer(
		"veg_bruksklasser",
		mapVtServer_Ingress,
		"e68c7527-a35c-ca91-155e-3a061a7e673a",
		false,
	)
	mapLayer_SR16FG = VtFrontMapLayer(
		"SR16FG",
		mapVtServer_Ingress,
		"a63f9073-16a8-253d-46f6-f777d56d14b0",
		true,
	)
	mapLayer_LivslopstrarNorgeNortømmer = VtFrontMapLayer(
		"livslopstrar_norge_nt",
		mapVtServer_Ingress,
		"e8bc65a9-9f19-7733-9ab0-e30e5581c712",
		true,
	)
	mapLayer_LivslopstrarNorgeGlommen = VtFrontMapLayer(
		"livslopstrar_norge_gm",
		mapVtServer_Ingress,
		"b7a24237-32d2-220d-7714-60c6ec52df93",
		true,
	)

	mapLayer_Arter = VtFrontMapLayer(
		"arter",
		mapVtServer_IngressCompound,
		"dbarter",
		false,
	)
	mapLayer_MisTrysilPrevista = VtFrontMapLayer(
		"mis_trysil_prevista",
		mapVtServer_IngressCompound,
		"MiSTrysilPrevista",
		false,
	)

	//----------------------------------------------------
	//NORTOMMER
	mapLayer_NTMis = VtFrontMapLayer(
		"NTmis",
		mapVtServer_Ingress,
		"0ce16765-06dd-1619-63ee-544ff17a5e79",
		false,
	)
	mapLayer_NTEditor10Oppdragsflater = VtFrontMapLayer(
		"nortommer_editor_10_oppdragsflater",
		mapVtServer_Ingress,
		"714c8b1d-f57d-5cd7-d110-e12b093c5cb4",
		false,
	)
	mapLayer_NTMiljo = VtFrontMapLayer(
		"ntmiljo",
		mapVtServer_IngressCompound,
		"dbntmiljo",
		false,
	)
	mapLayer_NTBestand = VtFrontMapLayer(
		"ntbestand",
		mapVtServer_Ingress,
		"6f04efcc-9f8c-4ad0-6095-0084099e3b4a",
		true,
	)
	mapLayer_NTBestandslinjer = VtFrontMapLayer(
		"ntbestandslinjer",
		mapVtServer_Ingress,
		"81249bb6-c1af-a89d-8efd-604f36e507c7",
		true,
	)

	//----------------------------------------------------
	//FJORDTOMMER
	mapLayer_FTMis = VtFrontMapLayer(
		"FTmis",
		mapVtServer_Ingress,
		"910565d9-edbe-3e8c-56e1-355202674a79",
		false,
	)

	//----------------------------------------------------
	//MATHIESEN EIDSVOLD VÆRK
	mapLayer_MELinnea = VtFrontMapLayer(
		"Mev",
		mapVtServer_IngressCompound,
		"Mev",
		false,
	)

	//----------------------------------------------------
	//FRITZOE
	mapLayer_FZMiljø = VtFrontMapLayer(
		"Miljø",
		mapVtServer_IngressCompound,
		"FZmiljo",
		false,
	)
	mapLayer_FZInfrastruktur = VtFrontMapLayer(
		"Infrastruktur",
		mapVtServer_IngressCompound,
		"FZsamferdsel",
		false,
	)
	/*
		Fra fz: De har to kartlag for bestand, ett med og ett uten etiketter, fjern det uten etiketter.
		mapLayer_FZBestandsflater = VtFrontMapLayer(
			"Bestandsflater",
			mapVtServer_Ingress,
			"bf47325f-2823-98c1-ae2e-c2f9360802a1",
			true,
		)
	*/
	mapLayer_FZTreeVol = VtFrontMapLayer(
		"trees_vol",
		mapVtServer_Ingress,
		"0ddf4de5-f20b-8457-c1ae-d4029bbe0a7c",
		true,
	)
	mapLayer_FZTeig = VtFrontMapLayer(
		"Teig",
		mapVtServer_Ingress,
		"5a0cfc3c-d3b5-76ca-a8ff-a1bce5d19407",
		true,
	)
	mapLayer_FZBestandsflaterMedEtiketter = VtFrontMapLayer(
		"Bestandsflater",
		mapVtServer_Ingress,
		"cd31e6fc-b2bd-1ea8-1c3f-cd59698b2906",
		true,
	)
	mapLayer_FZBestandSkogtyper = VtFrontMapLayer(
		"bestand_skogtyper",
		mapVtServer_Ingress,
		"a29257b7-239c-346b-ee8d-25abf3ceef08",
		true,
	)
	mapLayer_FZGytebekker = VtFrontMapLayer(
		"gytebekker",
		mapVtServer_Ingress,
		"8e7de85f-9826-eb0b-437b-22912db2d3d9",
		true,
	)

	//----------------------------------------------------
	//LØVENSKJOLD FOSSUM
	mapLayer_LFBehandlingsareal = VtFrontMapLayer(
		"Behandlingsareal",
		mapVtServer_Ingress,
		"965e7309-e30c-10fc-9b92-0b487d637405",
		true,
	)
	mapLayer_LFBom = VtFrontMapLayer(
		"Bom",
		mapVtServer_Ingress,
		"44c754b9-c13a-af9c-d27d-fb92aaa432bb",
		true,
	)
	mapLayer_LFBro = VtFrontMapLayer(
		"Bro",
		mapVtServer_Ingress,
		"0f88ad54-6574-6644-a1d7-23224ab3e0a6",
		true,
	)
	mapLayer_LFBronn = VtFrontMapLayer(
		"Bronn",
		mapVtServer_Ingress,
		"884ef2b2-43ac-9a2e-e14b-a62d3cfd0cc8",
		true,
	)
	mapLayer_LFEgneKulturminner = VtFrontMapLayer(
		"Egne_kulturminner",
		mapVtServer_Ingress,
		"c1342dec-694e-fc0d-8657-fae5a34e5740",
		true,
	)
	mapLayer_LFEiendomspolygoner = VtFrontMapLayer(
		"Eiendomspolygoner",
		mapVtServer_Ingress,
		"ff381cba-ae16-beb4-e756-c7b6dd41f0f4",
		true,
	)
	mapLayer_LFFugl = VtFrontMapLayer(
		"Fugl",
		mapVtServer_Ingress,
		"939c1578-00cf-e283-84d3-dfc0a7227ab6",
		true,
	)
	mapLayer_LFGrensemerker = VtFrontMapLayer(
		"Grensemerker",
		mapVtServer_Ingress,
		"cddd95ec-b612-09aa-bf96-54c227661b59",
		true,
	)
	mapLayer_LFGytebekk = VtFrontMapLayer(
		"Gytebekk",
		mapVtServer_Ingress,
		"b3efdb5c-f3cc-a386-d865-cdbe0e1ef2fc",
		true,
	)
	mapLayer_LFKjorespor = VtFrontMapLayer(
		"Kjorespor",
		mapVtServer_Ingress,
		"60aea971-792f-75ab-e34d-f314cbf8684a",
		true,
	)
	mapLayer_LFLivslopstre = VtFrontMapLayer(
		"Livslopstre",
		mapVtServer_Ingress,
		"82cc8083-78b5-df45-603e-afba2567125d",
		true,
	)
	mapLayer_LFNokkelbiotop = VtFrontMapLayer(
		"Nokkelbiotop",
		mapVtServer_Ingress,
		"73877e7e-1d72-5157-93a7-6edd58a75ff6",
		true,
	)
	mapLayer_LFSamferdsel = VtFrontMapLayer(
		"Samferdsel",
		mapVtServer_Ingress,
		"054b9a70-8094-f4d1-865a-49c866c2e53a",
		true,
	)
	mapLayer_LFBygninger = VtFrontMapLayer(
		"Bygninger",
		mapVtServer_Ingress,
		"be8fd4f7-1b5e-f357-6371-1d337ec698d2",
		true,
	)
	mapLayer_LFSkoglinjer = VtFrontMapLayer(
		"Skoglinjer",
		mapVtServer_Ingress,
		"f90a5ad3-5364-fcb9-3cea-f3420ea68be4",
		true,
	)
	mapLayer_LFSkogpunkter = VtFrontMapLayer(
		"Skogpunkter",
		mapVtServer_Ingress,
		"d4383bab-a574-27fd-8f13-13969c888a1b",
		true,
	)
	mapLayer_LFSnuplassVelteplassMM = VtFrontMapLayer(
		"Snuplass_velteplass_mm",
		mapVtServer_Ingress,
		"e3243625-e1a5-f564-54e9-2cef4ca13ceb",
		true,
	)
	mapLayer_LFStikkrenne = VtFrontMapLayer(
		"Stikkrenne",
		mapVtServer_Ingress,
		"4c5bce97-f61b-2c51-5ea1-3deb9c0b1af0",
		true,
	)
	mapLayer_LFVeierBehandling = VtFrontMapLayer(
		"Veier_behandling",
		mapVtServer_Ingress,
		"de1b518b-83ea-4694-a14b-d68bb319990c",
		true,
	)
	mapLayer_LFBestandsflater = VtFrontMapLayer(
		"Bestandsflater",
		mapVtServer_Ingress,
		"3f81478d-bdfa-1214-d25a-a9ba34f5f72a",
		true,
	)

	//----------------------------------------------------
	//CAPELLEN
	mapLayer_CABehandlingsareal = VtFrontMapLayer(
		"Behandlingsareal",
		mapVtServer_Ingress,
		"c0c6015a-781e-6583-23d0-4c77bb950e18",
		true,
	)
	mapLayer_CABrønn = VtFrontMapLayer(
		"Brønn",
		mapVtServer_Ingress,
		"fe9cbb8c-169a-28ec-cda8-81419073effe",
		true,
	)
	mapLayer_CABygninger = VtFrontMapLayer(
		"Bygninger",
		mapVtServer_Ingress,
		"414b994f-6806-c81a-567e-7064faad89cc",
		true,
	)
	mapLayer_CASkoglinjer = VtFrontMapLayer(
		"Skoglinjer",
		mapVtServer_Ingress,
		"3f60b07b-a191-b1b4-f47a-c0997f6ebc6d",
		true,
	)
	mapLayer_CASkogpunkter = VtFrontMapLayer(
		"Skogpunkter",
		mapVtServer_Ingress,
		"4edad926-7c7b-419e-4a91-89fd934a3f4b",
		true,
	)
	mapLayer_CATeiger = VtFrontMapLayer(
		"Teiger",
		mapVtServer_Ingress,
		"a2896023-cd45-b4d0-f769-f5281ba41453",
		true,
	)
	mapLayer_CABestandsflater = VtFrontMapLayer(
		"Bestandsflater",
		mapVtServer_Ingress,
		"00bb7c15-498e-de22-749f-f006e5fe0aa9",
		true,
	)
	mapLayer_MEBestandsflater = VtFrontMapLayer(
		"Bestandsflater",
		mapVtServer_Ingress,
		"d0be7f97-62cd-ba87-b7f7-f72ddd4af7ec",
		true,
	)
)

// ----------------------------------------------
// ----------------------------------------------
// VMTS

//Vector Map Tile Service?

var (
	mapLayerUrl_WmtsIngressDev  = "https://feltgis-testing-ingress-fppskc73cq-lz.a.run.app/wmts/"
	mapLayerUrl_WmtsIngressProd = "https://feltgis-prod-ingress-fppskc73cq-lz.a.run.app/wmts/"
)

func VmtsIngressServerMapLayer(Name string, UrlConfigPart string, LayerId string) MapLayer {

	//LayerId is the layer id passed on to arcgis and used by the map sdk
	//UrlLayerId is added to the url so that the url is unique when passing through our client cache server

	Type := mapLayerType_Wmts
	var DevUrl = mapLayerUrl_WmtsIngressDev
	var ProdUrl = mapLayerUrl_WmtsIngressProd

	//This "part" of the url decides if a layer runs on "generic" config, or common config.
	if len(UrlConfigPart) > 0 {
		DevUrl += UrlConfigPart + "/"
		ProdUrl += UrlConfigPart + "/"
	}
	DevUrl += "wmts/1.0.0/WMTSCapabilities.xml"
	ProdUrl += "wmts/1.0.0/WMTSCapabilities.xml"

	//We add layer ID to urls, so that the urls are unique, a requirement from the cache server!
	DevUrl += "?layerid=" + LayerId
	ProdUrl += "?layerid=" + LayerId

	var DevStatusUrl = ""
	var ProdStatusUrl = ""

	DevBodyReplacements := []ConfigMapLayerBodyReplacement{}
	ProdBodyReplacements := []ConfigMapLayerBodyReplacement{}

	//reroute test to prod //TODO: And we should not do that :-) But but but
	ProdBodyReplacements = append(ProdBodyReplacements,
		ConfigMapLayerBodyReplacement{
			Collection:     "mapcache",
			ContentType:    "",
			UrlContains:    "WMTSCapabilities.xml",
			UrlNotContains: "",
			Search:         "feltgis-testing-ingress-fppskc73cq-lz.a.run.app",
			Replace:        "feltgis-prod-ingress-fppskc73cq-lz.a.run.app",
		},
	)

	//reroute through localhost cache!
	var TilesReplacementUrlPart = ""
	if len(UrlConfigPart) > 0 {
		TilesReplacementUrlPart = UrlConfigPart + "/"
	}
	DevBodyReplacements = append(DevBodyReplacements,
		ConfigMapLayerBodyReplacement{
			Collection:     "mapcache",
			ContentType:    "",
			UrlContains:    "WMTSCapabilities.xml",
			UrlNotContains: "",
			Search:         "https://feltgis-testing-ingress-fppskc73cq-lz.a.run.app/wmts/" + TilesReplacementUrlPart + "wmts/1.0.0/" + LayerId + "/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.xxx",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/feltgis-testing-ingress-fppskc73cq-lz.a.run.app/wmts/" + TilesReplacementUrlPart + "wmts/1.0.0/" + LayerId + "/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.xxx",
		},
	)
	ProdBodyReplacements = append(ProdBodyReplacements,
		ConfigMapLayerBodyReplacement{
			Collection:     "mapcache",
			ContentType:    "",
			UrlContains:    "WMTSCapabilities.xml",
			UrlNotContains: "",
			Search:         "https://feltgis-prod-ingress-fppskc73cq-lz.a.run.app/wmts/" + TilesReplacementUrlPart + "wmts/1.0.0/" + LayerId + "/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.xxx",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/feltgis-prod-ingress-fppskc73cq-lz.a.run.app/wmts/" + TilesReplacementUrlPart + "wmts/1.0.0/" + LayerId + "/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.xxx",
		},
	)

	//handle unknown content type
	BodyReplacements := ConfigMapLayerBodyReplacement{
		Collection:     "mapcache",
		ContentType:    "",
		UrlContains:    "WMTSCapabilities.xml",
		UrlNotContains: "",
		Search:         "image/unknown",
		Replace:        "image/png",
	}
	DevBodyReplacements = append(DevBodyReplacements, BodyReplacements)
	ProdBodyReplacements = append(ProdBodyReplacements, BodyReplacements)

	return MapLayer{
		Name:                 Name,
		Type:                 Type,
		LayerId:              LayerId,
		DevUrl:               DevUrl,
		DevStatusUrl:         DevStatusUrl,
		DevBodyReplacements:  DevBodyReplacements,
		ProdUrl:              ProdUrl,
		ProdStatusUrl:        ProdStatusUrl,
		ProdBodyReplacements: ProdBodyReplacements,
	}
}

var (
	// NIBIO Skadeovervåking -->
	mapLayer_Nibio_SO_Prosjektavgrensing = VmtsIngressServerMapLayer(
		"Prosjektavgrensing",
		"nibio_skadeovervaking",
		"Prosjektavgrensing_cache",
	)
	mapLayer_Nibio_SO_Hogstflate = VmtsIngressServerMapLayer(
		"Hogstflate",
		"nibio_skadeovervaking",
		"Hogstflate_cache",
	)
	mapLayer_Nibio_SO_PBarkbilleskade = VmtsIngressServerMapLayer(
		"Barkbilleskade",
		"nibio_skadeovervaking",
		"Barkbilleskade_cache",
	)
	mapLayer_Nibio_SO_Barkbilleskade_gradert = VmtsIngressServerMapLayer(
		"Barkbilleskade_gradert",
		"nibio_skadeovervaking",
		"Barkbilleskade_gradert_cache",
	)
	mapLayer_Nibio_SO_Oversiktskart = VmtsIngressServerMapLayer(
		"Oversiktskart",
		"nibio_skadeovervaking",
		"Oversiktskart_cache",
	)
	// <-- NIBIO Skadeovervåking

	mapLayer_FZTrevolumRaster = VmtsIngressServerMapLayer(
		"Trevolum (raster)",
		"FZ_Fritzoe",
		"trees_cache",
	)
	mapLayer_FZOrto = VmtsIngressServerMapLayer(
		"Orto",
		"",
		"orto_optimized_cache",
	)
	/*
		https://app.asana.com/0/1207973070215678/1208134421241111/f
		From Fz: Kan også fjerne den grønne terrengmodellen (den heter «terreng»)
		mapLayer_FZTerreng = VmtsIngressServerMapLayer(
			"Terreng",
			"",
			"terrain_green_optimized_cache",
		)
	*/
	mapLayer_FZMarkfuktighetskart = VmtsIngressServerMapLayer(
		"Markfuktighetskart",
		"",
		"wetmap_optimized_cache",
	)
	mapLayer_FZTerrengGrå = VmtsIngressServerMapLayer(
		"Terreng grå",
		"",
		"terrain_grey_optimized_cache",
	)
	mapLayer_FZTrehøyde = VmtsIngressServerMapLayer(
		"Trehøyde",
		"",
		"skogshojd_optimized_cache",
	)
	/*
		https://app.asana.com/0/1207973070215678/1208134421241111/f
		//From Fz: Bruker ikke slope 25-29/20-24 – Holder med 30.
		mapLayer_FZSlope_20_24 = VmtsIngressServerMapLayer(
			"slope_20_24",
			"",
			"slope_20_24_optimized_cache",
		)
		mapLayer_FZSlope_25_29 = VmtsIngressServerMapLayer(
			"slope_25_29",
			"",
			"slope_25_29_optimized_cache",
		)
	*/
	mapLayer_FZSlope_30 = VmtsIngressServerMapLayer(
		"slope_30",
		"",
		"slope_30_optimized_cache",
	)
	mapLayer_FZExternal_gf_drone_ortophoto_cache = VmtsIngressServerMapLayer(
		"External: Drone orto photo",
		"fz_extern_wms",
		"gf_drone_ortophoto_cache",
	)
	mapLayer_FZExternal_gf_feature_lines_cache = VmtsIngressServerMapLayer(
		"External: Feature lines",
		"fz_extern_wms",
		"gf_feature_lines_cache",
	)
	mapLayer_FZExternal_gf_feature_points_cache = VmtsIngressServerMapLayer(
		"External: Feature points",
		"fz_extern_wms",
		"gf_feature_points_cache",
	)
	mapLayer_FZExternal_gf_feature_polygons_cache = VmtsIngressServerMapLayer(
		"External: Feature polygons",
		"fz_extern_wms",
		"gf_feature_polygons_cache",
	)
	mapLayer_FZExternal_gf_tracklog_lines_cache = VmtsIngressServerMapLayer(
		"External: Tracklog lines",
		"fz_extern_wms",
		"gf_tracklog_lines_cache",
	)
	mapLayer_FZData = VmtsIngressServerMapLayer(
		"FZ: Data",
		"fzdata",
		"FZsync_cache",
	)

	mapLayer_CAOrto = VmtsIngressServerMapLayer(
		"Orto",
		"",
		"979200048_raster_CIR_orto_cache",
	)
	mapLayer_CAGYL = VmtsIngressServerMapLayer(
		"GYL",
		"",
		"979200048_raster_GYL_cache",
	)
	mapLayer_CAMarkfuktighet = VmtsIngressServerMapLayer(
		"Markfuktighet",
		"",
		"979200048_raster_Markfuktighet_cache",
	)
	mapLayer_CAMarkstruktur = VmtsIngressServerMapLayer(
		"Markstruktur",
		"",
		"979200048_raster_Markstruktur_cache",
	)
	mapLayer_CA_RGB_orto = VmtsIngressServerMapLayer(
		"RGB_orto",
		"",
		"979200048_raster_RGB_orto_cache",
	)
	mapLayer_CATrehøyde = VmtsIngressServerMapLayer(
		"Trehøyde",
		"",
		"979200048_raster_Skogshojd_cache",
	)
)

//----------------------------------------------
//----------------------------------------------
// CUSTOM MAP LAYERS

var (
	mapLayer_IngressFonts_BodyReplacements = []ConfigMapLayerBodyReplacement{
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "-ingress-fppskc73cq-lz.a.run.app/vtfront",
			UrlNotContains: "tilemap",
			Search:         "https://vector.services.geodataonline.no",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/vector.services.geodataonline.no",
		},
	}
	mapLayer_Hoydekurver_BodyReplacements = []ConfigMapLayerBodyReplacement{
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "VectorTileServer",
			UrlNotContains: "tilemap",
			Search:         "https://vector.services.geodataonline.no",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/vector.services.geodataonline.no",
		},
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "VectorTileServer",
			UrlNotContains: "tilemap",
			Search:         "https://services.geodataonline.no",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/services.geodataonline.no",
		},
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "mapprod.feltgis.no",
			UrlNotContains: "tilemap",
			Search:         "https://vector.services.geodataonline.no",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/vector.services.geodataonline.no",
		},
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "mapprod.feltgis.no",
			UrlNotContains: "tilemap",
			Search:         "https://services.geodataonline.no",
			Replace:        "http://localhost:[CACHEPORT]/mapcache/services.geodataonline.no",
		},
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "?f=json",
			UrlNotContains: "tilemap",
			Search:         "TilesOnly,Tilemap",
			Replace:        "TilesOnly",
		},
		{
			Collection:     "mapcache",
			ContentType:    "application/json",
			UrlContains:    "?f=json",
			UrlNotContains: "tilemap",
			Search:         "\"tileMap\"",
			Replace:        "\"_tileMap\"",
		},
	}
	mapLayer_Hoydekurver = MapLayer{
		Name:                 "Hoydekurver",
		Type:                 mapLayerType_Vector,
		DevUrl:               "https://services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer",
		DevStatusUrl:         "",
		DevBodyReplacements:  mapLayer_Hoydekurver_BodyReplacements,
		ProdUrl:              "https://services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer",
		ProdStatusUrl:        "",
		ProdBodyReplacements: mapLayer_Hoydekurver_BodyReplacements,
		CacheWarmupUrls: []ConfigMapLayerCacheWarmupUrls{
			{
				Collection: "mapcache",
				Urls: []string{
					"https://services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer?f=json",
					"https://services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer/resources/styles?f=json",
					"https://vector.services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer?f=json",
					"https://vector.services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer/resources/sprites/sprite@2x.json?f=json",
					"https://vector.services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer/resources/sprites/sprite@2x.png",
					"https://vector.services.geodataonline.no/arcgis/rest/services/GeocacheVector/GeocacheHoydekurver/VectorTileServer/?f=json",
				},
			},
		},
	}

	mapLayer_Geocachenames = MapLayer{
		Name:    "geocachenames",
		Type:    mapLayerType_Vector,
		DevUrl:  mapLayerUrl_VtFrontIngressDev + "geocachenames",
		ProdUrl: mapLayerUrl_VtFrontIngressProd + "geocachenames",
	}
)
