package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type FormFile struct {
	Path string
	Name string
	Prod bool
}

func NewFormFile(Name string, Prod bool) *FormFile {
	p := new(FormFile)
	p.Path = "layerdefinitions"
	p.Name = Name
	p.Prod = Prod
	return p
}

func (f *FormFile) Filename() string {
	var result = "../"
	if f.Path != "" {
		result += f.Path + "/"
	}
	result += f.Name
	if f.Prod {
		result += ".prod"
	} else {
		result += ".test"
	}
	result += ".json"
	return result
}

func (f *FormFile) ParseForm() *Form {

	// Open our jsonFile
	jsonFile, err := os.Open(f.Filename())
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println("Successfully Opened " + f.Filename())

	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our Users array
	var form Form

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &form)

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	return &form
}

func (f *FormFile) WriteForm(form *Form) {

	// Create our jsonFile
	jsonFile, err := os.Create(f.Filename())
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// write our opened jsonFile as a byte array.
	var jsonString = form.Json()

	//write
	jsonFile.Write([]byte(jsonString))

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
}
