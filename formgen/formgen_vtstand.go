package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

type SqlRow struct {
	DBName string
	Type   string
}

// Funksjon for å parse SQL-tabellstrukturen fra filen
func parseSQLTable(schema string) []SqlRow {
	var fields []SqlRow

	// Regex for å matche kolonnenavn og type
	re := regexp.MustCompile(`(?m)^\s*([a-zA-Z0-9_]+)\s+([a-zA-Z ]+)(?:$begin:math:text$.*$end:math:text$)?,?`)

	matches := re.FindAllStringSubmatch(schema, -1)
	for _, match := range matches {
		if len(match) < 3 {
			continue
		}

		dbName := match[1]
		dbType := match[2]

		// Konvertering av PostgreSQL-typer til formtyper
		var formType string
		switch strings.ToLower(dbType) {
		case "text":
			formType = "String"
		case "bigint", "smallint", "integer":
			formType = "Integer"
		case "double precision":
			formType = "Double"
		case "geometry":
			formType = "Geometry"
		default:
			formType = "Unknown"
		}

		fields = append(fields, SqlRow{dbName, formType})
	}

	return fields
}

// Funksjon for å lese schema fra embed-filen
func getSchemaFromFile(prefix string) (string, error) {
	data, err := os.ReadFile("vtfront/" + prefix + ".pq")
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// Funksjon for å dynamisk generere skjemaet
func PopulateVTStandForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "etikett"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	/* To figure out what field to use as global-id field, run this on the vtfront database
		SELECT distinct on (kcu.table_name)
	   tc.table_name, kcu.column_name, tc.constraint_type
	FROM
	    information_schema.key_column_usage kcu
	JOIN
	    information_schema.table_constraints tc
	    ON kcu.table_schema=tc.constraint_schema and kcu.constraint_name = tc.constraint_name
	WHERE
	    tc.constraint_type in ('UNIQUE', 'PRIMARY KEY')
	    AND
		tc.table_schema = 'fritzoe'
	  --  AND tc.table_name = 'bestand_bestand'
		ORDER BY kcu.table_name, tc.constraint_type
	*/
	b.form.GlobalIdField = "objectid"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "objectid"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("etikett")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	schema, err := getSchemaFromFile(b.company.Prefix)
	if err != nil {
		fmt.Println("Error reading schema file:", err)
		return
	}

	fields := parseSQLTable(schema)

	// Custom fields created by sync event logic below
	fields = append(fields,
		SqlRow{"etikett", "String"},
		SqlRow{"style", "String"},
	)

	// Generate fields dynamically using existing FormBuilder methods.
	for _, field := range fields {
		if field.DBName == b.form.GlobalIdField {
			if strings.HasPrefix(field.DBName, "_") {
				b.AddHiddenStringField(field.DBName, field.DBName)
			} else {
				b.AddUneditableRequiredStringField(field.DBName, field.DBName)
			}
			continue
		}
		switch field.Type {
		case "String":
			if strings.HasPrefix(field.DBName, "_") {
				b.AddHiddenStringField(field.DBName, field.DBName)
			} else {
				b.AddUneditableRequiredStringField(field.DBName, field.DBName)
			}
		case "Integer":
			b.AddUneditableRequiredIntegerField(field.DBName, field.DBName)
		case "Double":
			b.AddUneditableRequiredDoubleField(field.DBName, field.DBName)
		case "Geometry":
			fmt.Printf("Skipping Geometry field: %s\n", field.DBName) // Geometry trenger kanskje spesialhåndtering
		default:
			fmt.Printf("Unknown field type: %s (%s)\n", field.DBName, field.Type)
		}
	}

	// Generate a field ID series based on the field order
	fieldNames := make([]string, len(fields))
	for i, field := range fields {
		fieldNames[i] = field.DBName
	}

	// Fill in the attribute "style", used to determine which styling to use
	b.WithSyncEvent(SyncEventConfig{
		Type:             SyncEventTypeAddAttributeOutgoing,
		SourceCollection: b.form.Name,
		TemplateString:   "{{if eq .driftsomrnavn \"Utført Lukket Hogst\"}}{{.hkl}} {{.driftsomrnavn}}{{else}}{{.hkl}}{{end}}",
		TargetField:      "style",
	})

	// Populate the attribute ‘etikett’, used for adding labels to the map.
	b.WithSyncEvent(SyncEventConfig{
		Type:             SyncEventTypeAddAttributeOutgoing,
		SourceCollection: b.form.Name,
		TemplateString: `{{.presentasj}}
{{.bontresl}}{{.bon}}`,
		TargetField: "etikett",
	})

	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.WithDataSource(&DataSource{
			Type:      DataSourceVTFront,
			Schema:    "fritzoe",
			TableName: "bestand_bestand",
		})
	}

	if b.IsCappelen() {
		b.WithDataSource(&DataSource{
			Type:      DataSourceVTFront,
			Schema:    "cappelen",
			TableName: "bestand_bestand_m_data_2022",
		})
	}

	if b.IsLøvenskioldFossum() {
		b.WithDataSource(&DataSource{
			Type:      DataSourceVTFront,
			Schema:    "l_fossum",
			TableName: "bestandsflater_bestand_f_meddata_29042024",
		})
	}

	if b.IsMathiesenEidsvoldVærk() {
		b.WithDataSource(&DataSource{
			Type:      DataSourceVTFront,
			Schema:    "mev",
			TableName: "bestand_bestand",
		})
	}

	// Renderer
	di := b.AddDrawingInfo()
	di.WithLabelingInfo("etikett").
		WithCenterAlignedSymbol(
			FormDrawingInfoColor(255, 255, 255, 255), //black color
			10,
			FormDrawingInfoColor(0, 0, 0, 127), //white halo color
			1,
		)

	renderer := di.WithRenderer("style")
	renderer.WithDefaultSymbol(
		FormDrawingInfoColor(0, 0, 0, 0),
	)

	for _, add := range []string{"", " Utført Lukket Hogst"} {
		// Defining the outline color and width based on whether it is "Utført Lukket Hogst"
		outlineColor := FormDrawingInfoColor(38, 115, 0, 255) // Default green outline
		strokeWidth := float32(2.0)

		style := ""
		var dotLayer []CIMSymbolLayer
		if add == " Utført Lukket Hogst" {
			outlineColor = FormDrawingInfoColor(255, 192, 203, 255) // Pink outline
			strokeWidth = 4.0                                       // Thicker stroke
			style = "esriSFSDiagonalCross"
			dotLayer = append(dotLayer, FormDrawingVectorDotsLayer())
		}

		renderer.WithUniqueValueInfo("uprod", "uprod").
			WithStringValue("0"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(0, 0, 0, 0),
				1,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(0, 0, 0, 0),
				1,
				dotLayer...,
			)

		renderer.WithUniqueValueInfo("hkl 1"+add, "hkl 1"+add).
			WithStringValue("1"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(255, 255, 255, 255),
				outlineColor,
				strokeWidth,
				style,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(255, 255, 255, 255),
				outlineColor,
				1,
				dotLayer...,
			)

		renderer.WithUniqueValueInfo("hkl 2"+add, "hkl 2"+add).
			WithStringValue("2"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 197, 255, 255),
				outlineColor,
				strokeWidth,
				style,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(0, 197, 255, 255),
				outlineColor,
				1,
				dotLayer...,
			)

		renderer.WithUniqueValueInfo("hkl 3"+add, "hkl 3"+add).
			WithStringValue("3"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(209, 255, 115, 255),
				outlineColor,
				strokeWidth,
				style,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(209, 255, 115, 255),
				outlineColor,
				1,
				dotLayer...,
			)

		renderer.WithUniqueValueInfo("hkl 4"+add, "hkl 4"+add).
			WithStringValue("4"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(255, 210, 0, 255),
				outlineColor,
				strokeWidth,
				style,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(255, 210, 0, 255),
				outlineColor,
				1,
				dotLayer...,
			)

		renderer.WithUniqueValueInfo("hkl 5"+add, "hkl 5"+add).
			WithStringValue("5"+add).
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(230, 0, 0, 255),
				outlineColor,
				strokeWidth,
				style,
			).
			WithCimOutlinedFillSymbol(
				FormDrawingInfoColor(230, 0, 0, 255),
				outlineColor,
				1,
				dotLayer...,
			)
	}

	b.SetFieldIdSeries(1, fieldNames...)
}
