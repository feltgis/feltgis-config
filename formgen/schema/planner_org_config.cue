adminDefinition: {
	superusers: [...string]
}

environmentClearedEmailsDefinition: {
	subject: string
	to: [...string]
	cc: [...{name: string, email: string}]
	bcc: [...{name: string, email: string}]
	template: string
}

featureSwitch: {
	enabled: bool
	feature: string
	username: string
}

newFeatureDefault: {
	collection: string
	values: {
		...
	}
}

newFeatureCopyOperation: {
	source: string
	target: string
	copyFields: {
		...
	}
}

addFeature: {
	collection: string
	displayname: string
}

bodyReplacement: {
	collection: string
	contenttype: string
	replace: string
	search: string
	urlcontains: string
	urlnotcontains: string
}

mapLayer: {
	name: string
	alias: string
	layerid: string
	defaultopacity: float | int
	minopacity: float | int
	type: string
	preview: string
	url: string
	statusUrl: string
	hidefromlayerlist: bool
	minscale: int
	minLevel: int
	identifyDisabled: bool
	keepInBackgroundMap: bool
	bodyreplacements: [...bodyReplacement]
	rendering: {
		includeFields: [...string]
	}
	cachewarmupurls: [...{
		collection: string
		urls: [...string]
	}]
}

mapDefinition: {
	addFeatures: [...addFeature]
	background: [...{
		name: string
		type: string
		url: string
		hidden: bool
	}]
	freeHandPointDistance: int
	layers: [...mapLayer]
}

purchaseOrder: {
	alias: string
	head: {
		BuyerAdress: string
		BuyerCity: string
		BuyerContactEMail: string
		BuyerContactEMailCopyMeasuringReceipt: string
		BuyerContactName: string
		BuyerContactPhone: string
		BuyerCountryCode: string
		BuyerCountryName: string
		BuyerDepartmentName: string
		BuyerDepartmentNum: int
		BuyerEMail: string
		BuyerName: string
		BuyerOrgNum: string
		BuyerPartyVSYSName: string
		BuyerPartyVSYSNum: int
		BuyerZipCode: string
		SellerAddress: string
		SellerCity: string
		SellerCountryCode: string
		SellerCountryName: string
		SellerDepartmentName: string
		SellerDepartmentNum: int
		SellerName: string
		SellerOrgNum: string
		SellerPartyVSYSName: string
		SellerPartyVSYSNum: int
		SellerZipCode: string
	}
}

issuingInstructionDefinition: {
	alias: string
	issuingInstructionNum: int
	mPartyNum: int
	settlementMMetNum: int
	trAdmNum: int
	transportAccountableNum: int
}

searchDefinition: [...{
	buttonText: string
	autocompleteField: string
	fields: [...{
		title: string
		alias: string
		type: string
	}]
	symbol: string
	title: string
	type: string
	url: string
	maxAutoCompleteResults: int
	resultTitleTemplate: string
	resultSubTitleTemplate: string
}]

serverQuery: {
	name: string
	returnFields: [...string]
}

syncDefinition: {
	bucket: string
	intervalMinutes: int
	orgPrefix: string
	collections: [...{
		name: string
		alias: string
		appFeature: string
		batchSize: int
		disablePush: bool
		disablePull: bool
		attachmentsBucket: string
		indexFields: [...string]
		ftsFields: [...string]
		initialDump: string
		syncByLocalGlobalIds: bool
		syncFilter: {
			syncEverything: bool
		}
		children: [...{
			collectionName: string
			referenceField: string
		}]
	}]
}

#PlannerOrgConfig: {
	name: string
	admin: adminDefinition
	environmentClearedEmails: environmentClearedEmailsDefinition
	customConfig: _
	featureSwitches: [...featureSwitch]
	newFeatureDefaults: [...newFeatureDefault]
	newFeatureCopyOperations: [...newFeatureCopyOperation]
	map: mapDefinition
	purchaseOrderPresets: {
		issuingInstruction: [...issuingInstructionDefinition]
		defaultOrders: [...purchaseOrder]
	}
	genericReports: {
		template: string
	}
	reports: [...{
		collection: string
		template: string
		languages: [...string]
	}]
	search: searchDefinition
	serverQueries: [...serverQuery]
	sync: syncDefinition
	fillInSellerEmail: bool
	sellerContactEMailCopyMeasuringReceipt: bool
}