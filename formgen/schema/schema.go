package schema

import (
	"cuelang.org/go/cue"
	"cuelang.org/go/cue/cuecontext"
	"cuelang.org/go/encoding/json"
	_ "embed"
	"fmt"
)

//go:embed planner_org_config.cue
var plannerOrgConfigCue []byte

type Validator interface {
	Validate(fileName string, inputFileBytes []byte) error
}

type PlannerValidator struct {
	ctx            *cue.Context
	schemaDocument cue.Value
	orgConfig      cue.Value
}

func NewPlannerValidator() (*PlannerValidator, error) {
	ctx := cuecontext.New()
	schemaDocument := ctx.CompileBytes(plannerOrgConfigCue)
	if err := schemaDocument.Err(); err != nil {
		return nil, fmt.Errorf("failed to compile schema: %w", err)
	}
	orgConfig := schemaDocument.LookupPath(cue.ParsePath("#PlannerOrgConfig"))
	if err := orgConfig.Err(); err != nil {
		return nil, fmt.Errorf("failed to lookup OrgConfig: %w", err)
	}

	return &PlannerValidator{
		ctx:            ctx,
		schemaDocument: schemaDocument,
		orgConfig:      orgConfig,
	}, nil
}

func (v *PlannerValidator) Validate(fileName string, inputFileBytes []byte) error {
	jsonFile, err := json.Extract(fileName, inputFileBytes)
	if err != nil {
		return fmt.Errorf("failed to extract json: %w", err)
	}

	jsonAsCue := v.ctx.BuildExpr(jsonFile)
	if err := jsonAsCue.Validate(); err != nil {
		return fmt.Errorf("failed to validate json: %w", err)
	}

	unified := v.orgConfig.Unify(jsonAsCue)
	if err := unified.Validate(); err != nil {
		return fmt.Errorf("failed to validate unified: %w", err)
	}

	if err := unified.Err(); err != nil {
		return fmt.Errorf("error from unified: %w", err)
	}

	return nil
}
