package main

func PopulatePOrdreForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.GeometryType = "esriGeometryPoint"
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_headId"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 500000,
		XMin: 0,
		YMax: 8000000,
		YMin: 5000000,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GlobalIdField = "_headId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("null")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	b.AddDrawingInfo().WithEmptyRenderer()

	//FIELDS
	b.AddHiddenStringField("_headId", "GlobalId")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_value", "Type")
	b.AddHiddenStringField("_orderId", "ContractorOrderId")
	b.AddHiddenStringField("_serviceOrderId", "Service Order Id")

	//FIELD IDS
	b.SetFieldIdSeries(
		0,
		"_headId",
		"_modified_Date",
		"_modifiedBy",
		"_value",
		"_orderId",
		"_serviceOrderId",
	)

	if b.IsNortømmer() {

		b.WithSyncEvent(SyncEventConfig{
			Type:             SyncEventTypeCopyIncoming,
			SourceCollection: b.company.Prefix + "POrdre", // Implicit and could be removed (?)
			TargetCollection: b.company.Prefix + "POrdreRegulatedOriginEmail",
			Filter: &SyncEventFilter{
				Key: "_value",
				// json.Unmarshal parses orderNum as float64
				JMESPath:  "additionalFields.regulatedOrigin && head.OrderNum > `350206.0`",
				BoolValue: true,
			},
		})
	}

	b.WithAutoSortex()
}
