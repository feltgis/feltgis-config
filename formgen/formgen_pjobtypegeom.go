package main

type geomFormTypeType string

const (
	geomFormTypeForhandsrydding  geomFormTypeType = "Forhåndsrydding"
	geomFormTypeGrofterensk      geomFormTypeType = "Grøfterensk"
	geomFormTypeMarkberedning    geomFormTypeType = "Markberedning"
	geomFormTypePlanting         geomFormTypeType = "Planting"
	geomFormTypeSaaing           geomFormTypeType = "Såing"
	geomFormTypeUngskogpleie     geomFormTypeType = "Ungskogpleie"
	geomFormTypeSkogskade        geomFormTypeType = "Skogskade"
	geomFormTypeInfrastruktur    geomFormTypeType = "Infrastruktur"
	geomFormTypeGjodsling        geomFormTypeType = "Gjødsling"
	geomFormTypeSproyting        geomFormTypeType = "Sprøyting"
	geomFormTypeGravearbeid      geomFormTypeType = "Gravearbeid"
	geomFormTypeSporskaderetting geomFormTypeType = "Sporskaderetting"
	geomFormTypeStammekvisting   geomFormTypeType = "Stammekvisting"
)

func PopulatePJobTypeGeomForm(b *FormBuilder) {

	if b.IsGlommenMjosen() {
		PopulatePJobTypeGeomExtGmForm(b)
		return
	}

	//find form type
	formType := findFormType(b)

	//common...
	populatePJobTypeGeomFormCommon(b, formType)
	populatePJobTypeGeomFormDrawingInfo(b, formType)
	populatePJobTypeGeomFormTypes(b, formType)

	populatePJobTypeGeomFormHiddenFields(b, formType)

	populatePJobTypeGeomFormInfoSection(b, formType)

	populatePJobTypeGeomFormGeneralSection(b, formType)

	populatePJobTypeGeomFormØnsketTreslagsfordelingSection(b, formType)

	populatePJobTypeGeomFormArbeidsinstruksSection(b, formType)

	populatePJobTypeGeomFormMiljoSection(b, formType)

	if formType == geomFormTypePlanting {
		PopulatePlantTypeSections(b, PlantTypeSectionsSource_Planting)
	}

	populatePJobTypeGeomFormForestOwnerCommentSection(b, formType)

	b.WithAutoSortex()
}
