package main

import (
	"encoding/json"
	"net/http"
	"os"
	"path"
	"strings"
	"testing"
	"time"
)

func TestAllUrlsValid_FeltLoggAndFeltPlan(t *testing.T) {
	doTestUrlsValid(t, "feltlogg-")
	doTestUrlsValid(t, "planner-")
}

func doTestUrlsValid(t *testing.T, prefix string) {
	dir := ".."
	files, err := os.ReadDir(dir)
	if err != nil {
		t.Fatalf("Error reading directory %s: %v", dir, err)
	}
	urls := map[string]bool{}
	for _, f := range files {
		// Skip this file as it is truncated to stop old apps, and will fail
		if f.Name() == "feltlogg-310.json" {
			continue
		}
		if !f.IsDir() && strings.HasPrefix(f.Name(), prefix) && strings.HasSuffix(f.Name(), ".json") {
			bb, err := os.ReadFile(path.Join(dir, f.Name()))
			if err != nil {
				t.Fatalf("Error reading %s: %v", f.Name(), err)
			}
			var config Config
			err = json.Unmarshal(bb, &config)
			if err != nil {
				t.Fatalf("Error unmarshalling %s: %v", f.Name(), err)
			}
			for _, l := range config.Map.Layers {
				if l.Url != "" {
					urls[l.Url] = true
				}
			}
		}
	}
	c := http.Client{Timeout: 10 * time.Second}
	for url := range urls {
		resp, err := c.Get(url + "?f=json")
		if err != nil {
			t.Errorf("Error fetching %s: %v", url, err)
		}
		if resp.StatusCode == http.StatusUnauthorized {
			continue
		}
		if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusMultipleChoices {
			t.Errorf("Error fetching %s: %v", url, resp.Status)
		}
	}
}
