package main

func PopulatePJobAssignmentForm(b *FormBuilder) {
	b.form.Alias = strRef("Oppdragstilknytning")
	b.form.AllowGeometryUpdates = true
	b.form.GeometryType = "esriGeometryPoint"
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "workTeamOrgName"
	b.form.EditFieldsInfo = nil
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 1000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_globalId", "GUID")

	b.AddHiddenStringField("_parentFieldName", "Name of root object field name (_globalId)")
	b.AddHiddenStringField("_parentCollection", "Name of root object collection name (XXPSkogkultur)")
	b.AddHiddenStringField("_subCollection", "Name of sub/action-collection (XXPPlantingGeom)")
	b.AddHiddenStringField("_parentGlobalId", "The global id of the referenced object (GUID)")
	b.AddHiddenStringField("_referenceFieldName", "The field name of the reference field child->parent (_parentGlobalId)")
	b.AddHiddenStringField("_serviceOrderId", "If this is a contractor order, we also need the service order id")
	b.AddHiddenStringField("_headId", "If this is a purchase order, we also need the PO order id")
	b.AddHiddenStringField("_orderId", "If this is a contractor order, this is the contractor order id")

	b.AddSection("Oppdragtaker")
	b.AddHiddenStringField("_workTeamOrgNum", "Entreprenenørorganisasjonsnummer")
	b.AddRequiredStringField("workTeamOrgName", "Entreprenenørorganisasjon")
	b.AddRequiredStringField("teamName", "Lagnavn")

	b.AddSection("Oppdrag")
	b.AddUneditableRequiredIntegerField("jobNumber", "Tiltaksnummer")
	b.AddRequiredStringField("jobName", "Tiltaksnavn")
	b.AddNullableStringField("jobType", "Tiltakstype")
	b.AddNullableStringField("ownershipMark", "Tømmermerke")
	b.AddNullableStringField("parentJobNumber", "Innkjøpsordrenummer/tilknyttet oppdragsnummer")

	b.AddSection("Oppdragsgiver")
	b.AddRequiredStringField("plannerOrgNum", "OrgNum")
	b.AddRequiredStringField("plannerOrgName", "OrgName")

	b.SetFieldIdSeries(1,
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_globalId",
		"_parentFieldName",
		"_parentCollection",
		"_subCollection",
		"_parentGlobalId",
		"_referenceFieldName",
		"_workTeamOrgNum",
		"workTeamOrgName",
		"plannerOrgNum",
		"plannerOrgName",
		"jobNumber",
		"jobName",
		"_serviceOrderId",
		"teamName",
		"_headId",
		"jobType",
		"ownershipMark",
		"parentJobNumber",
		"_orderId",
	)

	b.WithAutoSortex()
}
