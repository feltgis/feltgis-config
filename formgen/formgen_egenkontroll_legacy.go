package main

import (
	"log"
	"strings"
)

func PopulateEgenkontrollLegacyForm(b *FormBuilder) {

	//TYPE
	if strings.Contains(b.file.Name, "WorkReport") {
		log.Fatalf("Egenkontroll for Alma should not be created here %v", b.file.Name)
	}
	if strings.Contains(b.file.Name, "Hogst") {
		log.Fatalf("Egenkontroll for Hogst should not be created here %v", b.file.Name)
	}
	if strings.Contains(b.file.Name, "Lass") {
		log.Fatalf("Egenkontroll for Lass should not be created here %v", b.file.Name)
	}

	//DEVIATION FIELD NAME
	var deviationFieldHidden = false //TODO: Probably just messy?
	if b.company.isCompany(company_Cappelen, company_FritzøeSkoger, company_Fritzøe, company_LøvenskioldFossum) {
		deviationFieldHidden = true
	} else if b.company.isCompany(company_Fjordtømmer, company_Nortømmer, company_StoraEnso) {
		deviationFieldHidden = true
	}

	var deviationBoolFiledName = "dev_Boolean"
	if deviationFieldHidden {
		deviationBoolFiledName = "_dev_Boolean"
	}

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = deviationBoolFiledName

	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 32633,
			Wkid:       32633,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_orderId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef(deviationBoolFiledName)
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	renderer :=
		b.AddDrawingInfo().
			WithTransparency(0).
			WithRenderer(deviationBoolFiledName)
	renderer.WithUniqueValueInfo("", "Avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgpJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIRozCxR1IntORZmrhOL033nlM0R8eXN75n98958A9Qf6zgj6xgFKq+4BIcjs0hSH0HZam4WtG68fGmJmagVrrtmFjzvWJNG+FWMAVK8Dya2N678HsTbgAzPkCU3B0xJjRbthpu6weQu3Q2gotcYie13rcGPPKCkwolRwTGeuCeNVB/AHXnYLOoONMpOHIaqVuYGhQ5GItMLeOi3R8hquX4bAH2A9D/bD7b2ClSjkEeya1ThhjZsrANuhphIgt6QuQBWLAekt8F0T7HGfwOoyUgRtgow2WARKAAF3AIwu0HoiJRMHV8jposAHflmAAz4FZoMPii5Tyy8AiFG3AZtdZAZtsJqAABQ/QUSqHyBrjXmAa+AC0Y5/LLyAHOQ9wQeRdHlrCFeYAsK/0VVMWijl44AHOw5lncDAFjT65Vk0p9fGSyH0PcAIWtil1JysyunmlsJr0En6IyDgrnXufXlrk9G2IJ6FnSw2wN7CcUerakMiT1X9rlsMJ2H8XJt/DQCeEK2cKYIApMHmtzx4z5pY7Zl1fA5B+Clcewo0G2BGBpjoI/YSlRfiWhxd5GD5pTL4yt+qCTcEnoNenY6v8NvY/6Tfiz5iwRteyOAAAAABJRU5ErkJggg==",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(1)
	renderer.WithUniqueValueInfo("", "Ingen avvik registrert").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgZJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIJorCxR1IntORZmrh2Lyn9z2m6A8XLvd/zo9zDtwT5j8rHOCFlFLdsk/SbKWJKBG+s8A0X3VOPzTGzNQM1Fq3mWFzRvqkmc0kCLnMEovmpenlDrNc5xzwKRiY4bAZMaN0s91adz0R2mmllRaSxPVZPW6MeWEFqpRKy5iM0UXSbw4ucB0n6HTCzgRZDi1X6gZGZFDO1wRzSY5KBx+5zEUOeoH9DNHPzr+BVSqFA+zSkzpljJmpAtvooZGYNekLkAcSwFqLv4O40+cMcpWRKnAd662wHJACBOgCHlig9SAJiYO75TU0WIGvKzCAp8As0GGJiy3lV4FlylZgs+uugA3WKChR8gCVowrypxSXdgPTwDugHayD+QUUKHiAMidvKNJCdEVwCNhTOX7KU6bAPQ+Qz5ziCfvJ0BiQapWaUu/lgtz1AieYU1vULcnLKBs9vzdYz/khIuMsNe79epKVk9wkSZoeNtUAe8WiyqkrMiSPlp9WL4dj7OU2k7xlgE6iq2YKYIApjC7q0+aIueG27PtwgCyPucR9rtHANmI0UUeEnywwzzeKPKPIsDluiitT/Rdshg9Ar6/vo6CN/U/6DbwMmLDH65GJAAAAAElFTkSuQmCC",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		).
		WithIntValue(0)

	//FIELDS
	b.AddHiddenStringField("_orderId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.SetFieldIdSeries(0,
		"_orderId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)

	b.AddHiddenStringField("_orderNum", "Ordrenummer")
	b.SetFieldIdSeries(
		5,
		"_orderNum",
	)

	if b.company.isCompany(company_Fjordtømmer) {

	} else {
		b.AddRequiredIntegerField("volume", "1: Avvirket volum (m3)")
		b.AddRequiredBooleanField("tracks_Boolean", "2: Er det oppstått kjørespor med behov for oppretting?")
		b.AddNullableBooleanField("marked_Boolean", "3: Hvis det er oppstått kjørespor, er sted for oppretting avmerket på kartet?")
		b.AddRequiredBooleanField("tree_Boolean", "4: Er livsløpstre kartfestet?")
		b.AddRequiredBooleanField("asplanned_Boolean", "5: Er arbeidene gjennomført i samsvar med hogstinstruks, produkt- og miljøkrav?")
		b.AddRequiredBooleanField("pefc_Boolean", "6: Er arbeidene gjennomført i samsvar med Norsk PEFC Skogstandard?")

		b.SetFieldIdSeries(
			6,
			"volume",
			"tracks_Boolean",
			"marked_Boolean",
			"tree_Boolean",
			"asplanned_Boolean",
			"pefc_Boolean",
		)
	}

	if b.company.isCompany(company_Fjordtømmer) {

	} else {
		b.AddNullableStringField("comments", "9: Merknad. Hvis avvik på noen av de foregående punktene, angi begrunnelse:").
			WithLength(3000)
		b.AddRequiredStringField("sign", "10: Maskinførers signatur").
			WithLength(3000)
		b.SetFieldIdSeries(
			12,
			"comments",
			"sign",
		)
	}

	if deviationFieldHidden {
		//TODO: MOVE!
		b.AddHiddenBooleanField(deviationBoolFiledName, "Avvik registrert")
		b.SetFieldIdSeries(
			14,
			deviationBoolFiledName,
		)
	} else {
		b.AddUneditableNullableBooleanField(deviationBoolFiledName, "Avvik registrert")
		b.SetFieldIdSeries(
			14,
			deviationBoolFiledName,
		)

	}

	if b.company.isCompany(company_Fjordtømmer) {

	} else {
		//TODO: MOVE UP!
		b.AddNullableIntegerField("padding", "7: Kantsone – hva er gjennomsnittlig bredde på gjensatt kantsone?")
		b.AddNullableIntegerField("trackdamage", "8: Sporskader – hvor mange meter sporskader trenger oppretting?")

		b.SetFieldIdSeries(
			15,
			"padding",
			"trackdamage",
		)
	}

	b.AddHiddenStringField("_headId", "IO Head Id")
	b.AddHiddenStringField("_serviceOrderId", "Service Order Id")

	b.SetFieldIdSeries(
		17,
		"_headId",
		"_serviceOrderId",
	)

	if b.company.isCompany(company_Fjordtømmer) {
		b.AddRequiredDateField("before_Date", "Dato oppstart")
		b.AddRequiredBooleanField("digging_Boolean", "Graving")
		b.AddRequiredBooleanField("skiltplan_Boolean", "Skiltplan")
		b.AddMultipleChoiceField("forest_type", "Skogtype").
			WithCodedOption("Kulturskog").
			WithCodedOption("Naturskog")
		b.AddMultipleChoiceField("hogstform", "Hogstform").
			WithCodedOption("Flate").
			WithCodedOption("Frøtrestilling").
			WithCodedOption("Lukka hogst")
		b.AddMultipleChoiceField("nokkelbiotop", "Nøkkelbiotop").
			WithCodedOption("Godkjent").
			WithCodedOption("Førevar skjema").
			WithCodedOption("Ikkje aktuelt")
		b.AddRequiredBooleanField("hogstmoden_Boolean", "Er skogen hogstmoden?")
		b.AddRequiredStringField("hogstmoden_reason", "Årsak til at skogen ikke er hogstmoden").
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(0),
					OtherFieldName:     "hogstmoden_Boolean",
				},
			)

		b.SetFieldIdSeries(
			20,
			"before_Date", //20
			"digging_Boolean",
			"skiltplan_Boolean",
			"forest_type",
			"hogstform",
			"nokkelbiotop", //25
			"hogstmoden_Boolean",
			"hogstmoden_reason",
		)
	}

	if b.company.isCompany(company_Fjordtømmer) {
		b.AddSection("Miljøelement")

		b.AddRequiredBooleanField("kulturminne_Boolean", "Kulturminne")
		b.AddMultipleChoiceField("kulturminne_comment", "Kulturminne: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "kulturminne_Boolean",
				},
			)

		b.AddRequiredBooleanField("naturtyper_Boolean", "Viktige naturtyper")
		b.AddMultipleChoiceField("naturtyper_comment", "Viktige naturtyper: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "naturtyper_Boolean",
				},
			)

		b.AddRequiredBooleanField("truaartar_Boolean", "Trua artar")
		b.AddMultipleChoiceField("truaartar_comment", "Trua artar: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "truaartar_Boolean",
				},
			)

		b.AddRequiredBooleanField("nokkelbiotop_Boolean", "Nøkkelbiotopar")
		b.AddMultipleChoiceField("nokkelbiotop_comment", "Nøkkelbiotopar: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "nokkelbiotop_Boolean",
				},
			)

		b.AddRequiredBooleanField("rovfugl_Boolean", "Rovfugl/ugler/tiurleik")
		b.AddMultipleChoiceField("rovfugl_comment", "Rovfugl/ugler/tiurleik: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "rovfugl_Boolean",
				},
			)

		b.AddRequiredBooleanField("friluft_Boolean", "Friluftsanlegg/stiar")
		b.AddMultipleChoiceField("friluft_comment", "Friluftsanlegg/stiar: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "friluft_Boolean",
				},
			)

		b.AddRequiredBooleanField("turomr_Boolean", "Turområde/bustadfelt")
		b.AddMultipleChoiceField("turomr_comment", "Turområde/bustadfelt: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "turomr_Boolean",
				},
			)

		b.AddRequiredBooleanField("kantsone_Boolean", "Kantsone mot vann")
		b.AddMultipleChoiceField("kantsone_comment", "Kantsone mot vann: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "kantsone_Boolean",
				},
			)

		b.AddRequiredBooleanField("drikke_Boolean", "Drikkevasskjelde")
		b.AddMultipleChoiceField("drikke_comment", "Drikkevasskjelde: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "drikke_Boolean",
				},
			)

		b.AddRequiredBooleanField("folsom_Boolean", "Følsomme vassmiljø")
		b.AddMultipleChoiceField("folsom_comment", "Følsomme vassmiljø: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "folsom_Boolean",
				},
			)

		b.AddRequiredBooleanField("torre_Boolean", "Tørre/grove leger")
		b.AddMultipleChoiceField("torre_comment", "Tørre/grove leger: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "torre_Boolean",
				},
			)

		b.AddRequiredBooleanField("avrenning_Boolean", "Avrenning")

		b.AddRequiredBooleanField("anna_Boolean", "Anna")
		b.AddMultipleChoiceField("anna_comment", "Anna: Avvik skjedd?").
			WithSuggestedPreferredNoOption().
			WithLogic().
			WithVisibleIfField(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldIntValue: intRef(1),
					OtherFieldName:     "anna_Boolean",
				},
			)

		b.SetFieldIdSeries(
			30,
			"kulturminne_Boolean",
			"kulturminne_comment",
			"naturtyper_Boolean",
			"naturtyper_comment",
			"truaartar_Boolean",
			"truaartar_comment",
			"nokkelbiotop_Boolean",
			"nokkelbiotop_comment",
			"rovfugl_Boolean",
			"rovfugl_comment",
			"friluft_Boolean",
			"friluft_comment",
			"turomr_Boolean",
			"turomr_comment",
			"kantsone_Boolean",
			"kantsone_comment",
			"drikke_Boolean",
			"drikke_comment",
			"folsom_Boolean",
			"folsom_comment",
			"torre_Boolean",
			"torre_comment",
			"avrenning_Boolean",
		)
		b.SetFieldIdSeries(
			54,
			"anna_Boolean",
			"anna_comment",
		)
	}

	if b.company.isCompany(company_Fjordtømmer) {
		b.AddSection("Avrenning") //Avrenning

		isAvrenningLogic := FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "avrenning_Boolean",
		}

		b.AddRequiredBooleanField("utavspor_Boolean", "Leia vatn ut av spor").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredBooleanField("fylltpaa_Boolean", "Fyllt på med bar på blaute parti").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredBooleanField("lagabruer_Boolean", "Laga bruer").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredBooleanField("endratrasse_Boolean", "Endra utkøyringstrasse").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredBooleanField("avventahogst_Boolean", "Avventa hogst").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredBooleanField("avbraatenhogst_Boolean", "Avbråten hogst").
			WithLogic().WithVisibleIfField(isAvrenningLogic)
		b.AddRequiredStringField("avrenningarsak_comment", "Årsak til avrenning").
			WithLogic().WithVisibleIfField(isAvrenningLogic)

		b.SetFieldIdSeries(
			56,
			"utavspor_Boolean",
			"fylltpaa_Boolean",
			"lagabruer_Boolean",
			"endratrasse_Boolean",
			"avventahogst_Boolean", //60
			"avbraatenhogst_Boolean",
			"avrenningarsak_comment",
		)
	}

	if b.company.isCompany(company_Fjordtømmer) {
		b.AddSection("")

		b.AddRequiredIntegerField("ant_llt", "Antall livsløpstre")
		b.AddRequiredBooleanField("sporskader_Boolean", "Sporskader")

		isSporskaderLogic := FormFieldLogicField{
			Operator:           "==",
			OtherFieldIntValue: intRef(1),
			OtherFieldName:     "sporskader_Boolean",
		}
		b.AddRequiredStringField("sporskader_responsible", "Sporskader, ansvar for utbedring").
			WithLogic().WithVisibleIfField(isSporskaderLogic)
		b.AddRequiredDateField("sporskaderutbedring_Date", "Sporskader, frist for utbedring").
			WithLogic().WithVisibleIfField(isSporskaderLogic)
		b.SetFieldIdSeries(
			63,
			"ant_llt",
			"sporskader_Boolean",
			"sporskader_responsible",
			"sporskaderutbedring_Date",
		)
	}

	if b.company.isCompany(company_Fjordtømmer) {
		b.AddSection("")

		b.AddRequiredDateField("etterhogst_Date", "Dato avsluttet")
		b.AddRequiredStringField("sign", "Maskinførers signatur").
			WithLength(3000)

		b.SetFieldIdSeries(
			67,
			"etterhogst_Date",
			"sign",
		)
	}

	//TYPES
	b.form.Types = []FormType{
		{
			Domains:   FormTypeDomains{},
			Id:        "Egenkontroll",
			Name:      "Egenkontroll",
			Templates: []FormTypeTemplate{},
		},
	}

	b.WithAutoSortex()

	//LEGACY FIELD ORDER TODO: REMOVE
}
