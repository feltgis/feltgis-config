package main

import (
	"fmt"
	"strings"
)

func PopulateFeltloggConfig(b *ConfigBuilder) {
	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//------------------------------------------------------------------------------------
	//common
	//------------------------------------------------------------------------------------
	if b.IsAllma() {
		b.config.Map.PrioritizedExtentLayer = prefix + "Action"
	}
	b.config.Map.Sections = []ConfigSection{
		{
			Id:    1,
			Title: "Stammedata fra produksjonsfil",
		},
	}
	var nameSuffix = ""
	if b.IsProd() {
		nameSuffix = " prod"
	} else if b.IsDev() {
		nameSuffix = " dev"
	} else if b.IsBeta() {
		nameSuffix = " beta"
	}

	if b.company != nil {
		b.config.Name = b.company.Name + nameSuffix
		b.config.OrgNum = b.company.OrgNum
	}

	if b.IsGlommenMjosen() {
		b.config.ProducedMessagesMustHaveCreatedBy = "vhhangfire@skogdata.no"
	} else if b.IsGlommenSkog() {
		b.config.ProducedMessagesMustHaveCreatedBy = "vhhangfire@skogdata.no"
	} else if b.IsDefault() {
		b.config.Name = "Default" + nameSuffix
	}

	//------------------------------------------------------------------------------------
	//customConfigObjects
	//------------------------------------------------------------------------------------
	if b.IsAllma() {
		b.AddCustomConfigObjectsCountOperation(ConfigCustomConfigObjectsCountOperation{
			CountField:       "Count",
			CountType:        "count",
			SearchCollection: prefix + "HenPoint",
			SearchFieldName:  "Type",
			SearchFieldValue: "Livsløpstrær",
			TargetCollection: prefix + "WorkReport",
			WriteToField:     "RetentionTreesAccountedForAmount",
		})
		b.AddCustomConfigObjectsCountOperation(ConfigCustomConfigObjectsCountOperation{
			CountType:        "length",
			SearchCollection: prefix + "HenLine",
			SearchFieldName:  "Type",
			SearchFieldValue: "Sporskader",
			TargetCollection: prefix + "WorkReport",
			WriteToField:     "TrackDamageAccountedForMeters",
		})
		b.AddCustomConfigObjectsUpdateReaction(ConfigCustomConfigObjectsUpdateReaction{
			ReactionCollection: prefix + "WorkReport",
			SearchCollection:   prefix + "HenPoint",
			SearchKey:          "Type",
			SearchValue:        "Livsløpstrær",
		})
		b.AddCustomConfigObjectsUpdateReaction(ConfigCustomConfigObjectsUpdateReaction{
			ReactionCollection: prefix + "WorkReport",
			SearchCollection:   prefix + "HenLine",
			SearchKey:          "Type",
			SearchValue:        "Sporskader",
		})
	}

	if b.IsGlommenMjosen() {
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("kristoffer@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("alte@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("andreas@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("eh@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("eh@mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ops@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("oab@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("lars@holth-skogsdrift.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("jorgen@holth-skogsdrift.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ahwintervold@gmail.com")
	} else if b.IsGlommenSkog() {
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("admin@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("eh@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("kristoffer@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("alte@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ops@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("oab@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ths@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ah@glommen-mjosen.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("lars@holth-skogsdrift.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("jorgen@holth-skogsdrift.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ern@glommen.no")
	} else if b.IsNortømmer() {
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("kristoffer@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("alte@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("ms@nortommer.no")
	} else if b.IsFjordtømmer() {
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("kristoffer@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("alte@feltgis.no")
		b.AddCustomConfigUserNotRequiredToSignWorkOrder("info@feltgis.no")
	}

	//------------------------------------------------------------------------------------
	//settingsSwitches
	//------------------------------------------------------------------------------------
	b.AddSetting(
		"Stammedata",
		"Del posisjon av hogstaggregat når treet felles",
		"StemType:StemCoordinates",
		false,
	)
	b.AddSetting(
		"Tvangskapp",
		"Del tvangskapp og årsak til dette",
		"CuttingCategoryType:CuttingReason",
		false,
	)

	//------------------------------------------------------------------------------------
	//enabledfeatures
	//------------------------------------------------------------------------------------
	b.AddEnabledFeature("prodForw")
	if b.IsStatSkog() {
		//no more
	} else if b.IsNortømmer() {
		b.AddEnabledFeature("attachments")
		b.AddEnabledFeature("keyFigures")
		b.AddEnabledFeature("removeFromList")
		b.AddEnabledFeature("deviations")
		b.AddEnabledFeature("measuringtickets")
	} else if b.IsForestOwner() || b.IsAllma() || b.IsFjordtømmer() || b.IsStoraEnso() {
		b.AddEnabledFeature("attachments")
		b.AddEnabledFeature("keyFigures")
		b.AddEnabledFeature("removeFromList")
		b.AddEnabledFeature("map")
		b.AddEnabledFeature("prepareOffline")
		b.AddEnabledFeature("deviations")
		b.AddEnabledFeature("measuringtickets")
	} else if b.IsDefault() {
		b.AddEnabledFeature("attachments")
		b.AddEnabledFeature("keyFigures")
		b.AddEnabledFeature("removeFromList")
	}

	//------------------------------------------------------------------------------------
	//vsysFeatures
	//------------------------------------------------------------------------------------
	b.AddVsysFeatureForAllUsers("prodForw")
	if b.IsDefault() {
		//none
	} else if b.IsStatSkog() {
		//none
	} else {
		b.AddVsysFeatureForAllUsers("autoCopyFilesFromBox")
		b.AddVsysFeatureForAllUsers("everythingProdForw")
		b.AddVsysFeatureForAllUsers("productLines")
		b.AddVsysFeatureForAllUsers("attachments")
		b.AddVsysFeatureForAllUsers("keyFigures")
		b.AddVsysFeatureForAllUsers("removeFromList")
		b.AddVsysFeatureForAllUsers("measuringtickets")
		b.AddVsysFeatureForAllUsers("wreckanddowngrade")
		b.AddVsysFeatureForAllUsers("map")
		b.AddVsysFeatureForAllUsers("prepareOffline")
		b.AddVsysFeatureForAllUsers("incidents")
		b.AddVsysFeatureForAllUsers("Stems")
		b.AddVsysFeatureForAllUsers("AssortmentStems")
		b.AddVsysFeatureForAllUsers("workOrder")
		b.AddVsysFeatureForAllUsers("deviations")
		if b.IsAllma() {
			b.AddVsysFeatureForAllUsers("dataProcessorAgreementEnabled")
		}
	}

	//------------------------------------------------------------------------------------
	//map.addFeatures
	//------------------------------------------------------------------------------------

	if b.IsDefault() {
		//no addFeature
	} else if b.IsStatSkog() {
		//no addFeature
	} else if b.IsAllma() {
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "ContractorPoint", Displayname: "Private entreprenørdata"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "HenPoint", Displayname: "Registreringer punkt"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "HenLine", Displayname: "Registreringer linjer"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "HenPolygon", Displayname: "Registreringer flater"})
	} else {
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "ContractorPoint", Displayname: "Private entreprenørdata"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerPunkt", Displayname: "Registreringer punkt"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerLinjer", Displayname: "Registreringer linjer"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerFlater", Displayname: "Registreringer flater"})
	}

	//------------------------------------------------------------------------------------
	//map.editFeatures
	//------------------------------------------------------------------------------------
	if !b.IsDefault() {
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "ContractorPoint", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "authoredOnly"})
	}
	if b.IsDefault() {
		//no editFeatures
	} else if b.IsStatSkog() {
		//no editFeatures
	} else if b.IsAllma() {
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "HenPoint", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "HenLine", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "HenPolygon", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "WorkReport", DeleteMode: "disabled", EditMode: "everybody", GeometryEditing: "disabled"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Incident", DeleteMode: "disabled", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Sporlogg", DeleteMode: "disabled", EditMode: "disabled", GeometryEditing: "disabled"})
	} else {
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerPunkt", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerLinjer", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerFlater", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})

		if b.IsFjordtømmer() {
			b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Egenkontroll", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "disabled"})
		} else {

			// TODO: Consider opening this up for everybody, since it may be multiple people working on the same report. Testing with NT first
			canEditEgenkontroll := "authoredOnly"
			if b.IsNortømmer() || b.IsFjordtømmer() {
				canEditEgenkontroll = "everybody"
			}

			b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Egenkontroll", DeleteMode: "authoredOnly", EditMode: canEditEgenkontroll, GeometryEditing: "disabled"})
			b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "EgenkontrollHogst", DeleteMode: "authoredOnly", EditMode: canEditEgenkontroll, GeometryEditing: "disabled"})
			b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "EgenkontrollLass", DeleteMode: "authoredOnly", EditMode: canEditEgenkontroll, GeometryEditing: "disabled"})
		}

		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Incident", DeleteMode: "disabled", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "Sporlogg", DeleteMode: "disabled", EditMode: "disabled", GeometryEditing: "disabled"})
	}

	//------------------------------------------------------------------------------------
	//map.background
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//none
	} else if b.IsForestOwner() {
		b.AddRasterMapBackground("Kart", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
		b.AddRasterMapBackground("Foto", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
	} else {
		b.AddRasterMapBackground("Foto", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
		b.AddRasterMapBackground("Kart", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
	}

	//------------------------------------------------------------------------------------
	//map.layers
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//no map layers
	} else {
		b.AddMapLayer(mapLayer_FGfugl).
			WithAlias("Fugl hensyn").
			SetHiddenFromLayerList()
		if b.IsStoraEnso() || b.IsNortømmer() || b.IsFjordtømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Skyggerelieff).
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("skyggerelieff.png")
		}

		if b.IsStoraEnso() || b.IsAllma() {
			b.AddMapLayer(mapLayer_Aldersklasseriskog).
				WithDefaultOpacity(0.0).
				WithPreview("Aldersklasseriskog.png")
		}

		if b.IsFjordtømmer() || b.IsNortømmer() {
			b.AddMapLayer(mapLayer_BrattTerreng).
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("bratterreng.png")
		}

		if b.IsAllma() {
			b.AddCollectionMapLayer("Stand", mapLayerType_Polygon).
				WithAlias("Allma bestand").
				WithDefaultOpacity(0.3).
				WithRendererIncludedFields("_label", "_serviceOrderId").
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithPreview("AllmaBestand.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NTBestand).
				WithAlias("Bestand (NT)").
				WithDefaultOpacity(0.5).
				WithPreview("ntbestand.png")
			b.AddMapLayer(mapLayer_NTBestandslinjer).
				WithAlias("Bestandslinjer (NT)").
				WithDefaultOpacity(1.0).
				WithIdentityDisabled().
				WithPreview("ntbestandslinjer.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_LivslopstrarNorgeNortømmer).
				WithAlias("Livsløpstrær Norge").
				WithDefaultOpacity(0.5).
				WithPreview("LLTNorgeNT.png")
		}
		if b.IsGlommen() {
			b.AddMapLayer(mapLayer_LivslopstrarNorgeGlommen).
				WithAlias("Livsløpstrær Norge").
				WithDefaultOpacity(0.5).
				WithPreview("LLTNorgeGM.png")
		}

		if b.IsNortømmer() || b.IsForestOwner() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Misc).
				WithAlias("MiS (tidlig)").
				WithDefaultOpacity(0.8).
				WithMinScale(20000)
		}

		if b.IsFjordtømmer() || b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_SR16FG).
				WithAlias("SR16").
				WithDefaultOpacity(0.0).
				WithPreview("sr16.png")
			b.AddMapLayer(mapLayer_Aldersklasseriskog).
				WithDefaultOpacity(0.0).
				WithPreview("Aldersklasseriskog.png")
		}

		if b.IsAllma() {
			b.AddMapLayer(mapLayer_Mis).
				WithAlias("MiS").
				WithMinOpacity(0.5).
				WithPreview("mis.png")
			b.AddCollectionMapLayer("MisPolygon", mapLayerType_Polygon).
				WithAlias("MiS Polygon").
				WithMinOpacity(0.5).
				WithPreview("mis.png")
		}

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			// They have their own wet map
		} else if b.IsAllma() {
			b.AddMapLayer(mapLayer_GMMarkfuktighetskart_Helelandet).
				WithIdentityDisabled().
				WithDefaultOpacity(0.35).
				WithPreview("markfuktighetskart.png")
		} else {
			b.AddMapLayer(mapLayer_Markfuktighetskart).
				WithIdentityDisabled().
				WithDefaultOpacity(0.35).
				WithPreview("markfuktighetskart.png")
		}

		if b.IsForestOwner() || b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NaturtyperHb13AlleAB).
				WithAlias("Naturtyper HB13").
				WithDefaultOpacity(1.0).
				WithPreview("naturtyperhb13.png")
		} else {
			b.AddMapLayer(mapLayer_NaturtyperHb13NaturtyperHb13Alle).
				WithAlias("Naturtyper HB13").
				WithDefaultOpacity(1.0).
				WithPreview("naturtyperhb13.png")
		}

		if b.IsAllma() {
			//not
		} else {
			b.AddMapLayer(mapLayer_Narin).
				WithAlias("Narin")
			b.AddMapLayer(mapLayer_NaturtyperUnfiltered).
				WithAlias("NiN")
			b.AddMapLayer(mapLayer_FriluftslivStatligSikra).
				WithAlias("Friluftsliv")
		}

		if b.IsNortømmer() || b.IsStoraEnso() {
			b.AddMapLayer(mapLayer_Markagrensen).
				WithAlias("Markagrensen").
				WithPreview("markagrensen.png")
		}

		b.AddMapLayer(mapLayer_VernNaturvernOmrade).
			WithAlias("Vern").
			WithPreview("vern.png")
		b.AddMapLayer(mapLayer_Vernskog).
			WithAlias("Vernskog").
			WithPreview("vernskog.png")

		if b.IsAllma() {
			b.AddMapLayer(mapLayer_GMNaturtyper_NiN).
				WithAlias("Naturtype NiN").
				WithMinOpacity(0.5).
				WithPreview("gmnaturtypernin.png")
			b.AddMapLayer(mapLayer_GMNokkelbiotoper).
				WithAlias("Nøkkelbiotop").
				WithMinOpacity(0.5).
				WithPreview("gmnokkelbiotop.png")
		}

		if b.IsForestOwner() || b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Artsdata).
				WithAlias("Artsdata NIBIO").
				WithDefaultOpacity(1.0).
				WithMinOpacity(1.0)
		} else if b.IsFjordtømmer() {
			//only difference is opacity
			b.AddMapLayer(mapLayer_Artsdata).
				WithAlias("Artsdata NIBIO").
				WithDefaultOpacity(0.0)
		}

		if b.IsAllma() {
			//not
		} else {
			b.AddMapLayer(mapLayer_Arter).
				WithAlias("Arter")
		}

		if b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_TiurleikerStjørdal).
				WithAlias("Tiurleiker Stjørdal")
		}
		if b.IsNortømmer() || b.IsStatSkog() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_Skredkvikkleire2Kvikkleirefaregrad).
				WithAlias("Kvikkleirefaregrad").
				WithPreview("kvikkleirefaregrad.png")
		}
		if b.IsNortømmer() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_JordOgFlomskred).
				WithAlias("Jord og flomskred").
				WithPreview("jordogflom.png")
		}

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZMarkfuktighetskart).
				WithAlias("Markfuktighetskart").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZTerrengGrå).
				WithAlias("Terreng grå").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZTrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZSlope_30).
				WithMinLevel(10).
				WithAlias("slope_30").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
		}

		if b.IsCappelen() {
			b.AddMapLayer(mapLayer_CAOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAGYL).
				WithAlias("GYL").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkfuktighet).
				WithAlias("Markfuktighet").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkstruktur).
				WithAlias("Markstruktur").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CA_RGB_orto).
				WithAlias("RGB_orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CATrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
		}

		if b.IsFjordtømmer() || b.IsNortømmer() || b.IsForestOwner() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Reguleringsplaner).
				WithAlias("Reguleringsplaner").
				WithDefaultOpacity(0.0).
				WithPreview("reguleringsplaner.png")
			b.AddMapLayer(mapLayer_Teig).
				WithAlias("Eiendom").
				WithIdentityDisabled().
				WithPreview("teig.png")
			b.AddMapLayer(mapLayer_Grensepunkter).
				WithAlias("Grensepunkter").
				WithIdentityDisabled().
				WithPreview("grensepunkter.png")
			b.AddMapLayer(mapLayer_Andre).
				WithAlias("Andre hensyn").
				WithPreview("andre.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NTMiljo).
				WithAlias("Miljø (NT)").
				WithPreview("ntmiljo.png")
		}
		if b.IsNortømmer() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_NTMis).
				WithAlias("NTmis").
				WithPreview("ntmis.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_Mis2).
				WithAlias("MiS").
				WithPreview("mis.png")
		} else if !b.IsAllma() {
			b.AddMapLayer(mapLayer_Mis).
				WithAlias("MiS").
				WithPreview("mis.png")
		}

		if b.IsFjordtømmer() {
			b.AddMapLayer(mapLayer_FTMis).
				WithPreview("ftmis.png")
		}

		/* b.AddMapLayer(mapLayer_KulturminnerWMS).
		WithPreview("ra.png").
		WithMinOpacity(0.3)*/

		b.AddMapLayer(mapLayer_Kulturminner).
			WithAlias("Kulturminner").
			WithPreview("ra.png").
			WithMinOpacity(0.5)

		b.AddMapLayer(mapLayer_Hoydekurver).
			WithAlias("Høydekurver").
			WithIdentityDisabled().
			WithDefaultOpacity(0.6).
			WithPreview("hoydekurver.png")

		if b.IsStatSkog() {
			//none
		} else if b.IsAllma() {
			//none
		} else {
			layer := b.AddCollectionMapLayer("PTommerdrift", mapLayerType_Polygon).
				WithAlias("Driftsområde").
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithRendererIncludedFields("_serviceOrderId").
				WithMinOpacity(0.3)
			if b.IsFritzøe() || b.IsFritzøeSkoger() {
				layer.WithPreview("fzdriftsomraade.png")
			}
			if b.IsMathiesen() {
				layer.WithPreview("mevdriftsomraade.png")
			}
		}

		if b.IsAllma() {
			b.AddCollectionMapLayer("Action", mapLayerType_Polygon).
				WithAlias("Tiltakspolygon").
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithRendererIncludedFields("_serviceOrderId").
				WithPreview("gmaction.png")
		}

		//---------
		//FLATER
		if b.IsMathiesen() {
			b.AddMapLayer(mapLayer_MELinnea).
				WithAlias("Linnea").
				WithPreview("mevlinnea.png")
		}
		if b.IsAllma() {
			b.AddCollectionMapLayer("HenPolygon", mapLayerType_Polygon).
				WithAlias("Hensynsflate").
				WithPreview("gmhenpolygon.png").
				WithRendererIncludedFields("Description", "_serviceOrderId").
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithMinOpacity(0.5)
		} else if b.IsStatSkog() {
			//none
		} else {
			b.AddCollectionMapLayer("MiljoFlater", mapLayerType_Polygon).
				WithKeepInBackgroundMap()
			b.AddCollectionMapLayer("RegistreringerFlater", mapLayerType_Polygon).
				WithAlias("Registreringer flater").
				WithRendererIncludedFields("comment").
				WithPreview("registreringerflater.png").
				WithMinOpacity(0.5)
		}

		//---------
		//LINJER
		if b.IsStatSkog() {
			//none
		} else {
			b.AddCollectionMapLayer("Sporlogg", mapLayerType_Line).
				WithAlias("Sporlogg").
				WithIdentityDisabled().
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithRendererIncludedFields("_serviceOrderId").
				WithPreview("gmsporlogg.png")
		}

		if b.IsAllma() {
			b.AddCollectionMapLayer("HenLine", mapLayerType_Line).
				WithAlias("Hensynslinje").
				WithPreview("gmhenline.png")
		} else if b.IsStatSkog() {
			//none
		} else {
			b.AddCollectionMapLayer("MiljoLinjer", mapLayerType_Line).
				WithKeepInBackgroundMap()
			b.AddCollectionMapLayer("RegistreringerLinjer", mapLayerType_Line).
				WithAlias("Registreringer linjer").
				WithRendererIncludedFields("comment").
				WithPreview("registreringerlinjer.png")
		}

		//Eiendomsgrenser må være øverst!
		if b.IsStoraEnso() || b.IsGlommen() {
			b.AddMapLayer(mapLayer_Teig).
				WithPreview("matrikkel.png")
		}

		//---------
		//PUNKTER
		if b.IsAllma() {
			b.AddCollectionMapLayer("HenPoint", mapLayerType_Point).
				WithAlias("Hensynspunkt").
				WithPreview("gmhenpoint.png").
				WithRendererIncludedFields("Description", "_serviceOrderId").
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'")
		} else if b.IsStatSkog() {
			//none
		} else {
			b.AddCollectionMapLayer("MiljoPunkt", mapLayerType_Point).
				WithKeepInBackgroundMap()
			b.AddCollectionMapLayer("RegistreringerPunkt", mapLayerType_Point).
				WithAlias("Registreringer punkt").
				WithRendererIncludedFields("comment").
				WithPreview("registreringerpunkt.png")
		}

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZTeig)
			b.AddMapLayer(mapLayer_FZBestandsflaterMedEtiketter).
				WithDefaultOpacity(0.0).
				WithPreview("fzbestandsflater.png")
			b.AddMapLayer(mapLayer_FZBestandSkogtyper).
				WithAlias("Bestand skogtyper").
				WithPreview("fzbestandskogtyper.png")
			b.AddMapLayer(mapLayer_FZGytebekker).
				WithAlias("Gytebekker").
				SetHiddenFromLayerList()
			b.AddMapLayer(mapLayer_FZMiljø).
				SetHiddenFromLayerList()
			b.AddMapLayer(mapLayer_FZInfrastruktur)
		} else if b.IsLøvenskioldFossum() {
			b.AddMapLayer(mapLayer_LFBestandsflater).
				WithDefaultOpacity(0.5)
		} else if b.IsCappelen() {
			b.AddMapLayer(mapLayer_CABestandsflater).
				WithAlias("Bestandsflater").
				WithDefaultOpacity(0.5).
				WithPreview("cabestand.png")
		} else if b.IsMathiesen() {
			b.AddMapLayer(mapLayer_MEBestandsflater).
				WithAlias("Bestandsflater").
				WithDefaultOpacity(0.5).
				WithPreview("cabestand.png")
		}

		if b.IsAllma() {
			b.AddCollectionMapLayer("MisPoint", mapLayerType_Point).
				WithAlias("MiS Punkt")
		}

		if b.IsStatSkog() {
			//none
		} else {
			b.AddCollectionMapLayer("Stem", mapLayerType_Point).
				WithAlias("Stammepunkt").
				WithDefaultOpacity(0.3).
				WithIdentityDisabled().
				WithRendererDefinitionExpression("_serviceOrderId='{{_serviceOrderId}}'").
				WithRendererIncludedFields("_serviceOrderId").
				WithPreview("stammepunkt.png")

			if b.company.hasFeature(hasVsys) {
				// When VSYS enabled, these data are backed by vsys and not synced to FG.
				b.AddPrefixedCollectionMapLayer("NT", "Skogdata", mapLayerType_Point).
					WithPreview("ntskogdata.png")
			} else {
				// When VSYS disabled, these data are backed by FG and synced to FG.
				b.AddPrefixedCollectionMapLayer(prefix, "Skogdata", mapLayerType_Point).
					WithPreview("ntskogdata.png")
			}

			b.AddMapLayer(mapLayer_Geocachenames).
				WithAlias("Stedsnavn").
				WithDefaultOpacity(0.8).
				WithIdentityDisabled().
				WithPreview("stedsnavn.png")
		}
	}

	//------------------------------------------------------------------------------------
	//hiddenFromLayerList
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//ignore
	} else {

		if b.IsFjordtømmer() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"kulturminner",
				prefix+"MiljoFlater",
				prefix+"RegistreringerFlater",
				prefix+"Sporlogg",
				prefix+"MiljoLinjer",
				prefix+"RegistreringerLinjer",
				prefix+"MiljoPunkt",
				prefix+"RegistreringerPunkt",
				"NTSkogdata",
			)
		}

		if b.IsForestOwner() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_alle_a_b",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"teig",
				"Andre",
				"NTmis",
				"mis",
				prefix+"MiljoFlater",
				prefix+"RegistreringerFlater",
				prefix+"MiljoLinjer",
				prefix+"RegistreringerLinjer",
				prefix+"MiljoPunkt",
				prefix+"RegistreringerPunkt",
				"NTSkogdata",
			)
		}

		if b.IsAllma() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"vern_naturvern_omrade",
				"vernskog",
				"kulturminner",
				prefix+"MisPoint",
				"NTSkogdata",
			)
		}

		if b.IsNortømmer() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_alle_a_b",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"markagrensen",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"tiurleiker_stjørdal",
				"teig",
				"Andre",
				"NTmis",
				"mis2",
				"ntmiljo",
				"NTSkogdata",
				prefix+"PTommerdrift",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		if b.IsStatSkog() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_naturtyper_hb13_alle",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"tiurleiker_stjørdal",
				"teig",
				"Andre",
				"mis",
				"kulturminner",
			)
		}

		if b.IsStoraEnso() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"arter",
				"mis",
				"kulturminner",
				"NTSkogdata",
				prefix+"PTommerdrift",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		//DEBUG:
		if false {
			fmt.Println("<---- hidden debug")
			for _, layer := range b.config.Map.Layers {
				if layer.Hidefromlayerlist != nil && *layer.Hidefromlayerlist == true {
					fmt.Printf("%s\n", layer.Name)
				}
			}
			fmt.Println("hidden debug ---->")
		}
	}

	//------------------------------------------------------------------------------------
	//reports
	//------------------------------------------------------------------------------------
	var reportFolder = "REPORT_FOLDER_NOT_SET"
	if b.company != nil {
		reportFolder = b.company.ReportFolder
	} else if b.IsDefault() {
		reportFolder = "default"
	}

	var reportLanguages []string = nil //translations

	var reportBitBucketUrl = "https://bitbucket.org/feltgis/feltgis-config/raw/master/"
	reportUrl := reportBitBucketUrl + "rapporter/" + reportFolder

	b.AddGenericReportTemplates(
		reportUrl + "/generic.html",
	)
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-forhandsrydding-en", reportUrl+"/staticHtml/arbeidsinstruks-forhandsrydding-en.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-forhandsrydding", reportUrl+"/staticHtml/arbeidsinstruks-forhandsrydding.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-markberedning", reportUrl+"/staticHtml/arbeidsinstruks-markberedning.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-planting-en", reportUrl+"/staticHtml/arbeidsinstruks-planting-en.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-planting", reportUrl+"/staticHtml/arbeidsinstruks-planting.html")
	}

	if b.IsNortømmer() || b.IsForestOwner() || b.IsFjordtømmer() {
		b.AddReportTemplate(prefix+"Egenkontroll", reportUrl+"/egenkontroll.html", reportLanguages)
	}

	if b.IsNortømmer() || b.IsForestOwner() || b.IsFjordtømmer() || b.IsStoraEnso() {
		b.AddReportTemplate(prefix+"EgenkontrollLass", reportUrl+"/egenkontroll-lass.html", reportLanguages)
		b.AddReportTemplate(prefix+"EgenkontrollHogst", reportUrl+"/egenkontroll-hogst.html", reportLanguages)
	}

	b.VerifyReportTemplates(true)

	//------------------------------------------------------------------------------------
	//proximityAlarms
	//------------------------------------------------------------------------------------
	if b.IsGlommen() {
		b.WithGradientProximityAlarmOutsideCollection(
			"Driftsområde",
			prefix+"Action",
			10,
			true,
		)

	} else {
		b.WithGradientProximityAlarmOutsideCollection(
			"Driftsområde",
			prefix+"PTommerdrift",
			10,
			true,
		)
	}
	if b.IsGlommen() {
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynsflate",
			prefix+"HenPolygon",
			"Type",
			nil, // include all
			10,
		)
		b.WithGradientProximityAlarmInsideWhere(
			"HensynsPunkt",
			prefix+"HenPoint",
			"Type",
			[]string{"Livsløpstrær", "Artsreg (Rødliste,Henyn mm)", "Kulturminne", "Brønn", "Reir - Ørn", "Reir - Hønsehauk"},
			10,
		)
		//TODO(kristoffer): Remove port
		b.WithGradientProximityAlarmInsideArcGIS(
			"Naturtyper NiN",
			"http://35.228.152.51:8000/GM_Nokkelbiotoper/MapServer/identify?geometryType=esriGeometryEnvelope&sr=25833&layers=*&tolerance=0&returnGeometry=true",
			10,
		)

	}
	if b.IsNortømmer() {
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynspunkt",
			prefix+"RegistreringerPunkt",
			"_tema",
			[]string{"Reirtre", "Kulturminner", "Storfuglleik", "Balplass", "Ikke kryss", "Ledning", "Jordkabel", "Brønn", "Annen fare"},
			10,
		)
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynsflate",
			prefix+"RegistreringerFlater",
			"_tema",
			nil, // include all
			10,
		)
		b.WithGradientProximityAlarmInsideVector(
			"MiS",
			"https://vtfront.feltgismaps.com/geometries?TYPENAME=nt_new:nortommer_nokkelbiotop&FORMAT=esrijson&ATTRIBUTEFLDS=skjotsel",
			10,
		)
	}

	//------------------------------------------------------------------------------------
	//serverQueries
	//------------------------------------------------------------------------------------
	b.AddServerQuery(
		"matrikkel",
		[]string{
			"eiernavn",
			"bruksnavn",
			"matrikkeltype",
			"fylkesnavn",
			"kommunenavn",
			"kommunenr",
			"gardsnr",
			"bruksnr",
			"lagretareal",
			"eierdatofra",
		},
	)

	//------------------------------------------------------------------------------------
	//sync
	//------------------------------------------------------------------------------------
	b.config.Sync.IntervalMinutes = intRef(2)
	var attachmentBucketSuffix string
	if b.IsProd() {
		attachmentBucketSuffix = ""
	} else {
		attachmentBucketSuffix = "-test"
	}

	if b.IsDefault() {
		//no sync
	} else if b.IsStatSkog() {
		b.AddSyncCollection(prefix + "Session").
			WithAlias("Sesjonsdata").
			WithAppFeature("Session").
			WithBatchSize(100).
			WithAttachmentsBucket(strings.ToLower(prefix) + "session" + attachmentBucketSuffix).
			WithDisablePull(true)
	} else {
		b.AddSyncCollection(prefix + "Session").
			WithAlias("Sesjonsdata").
			WithAppFeature("Session").
			WithBatchSize(100).
			WithAttachmentsBucket(strings.ToLower(prefix) + "session" + attachmentBucketSuffix).
			WithDisablePull(true)

		b.AddSyncCollection(prefix+"Stem").
			WithAlias("Stammedata fra produksjonsfil").
			WithAppFeature("Stems").
			WithBatchSize(2000).
			WithIndexFields("_orderId", "_serviceOrderId").
			WithSyncFilterUseServiceOrderId()

		b.AddSyncCollection(prefix+"Incident").
			WithAlias("Hendelser").
			WithAppFeature("Incidents").
			WithAttachmentFilenamesField("_filenames").
			WithAttachmentsBucket(strings.ToLower(prefix)+"incident"+attachmentBucketSuffix).
			WithBatchSize(100).
			WithIndexFields("_orderId", "_serviceOrderId").
			WithSyncFilterUseServiceOrderId()

		if b.IsAllma() {
			b.AddSyncCollection(prefix+"Action").
				WithAlias("Tiltakspolygon").
				WithAppFeature("Tømmerdrift").
				WithBatchSize(100).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithSyncFilterUseServiceOrderId().
				WithDisablePush(true)
			b.AddSyncCollection(prefix+"Stand").
				WithAlias("Allma bestand").
				WithBatchSize(100).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithSyncFilterUseServiceOrderId().
				WithDisablePush(true)
			b.AddSyncCollection(prefix+"HenPoint").
				WithAlias("Hensynspunkt").
				WithBatchSize(100).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithSyncFilterUseServiceOrderId()
			b.AddSyncCollection(prefix+"HenLine").
				WithAlias("Hensynslinje").
				WithBatchSize(100).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithSyncFilterUseServiceOrderId()
			b.AddSyncCollection(prefix+"HenPolygon").
				WithAlias("Hensynsflate").
				WithBatchSize(100).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithSyncFilterUseServiceOrderId()
			b.AddSyncCollection(prefix + "MisPoint").
				WithAlias("MiS Punkt").
				WithBatchSize(100).
				WithIndexFields("_orderId").
				WithDisablePush(true)
			b.AddSyncCollection(prefix + "MisPolygon").
				WithAlias("MiS Polygon").
				WithBatchSize(100).
				WithIndexFields("_orderId").
				WithDisablePush(true)
		}

		if !b.IsAllma() {
			b.AddSyncCollection(prefix + "MiljoPunkt").
				WithSyncFilterSyncEverything()
			b.AddSyncCollection(prefix + "MiljoLinjer").
				WithSyncFilterSyncEverything()
			b.AddSyncCollection(prefix + "MiljoFlater").
				WithSyncFilterSyncEverything()
			b.AddSyncCollection(prefix+"RegistreringerPunkt").
				WithBatchSize(1000).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithRpi().
				WithSyncFilterUseServiceOrderId()
			b.AddSyncCollection(prefix+"RegistreringerLinjer").
				WithBatchSize(1000).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithRpi().
				WithSyncFilterUseServiceOrderId()
			b.AddSyncCollection(prefix+"RegistreringerFlater").
				WithBatchSize(1000).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithRpi().
				WithSyncFilterUseServiceOrderId()
		}

		sporlogg := b.AddSyncCollection(prefix+"Sporlogg").
			WithAlias("Sporlogg").
			WithAppFeature("Sporlogg").
			WithBatchSize(1000).
			WithIndexFields("_orderId", "_serviceOrderId").
			WithSyncFilterUseServiceOrderId()

		if b.IsAllma() {
		} else {
			sporlogg.WithSkipWaitOnAdd()
		}

		//SINGLE
		if b.IsFjordtømmer() {
			b.AddSyncCollection(prefix+"Egenkontroll").
				WithAppFeature("Egenkontroll").
				WithBatchSize(10).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithAttachmentsBucket(strings.ToLower(prefix) + "egenkontroll" + attachmentBucketSuffix)
		}

		//LEGACY
		if b.IsForestOwner() || b.IsNortømmer() {
			b.AddSyncCollection(prefix+"Egenkontroll").
				WithAppFeature("Egenkontroll").
				WithBatchSize(10).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithAttachmentsBucket(strings.ToLower(prefix) + "egenkontroll" + attachmentBucketSuffix)
		}

		//LASS + HOGST
		if b.IsForestOwner() || b.IsNortømmer() || b.IsStoraEnso() || b.IsContractor() {
			b.AddSyncCollection(prefix+"EgenkontrollLass").
				WithAppFeature("EgenkontrollLass").
				WithBatchSize(10).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithAttachmentsBucket(strings.ToLower(prefix) + "egenkontroll-lass" + attachmentBucketSuffix)
			b.AddSyncCollection(prefix+"EgenkontrollHogst").
				WithAppFeature("EgenkontrollHogst").
				WithBatchSize(10).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithAttachmentsBucket(strings.ToLower(prefix) + "egenkontroll-hogst" + attachmentBucketSuffix)
		}

		if b.company.hasFeature(hasVsys) {
			// When VSYS enabled, these data are backed by vsys and not synced to FG.
			b.AddSyncCollection("NT" + "Skogdata").
				WithIndexFields("_orderId").
				WithDisablePull(true).
				WithDisablePush(true).
				WithAppFeature("SupplyPoints").
				WithRpi()
		} else {
			// When VSYS disabled, these data are backed by FG and synced to FG.
			b.AddSyncCollection(prefix+"Skogdata").
				WithIndexFields("_orderId", "_serviceOrderId", "_headId").
				WithDisablePull(false).
				WithDisablePush(true).
				WithAppFeature("SupplyPoints").
				WithSyncFilterUseServiceOrderId()
		}

		if !b.IsAllma() {
			b.AddSyncCollection(prefix+"PTommerdrift").
				WithAppFeature("Tømmerdrift").
				WithBatchSize(10).
				WithDisablePush(true).
				WithIndexFields("_orderId", "_serviceOrderId").
				WithRpi().
				WithSyncFilterUseServiceOrderId()
		}

		b.AddSyncCollection(prefix + "WorkOrderAcceptance").
			WithAppFeature("work order acceptance").
			WithIndexFields("_orderId").
			WithDisablePull(true)

		if b.IsAllma() {
			b.AddSyncCollection(prefix + "WorkReport").
				WithAlias("Arbeidsrapport").
				WithAppFeature("Egenkontroll").
				WithBatchSize(10).
				WithIndexFields("_orderId").
				WithAttachmentsBucket(strings.ToLower(prefix) + "egenkontroll" + attachmentBucketSuffix)
		}

		b.AddSyncCollection(prefix+"ContractorPoint").
			WithAlias("Entreprenørdata").
			WithAppFeature("ContractorPoints").
			WithBatchSize(1000).
			WithIndexFields("_orderId", "_serviceOrderId", "_visibilityMode").
			WithSyncFilterUseServiceOrderId()
	}
	if !b.company.hasFeature(hasVsys) {
		b.AddSyncCollection("FGPJobAssignment").
			WithAppFeature("JobAssignment").
			WithAlias("Tilknytta oppdrag").
			WithIndexFields("_serviceOrderId", "_orderId", "_workTeamOrgNum").
			WithDisableMap()
	}
	
	b.VerifySync()
}
