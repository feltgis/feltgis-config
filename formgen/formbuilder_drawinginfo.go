package main

type FormDrawingInfoBuilder struct {
	drawingInfo *FormDrawingInfo
}

func (b *FormBuilder) AddDrawingInfo() *FormDrawingInfoBuilder {
	b.form.DrawingInfo = &FormDrawingInfo{}
	return &FormDrawingInfoBuilder{
		drawingInfo: b.form.DrawingInfo,
	}
}

func (b *FormDrawingInfoBuilder) WithTransparency(transparency int) *FormDrawingInfoBuilder {
	b.drawingInfo.Transparency = &transparency
	return b
}

type FormDrawingInfoLabelingInfoBuilder struct {
	labelingInfo *FormDrawingInfoLabelingInfo
}

func (b *FormDrawingInfoBuilder) WithLabelingInfo(labelExpression string) *FormDrawingInfoLabelingInfoBuilder {
	if b.drawingInfo.LabelingInfo == nil {
		labelingInfo := []*FormDrawingInfoLabelingInfo{}
		b.drawingInfo.LabelingInfo = labelingInfo
	}
	labelingInfo := FormDrawingInfoLabelingInfo{
		LabelExpression: "[" + labelExpression + "]",
		LabelPlacement:  "esriServerPolygonPlacementAlwaysHorizontal",
		MaxScale:        0,
		MinScale:        9000,
	}
	b.drawingInfo.LabelingInfo = append(
		b.drawingInfo.LabelingInfo,
		&labelingInfo,
	)
	return &FormDrawingInfoLabelingInfoBuilder{
		labelingInfo: &labelingInfo,
	}
}

func (b *FormDrawingInfoLabelingInfoBuilder) WithWhereClause(where string) *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.Where = where
	return b
}

func (b *FormDrawingInfoLabelingInfoBuilder) WithCenterAlignedSymbol(color []int, fontSize int, haloColor []int, haloSize int) *FormDrawingInfoLabelingInfoBuilder {
	return b.WithSymbol(color, fontSize, haloColor, haloSize, "center")
}
func (b *FormDrawingInfoLabelingInfoBuilder) WithLeftAlignedSymbol(color []int, fontSize int, haloColor []int, haloSize int) *FormDrawingInfoLabelingInfoBuilder {
	return b.WithSymbol(color, fontSize, haloColor, haloSize, "left")
}

func (b *FormDrawingInfoLabelingInfoBuilder) WithSymbol(color []int, fontSize int, haloColor []int, haloSize int, horizontalAlignment string) *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.Symbol = &FormDrawingInfoLabelingInfoSymbol{
		Color: color,
		Font: FormDrawingInfoLabelingFont{
			Decoration: "none",
			Family:     "Arial",
			Size:       fontSize,
			Style:      "normal",
			Weight:     "normal",
		},
		HaloColor:           haloColor,
		HaloSize:            haloSize,
		HorizontalAlignment: horizontalAlignment,
		Kerning:             true,
		Type:                "esriTS",
		VerticalAlignment:   "bottom",
	}
	return b
}

func (b *FormDrawingInfoLabelingInfoBuilder) WithNoMinScale() *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.MinScale = 0
	return b
}
func (b *FormDrawingInfoLabelingInfoBuilder) WithCenteredPointPlacement() *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.LabelPlacement = "esriServerPointLabelPlacementCenterCenter"
	return b
}
func (b *FormDrawingInfoLabelingInfoBuilder) WithAboveRightPointPlacement() *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.LabelPlacement = "esriServerPointLabelPlacementAboveRight"
	return b
}
func (b *FormDrawingInfoLabelingInfoBuilder) WithUseCodedValues() *FormDrawingInfoLabelingInfoBuilder {
	b.labelingInfo.UseCodedValues = true
	return b
}

type FormDrawingInfoRendererBuilder struct {
	renderer *FormDrawingInfoRenderer
}

func (b *FormDrawingInfoBuilder) WithEmptyRenderer() *FormDrawingInfoRendererBuilder {
	renderer := FormDrawingInfoRenderer{
		FieldDelimiter:   ", ",
		Type:             "uniqueValue",
		UniqueValueInfos: []*FormDrawingInfoRendererUniqueValueInfo{},
		NoteToFutureSelf: "!!! HUSK AT DETTE FELTET MÅ VÆRE EDITABLE !!!",
	}
	b.drawingInfo.Renderer = &renderer
	return &FormDrawingInfoRendererBuilder{
		&renderer,
	}
}

func (b *FormDrawingInfoBuilder) WithRenderer(field1 string) *FormDrawingInfoRendererBuilder {
	renderer := FormDrawingInfoRenderer{
		Field1:           strRef(field1),
		FieldDelimiter:   ", ",
		Type:             "uniqueValue",
		UniqueValueInfos: []*FormDrawingInfoRendererUniqueValueInfo{},
		NoteToFutureSelf: "!!! HUSK AT DETTE FELTET MÅ VÆRE EDITABLE !!!",
	}
	b.drawingInfo.Renderer = &renderer
	return &FormDrawingInfoRendererBuilder{
		&renderer,
	}
}

func (b *FormDrawingInfoRendererBuilder) WithDefaultLabel(label string) *FormDrawingInfoRendererBuilder {
	b.renderer.DefaultLabel = &label
	return b
}

func (b *FormDrawingInfoRendererBuilder) WithDefaultSymbol(color []int) *FormDrawingInfoRendererBuilder {
	b.renderer.DefaultSymbol = &FormDrawingInfoRendererSymbol{
		Color: &color,
		Type:  "esriSLS",
		Style: strRef("esriSLSSolid"),
		Width: f64Ref(1),
	}
	return b
}

func (b *FormDrawingInfoRendererBuilder) WithSolidOutlineDefaultSymbol(color []int, outlineColor []int, width float32, backwardDiagonal bool) *FormDrawingInfoRendererBuilder {
	var style = "esriSFSNull"
	if backwardDiagonal {
		style = "esriSFSBackwardDiagonal"
	}
	b.renderer.DefaultSymbol = &FormDrawingInfoRendererSymbol{
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
			Width: width,
		},
		Style: &style,
		Type:  "esriSFS",
	}
	return b
}

func (b *FormDrawingInfoRendererBuilder) WithImageDefaultSymbol(imageData string, url string, width float64, height float64) *FormDrawingInfoRendererBuilder {
	b.renderer.DefaultSymbol = &FormDrawingInfoRendererSymbol{
		Angle:       intRef(0),
		ContentType: strRef("image/png"),
		ImageData:   &imageData,
		Type:        "esriPMS",
		Url:         &url,
		Width:       &width,
		Height:      &height,
		XOffset:     intRef(0),
		YOffset:     intRef(0),
	}
	return b
}

type FormDrawingInfoUniqueValueInfoBuilder struct {
	uniqueValueInfo *FormDrawingInfoRendererUniqueValueInfo
}

func (b *FormDrawingInfoRendererBuilder) WithUniqueValueInfo(description string, label string) *FormDrawingInfoUniqueValueInfoBuilder {
	uniqueValueInfo := FormDrawingInfoRendererUniqueValueInfo{
		Description: description,
		Label:       label,
	}
	b.renderer.UniqueValueInfos = append(
		b.renderer.UniqueValueInfos,
		&uniqueValueInfo,
	)
	return &FormDrawingInfoUniqueValueInfoBuilder{
		uniqueValueInfo: &uniqueValueInfo,
	}
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithSolidLineSymbol(color []int, width float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Style: strRef("esriSLSSolid"),
		Width: &width,
		Type:  "esriSLS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithDashedLineSymbol(color []int, width float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Style: strRef("esriSLSDash"),
		Width: &width,
		Type:  "esriSLS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCimSymbol(symbolType CIMSymbolType, layers ...CIMSymbolLayer) *FormDrawingInfoUniqueValueInfoBuilder {
	symbol := CimSymbol(symbolType, layers...)
	b.uniqueValueInfo.CIMSymbol = &symbol
	return b
}

func CimSymbol(symbolType CIMSymbolType, layers ...CIMSymbolLayer) FormDrawingInfoRendererSymbol {
	return FormDrawingInfoRendererSymbol{
		Type: "CIMSymbolReference",
		Symbol: &FormDrawingInfoRendererSymbol{
			Type:         string(symbolType),
			SymbolLayers: layers,
		},
	}
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCimOutlinedSemitransparentFillSymbol(lineColor []int, lineWidth float64, fillOpacity int) *FormDrawingInfoUniqueValueInfoBuilder {
	fillColor := make([]int, len(lineColor))
	copy(fillColor, lineColor)
	fillColor[len(fillColor)-1] = fillOpacity
	return b.WithCimSymbol(
		Poly,
		FormDrawingSolidLineSymbol(lineColor, lineWidth),
		FormDrawingSolidFillSymbol(fillColor),
	)
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCimOutlinedFillSymbol(fillColor, lineColor []int, lineWidth float64, layers ...CIMSymbolLayer) *FormDrawingInfoUniqueValueInfoBuilder {
	toAppend := layers
	toAppend = append(toAppend,
		FormDrawingSolidLineSymbol(lineColor, lineWidth),
		FormDrawingSolidFillSymbol(fillColor),
	)
	toAppend = append(toAppend, layers...)
	return b.WithCimSymbol(
		Poly,
		toAppend...,
	)
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCimDashedLineSymbol(baseColor []int, lineWidth float64, lineCapStyle CIMLineCapStyle, dashColor []int, dashWidth float64, dashTemplate []float64) *FormDrawingInfoUniqueValueInfoBuilder {
	return b.WithCimSymbol(
		Line,
		FormDrawingDashedLineSymbol(dashColor, dashWidth, lineCapStyle, dashTemplate),
		FormDrawingSolidLineSymbol(baseColor, lineWidth),
	)
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithDashDotedLineSymbol(color []int, width float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Style: strRef("esriSLSDashDot"),
		Width: &width,
		Type:  "esriSLS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithDashedOutlineSymbol(color []int, outlineColor []int, width float32, solid bool) *FormDrawingInfoUniqueValueInfoBuilder {
	var style = "esriSFSNull"
	if solid {
		style = "esriSFSSolid"
	}
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Style: "esriSLSDash",
			Type:  "esriSLS",
			Width: width,
		},
		Style: &style,
		Type:  "esriSFS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithSolidOutlineSymbol(color []int, outlineColor []int, width float32) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
			Width: width,
		},
		Style: strRef("esriSFSNull"),
		Type:  "esriSFS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithSolidOutlineBackwardDiagonalSymbol(color []int, outlineColor []int, width float32) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
			Width: width,
		},
		Style: strRef("esriSFSBackwardDiagonal"),
		Type:  "esriSFS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithSolidOutlineSolidSymbol(color []int, outlineColor []int, width float32, style ...string) *FormDrawingInfoUniqueValueInfoBuilder {
	styleRef := strRef("esriSFSSolid")
	if len(style) > 0 && len(style[0]) > 0 {
		styleRef = strRef(style[0])
	}

	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
			Width: width,
		},
		Style: styleRef,
		Type:  "esriSFS",
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCircleOutlineSymbol(color []int, outlineColor []int, width float32) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Angle: intRef(0),
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Width: width,
		},
		Style:   strRef("esriSMSCircle"),
		Type:    "esriSMS",
		Size:    f64Ref(8),
		XOffset: intRef(0),
		YOffset: intRef(0),
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithCircleSolidOutlineSymbol(color []int, outlineColor []int, width float32) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Angle: intRef(0),
		Color: &color,
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Width: width,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
		},
		Style:   strRef("esriSMSCircle"),
		Type:    "esriSMS",
		Size:    f64Ref(8),
		XOffset: intRef(0),
		YOffset: intRef(0),
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithColoredImageSymbol(color []int, imageData string, url string, width float64, height float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Angle:       intRef(0),
		Color:       &color,
		ContentType: strRef("image/png"),
		ImageData:   &imageData,
		Type:        "esriPMS",
		Url:         &url,
		Width:       &width,
		Height:      &height,
		XOffset:     intRef(0),
		YOffset:     intRef(0),
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithOutlinedImageSymbol(outlineColor []int, outlineWidth float32, imageData string, url string, width float64, height float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Angle: intRef(0),
		Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
			Color: outlineColor,
			Width: outlineWidth,
			Style: "esriSLSSolid",
			Type:  "esriSLS",
		},
		ContentType: strRef("image/png"),
		ImageData:   &imageData,
		Type:        "esriPFS",
		Url:         &url,
		Width:       &width,
		Height:      &height,
		XOffset:     intRef(0),
		YOffset:     intRef(0),
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithImageSymbol(imageData string, url string, width float64, height float64) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
		Angle:       intRef(0),
		ContentType: strRef("image/png"),
		ImageData:   &imageData,
		Type:        "esriPMS",
		Url:         &url,
		Width:       &width,
		Height:      &height,
		XOffset:     intRef(0),
		YOffset:     intRef(0),
	}
	return b
}

func (b *FormDrawingInfoUniqueValueInfoBuilder) WithStringValue(string string) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Value = StringOrInt{StringValue: &string}
	return b
}
func (b *FormDrawingInfoUniqueValueInfoBuilder) WithIntValue(int int) *FormDrawingInfoUniqueValueInfoBuilder {
	b.uniqueValueInfo.Value = StringOrInt{IntValue: &int}
	return b
}

func FormDrawingInfoColor(r int, g int, b int, alpha int) []int {
	return []int{r, g, b, alpha}
}

func FormDrawingSolidLineSymbol(color []int, width float64) CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:   "CIMSolidStroke",
		Enable: true,
		Color:  color,
		Width:  width,
	}
}
func FormDrawingDashedLineSymbol(color []int, width float64, lineCapStyle CIMLineCapStyle, dashTemplate []float64) CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:     "CIMSolidStroke",
		Enable:   true,
		Color:    color,
		Width:    width,
		CapStyle: strRef(string(lineCapStyle)),
		Effects: []CIMSymbolEffect{
			{
				Type:            "CIMGeometricEffectDashes",
				DashTemplate:    dashTemplate,
				LineDashEnding:  "HalfPattern",
				OffsetAlongLine: 0,
			},
		},
	}
}

func FormDrawingSolidFillSymbol(color []int) CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:   "CIMSolidFill",
		Enable: true,
		Color:  color,
	}
}
