package main

func PopulateForevarForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "name"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GlobalIdField = "_headId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_headId", "HeadID")
	b.AddHiddenStringField("_orderId", "Contractor Order Id")
	b.AddHiddenStringField("_serviceOrderId", "Service Order Id")
	b.AddHiddenStringField("_orderNum", "IO")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	//b.AddHiddenStringField("_globalId", "GUID")

	b.SetFieldIdSeries(0,
		"_headId",
		"_orderId",
		"_serviceOrderId",
		"_orderNum",
		"_created_Date",
		"_createdBy", //5
		"_modified_Date",
		"_modifiedBy",
	)

	//SECTION 1
	b.AddSection("Rik bakkevegetasjon (vegetasjonstyper) ")

	b.AddMultipleChoiceField("bakkevegetasjon", "En eller flere av følgende vegetasjonstyper forekommer:").
		WithNotes("En eller flere av følgende vegetasjonstyper forekommer").
		WithMultipleChoiceSeparator("|").
		WithSuggestedPreferredNoOption().
		WithSuggestedOption("Høgstaudeskog").
		WithSuggestedOption("Lågurtskog").
		WithSuggestedOption("Kalkpåvirket skog").
		WithSuggestedOption("Sumpskog").
		WithSuggestedOption("Tresatt flommark/oversvømmingsareal").
		WithSuggestedOption("Gjengrodd rik slåtte-/beitemark").
		WithSuggestedOption("Oppkomme/kildeframspring")

	b.AddMultipleChoiceField("gamletraer", "Gamle trær").
		WithNotes("Minsteareal: 2 daa. \\Inngang: >= 3 gamle trær pr daa (maks avstand 20 meter)\\Definisjon gamle trær: \\- Furu >=200 år\\- Gran >=150 år\\- Eik >= 50 cm bhd\\- Edellauvtrær, osp, selje, bjørk: > = 40 cm bhd\\- Rogn, gråor: > = 30 cm bhd").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("eldrelauvsuksesjon", "Eldre lauvsuksesjon").
		WithNotes("Minsteareal: 2 daa\\Inngang: >= 4 gamle lauvtrær pr. daa (maks avstand 15 meter)\\Gjelder kun nordiske lauvtreslag. Gjelder ikke skjøttet skog.\\Definisjon gamle lauvtrær: > 20 cm i bhd.").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("hulelauvtraer", "Hule lauvtrær").
		WithNotes("Inngang: Forekomst av Styva eller hule lauvtrær > 30 cm i bhd ").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("staende", "Stående/liggende død ved").
		WithNotes("Minsteareal: 2 daa Minstedimensjon: 10 cm bhd\\Inngang:\\- Min. 4 liggende døde trær <30 cm  bhd  pr daa (maks avstand 15 meter)\\- Min. 4 stående døde trær <30 cm  bhd  pr daa (maks avstand 15 meter)\\- Min. 2 liggende døde trær >=30 cm  bhd  pr daa (maks avstand 25 meter)\\- Min. 2 stående døde trær >=30 cm  bhd  pr daa (maks avstand 25 meter)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("rikbarkstraer", "Rikbarkstrær").
		WithNotes("Minsteareal: 2 daa\\Inngang:\\- Minimum 2 trær pr dekar (maks avstand 25 meter) med neverlav (lungenever etc)\\- Og/eller minimum 2 spisslønn > 20 cm bhd pr daa (maks avstand 25 meter)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("lysehengelaver", "Lyse hengelaver").
		WithNotes("Minsteareal: 2 daa\\Inngang:\\- Minimum 10 trær pr dekar (maks 10 meter mellom trærne) med gode forekomster av lyse hengelaver (10 individer lengere enn 10 cm på treets rikeste kvadratmeter)\\- Og/eller forekomst av trær med huldrestry, mjuktjafs").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("brentskog", "Brent skog").
		WithNotes("Inngang: Forekomster av stubber med spor av brann").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.AddMultipleChoiceField("topografi", "Topografi").
		WithNotes("Inngang: Forekomst av en eller flere av følgende landformer\\- Bekkekløft (årsikker vannføring) Lengde mer enn 25 m, og gjennomsnittlig høydeforskjell over 5 m fra bunn til topp\\- Ravine (leire og løsmasser), lengde mer enn 25 meter\\- Store bergvegger (> 3 m høye, 80 grader stigning), og/eller blokkmark > 1daa, og/eller rasmark > 1 daa").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption()

	b.SetFieldIdSeries(10, "bakkevegetasjon")
	b.SetFieldIdSeries(20, "gamletraer")
	b.SetFieldIdSeries(30, "eldrelauvsuksesjon")
	b.SetFieldIdSeries(40, "hulelauvtraer")
	b.SetFieldIdSeries(50, "staende")
	b.SetFieldIdSeries(60, "rikbarkstraer")
	b.SetFieldIdSeries(70, "lysehengelaver")
	b.SetFieldIdSeries(80, "brentskog")
	b.SetFieldIdSeries(90, "topografi")

	//SECTION 2 / table2
	b.AddSection("Tilleggsopplysninger")

	//logic to decide if we show this :-D
	b.AddHiddenBooleanField("_showtable2_Boolean", "Vis ekstra tabell").
		WithLogic().
		WithSetValue(FormFieldLogicSetValue{
			ElseSetValue: &FormFieldLogicSetValueElseSetValue{
				StringValue: strRef("Nei"), //mix of int and string?
			},
			IfValueIs: FormFieldLogicIfValueIs{
				AnyOf: []FormFieldLogicFieldInArray{
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "bakkevegetasjon",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "gamletraer",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "eldrelauvsuksesjon",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "hulelauvtraer",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "staende",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "rikbarkstraer",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "lysehengelaver",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "brentskog",
						OtherFieldStringValue: strRef("Nei"),
					}},
					{FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "topografi",
						OtherFieldStringValue: strRef("Nei"),
					}},
				},
			},
			ThenSetValue: FormFieldLogicSetValueThenSetValue{
				IntValue: intRef(1), //mix of int and string?
			},
		})
	b.SetFieldIdSeries(190, "_showtable2_Boolean")

	showTable2Logic := FormFieldLogicField{
		Operator:           "==",
		OtherFieldIntValue: intRef(1),
		OtherFieldName:     "_showtable2_Boolean",
	}

	b.AddNullableMultipleChoiceField("stordiamlevende", "Stor diameterspredning på hovedtreslaget (levende)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("stordiamdodt", "Stor diameterspredning på hovedtreslaget (dødt)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("flersjiktet", "Flersjiktet bestand (kan være flere treslag i sjiktingen)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("flerehull", "Flere \"hull\"/åpninger i bestandet (> 0,1 daa)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("hule", "Flere hule trær/reirtrær (> 2 tre daa)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("flereseintvoksende", "Flere seintvoksende trær (overaldrige)").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("plantetbestand", "Plantet bestand").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("nordligeksponering", "Nordlig eksponering eller forsenkning ").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.AddNullableMultipleChoiceField("sydligeksponering", "Sydlig eksponering eller på høyde/rygg").
		WithSuggestedOption("Ja").
		WithSuggestedPreferredNoOption().
		WithLogic().
		WithVisibleIfField(showTable2Logic)

	b.SetFieldIdSeries(200, "stordiamlevende")
	b.SetFieldIdSeries(210, "stordiamdodt")
	b.SetFieldIdSeries(220, "flersjiktet")
	b.SetFieldIdSeries(240, "flerehull")
	b.SetFieldIdSeries(250, "hule")
	b.SetFieldIdSeries(260, "flereseintvoksende")
	b.SetFieldIdSeries(270, "plantetbestand")
	b.SetFieldIdSeries(280, "nordligeksponering")
	b.SetFieldIdSeries(290, "sydligeksponering")

	b.WithAutoSortex()
}
