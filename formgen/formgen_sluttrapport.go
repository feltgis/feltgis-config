package main

func PopulateSluttrapportForm(b *FormBuilder) {

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_job_nr"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 32633,
			Wkid:       32633,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	//di := b.AddDrawingInfo().
	//	WithTransparency(0)
	//renderer := di.WithRenderer("dev_Boolean")

	//FIELDS
	b.AddHiddenStringField("_globalId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_job_nr", "Ordrenummer")

	b.AddHiddenStringField("_workTeamOrgNum", "Organisasjonsnummer arbeidslag")
	b.AddHiddenStringField("_workTeamOrgWorkers", "Arbeidslagsarbeidere")
	b.AddHiddenStringField("_workTeamOrgTeams", "Arbeidslags team-ids")

	b.AddNullableStringField("workStatus", "Arbeidsstatus")
	b.AddHiddenDateField("_firstSigned_Date", "Første signeringsdato")
	b.AddHiddenNullableStringField("_rejectReason", "Avvisningsårsak")

	//fieldIds
	b.SetFieldIdSeries(0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_job_nr",
	)

	b.SetFieldIdSeries(
		19,
		"_workTeamOrgNum",
		"workStatus",
		"_workTeamOrgWorkers",
	)

	b.SetFieldIdSeries(
		100,
		"_firstSigned_Date",
		"_workTeamOrgTeams",
		"_rejectReason",
	)

	if b.IsNortømmer() {

		b.WithSyncEvent(SyncEventConfig{
			Type:             SyncEventTypeCopyIncoming,
			SourceCollection: b.company.Prefix + "Sluttrapport", // Implicit and could be removed (?)
			TargetCollection: b.company.Prefix + "SluttrapportWorkStartedSMS",
			Filter: &SyncEventFilter{
				Key:         "workStatus",
				StringValue: "Started",
			},
		})
	}

	b.WithAutoSortex()
}
