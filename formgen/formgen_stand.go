package main

func PopulateStandForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "Etikett"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "GlobalID"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("bon")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	// Renderer
	di := b.AddDrawingInfo()

	di.WithLabelingInfo("Etikett").
		WithCenterAlignedSymbol(
			FormDrawingInfoColor(255, 255, 255, 255), //black color
			10,
			FormDrawingInfoColor(0, 0, 0, 127), //white halo color
			1,
		)

	renderer := di.WithRenderer("Hkl")
	renderer.WithDefaultSymbol(
		FormDrawingInfoColor(0, 0, 0, 0),
	)
	renderer.WithUniqueValueInfo("Uprod", "Uprod").
		WithStringValue("0").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(0, 0, 0, 0),
			FormDrawingInfoColor(0, 0, 0, 0),
			1,
		)

	renderer.WithUniqueValueInfo("Hkl 1", "Hkl 1").
		WithStringValue("1").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(255, 255, 255, 255),
			FormDrawingInfoColor(38, 115, 0, 255),
			1,
		)

	renderer.WithUniqueValueInfo("Hkl 2", "Hkl 2").
		WithStringValue("2").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(0, 197, 255, 255),
			FormDrawingInfoColor(38, 115, 0, 255),
			1,
		)

	renderer.WithUniqueValueInfo("Hkl 3", "Hkl 3").
		WithStringValue("3").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(209, 255, 115, 255),
			FormDrawingInfoColor(38, 115, 0, 255),
			1,
		)

	renderer.WithUniqueValueInfo("Hkl 4", "Hkl 4").
		WithStringValue("4").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(255, 210, 0, 255),
			FormDrawingInfoColor(38, 115, 0, 255),
			1,
		)

	renderer.WithUniqueValueInfo("Hkl 5", "Hkl 5").
		WithStringValue("5").
		WithSolidOutlineSolidSymbol(
			FormDrawingInfoColor(230, 0, 0, 255),
			FormDrawingInfoColor(38, 115, 0, 255),
			1,
		)

	// The fields as they appear in Fritzøe's ArcGIS online feature service

	// Fields
	b.AddUneditableRequiredIntegerField("OBJECTID", "Object ID")
	b.AddUneditableRequiredStringField("GIS_ID", "GIS ID")
	b.AddUneditableRequiredStringField("created_user", "Created User")
	b.AddUneditableRequiredIntegerField("created_date", "Created Date")
	b.AddUneditableRequiredStringField("last_edited_user", "Last Edited User")
	b.AddUneditableRequiredIntegerField("last_edited_date", "Last Edited Date")
	b.AddUneditableRequiredIntegerField("Figur_ID", "Figur ID")
	b.AddUneditableRequiredIntegerField("Prosjekt", "Prosjekt")
	b.AddUneditableRequiredIntegerField("Plan_", "Plan")
	b.AddUneditableRequiredStringField("PlanNavn", "Plan Navn")
	b.AddUneditableRequiredIntegerField("Teig", "Teig")
	b.AddUneditableRequiredStringField("TeigNavn", "Teig Navn")
	b.AddUneditableRequiredIntegerField("Grend", "Grend")
	b.AddUneditableRequiredIntegerField("BestNr", "Bestandsnummer")
	b.AddUneditableRequiredIntegerField("DelNr", "Del Nummer")
	b.AddUneditableRequiredStringField("Presentasj", "Presentasjon")
	b.AddUneditableRequiredStringField("Navn", "Navn")
	b.AddUneditableRequiredDoubleField("ArealBrutto", "Areal Brutto")
	b.AddUneditableRequiredDoubleField("ImpPros", "Improdusert Prosent")
	b.AddUneditableRequiredIntegerField("ImpBon", "Improdusert Bonitet")
	b.AddUneditableRequiredDoubleField("ArealUprod", "Areal Uproduktiv")
	b.AddUneditableRequiredDoubleField("ArealProd", "Areal Produksjon")
	b.AddUneditableRequiredStringField("BonTresl", "Bon Treslag")
	b.AddUneditableRequiredIntegerField("Bon", "Bonitet")
	b.AddUneditableRequiredStringField("BonTekst", "Bon Tekst")
	b.AddUneditableRequiredIntegerField("Alder", "Alder")
	b.AddRequiredIntegerField("Hkl", "Hkl")
	b.AddUneditableRequiredStringField("Tetth", "Tetthet")
	b.AddUneditableRequiredDoubleField("Hoyde", "Høyde")
	b.AddUneditableRequiredDoubleField("VolDaa", "Volum per Dekar")
	b.AddUneditableRequiredDoubleField("VolPros_G", "Volum Prosent Gran")
	b.AddUneditableRequiredDoubleField("VolPros_F", "Volum Prosent Furu")
	b.AddUneditableRequiredDoubleField("VolPros_L", "Volum Prosent Lauv")
	b.AddUneditableRequiredDoubleField("VolTot", "Volum Totalt")
	b.AddUneditableRequiredDoubleField("TreAntDaa", "Tre Antall per Dekar")
	b.AddUneditableRequiredDoubleField("AntPros_G", "Antall Prosent Gran")
	b.AddUneditableRequiredDoubleField("AntPros_F", "Antall Prosent Furu")
	b.AddUneditableRequiredDoubleField("AntPros_L", "Antall Prosent Lauv")
	b.AddUneditableRequiredDoubleField("TilvPros", "Tilvekst Prosent")
	b.AddUneditableRequiredDoubleField("TilvDaa", "Tilvekst per Dekar")
	b.AddUneditableRequiredDoubleField("TilvTot", "Tilvekst Totalt")
	b.AddUneditableRequiredIntegerField("Sunnhet", "Sunnhet")
	b.AddUneditableRequiredStringField("SunnhetNavn", "Sunnhet Navn")
	b.AddUneditableRequiredIntegerField("SkogType", "Skogtype")
	b.AddUneditableRequiredStringField("SkogTypeNavn", "Skogtype Navn")
	b.AddUneditableRequiredIntegerField("VegetasjType", "Vegetasjonstype")
	b.AddUneditableRequiredStringField("VegetasjNavn", "Vegetasjon Navn")
	b.AddUneditableRequiredIntegerField("Sjikt", "Sjikt")
	b.AddUneditableRequiredStringField("SjiktNavn", "Sjikt Navn")
	b.AddUneditableRequiredDoubleField("GrFlate", "Grunnflate")
	b.AddUneditableRequiredIntegerField("Lilengde", "Linjelengde")
	b.AddUneditableRequiredIntegerField("Helning", "Helning")
	b.AddUneditableRequiredIntegerField("Transport", "Transport")
	b.AddUneditableRequiredIntegerField("BaereEvne", "Bæreevne")
	b.AddUneditableRequiredIntegerField("Kvalitet", "Kvalitet")
	b.AddUneditableRequiredStringField("KvalitetNavn", "Kvalitet Navn")
	b.AddUneditableRequiredDoubleField("SkadePros", "Skadeprosent")
	b.AddUneditableRequiredIntegerField("PotBon", "Potensial Bonitet")
	b.AddUneditableRequiredDoubleField("MidDiam", "Middeldiameter")
	b.AddUneditableRequiredStringField("DomTreslag", "Dominerende Treslag")
	b.AddUneditableRequiredStringField("VeiNr", "Veinummer")
	b.AddUneditableRequiredStringField("VeiPars", "Veiparsell")
	b.AddUneditableRequiredStringField("GjeldendeDato", "Gjeldende Dato")
	b.AddUneditableRequiredStringField("DatoLaget", "Dato Laget")
	b.AddUneditableRequiredStringField("DatoEndret", "Dato Endret")
	b.AddUneditableRequiredStringField("Merknader", "Merknader")
	b.AddUneditableRequiredStringField("GlobalID", "Global ID")
	b.AddUneditableRequiredDoubleField("Shape__Area", "Shape Area")
	b.AddUneditableRequiredDoubleField("Shape__Length", "Shape Length")
	b.AddUneditableRequiredStringField("Etikett", "Etikett")

	b.SetFieldIdSeries(1,
		"OBJECTID",
		"GIS_ID",
		"created_user",
		"created_date",
		"last_edited_user",
		"last_edited_date",
		"Figur_ID",
		"Prosjekt",
		"Plan_",
		"PlanNavn",
		"Teig",
		"TeigNavn",
		"Grend",
		"BestNr",
		"DelNr",
		"Presentasj",
		"Navn",
		"ArealBrutto",
		"ImpPros",
		"ImpBon",
		"ArealUprod",
		"ArealProd",
		"BonTresl",
		"Bon",
		"BonTekst",
		"Alder",
		"Hkl",
		"Tetth",
		"Hoyde",
		"VolDaa",
		"VolPros_G",
		"VolPros_F",
		"VolPros_L",
		"VolTot",
		"TreAntDaa",
		"AntPros_G",
		"AntPros_F",
		"AntPros_L",
		"TilvPros",
		"TilvDaa",
		"TilvTot",
		"Sunnhet",
		"SunnhetNavn",
		"SkogType",
		"SkogTypeNavn",
		"VegetasjType",
		"VegetasjNavn",
		"Sjikt",
		"SjiktNavn",
		"GrFlate",
		"Lilengde",
		"Helning",
		"Transport",
		"BaereEvne",
		"Kvalitet",
		"KvalitetNavn",
		"SkadePros",
		"PotBon",
		"MidDiam",
		"DomTreslag",
		"VeiNr",
		"VeiPars",
		"GjeldendeDato",
		"DatoLaget",
		"DatoEndret",
		"Merknader",
		"GlobalID",
		"Shape__Area",
		"Shape__Length",
		"Etikett",
	)

}
