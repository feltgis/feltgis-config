package main

import (
	"fmt"
	"strings"
)

type FormFieldBuilder struct {
	field *FormField

	currentOptionTranslationKey *string //makes it easy to add translation to options...
}

func (b *FormBuilder) AddRawField(name string, alias string, editable bool, nullable bool, fieldtype string) *FormFieldBuilder {
	var section *int = nil
	if !strings.HasPrefix(name, "_") { //add section to non-hidden values
		section = b.currentSection
	}
	field := FormField{
		Alias:    alias,
		Editable: editable,
		FieldId:  -1,
		Name:     name,
		Nullable: nullable,
		Type:     fieldtype,
		Section:  section,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return &FormFieldBuilder{field: &field}
}

/* -----------
Hidden
----------- */

func (b *FormBuilder) AddHiddenStringField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, false, "esriFieldTypeString")
}

func (b *FormBuilder) AddHiddenNullableStringField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeString")
}

// The renderer field must be editable. Even though it does not make sense to make a hidden field editable..
func (b *FormBuilder) AddHiddenEditableStringFieldForRendering(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeString")
}

func (b *FormBuilder) AddHiddenDateField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if !strings.HasSuffix(name, "_Date") {
		panic("date field without _Date suffix")
	}
	return b.AddRawField(name, alias, false, false, "esriFieldTypeDouble")
}
func (b *FormBuilder) AddHiddenIntegerField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeInteger")
}
func (b *FormBuilder) AddHiddenDoubleField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("double field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("double field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("double with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeDouble")
}
func (b *FormBuilder) AddHiddenBooleanField(name string, alias string) *FormFieldBuilder {
	if !strings.HasPrefix(name, "_") {
		panic("hidden field without _ prefix")
	}
	if !strings.HasSuffix(name, "_Boolean") {
		panic("boolean field without _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeInteger")
}

/* -----------
Required
----------- */

func (b *FormBuilder) AddRequiredIntegerField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeInteger")
}

func (b *FormBuilder) AddRequiredStringField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		b.reporter.Warning(fmt.Sprintf("'%v' string field with _Date suffix; It works and is legacy, but is really inconsistent", name))
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeString")
}
func (b *FormBuilder) AddRequiredBooleanField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if !strings.HasSuffix(name, "_Boolean") {
		panic("boolean field without _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeInteger")
}
func (b *FormBuilder) AddRequiredDateField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if !strings.HasSuffix(name, "_Date") {
		panic("date field without _Date suffix")
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeDouble")
}
func (b *FormBuilder) AddRequiredDecimalField(name string, alias string, numberOfDecimals int) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	suffix := fmt.Sprintf("_Decimal%v", numberOfDecimals)
	if !strings.HasSuffix(name, suffix) {
		panic("decimal field without _Decimal{N} suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeString")
}
func (b *FormBuilder) AddRequiredDoubleField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, false, "esriFieldTypeDouble")
}

/* -----------
Nullable
----------- */

func (b *FormBuilder) AddNullableIntegerField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeInteger")
}

func (b *FormBuilder) AddNullableStringField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeString")
}

func (b *FormBuilder) AddNullableDoubleField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeDouble")
}

func (b *FormBuilder) AddNullableBooleanField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if !strings.HasSuffix(name, "_Boolean") {
		panic("boolean field without _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeInteger")
}

func (b *FormBuilder) AddNullableDecimalField(name string, alias string, numberOfDecimals int) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	suffix := fmt.Sprintf("_Decimal%v", numberOfDecimals)
	if !strings.HasSuffix(name, suffix) {
		panic("decimal field without _Decimal{N} suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, true, true, "esriFieldTypeString")
}

/* -----------
Uneditable
----------- */

func (b *FormBuilder) AddUneditableNullableDateField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if !strings.HasSuffix(name, "_Date") {
		panic("date field without _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeDouble")
}
func (b *FormBuilder) AddUneditableNullableStringField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("string field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeString")
}
func (b *FormBuilder) AddUneditableNullableDoubleField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeDouble")
}
func (b *FormBuilder) AddUneditableNullableIntegerField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeInteger")
}
func (b *FormBuilder) AddUneditableNullableBooleanField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if !strings.HasSuffix(name, "_Boolean") {
		panic("boolean field without _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeInteger")
}
func (b *FormBuilder) AddUneditableNullableDecimalField(name string, alias string, numberOfDecimals int) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	suffix := fmt.Sprintf("_Decimal%v", numberOfDecimals)
	if !strings.HasSuffix(name, suffix) {
		panic("decimal field without _Decimal{N} suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("non date field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, true, "esriFieldTypeString")
}
func (b *FormBuilder) AddUneditableRequiredStringField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("string field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, false, "esriFieldTypeString")
}

func (b *FormBuilder) AddUneditableRequiredIntegerField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("string field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, false, "esriFieldTypeInteger")
}

func (b *FormBuilder) AddUneditableRequiredDoubleField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	if strings.HasSuffix(name, "_Boolean") {
		panic("non boolean field with _Boolean suffix")
	}
	if strings.HasSuffix(name, "_Decimal2") {
		panic("non decimal2 field with _Decimal2 suffix")
	}
	if strings.HasSuffix(name, "_Date") {
		panic("double field with _Date suffix")
	}
	return b.AddRawField(name, alias, false, false, "esriFieldTypeDouble")
}

/* -----------
MultipleChoice
----------- */

func (b *FormBuilder) AddMultipleChoiceField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			SuggestedValues: []FormFieldSuggestedValue{},
			Type:            "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: false,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return &FormFieldBuilder{field: &field}
}
func (b *FormBuilder) AddNullableMultipleChoiceField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			SuggestedValues: []FormFieldSuggestedValue{},
			Type:            "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: true,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return &FormFieldBuilder{field: &field}
}

func (b *FormBuilder) AddYesNoPreferredYesField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			MultipleChoiceSeparator: nil,
			SuggestedValues:         []FormFieldSuggestedValue{},
			Type:                    "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: false,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return (&FormFieldBuilder{field: &field}).WithSuggestedPreferredYesOption().WithSuggestedOption("Nei")
}
func (b *FormBuilder) AddYesNoPreferredNoField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			MultipleChoiceSeparator: nil,
			SuggestedValues:         []FormFieldSuggestedValue{},
			Type:                    "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: false,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return (&FormFieldBuilder{field: &field}).WithSuggestedOption("Ja").WithSuggestedPreferredNoOption()
}
func (b *FormBuilder) AddYesNoPreferredNoWithoutSoloField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			MultipleChoiceSeparator: nil,
			SuggestedValues:         []FormFieldSuggestedValue{},
			Type:                    "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: false,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return (&FormFieldBuilder{field: &field}).WithSuggestedOption("Ja").WithSuggestedPreferredNoWithoutSoloOption()
}
func (b *FormBuilder) AddYesNoPreferredNoAndMultipleField(name string, alias string) *FormFieldBuilder {
	if strings.HasPrefix(name, "_") {
		panic("visible field with _ prefix")
	}
	field := FormField{
		Alias: alias,
		Domain: &FormFieldDomain{
			MultipleChoiceSeparator: nil,
			SuggestedValues:         []FormFieldSuggestedValue{},
			Type:                    "codedValue",
		},
		Editable: true,
		FieldId:  -1,
		Name:     name,
		Nullable: false,
		Type:     "esriFieldTypeString",
		Section:  b.currentSection,
	}
	b.form.Fields = append(b.form.Fields, &field)
	return (&FormFieldBuilder{field: &field}).WithMultipleChoiceSeparator("|").WithSuggestedOption("Ja").WithSuggestedPreferredNoOption()
}

func (b *FormFieldBuilder) WithNotes(notes string) *FormFieldBuilder {
	b.field.Notes = &notes
	return b
}

// Dont use this, use autosortex... only use this for null-diff
func (b *FormFieldBuilder) DebugWithSortix(sortix int) *FormFieldBuilder {
	b.field.Sortix = &sortix
	return b
}
func (b *FormFieldBuilder) WithMultipleChoiceSeparator(sep ...string) *FormFieldBuilder {
	multipleChoiceSeparator := "|"
	if len(sep) > 0 {
		multipleChoiceSeparator = sep[0]
	}
	b.field.Domain.MultipleChoiceSeparator = &multipleChoiceSeparator
	return b
}

func (b *FormFieldBuilder) WithSuggestedOption(option string) *FormFieldBuilder {
	b.WithSuggestedOptionWithCustomName(option, option)
	return b
}
func (b *FormFieldBuilder) WithSuggestedOptionWithCustomName(option string, name string) *FormFieldBuilder {
	if b.field.Domain == nil {
		panic("Trying to add SuggestedOption to a non MultipleChoiceField?")
	}
	b.currentOptionTranslationKey = &name
	b.field.Domain.SuggestedValues = append(
		b.field.Domain.SuggestedValues,
		FormFieldSuggestedValue{
			Name:  name,
			Value: option,
		},
	)
	return b
}

func (b *FormFieldBuilder) WithCodedOption(option string) *FormFieldBuilder {
	return b.WithCodedOptionWithCustomName(option, option)
}
func (b *FormFieldBuilder) WithCodedOptionWithCustomName(option string, name string) *FormFieldBuilder {
	if b.field.Domain == nil {
		panic("Trying to add CodedOption to a non MultipleChoiceField?")
	}
	b.currentOptionTranslationKey = &option
	b.field.Domain.CodedValues = append(
		b.field.Domain.CodedValues,
		FormFieldCodedValue{
			Name:  name,
			Value: option,
		},
	)
	return b
}

func (b *FormFieldBuilder) WithSuggestedPreferredNoOption() *FormFieldBuilder {
	var _true = true
	b.field.Domain.SuggestedValues = append(
		b.field.Domain.SuggestedValues,
		FormFieldSuggestedValue{
			Name:      "Nei",
			Preferred: &_true,
			Solo:      &_true,
			Value:     "Nei",
		},
	)
	return b
}
func (b *FormFieldBuilder) WithSuggestedPreferredNoWithoutSoloOption() *FormFieldBuilder {
	var _true = true
	b.field.Domain.SuggestedValues = append(
		b.field.Domain.SuggestedValues,
		FormFieldSuggestedValue{
			Name:      "Nei",
			Preferred: &_true,
			Value:     "Nei",
		},
	)
	return b
}
func (b *FormFieldBuilder) WithSuggestedPreferredYesOption() *FormFieldBuilder {
	var _true = true
	b.field.Domain.SuggestedValues = append(
		b.field.Domain.SuggestedValues,
		FormFieldSuggestedValue{
			Name:      "Ja",
			Preferred: &_true,
			Solo:      &_true,
			Value:     "Ja",
		},
	)
	return b
}

func (b *FormFieldBuilder) WithAliasTranslation(language string, text string) *FormFieldBuilder {
	if language != "en" {
		panic("Only supported language is 'en', not " + language)
	}
	b.field.Translations = append(b.field.Translations,
		FormTranslation{
			Key:      b.field.Alias,
			Language: language,
			Value:    text,
		},
	)
	return b
}
func (b *FormFieldBuilder) WithNotesTranslation(language string, text string) *FormFieldBuilder {
	if language != "en" {
		panic("Only supported language is 'en', not " + language)
	}
	if b.field.Notes == nil {
		panic("can not translate notes if notes is not set :-)")
	}
	b.field.Translations = append(b.field.Translations,
		FormTranslation{
			Key:      *b.field.Notes,
			Language: language,
			Value:    text,
		},
	)
	return b
}

func (b *FormFieldBuilder) WithOptionTranslation(language string, text string) *FormFieldBuilder {
	if language != "en" {
		panic("Only supported language is 'en', not " + language)
	}
	if b.currentOptionTranslationKey == nil {
		panic("WithOptionTranslation called without a currentOptionTranslationKey")
	}
	b.field.Translations = append(b.field.Translations,
		FormTranslation{
			Key:      *b.currentOptionTranslationKey,
			Language: language,
			Value:    text,
		},
	)
	return b
}

func (b *FormFieldBuilder) WithComment(comment string) *FormFieldBuilder {
	b.field.Comment = &comment
	return b
}

func (b *FormFieldBuilder) WithLength(length int) *FormFieldBuilder {
	b.field.Length = &length
	return b
}

func (b *FormFieldBuilder) WithNegativeNumbersAllowed() *FormFieldBuilder {
	//TODO: should be part og "logic-builder"
	allowed := true
	if b.field.Logic == nil {
		b.field.Logic = &FormFieldLogic{}
	}
	if b.field.Logic.Validation == nil {
		b.field.Logic.Validation = &FormFieldLogicValidation{}
	}
	b.field.Logic.Validation.NegativeNumbersAllowed = &allowed
	return b
}

func (b *FormFieldBuilder) WithRequiredIf(requiredIf FormFieldLogicIfValueIs) *FormFieldBuilder {
	b.field.RequiredIf = &requiredIf
	return b
}

func (b *FormFieldBuilder) WithRequiredIfField(field FormFieldLogicField) *FormFieldBuilder {
	b.field.RequiredIf = &FormFieldLogicIfValueIs{
		Field: &field,
	}
	return b
}

//DO NOT! Autocomplete IDs with smart code :-D
