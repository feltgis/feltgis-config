package main

func PopulateSKOrgConfigForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.GeometryType = "esriGeometryPoint"
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_headId"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 500000,
		XMin: 0,
		YMax: 8000000,
		YMin: 5000000,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GlobalIdField = "orgNum"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("null")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	b.AddDrawingInfo().WithEmptyRenderer()

	//FIELDS
	b.AddUneditableNullableStringField("orgNum", "Skogkultur entrep orgNum")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_orderPrioPL", "Oppdragsprioritet planting")
	b.AddHiddenStringField("_orderPrioUP", "Oppdragsprioritet ungskogpleie")
	b.AddHiddenStringField("_orderPrioMB", "Oppdragsprioritet markberedning")
	b.AddHiddenStringField("_orderPrioFR", "Oppdragsprioritet forhåndsrydding")
	b.AddHiddenStringField("_orderPrioGR", "Oppdragsprioritet grøfterensk")
	b.AddHiddenStringField("_orderPrioSA", "Oppdragsprioritet såing")
	b.AddHiddenStringField("_orderPrioSS", "Oppdragsprioritet skogskade")
	b.AddHiddenStringField("_orderPrioIS", "Oppdragsprioritet infrastruktur")
	b.AddHiddenStringField("_orderPrioGJ", "Oppdragsprioritet gjødsling")
	b.AddHiddenStringField("_orderPrioSP", "Oppdragsprioritet sprøyting")
	b.AddHiddenStringField("_orderPrioGA", "Oppdragsprioritet gravearbeid")
	b.AddHiddenStringField("_orderPrioSR", "Oppdragsprioritet sporskaderetting")
	b.AddHiddenStringField("_orderPrioSK", "Oppdragsprioritet stammekvisting")
	//FIELD IDS
	b.SetFieldIdSeries(
		1,
		"orgNum",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_orderPrioPL",
		"_orderPrioUP",
		"_orderPrioMB",
		"_orderPrioFR",
		"_orderPrioGR",
		"_orderPrioSA",
		"_orderPrioSS",
		"_orderPrioIS",
		"_orderPrioGJ",
		"_orderPrioSP",
		"_orderPrioGA",
		"_orderPrioSR",
		"_orderPrioSK",
	)

	b.WithAutoSortex()
}
