package main

func PopulateFakturalinjeForm(b *FormBuilder) {

	//DEFAULT!
	b.form.Alias = strRef("Fakturalinje")
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      "Fakturalinje",
			Language: "en",
			Value:    "Billing line",
		},
	)
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_title"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 4
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 50000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("contractor_done_Boolean")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	b.AddDrawingInfo().WithEmptyRenderer()

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_workTeamOrgNum", "Organisasjonsnummer arbeidslag")
	b.AddHiddenStringField("_subCollection", "Subcollection")
	b.AddHiddenStringField("_parentGlobalId", "Skogkultur global id")

	//section 1
	b.AddSection("")

	b.AddNullableStringField("description", "Fritekst").
		WithAliasTranslation("en", "Text")

	b.AddMultipleChoiceField("unit", "Enhet").
		WithAliasTranslation("en", "Unit").
		WithCodedOption("Timer").
		WithOptionTranslation("en", "Hours").
		WithCodedOption("Meter").
		WithOptionTranslation("en", "Meters").
		WithCodedOption("Kilometer").
		WithOptionTranslation("en", "KM").
		WithCodedOption("Stk.").
		WithOptionTranslation("en", "Pieces")

	b.AddNullableDoubleField("count", "Antall").
		WithAliasTranslation("en", "Amount")

	b.AddNullableDoubleField("price", "Pris").
		WithAliasTranslation("en", "Price")

	b.AddUneditableNullableDoubleField("sum", "Sum").
		WithAliasTranslation("en", "Sum")

	//fieldIds
	b.SetFieldIdSeries(
		1,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_workTeamOrgNum",
		"_subCollection",
		"_parentGlobalId",
	)
	b.SetFieldIdSeries(
		100,
		"description",
		"unit",
		"count",
		"price",
		"sum",
	)

	//before we mingle up the file :-)
	b.WithAutoSortex()
}
