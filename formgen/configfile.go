package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type ConfigFile struct {
	Path string
	Name string
}

func NewConfigFile(Name string) *ConfigFile {
	p := new(ConfigFile)
	p.Path = ""
	p.Name = Name
	return p
}

func (f *ConfigFile) Filename() string {
	var result = "../"
	if f.Path != "" {
		result += f.Path + "/"
	}
	result += f.Name
	result += ".json"
	return result
}

func (f *ConfigFile) ParseConfig() *Config {

	// Open our jsonFile
	jsonFile, err := os.Open(f.Filename())
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println("Successfully Opened " + f.Filename())

	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our Users array
	var config Config

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &config)

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	return &config
}

func (f *ConfigFile) WriteConfig(config *Config) {

	// Create our jsonFile
	jsonFile, err := os.Create(f.Filename())
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// write our opened jsonFile as a byte array.
	var jsonString = config.Json()

	//write
	jsonFile.Write([]byte(jsonString))

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
}
