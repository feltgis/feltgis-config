package main

func populateDoneworkFormCommon(b *FormBuilder) {

	b.form.Alias = strRef("Utført arbeid")
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      "Utført arbeid",
			Language: "en",
			Value:    "Done work",
		},
	)

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_title"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 4
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 50000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("contractor_done_Boolean")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	di := b.AddDrawingInfo().
		WithTransparency(0)
	renderer := di.WithRenderer("contractor_done_Boolean")
	renderer.WithUniqueValueInfo("", "Arbeid ikke utført").
		WithStringValue("0").
		WithSolidOutlineBackwardDiagonalSymbol(
			FormDrawingInfoColor(0, 0, 0, 0),
			FormDrawingInfoColor(255, 255, 255, 255),
			1.5,
		)
	renderer.WithUniqueValueInfo("", "Arbeid utført").
		WithStringValue("1").
		WithSolidOutlineBackwardDiagonalSymbol(
			FormDrawingInfoColor(0, 255, 0, 100),
			FormDrawingInfoColor(255, 255, 255, 255),
			1.5,
		)

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_workTeamOrgNum", "Organisasjonsnummer arbeidslag")
	b.AddHiddenStringField("_subCollection", "Subcollection")
	b.AddHiddenStringField("_parentGlobalId", "Skogkultur global id")
	b.AddHiddenStringField("_title", "Tittel på tiltaket")

	b.SetFieldIdSeries(
		1,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy", //5
	)
	
	b.SetFieldIdSeries(
		8,
		"_workTeamOrgNum",
		"_subCollection",
		"_parentGlobalId", //10
	)
	b.SetFieldIdSeries(
		12,
		"_title",
	)
}
