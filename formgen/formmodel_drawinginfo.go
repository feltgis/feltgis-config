package main

type FormDrawingInfoLabelingFont struct {
	Decoration string `json:"decoration"`
	Family     string `json:"family"`
	Size       int    `json:"size"`
	Style      string `json:"style"`
	Weight     string `json:"weight"`
}

type FormDrawingInfoLabelingInfoSymbol struct {
	Angle               int                         `json:"angle"`
	BackgroundColor     interface{}                 `json:"backgroundColor"`
	BorderLineColor     interface{}                 `json:"borderLineColor"`
	BorderLineSize      interface{}                 `json:"borderLineSize"`
	Color               []int                       `json:"color"`
	Font                FormDrawingInfoLabelingFont `json:"font"`
	HaloColor           []int                       `json:"haloColor"`
	HaloSize            int                         `json:"haloSize"`
	HorizontalAlignment string                      `json:"horizontalAlignment"`
	Kerning             bool                        `json:"kerning"`
	RightToLeft         bool                        `json:"rightToLeft"`
	Type                string                      `json:"type"`
	VerticalAlignment   string                      `json:"verticalAlignment"`
	Xoffset             int                         `json:"xoffset"`
	Yoffset             int                         `json:"yoffset"`
}

type FormDrawingInfoLabelingInfo struct {
	Where           string                             `json:"where,omitempty"`
	LabelExpression string                             `json:"labelExpression"`
	LabelPlacement  string                             `json:"labelPlacement"`
	MaxScale        int                                `json:"maxScale"`
	MinScale        int                                `json:"minScale"`
	Symbol          *FormDrawingInfoLabelingInfoSymbol `json:"symbol,omitempty"`
	UseCodedValues  bool                               `json:"useCodedValues"`
}

type FormDrawingInfoRendererUniqueValueInfoSymbolOutline struct {
	Color []int   `json:"color"`
	Style string  `json:"style,omitempty"`
	Type  string  `json:"type,omitempty"`
	Width float32 `json:"width"`
}

type FormDrawingInfoRendererSymbol struct {
	//outline
	Color   *[]int                                               `json:"color,omitempty"`
	Outline *FormDrawingInfoRendererUniqueValueInfoSymbolOutline `json:"outline,omitempty"`
	Style   *string                                              `json:"style,omitempty"`

	//image
	Angle       *int     `json:"angle,omitempty"`
	ContentType *string  `json:"contentType,omitempty"`
	Height      *float64 `json:"height,omitempty"`
	ImageData   *string  `json:"imageData,omitempty"`
	Type        string   `json:"type"`
	Url         *string  `json:"url,omitempty"`

	Width   *float64 `json:"width,omitempty"`
	XOffset *int     `json:"xoffset,omitempty"`
	YOffset *int     `json:"yoffset,omitempty"`
	Size    *float64 `json:"size,omitempty"`

	Symbol       *FormDrawingInfoRendererSymbol `json:"symbol,omitempty"`
	SymbolLayers []CIMSymbolLayer               `json:"symbolLayers,omitempty"`
}

type CIMSymbolType string

const (
	Point CIMSymbolType = "CIMPointSymbol"
	Line  CIMSymbolType = "CIMLineSymbol"
	Poly  CIMSymbolType = "CIMPolygonSymbol"
)

type CIMLineCapStyle string

const (
	Round CIMLineCapStyle = "Round"
	Butt  CIMLineCapStyle = "Butt"
)

type CIMSymbolLayer struct {
	Type                       string                           `json:"type,omitempty"`
	Enable                     bool                             `json:"enable,omitempty"`
	Width                      float64                          `json:"width,omitempty"`
	Color                      []int                            `json:"color,omitempty"`
	CapStyle                   *string                          `json:"capStyle,omitempty"`
	JoinStyle                  *string                          `json:"joinStyle,omitempty"`
	LineStyle3D                *string                          `json:"lineStyle3D,omitempty"`
	MiterLimit                 *float64                         `json:"miterLimit,omitempty"`
	AnchorPointUnits           *string                          `json:"anchorPointUnits,omitempty"`
	DominantSizeAxis3D         *string                          `json:"dominantSizeAxis3D,omitempty"`
	Size                       *float64                         `json:"size,omitempty"`
	BillboardMode3D            *string                          `json:"billboardMode3D,omitempty"`
	MarkerPlacement            *CIMMarkerPlacementInsidePolygon `json:"markerPlacement,omitempty"`
	Frame                      *CIMFrame                        `json:"frame,omitempty"`
	Effects                    []CIMSymbolEffect                `json:"effects,omitempty"`
	MarkerGraphics             []CIMMarkerGraphic               `json:"markerGraphics,omitempty"`
	ScaleSymbolsProportionally *bool                            `json:"scaleSymbolsProportionally,omitempty"`
	RespectFrame               *bool                            `json:"respectFrame,omitempty"`
}

type CIMMarkerPlacementInsidePolygon struct {
	Type       string  `json:"type"`
	GridType   string  `json:"gridType"`
	OffsetX    float64 `json:"offsetX"`
	OffsetY    float64 `json:"offsetY"`
	Randomness float64 `json:"randomness"`
	Seed       int     `json:"seed,omitempty"`
	StepX      float64 `json:"stepX"`
	StepY      float64 `json:"stepY"`
	Clipping   string  `json:"clipping,omitempty"`
}

type CIMFrame struct {
	XMin float64 `json:"xmin"`
	YMin float64 `json:"ymin"`
	XMax float64 `json:"xmax"`
	YMax float64 `json:"ymax"`
}

type CIMMarkerGraphic struct {
	Type     string           `json:"type"`
	Geometry CIMGeometry      `json:"geometry"`
	Symbol   CIMPolygonSymbol `json:"symbol"`
}

type CIMGeometry struct {
	Rings [][][]float64 `json:"rings"`
}

type CIMPolygonSymbol struct {
	Type         string           `json:"type"`
	SymbolLayers []CIMSymbolLayer `json:"symbolLayers"`
}

type CIMSymbolEffect struct {
	Type            string    `json:"type,omitempty"`
	DashTemplate    []float64 `json:"dashTemplate,omitempty"`
	LineDashEnding  string    `json:"lineEnding,omitempty"`
	OffsetAlongLine float64   `json:"offsetAlongLine,omitempty"`
}

type FormDrawingInfoRendererUniqueValueInfo struct {
	Description string                         `json:"description"`
	Label       string                         `json:"label"`
	Symbol      FormDrawingInfoRendererSymbol  `json:"symbol"`
	CIMSymbol   *FormDrawingInfoRendererSymbol `json:"cimSymbol,omitempty"`
	Value       StringOrInt                    `json:"value"` //can be both string and int!
}

type FormDrawingInfoRenderer struct {
	DefaultLabel     *string                                   `json:"defaultLabel"`
	DefaultSymbol    *FormDrawingInfoRendererSymbol            `json:"defaultSymbol"`
	Field1           *string                                   `json:"field1"`
	Field2           interface{}                               `json:"field2"`
	Field3           interface{}                               `json:"field3"`
	FieldDelimiter   string                                    `json:"fieldDelimiter"`
	NoteToFutureSelf string                                    `json:"note to future self,omitempty"`
	Type             string                                    `json:"type"`
	UniqueValueInfos []*FormDrawingInfoRendererUniqueValueInfo `json:"uniqueValueInfos"`
}

type FormDrawingInfo struct {
	LabelingInfo []*FormDrawingInfoLabelingInfo `json:"labelingInfo"`
	Renderer     *FormDrawingInfoRenderer       `json:"renderer"`
	Transparency *int                           `json:"transparency,omitempty"`
}

func FormDrawingSolidStroke(color []int, width float64) CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:        "CIMSolidStroke",
		Enable:      true,
		Color:       color,
		Width:       width,
		CapStyle:    strRef("Round"),
		JoinStyle:   strRef("Round"),
		LineStyle3D: strRef("Strip"),
		MiterLimit:  f64Ref(10),
	}
}

func FormDrawingSolidFill(color []int) CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:   "CIMSolidFill",
		Enable: true,
		Color:  color,
	}
}

func FormDrawingVectorDotsLayer() CIMSymbolLayer {
	return CIMSymbolLayer{
		Type:               "CIMVectorMarker",
		Enable:             true,
		AnchorPointUnits:   strRef("Relative"),
		DominantSizeAxis3D: strRef("Y"),
		Size:               f64Ref(1.5),
		BillboardMode3D:    strRef("FaceNearPlane"),
		MarkerPlacement: &CIMMarkerPlacementInsidePolygon{
			Type:       "CIMMarkerPlacementInsidePolygon",
			GridType:   "Fixed",
			OffsetX:    3,
			OffsetY:    3,
			Randomness: 100,
			Seed:       13,
			StepX:      3,
			StepY:      3,
			Clipping:   "ClipAtBoundary",
		},
		Frame: &CIMFrame{
			XMin: -5,
			YMin: -5,
			XMax: 5,
			YMax: 5,
		},
		MarkerGraphics: []CIMMarkerGraphic{
			{
				Type: "CIMMarkerGraphic",
				Geometry: CIMGeometry{
					Rings: [][][]float64{
						{
							{-5, 5}, {5, 5}, {5, -5}, {-5, -5}, {-5, 5},
						},
					},
				},
				Symbol: CIMPolygonSymbol{
					Type: "CIMPolygonSymbol",
					SymbolLayers: []CIMSymbolLayer{
						FormDrawingSolidFill([]int{0, 0, 0, 255}), // Black fill
					},
				},
			},
		},
		ScaleSymbolsProportionally: boolRef(true),
		RespectFrame:               boolRef(true),
	}
}

func FormDrawingPolygonWithVectorDots() FormDrawingInfoRendererSymbol {
	return CimSymbol(
		Poly,
		FormDrawingSolidStroke([]int{0, 0, 0, 255}, 0.4), // Solid stroke
		FormDrawingVectorDotsLayer(),                     // Vector dots layer
		FormDrawingSolidFill([]int{255, 255, 255, 255}),  // White fill
	)
}
