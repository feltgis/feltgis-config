package main

func PopulatePSkogkulturForm(b *FormBuilder) {

	//prefix!
	var prefix string = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "forestOwnerName"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 500000,
		XMin: 0,
		YMax: 8000000,
		YMin: 5000000,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_type")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_type", "Type")
	b.AddHiddenStringField("_verifiedContractEnvelopeKey", "Nøkkel i Verified-systemet")
	b.AddHiddenDateField("_verifiedSent_Date", "Tidspunktet kontrakt ble sendt til Verified for signering")
	b.AddHiddenBooleanField("_verifiedSigned_Boolean", "Signert i Verified-systemet")

	b.AddHiddenBooleanField("_completedForForestOwner_Boolean", "Ferdig mot skogeier")
	b.AddHiddenBooleanField("_readyForFinalInvoicingToContractor_Boolean", "Klar til sluttfakturering mot entreprenør")

	b.AddHiddenDateField("_manualSent_Date", "Når denne ble sendt til manuell signering")
	b.AddHiddenDateField("_manualSigned_Date", "Signert manuelt")
	b.AddHiddenStringField("_job_nrs", "Ordrenummer. Ett nummer per tiltakstype (dvs antall unike nummer er likt antall tiltakstyper i ordren)")
	b.AddHiddenStringField("_headId", "Innkjøpsordrens headId")

	b.AddHiddenBooleanField("_contractIncludedInPurchaseOrder_Boolean", "Kontrakt er håndtert av tilkoblet IO")

	b.AddHiddenStringField("_"+prefix+"PForhandsryddingGeom_status", "Framdrift forhåndsrydding")
	b.AddHiddenStringField("_"+prefix+"PGrofterenskGeom_status", "Framdrift grøfterensk")
	b.AddHiddenStringField("_"+prefix+"PMarkberedningGeom_status", "Framdrift markberedning")
	b.AddHiddenStringField("_"+prefix+"PPlantingGeom_status", "Framdrift planting")
	b.AddHiddenStringField("_"+prefix+"PUngskogpleieGeom_status", "Framdrift ungskogpleie")
	b.AddHiddenStringField("_"+prefix+"PSaaingGeom_status", "Framdrift såing")
	b.AddHiddenStringField("_"+prefix+"PSkogskadeGeom_status", "Framdrift skogskade")
	b.AddHiddenStringField("_"+prefix+"PInfrastrukturGeom_status", "Framdrift infrastruktur")
	b.AddHiddenStringField("_"+prefix+"PGjodslingGeom_status", "Framdrift gjødsling")
	b.AddHiddenStringField("_"+prefix+"PSproytingGeom_status", "Framdrift sprøyting")
	b.AddHiddenStringField("_"+prefix+"PGravearbeidGeom_status", "Framdrift gravearbeid")
	b.AddHiddenStringField("_"+prefix+"PSporskaderettingGeom_status", "Framdrift sporskaderetting")
	b.AddHiddenStringField("_"+prefix+"PStammekvistingGeom_status", "Framdrift stammekvisting")

	if !b.IsForestOwner() {
		b.AddHiddenStringField("_"+prefix+"PForhandsryddingGeom_invoice_comment", "Fakturakommentar til forhåndsrydding")
		b.AddHiddenStringField("_"+prefix+"PGrofterenskGeom_invoice_comment", "Fakturakommentar til grøfterensk")
		b.AddHiddenStringField("_"+prefix+"PMarkberedningGeom_invoice_comment", "Fakturakommentar til markberedning")
		b.AddHiddenStringField("_"+prefix+"PPlantingGeom_invoice_comment", "Fakturakommentar til planting")
		b.AddHiddenStringField("_"+prefix+"PUngskogpleieGeom_invoice_comment", "Fakturakommentar til ungskogpleie")
		b.AddHiddenStringField("_"+prefix+"PSaaingGeom_invoice_comment", "Fakturakommentar til såing")
		b.AddHiddenStringField("_"+prefix+"PSkogskadeGeom_invoice_comment", "Fakturakommentar til skogskade")
		b.AddHiddenStringField("_"+prefix+"PInfrastrukturGeom_invoice_comment", "Fakturakommentar til infrastruktur")
		b.AddHiddenStringField("_"+prefix+"PGjodslingGeom_invoice_comment", "Fakturakommentar til gjødsling")
		b.AddHiddenStringField("_"+prefix+"PSproytingGeom_invoice_comment", "Fakturakommentar til sprøyting")
		b.AddHiddenStringField("_"+prefix+"PGravearbeidGeom_invoice_comment", "Fakturakommentar til gravearbeid")
		b.AddHiddenStringField("_"+prefix+"PSporskaderettingGeom_invoice_comment", "Fakturakommentar til sporskaderetting")
		b.AddHiddenStringField("_"+prefix+"PStammekvistingGeom_invoice_comment", "Fakturakommentar til stammekvisting")
	}

	b.AddHiddenStringField("_"+prefix+"PForhandsryddingGeom_final_comment", "Avsluttende kommentar til forhåndsrydding")
	b.AddHiddenStringField("_"+prefix+"PGrofterenskGeom_final_comment", "Avsluttende kommentar til grøfterensk")
	b.AddHiddenStringField("_"+prefix+"PMarkberedningGeom_final_comment", "Avsluttende kommentar til markberedning")
	b.AddHiddenStringField("_"+prefix+"PPlantingGeom_final_comment", "Avsluttende kommentar til planting")
	b.AddHiddenStringField("_"+prefix+"PUngskogpleieGeom_final_comment", "Avsluttende kommentar til ungskogpleie")
	b.AddHiddenStringField("_"+prefix+"PSaaingGeom_final_comment", "Avsluttende kommentar til såing")
	b.AddHiddenStringField("_"+prefix+"PSkogskadeGeom_final_comment", "Avsluttende kommentar til skogskade")
	b.AddHiddenStringField("_"+prefix+"PInfrastrukturGeom_final_comment", "Avsluttende kommentar til infrastruktur")
	b.AddHiddenStringField("_"+prefix+"PGjodslingGeom_final_comment", "Avsluttende kommentar til gjødsling")
	b.AddHiddenStringField("_"+prefix+"PSproytingGeom_final_comment", "Avsluttende kommentar til sprøyting")
	b.AddHiddenStringField("_"+prefix+"PGravearbeidGeom_final_comment", "Avsluttende kommentar til gravearbeid")
	b.AddHiddenStringField("_"+prefix+"PSporskaderettingGeom_final_comment", "Avsluttende kommentar til sporskaderetting")
	b.AddHiddenStringField("_"+prefix+"PStammekvistingGeom_final_comment", "Avsluttende kommentar til stammekvisting")

	b.AddHiddenDateField("_"+prefix+"PForhandsryddingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, forhåndsrydding")
	b.AddHiddenDateField("_"+prefix+"PGrofterenskGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, grøfterensk")
	b.AddHiddenDateField("_"+prefix+"PMarkberedningGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, markberedning")
	b.AddHiddenDateField("_"+prefix+"PPlantingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, planting")
	b.AddHiddenDateField("_"+prefix+"PUngskogpleieGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, ungskogpleie")
	b.AddHiddenDateField("_"+prefix+"PSaaingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, såing")
	b.AddHiddenDateField("_"+prefix+"PSkogskadeGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, skogskade")
	b.AddHiddenDateField("_"+prefix+"PInfrastrukturGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, infrastruktur")
	b.AddHiddenDateField("_"+prefix+"PGjodslingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, gjødsling")
	b.AddHiddenDateField("_"+prefix+"PSproytingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, sprøyting")
	b.AddHiddenDateField("_"+prefix+"PGravearbeidGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, gravearbeid")
	b.AddHiddenDateField("_"+prefix+"PSporskaderettingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, sporskaderetting")
	b.AddHiddenDateField("_"+prefix+"PStammekvistingGeom_billingSent_Date", "Fakturagunnlag sent til økonomi, stammekvisting")

	b.AddHiddenStringField("_workTeamOrgNumUP", "Orgnum arbeidslag ungskogpleie")
	b.AddHiddenStringField("_workTeamOrgNumFR", "Orgnum arbeidslag forhåndsrydding")
	b.AddHiddenStringField("_workTeamOrgNumMB", "Orgnum arbeidslag markberedning")
	b.AddHiddenStringField("_workTeamOrgNumPL", "Orgnum arbeidslag planting")
	b.AddHiddenStringField("_workTeamOrgNumGR", "Orgnum arbeidslag grøfterensk")
	b.AddHiddenStringField("_workTeamOrgNumSA", "Orgnum arbeidslag såing")
	b.AddHiddenStringField("_workTeamOrgNumSS", "Orgnum arbeidslag skogskade")
	b.AddHiddenStringField("_workTeamOrgNumIS", "Orgnum arbeidslag infrastruktur")
	b.AddHiddenStringField("_workTeamOrgNumGJ", "Orgnum arbeidslag gjødsling")
	b.AddHiddenStringField("_workTeamOrgNumSP", "Orgnum arbeidslag sprøyting")
	b.AddHiddenStringField("_workTeamOrgNumGA", "Orgnum arbeidslag gravearbeid")
	b.AddHiddenStringField("_workTeamOrgNumSR", "Orgnum arbeidslag sporskaderetting")
	b.AddHiddenStringField("_workTeamOrgNumSK", "Orgnum arbeidslag stammekvisting")

	b.AddHiddenStringField("_workTeamIdUP", "Team ID arbeidslag ungskogpleie")
	b.AddHiddenStringField("_workTeamIdFR", "Team ID arbeidslag forhåndsrydding")
	b.AddHiddenStringField("_workTeamIdMB", "Team ID arbeidslag markberedning")
	b.AddHiddenStringField("_workTeamIdPL", "Team ID arbeidslag planting")
	b.AddHiddenStringField("_workTeamIdGR", "Team ID arbeidslag grøfterensk")
	b.AddHiddenStringField("_workTeamIdSA", "Team ID arbeidslag såing")
	b.AddHiddenStringField("_workTeamIdSS", "Team ID arbeidslag skogskade")
	b.AddHiddenStringField("_workTeamIdIS", "Team ID arbeidslag infrastruktur")
	b.AddHiddenStringField("_workTeamIdGJ", "Team ID arbeidslag gjødsling")
	b.AddHiddenStringField("_workTeamIdSP", "Team ID arbeidslag sprøyting")
	b.AddHiddenStringField("_workTeamIdGA", "Team ID arbeidslag gravearbeid")
	b.AddHiddenStringField("_workTeamIdSR", "Team ID arbeidslag sporskaderetting")
	b.AddHiddenStringField("_workTeamIdSK", "Team ID arbeidslag stammekvisting")

	b.AddHiddenStringField("_plantelagerGlobalId", "Plantelager")
	b.AddHiddenStringField("_connectedFeatures", "Tilkoblede oppdrag (json)")

	b.AddHiddenStringField("_"+prefix+"PForhandsryddingGeom_coord", "Koordinator forhåndsrydding")
	b.AddHiddenStringField("_"+prefix+"PGrofterenskGeom_coord", "Koordinator grøfterensk")
	b.AddHiddenStringField("_"+prefix+"PMarkberedningGeom_coord", "Koordinator markberedning")
	b.AddHiddenStringField("_"+prefix+"PPlantingGeom_coord", "Koordinator planting")
	b.AddHiddenStringField("_"+prefix+"PUngskogpleieGeom_coord", "Koordinator ungskogpleie")
	b.AddHiddenStringField("_"+prefix+"PSaaingGeom_coord", "Koordinator såing")
	b.AddHiddenStringField("_"+prefix+"PInfrastrukturGeom_coord", "Koordinator infrastruktur")
	b.AddHiddenStringField("_"+prefix+"PSkogskadeGeom_coord", "Koordinator skogskade")

	b.AddHiddenStringField("_"+prefix+"PGjodslingGeom_coord", "Koordinator gjødsling")
	b.AddHiddenStringField("_"+prefix+"PSproytingGeom_coord", "Koordinator sprøyting")
	b.AddHiddenStringField("_"+prefix+"PGravearbeidGeom_coord", "Koordinator gravearbeid")
	b.AddHiddenStringField("_"+prefix+"PSporskaderettingGeom_coord", "Koordinator sporskaderetting")
	b.AddHiddenStringField("_"+prefix+"PStammekvistingGeom_coord", "Koordinator stammekvisting")

	b.AddSection("")
	b.AddNullableStringField("nickName", "Oppdragskallenavn").
		WithAliasTranslation("en", "Assignment Nickname")
	b.AddNullableMultipleChoiceField("region", "Region").
		WithAliasTranslation("en", "Region").
		WithCodedOption("Nord").
		WithCodedOption("Midt").
		WithCodedOption("Øst").
		WithCodedOption("Sørvest")

	b.AddSection("Skogeier").
		WithTranslation("en", "Forest Owner")
	b.AddRequiredStringField("forestOwnerEmail", "E-post").
		WithAliasTranslation("en", "Email")
	b.AddRequiredStringField("forestOwnerOrgNum", "OrgNum").
		WithAliasTranslation("en", "Organization Number")
	b.AddRequiredStringField("forestOwnerName", "Navn").
		WithAliasTranslation("en", "Name")
	b.AddRequiredStringField("forestOwnerAddress", "Adresse").
		WithAliasTranslation("en", "Address")
	b.AddRequiredStringField("forestOwnerZipCode", "Postnummer").
		WithAliasTranslation("en", "Postal Code")
	b.AddRequiredStringField("forestOwnerCity", "Sted").
		WithAliasTranslation("en", "Place")
	b.AddNullableStringField("levNr", "Leverandørnummer").
		WithAliasTranslation("en", "Supplier Number")
	b.AddRequiredStringField("forestOwnerPhone", "Telefon").
		WithAliasTranslation("en", "Phone")
	b.AddNullableStringField("forestOwnerAddress2", "Adresse").
		WithAliasTranslation("en", "Address")

	b.AddSection("Planlegger").
		WithTranslation("en", "Planlegger")
	b.AddRequiredStringField("plannerEmail", "E-post").
		WithAliasTranslation("en", "Email")
	b.AddRequiredStringField("plannerOrgNum", "OrgNum").
		WithAliasTranslation("en", "Organization Number")
	b.AddRequiredStringField("plannerContactName", "Navn").
		WithAliasTranslation("en", "Name")
	b.AddRequiredStringField("plannerContactPhone", "Telefon").
		WithAliasTranslation("en", "Phone")
	b.AddRequiredStringField("plannerOrgName", "Org navn").
		WithAliasTranslation("en", "Organization Name")

	b.AddSection("Eiendom").
		WithTranslation("en", "Property")
	if b.IsForestOwner() {
		b.AddNullableStringField("muniName", "Kommune").
			WithAliasTranslation("en", "Municipality")
		b.AddNullableIntegerField("muniNum", "Kommunenummer").
			WithAliasTranslation("en", "Municipality Number")
		b.AddNullableIntegerField("gridNum", "Gårdsnummer").
			WithAliasTranslation("en", "Grid number")
		b.AddNullableIntegerField("holdingNum", "Bruksnummer").
			WithAliasTranslation("en", "Holding number")
	} else {
		b.AddRequiredStringField("muniName", "Kommune").
			WithAliasTranslation("en", "Municipality")
		b.AddRequiredIntegerField("muniNum", "Kommunenummer").
			WithAliasTranslation("en", "Municipality Number")
		b.AddRequiredIntegerField("gridNum", "Gårdsnummer").
			WithAliasTranslation("en", "Grid number")
		b.AddRequiredIntegerField("holdingNum", "Bruksnummer").
			WithAliasTranslation("en", "Holding number")
	}

	b.AddSection("Oppdrag").
		WithTranslation("en", "Assignment")
	b.AddNullableStringField("comment", "Ordrebeskrivelse").
		WithAliasTranslation("en", "Order Description").
		WithLength(2048)

	//fieldIds
	b.SetFieldIdSeries(
		1,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_type",
		"_verifiedContractEnvelopeKey",
		"_verifiedSent_Date",
		"_verifiedSigned_Boolean", //9
	)

	b.SetFieldIdSeries(
		11,
		"forestOwnerEmail",
	)

	b.SetFieldIdSeries(
		13,
		"forestOwnerOrgNum",
		"forestOwnerName",
		"forestOwnerAddress",
		"forestOwnerZipCode",
		"forestOwnerCity",
	)

	b.SetFieldIdSeries(
		18,
		"plannerEmail",
		"plannerOrgNum",
		"plannerContactName",
		"plannerContactPhone",
	)

	b.SetFieldIdSeries(
		22,
		"muniName",
		"muniNum",
		"gridNum",
		"holdingNum",
	)

	b.SetFieldIdSeries(
		26,
		"levNr",
	)

	b.SetFieldIdSeries(
		27,
		"_manualSent_Date",
		"_manualSigned_Date",
	)

	b.SetFieldIdSeries(
		29,
		"plannerOrgName",
		"forestOwnerPhone",
	)
	b.SetFieldIdSeries(
		31,
		"_job_nrs",
	)

	b.SetFieldIdSeries(
		33,
		"comment",
	)

	b.SetFieldIdSeries(
		34,
		"_headId",
	)
	b.SetFieldIdSeries(
		35,
		"forestOwnerAddress2",
	)
	b.SetFieldIdSeries(
		36,
		"_contractIncludedInPurchaseOrder_Boolean",
	)

	b.SetFieldIdSeries(
		37,
		"_"+prefix+"PForhandsryddingGeom_status",
		"_"+prefix+"PGrofterenskGeom_status",
		"_"+prefix+"PMarkberedningGeom_status",
		"_"+prefix+"PPlantingGeom_status",
		"_"+prefix+"PUngskogpleieGeom_status",
		"_"+prefix+"PSaaingGeom_status",
	)
	b.SetFieldIdSeries(
		1000, //room for 1000 types
		"_"+prefix+"PGjodslingGeom_status",
		"_"+prefix+"PSproytingGeom_status",
		"_"+prefix+"PGravearbeidGeom_status",
		"_"+prefix+"PSporskaderettingGeom_status",
		"_"+prefix+"PStammekvistingGeom_status",
	)

	b.SetFieldIdSeries(
		80,
		"_"+prefix+"PSkogskadeGeom_status",
		"_"+prefix+"PInfrastrukturGeom_status",
	)

	b.SetFieldIdSeries(
		43,
		"_workTeamOrgNumUP",
		"_workTeamOrgNumFR",
		"_workTeamOrgNumMB",
		"_workTeamOrgNumPL",
		"_workTeamOrgNumGR",
	)

	b.SetFieldIdSeries(
		51,
		"_workTeamOrgNumSA",
	)
	b.SetFieldIdSeries(
		2000, //room for 1000 types
		"_workTeamOrgNumGJ",
		"_workTeamOrgNumSP",
		"_workTeamOrgNumGA",
		"_workTeamOrgNumSR",
		"_workTeamOrgNumSK",
	)

	b.SetFieldIdSeries(
		48,
		"_plantelagerGlobalId",
		"_connectedFeatures",
	)

	b.SetFieldIdSeries(
		50,
		"nickName",
	)

	b.SetFieldIdSeries(
		52,
		"region",
	)

	b.SetFieldIdSeries(
		55,
		"_workTeamOrgNumSS",
		"_workTeamOrgNumIS",
	)

	b.SetFieldIdSeries(
		60,
		"_"+prefix+"PForhandsryddingGeom_coord",
		"_"+prefix+"PGrofterenskGeom_coord",
		"_"+prefix+"PMarkberedningGeom_coord",
		"_"+prefix+"PPlantingGeom_coord",
		"_"+prefix+"PUngskogpleieGeom_coord",
		"_"+prefix+"PSaaingGeom_coord",
		"_"+prefix+"PInfrastrukturGeom_coord",
		"_"+prefix+"PSkogskadeGeom_coord",
	)
	b.SetFieldIdSeries(
		3000, //room for 1000 types
		"_"+prefix+"PGjodslingGeom_coord",
		"_"+prefix+"PSproytingGeom_coord",
		"_"+prefix+"PGravearbeidGeom_coord",
		"_"+prefix+"PSporskaderettingGeom_coord",
		"_"+prefix+"PStammekvistingGeom_coord",
	)

	if !b.IsForestOwner() {
		b.SetFieldIdSeries(
			70,
			"_"+prefix+"PForhandsryddingGeom_invoice_comment",
			"_"+prefix+"PGrofterenskGeom_invoice_comment",
			"_"+prefix+"PMarkberedningGeom_invoice_comment",
			"_"+prefix+"PPlantingGeom_invoice_comment",
			"_"+prefix+"PUngskogpleieGeom_invoice_comment",
			"_"+prefix+"PSaaingGeom_invoice_comment",
			"_"+prefix+"PInfrastrukturGeom_invoice_comment",
			"_"+prefix+"PSkogskadeGeom_invoice_comment",
		)
		b.SetFieldIdSeries(
			4000, //room for 1000 types
			"_"+prefix+"PGjodslingGeom_invoice_comment",
			"_"+prefix+"PSproytingGeom_invoice_comment",
			"_"+prefix+"PGravearbeidGeom_invoice_comment",
			"_"+prefix+"PSporskaderettingGeom_invoice_comment",
			"_"+prefix+"PStammekvistingGeom_invoice_comment",
		)
	}

	b.SetFieldIdSeries(
		90,
		"_"+prefix+"PForhandsryddingGeom_final_comment",
		"_"+prefix+"PGrofterenskGeom_final_comment",
		"_"+prefix+"PMarkberedningGeom_final_comment",
		"_"+prefix+"PPlantingGeom_final_comment",
		"_"+prefix+"PUngskogpleieGeom_final_comment",
		"_"+prefix+"PSaaingGeom_final_comment",
		"_"+prefix+"PInfrastrukturGeom_final_comment",
		"_"+prefix+"PSkogskadeGeom_final_comment",
	)
	b.SetFieldIdSeries(
		5000, //room for 1000 types
		"_"+prefix+"PGjodslingGeom_final_comment",
		"_"+prefix+"PSproytingGeom_final_comment",
		"_"+prefix+"PGravearbeidGeom_final_comment",
		"_"+prefix+"PSporskaderettingGeom_final_comment",
		"_"+prefix+"PStammekvistingGeom_final_comment",
	)

	b.SetFieldIdSeries(110,
		"_workTeamIdUP",
		"_workTeamIdFR",
		"_workTeamIdMB",
		"_workTeamIdPL",
		"_workTeamIdGR",
		"_workTeamIdSA",
		"_workTeamIdSS",
		"_workTeamIdIS")
	b.SetFieldIdSeries(
		6000, //room for 1000 types
		"_workTeamIdGJ",
		"_workTeamIdSP",
		"_workTeamIdGA",
		"_workTeamIdSR",
		"_workTeamIdSK",
	)

	b.SetFieldIdSeries(
		120,
		"_"+prefix+"PForhandsryddingGeom_billingSent_Date",
		"_"+prefix+"PGrofterenskGeom_billingSent_Date",
		"_"+prefix+"PMarkberedningGeom_billingSent_Date",
		"_"+prefix+"PPlantingGeom_billingSent_Date",
		"_"+prefix+"PUngskogpleieGeom_billingSent_Date",
		"_"+prefix+"PSaaingGeom_billingSent_Date",
		"_"+prefix+"PInfrastrukturGeom_billingSent_Date",
		"_"+prefix+"PSkogskadeGeom_billingSent_Date",
	)
	b.SetFieldIdSeries(
		7000, //room for 1000 types
		"_"+prefix+"PGjodslingGeom_billingSent_Date",
		"_"+prefix+"PSproytingGeom_billingSent_Date",
		"_"+prefix+"PGravearbeidGeom_billingSent_Date",
		"_"+prefix+"PSporskaderettingGeom_billingSent_Date",
		"_"+prefix+"PStammekvistingGeom_billingSent_Date",
	)

	b.SetFieldIdSeries(8000,
		"_completedForForestOwner_Boolean",
		"_readyForFinalInvoicingToContractor_Boolean",
	)

	b.WithAutoSortex()
}
