package main

import (
	"fmt"
	"log"
)

type PlantTypeSectionsSource string

const (
	PlantTypeSectionsSource_Planting = PlantTypeSectionsSource("Planting")
	PlantTypeSectionsSource_DoneWork = PlantTypeSectionsSource("DoneWork")
)

//For DoneWork, some fields are also "borrowed" by "Saaing", since "all the fields" are in DoneWork

func PopulatePlantTypeSections(
	b *FormBuilder,
	source PlantTypeSectionsSource,
) {

	var numberOf int
	if b.IsForestOwner() {
		numberOf = 4
	} else {
		numberOf = 2
	}

	for i := 1; i <= numberOf; i++ {
		PopulatePlantTypeSection(b, source, i)
	}
}

func PopulatePlantTypeSection(
	b *FormBuilder,
	source PlantTypeSectionsSource,
	number int, //1-indexed!

) {
	if number < 1 {
		log.Fatalf("PopulatePlantTypeSection is 1-indexed, cant handle %d", number)
	}

	fmt.Printf("PopulatePlantTypeSection:%v", number)

	numberString := fmt.Sprintf("%d", number)

	//section!
	b.AddSection("Plantetype "+numberString).
		WithTranslation("en", "Plant type "+numberString)
	if !b.IsForestOwner() && source == PlantTypeSectionsSource_Planting {
		addPlantTypeSectionField(b, source, "plant_price_Decimal2", number)
	}
	if source == PlantTypeSectionsSource_DoneWork { //ONLY ORDER
		addPlantTypeSectionField(b, source, "proveniens", number)
	}
	addPlantTypeSectionField(b, source, "tree_kind", number)
	addPlantTypeSectionField(b, source, "plant_type", number)
	if source == PlantTypeSectionsSource_Planting {
		addPlantTypeSectionField(b, source, "plant_type_comment", number)
	}

	if source == PlantTypeSectionsSource_Planting { //ONLY ORDER
		addPlantTypeSectionField(b, source, "proveniens", number)
	}

	addPlantTypeSectionField(b, source, "plant_treatment", number)
	if source == PlantTypeSectionsSource_Planting {
		addPlantTypeSectionField(b, source, "plant_treatment_comment", number)
	}
	if source == PlantTypeSectionsSource_Planting { //ALMOST ONLY ORDER
		if b.IsForestOwner() {
			addPlantTypeSectionField(b, source, "refnr_plants", number)
		}
	}
	addPlantTypeSectionField(b, source, "plant_type_percent", number)

	if source == PlantTypeSectionsSource_DoneWork {
		addPlantTypeSectionField(b, source, "num_planted", number)
	}
	if source == PlantTypeSectionsSource_DoneWork {
		addPlantTypeSectionField(b, source, "refnr_plants", number)
	}
}

func addPlantTypeSectionField(
	b *FormBuilder,
	source PlantTypeSectionsSource,
	name string,
	number int, //1-indexed!
) {
	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//
	numberString := fmt.Sprintf("%d", number)

	//a common usable "_X" for number > 1
	numberBitString := ""
	if number > 1 {
		numberBitString = fmt.Sprintf("_%d", number)
	}

	//--------------------------
	//--------------------------
	//--------------------------
	// FIELDS

	var fieldBuilder *FormFieldBuilder

	switch name {
	//--------------------------
	case "plant_price_Decimal2":
		fieldBuilder = b.AddRequiredDecimalField("plant_price"+numberBitString+"_Decimal2", "Pris per plante (eks. mva)", 2).
			WithAliasTranslation("en", "Price per plant (excluding VAT)")
		//--------------------------
	case "tree_kind":
		if number == 1 {
			fieldBuilder = b.AddMultipleChoiceField("tree_kind"+numberBitString, "Treslag").
				WithAliasTranslation("en", "Tree kind").
				WithCodedOption("Gran").
				WithCodedOption("Furu").
				WithCodedOption("Bjørk").
				WithCodedOption("Svartor").
				WithCodedOption("Lerk").
				WithCodedOption("Sitka")
		} else {
			fieldBuilder = b.AddNullableMultipleChoiceField("tree_kind"+numberBitString, "Treslag").
				WithAliasTranslation("en", "Tree kind").
				WithCodedOption("Gran").
				WithCodedOption("Furu").
				WithCodedOption("Bjørk").
				WithCodedOption("Svartor").
				WithCodedOption("Lerk").
				WithCodedOption("Sitka")
		}
		//--------------------------
	case "plant_type":
		switch source {
		case PlantTypeSectionsSource_Planting:
			if b.IsForestOwner() {
				fieldBuilder = b.AddNullableMultipleChoiceField("plant_type"+numberBitString, "Plantetype").
					WithAliasTranslation("en", "Plant type").
					WithSuggestedOption("M60 2år").
					WithSuggestedOption("M95 1år").
					WithSuggestedOption("M95 2år")
			} else {
				fieldBuilder = b.AddNullableMultipleChoiceField("plant_type"+numberBitString, "Plantetype gran").
					WithAliasTranslation("en", "Plant type spruce").
					WithSuggestedOption("M60").
					WithSuggestedOption("M95")
			}
		case PlantTypeSectionsSource_DoneWork:
			if b.IsForestOwner() {
				fieldBuilder = b.AddNullableMultipleChoiceField("plant_type"+numberBitString, "Plantetype").
					WithAliasTranslation("en", "Plant type").
					WithSuggestedOption("M60 2år").
					WithSuggestedOption("M95 1år").
					WithSuggestedOption("M95 2år")
			} else {
				fieldBuilder = b.AddMultipleChoiceField("plant_type"+numberBitString, "Plantetype gran").
					WithAliasTranslation("en", "Plant type spruce").
					WithSuggestedOption("M60").
					WithSuggestedOption("M95")
			}
		}
		//--------------------------
	case "plant_type_comment":
		fieldBuilder = b.AddNullableStringField("plant_type_comment"+numberBitString, "Plantetype, kommentar").
			WithAliasTranslation("en", "Plant type, comment")
		//--------------------------
	case "proveniens":
		fieldBuilder = b.AddNullableStringField("proveniens"+numberBitString, "Proveniens").
			WithAliasTranslation("en", "Provenance")
		//--------------------------
	case "num_planted":
		fieldBuilder = b.AddRequiredIntegerField("num_planted"+numberBitString, "Antall plantede planter").
			WithAliasTranslation("en", "Number of planted plants")
		//--------------------------
	case "plant_treatment":
		if b.IsForestOwner() {
			//TODO: verify this diff?
			switch source {
			case PlantTypeSectionsSource_Planting:
				fieldBuilder = b.AddNullableMultipleChoiceField("plant_treatment"+numberBitString, "Behandling").
					WithAliasTranslation("en", "Treatment").
					WithCodedOption("Voks").
					WithCodedOption("Kjemisk").
					WithCodedOption("Conniflex").
					WithCodedOption("Woodcoat").
					WithCodedOption("Ingen")
			case PlantTypeSectionsSource_DoneWork:
				fieldBuilder = b.AddMultipleChoiceField("plant_treatment"+numberBitString, "Behandling").
					WithAliasTranslation("en", "Treatment").
					WithCodedOption("Voks").
					WithCodedOption("Kjemisk").
					WithCodedOption("Conniflex").
					WithCodedOption("Woodcoat").
					WithCodedOption("Ingen")
			}
		} else {
			fieldBuilder = b.AddMultipleChoiceField("plant_treatment"+numberBitString, "Behandling").
				WithAliasTranslation("en", "Treatment").
				WithCodedOption("Voks").
				WithCodedOption("Kjemisk").
				WithCodedOption("Conniflex").
				WithCodedOption("Woodcoat").
				WithCodedOption("Ingen")
		}
		//--------------------------
	case "plant_treatment_comment":
		fieldBuilder = b.AddNullableStringField("plant_treatment_comment"+numberBitString, "Behandling, kommentar").
			WithAliasTranslation("en", "Treatment, comment")
		//--------------------------
	case "refnr_plants":
		if b.IsFritzøeSkoger() || b.IsFritzøe() {
			//This is a fritzoe-list :-)
			fieldBuilder = b.AddNullableMultipleChoiceField("refnr_plants"+numberBitString, "Referansenummer planter").
				WithAliasTranslation("en", "Reference number for plants").
				WithSuggestedOptionWithCustomName("SO24540", "SO24540 Furu M95 1år CV1").
				WithSuggestedOptionWithCustomName("BU240C1", "BU240C1 Furu M95 1år CV1").
				WithSuggestedOptionWithCustomName("SO24550", "SO24550 Furu M95 1år CV2").
				WithSuggestedOptionWithCustomName("SO24560", "SO24560 Furu M95 1år CV3").
				WithSuggestedOptionWithCustomName("BU240C3", "BU240C3 Furu M95 1år CV3").
				WithSuggestedOptionWithCustomName("BU240SO", "BU240SO Furu M95 1år Sollerön").
				WithSuggestedOptionWithCustomName("BU240MO", "BU240MO Furu M95 1år Mosås").
				WithSuggestedOptionWithCustomName("SO24110W", "SO24110W Gran M95 1år Bastøy").
				WithSuggestedOptionWithCustomName("TE24130", "TE24130 Gran M95 2år Jordtveit").
				WithSuggestedOptionWithCustomName("TE24330", "TE24330 Gran M60 2år Jordtveit").
				WithSuggestedOptionWithCustomName("TE24125", "TE24125 Gran M95 2år Sanderud").
				WithSuggestedOptionWithCustomName("SO24420W", "SO24420W Gran M95 2år Hallen").
				WithSuggestedOptionWithCustomName("TE24135", "TE24135 Gran M95 2år Hallen").
				WithSuggestedOptionWithCustomName("TE24335", "TE24335 Gran M60 2år Hallen").
				WithSuggestedOptionWithCustomName("TE24165", "TE24165 Gran M95 2år Kaupanger S")
		} else {
			fieldBuilder = b.AddNullableStringField("refnr_plants"+numberBitString, "Referansenummer planter").
				WithAliasTranslation("en", "Reference number for plants")
		}
		//--------------------------
	case "plant_type_percent":
		switch source {
		case PlantTypeSectionsSource_Planting:
			fieldBuilder = b.AddNullableIntegerField("plant_type_"+numberString+"_percent", "Prosentfordeling plantetype "+numberString).
				WithAliasTranslation("en", "Percentage distribution of plant type "+numberString)
		case PlantTypeSectionsSource_DoneWork:
			fieldBuilder = b.AddUneditableNullableIntegerField("plant_type_"+numberString+"_percent", "Prosentfordeling plantetype "+numberString).
				WithAliasTranslation("en", "Percent plant type "+numberString)
		}
		//--------------------------
	default:
		log.Fatalf("Unknown field name? %v", name)
		return
	}

	//--------------------------
	//--------------------------
	//--------------------------
	// LOGIC

	fieldLogicBuilder := fieldBuilder.WithLogic()

	thisTreeKindExistsLogic := FormFieldLogicField{
		Operator:       "exists",
		OtherFieldName: "tree_kind" + numberBitString,
	}

	switch source {
	case PlantTypeSectionsSource_Planting:

		//not relevant if forestowners
		var showIfNotPlantingLogic FormFieldLogicField
		if b.IsForestOwner() {
			showIfNotPlantingLogic = FormFieldLogicField{
				OtherFieldName:        "_type",
				Operator:              "!=",
				OtherFieldStringValue: strRef("AlwaysTrueSinceTypeCanNeverBeThis"),
			}
		} else {
			showIfNotPlantingLogic = FormFieldLogicField{
				OtherFieldName:        "_type",
				Operator:              "!=",
				OtherFieldStringValue: strRef("Planting"),
			}
		}

		//only show X if X-1 exists
		prevNumberBitString := ""
		if number > 2 { //dont add 1 to 2
			prevNumberBitString = fmt.Sprintf("_%d", number-1)
		}
		prevTreeKindExistsLogic := FormFieldLogicField{
			Operator:       "exists",
			OtherFieldName: "tree_kind" + prevNumberBitString,
		}

		switch name {
		//--------------------------
		case "plant_price_Decimal2":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "tree_kind":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_type":
			if b.IsForestOwner() {
				if number == 1 {
					//nada
				} else {
					fieldLogicBuilder.WithVisibleIfAllOf(prevTreeKindExistsLogic, thisTreeKindExistsLogic)
				}
			} else {

				isTreeKindSprouceLogic := FormFieldLogicField{
					Operator:              "contains",
					OtherFieldName:        "tree_kind" + numberBitString,
					OtherFieldStringValue: strRef("Gran"),
				}
				if number == 1 {
					fieldLogicBuilder.WithVisibleIfAllOf(isTreeKindSprouceLogic, showIfNotPlantingLogic)
				} else {
					fieldLogicBuilder.WithVisibleIfAllOf(isTreeKindSprouceLogic, showIfNotPlantingLogic, prevTreeKindExistsLogic)
				}

				fieldLogicBuilder.
					WithSetValue(FormFieldLogicSetValue{
						IfValueIs: FormFieldLogicIfValueIs{
							Field: &FormFieldLogicField{
								Operator:              "!=",
								OtherFieldName:        "tree_kind" + numberBitString,
								OtherFieldStringValue: strRef("Gran"),
							},
						},
						ThenSetValue: FormFieldLogicSetValueThenSetValue{
							StringValue: strRef(""),
						},
					})
			}
			//--------------------------
		case "plant_type_comment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "proveniens":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_treatment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_treatment_comment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(showIfNotPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "refnr_plants":
			if number == 1 {
				//nada
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_type_percent":

			//percentages should only show if we have 2+ types! Simplest to just test if exactly 2 exists!
			treeKind2ExistsLogic := FormFieldLogicField{
				Operator:       "exists",
				OtherFieldName: "tree_kind_2",
			}
			fmt.Printf("plant_type_percent %v", treeKind2ExistsLogic)

			if number == 1 {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, treeKind2ExistsLogic)
			} else if number == 2 || number == 3 {
				//so this is funky, 2 and 3 already includes 'treeKind2ExistsLogic' in prevTreeKindExistsLogic or thisTreeKindExistsLogic
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(showIfNotPlantingLogic, prevTreeKindExistsLogic, thisTreeKindExistsLogic, treeKind2ExistsLogic)
			}
			//--------------------------
		default:
			log.Fatalf("'%v' is unknown for source '%v'", name, source)
			return
		}
	case PlantTypeSectionsSource_DoneWork:

		//såing is in here and borrows a couple of fields...
		subCollectionIsSaaingLogic := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PSaaingGeom"),
		}

		subCollectionIsPlantingLogic := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PPlantingGeom"),
		}

		switch name {
		//--------------------------
		case "plant_price_Decimal2":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(thisTreeKindExistsLogic, subCollectionIsPlantingLogic)
			}
			//--------------------------
		case "tree_kind":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfAnyOf(subCollectionIsPlantingLogic, subCollectionIsSaaingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_type":

			if b.IsForestOwner() {
				if number == 1 {
					fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
				} else {
					fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
				}
			} else {

				isTreeKindSprouceLogic := FormFieldLogicField{
					Operator:              "contains",
					OtherFieldName:        "tree_kind" + numberBitString,
					OtherFieldStringValue: strRef("Gran"),
				}
				if number == 1 {
					fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, isTreeKindSprouceLogic)
				} else {
					fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, isTreeKindSprouceLogic, thisTreeKindExistsLogic)
				}
			}
			//--------------------------
		case "plant_type_comment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "proveniens":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfAnyOf(subCollectionIsPlantingLogic, subCollectionIsSaaingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "num_planted":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_treatment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_treatment_comment":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "refnr_plants":
			if number == 1 {
				fieldLogicBuilder.WithVisibleIfField(subCollectionIsPlantingLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			}
			//--------------------------
		case "plant_type_percent":

			//percentages should only show if we have 2+ types! Simplest to just test if exactly 2 exists!
			treeKind2ExistsLogic := FormFieldLogicField{
				Operator:       "exists",
				OtherFieldName: "tree_kind_2",
			}

			if number == 1 {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, treeKind2ExistsLogic)
			} else if number == 2 || number == 3 {
				//so this is funky, 2 and 3 already includes 'treeKind2ExistsLogic' in prevTreeKindExistsLogic or thisTreeKindExistsLogic
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic)
			} else {
				fieldLogicBuilder.WithVisibleIfAllOf(subCollectionIsPlantingLogic, thisTreeKindExistsLogic, treeKind2ExistsLogic)
			}
			//--------------------------
		default:
			log.Fatalf("'%v' is unknown for source '%v'", name, source)
			return
		}
	default:
		log.Fatalf("Unknown source %v", source)
		return
	}

	//--------------------------
	//--------------------------
	//--------------------------
	//IDS!

	baseId := 0 //for 1 and 2
	if number > 2 {
		baseId = (number * 1000) //and then 3 is same as 2 + 3000, then 4 is the same as 2 + 4000 etc...
	}

	switch source {
	case PlantTypeSectionsSource_Planting:
		switch name {
		//--------------------------
		case "plant_price_Decimal2":
			if number == 1 {
				b.SetFieldIdSeries(baseId+21, "plant_price"+numberBitString+"_Decimal2")
			} else {
				b.SetFieldIdSeries(baseId+60, "plant_price"+numberBitString+"_Decimal2")
			}
			//--------------------------
		case "tree_kind":
			if number == 1 {
				b.SetFieldIdSeries(baseId+10, name)
			} else {
				b.SetFieldIdSeries(baseId+67, name+numberBitString)
			}
			//--------------------------
		case "plant_type":
			if number == 1 {
				b.SetFieldIdSeries(baseId+13, name)
			} else {
				b.SetFieldIdSeries(baseId+61, name+numberBitString)
			}
			//--------------------------
		case "plant_type_comment":
			if number == 1 {
				b.SetFieldIdSeries(baseId+44, name)
			} else {
				b.SetFieldIdSeries(baseId+62, name+numberBitString)
			}
			//--------------------------
		case "proveniens":
			if number == 1 {
				b.SetFieldIdSeries(baseId+54, name)
			} else {
				b.SetFieldIdSeries(baseId+65, name+numberBitString)
			}
			//--------------------------
		case "plant_treatment":
			if number == 1 {
				b.SetFieldIdSeries(baseId+16, name)
			} else {
				b.SetFieldIdSeries(baseId+63, name+numberBitString)
			}
			//--------------------------
		case "plant_treatment_comment":
			if number == 1 {
				b.SetFieldIdSeries(baseId+45, name)
			} else {
				b.SetFieldIdSeries(baseId+64, name+numberBitString)
			}
			//--------------------------
		case "refnr_plants":
			if number == 1 {
				b.SetFieldIdSeries(baseId+50, name)
			} else {
				b.SetFieldIdSeries(baseId+500, name+numberBitString)
			}
			//--------------------------
		case "plant_type_percent":
			if number == 1 {
				b.SetFieldIdSeries(baseId+69, "plant_type_"+numberString+"_percent")
			} else {
				b.SetFieldIdSeries(baseId+68, "plant_type_"+numberString+"_percent")
			}
			//--------------------------
		default:
			log.Fatalf("'%v' is unknown for source '%v'", name, source)
			return
		}
	case PlantTypeSectionsSource_DoneWork:
		switch name {
		//--------------------------
		case "tree_kind":
			if number == 1 {
				b.SetFieldIdSeries(baseId+21, name)
			} else {
				b.SetFieldIdSeries(baseId+31, name+numberBitString)
			}
			//--------------------------
		case "plant_type":
			if number == 1 {
				b.SetFieldIdSeries(baseId+22, name)
			} else {
				b.SetFieldIdSeries(baseId+32, name+numberBitString)
			}
			//--------------------------
		case "proveniens":
			if number == 1 {
				//Super legacy random diff in ids...
				if b.IsForestOwner() || b.IsAltiskog() || b.IsValdresSkog() {
					b.SetFieldIdSeries(baseId+54, name)
				} else {
					b.SetFieldIdSeries(baseId+24, name)
				}
			} else {
				b.SetFieldIdSeries(baseId+34, name+numberBitString)
			}
			//--------------------------
		case "num_planted":
			if number == 1 {
				b.SetFieldIdSeries(baseId+25, name)
			} else {
				b.SetFieldIdSeries(baseId+35, name+numberBitString)
			}
			//--------------------------
		case "plant_treatment":
			if number == 1 {
				b.SetFieldIdSeries(baseId+23, name)
			} else {
				b.SetFieldIdSeries(baseId+33, name+numberBitString)
			}
			//--------------------------
		case "refnr_plants":
			if number == 1 {
				if b.IsForestOwner() {
					b.SetFieldIdSeries(baseId+50, name)
				} else {
					b.SetFieldIdSeries(baseId+26, name)
				}
			} else {
				b.SetFieldIdSeries(baseId+36, name+numberBitString)
			}
			//--------------------------
		case "plant_type_percent":
			if number == 1 {
				b.SetFieldIdSeries(baseId+69, "plant_type_"+numberString+"_percent")
			} else {
				b.SetFieldIdSeries(baseId+68, "plant_type_"+numberString+"_percent")
			}
			//--------------------------
		default:
			log.Fatalf("'%v' is unknown for source '%v'", name, source)
			return
		}
	default:
		log.Fatalf("Unknown source %v", source)
	}
}
