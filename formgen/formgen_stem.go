package main

func PopulateStemForm(b *FormBuilder) {
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_bucket"
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.company.Prefix + "Stem"
	b.form.ObjectIdField = "OBJECTID"
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_bucket")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}

	// Add drawing info
	di := b.AddDrawingInfo().
		WithTransparency(0)
	renderer := di.
		WithRenderer("_bucket")

	// Define common color for all values
	color := []int{143, 0, 121, 255}

	// Define unique value infos
	values := []struct {
		Value string
		Size  float64
	}{
		{"200", 39.89422804014327},
		{"100", 28.209479177387813},
		{"90", 26.76186174229157},
		{"80", 25.231325220201605},
		{"70", 23.60174359706574},
		{"60", 21.850968611841584},
		{"50", 19.947114020071634},
		{"40", 17.841241161527712},
		{"30", 15.450968080927582},
		{"20", 12.615662610100802},
		{"10", 8.920620580763856},
		{"5", 6.307831305050401},
		{"4", 5.641895835477563},
		{"3", 4.886025119029199},
		{"2", 3.989422804014327},
		{"1", 2.8209479177387813},
		{"0", 0},
	}
	// Add each unique value to the renderer
	for _, v := range values {
		uvi := renderer.WithUniqueValueInfo(v.Value, v.Value)
		uvi.uniqueValueInfo.Description = ""
		uvi.uniqueValueInfo.Symbol = FormDrawingInfoRendererSymbol{
			Color: &color,
			Outline: &FormDrawingInfoRendererUniqueValueInfoSymbolOutline{
				Color: []int{0, 0, 0, 255},
				Width: 1,
			},
			Style: strRef("esriSMSCircle"),
			Type:  "esriSMS",
			Size:  f64Ref(v.Size),
		}
		uvi.uniqueValueInfo.Value = StringOrInt{StringValue: strRef(v.Value)}
	}

	// Fields
	b.AddHiddenStringField("_globalId", "GlobalID")
	b.AddHiddenDateField("_created_Date", "CreatedDate")
	b.AddHiddenStringField("_createdBy", "CreatedBy")
	b.AddHiddenDateField("_modified_Date", "ModifiedDate")
	b.AddHiddenStringField("_modifiedBy", "ModifiedBy")
	b.AddHiddenStringField("_headId", "HeadID")
	b.AddHiddenStringField("_orderId", "ContractorOrderID")
	b.AddHiddenStringField("_serviceOrderId", "ServiceOrderID")
	b.AddUneditableRequiredStringField("orderNum", "PurchaseOrderNum")
	b.AddHiddenStringField("_type", "Type")
	b.AddUneditableRequiredIntegerField("count", "Antall stammer")

	displayField := b.AddHiddenStringField("_bucket", "Ulik styling for buckets 500,100,50,10,5,1")
	// By accident the field was created as hidden (_), but it has to be editable when used by the renderer
	displayField.field.Editable = true

	b.AddHiddenStringField("_volumeMap_JSON", "JSON serialisert volum map assNum(int):volum(float)")
	b.AddHiddenIntegerField("_numLogs", "Antall stokker")
	b.AddHiddenDoubleField("_volume", "Volum")
	b.AddHiddenIntegerField("_volumeBucket", "Volum bucket")
	b.AddHiddenNullableStringField("_fileName", "Filnavn")
	b.AddHiddenDoubleField("_avgStemVolume", "Gjennomsnittlig stammevolum")
	b.AddHiddenBooleanField("_shareStemPosition_Boolean", "Del stammeposisjon")

	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_headId",
		"_orderId",
		"_serviceOrderId",
		"orderNum",
	)
	b.SetFieldIdSeries(
		10,
		"_type",
		"count",
		"_bucket",
		"_volumeMap_JSON",
		"_numLogs",
		"_volume",
		"_volumeBucket",
		"_fileName",
		"_avgStemVolume",
		"_shareStemPosition_Boolean",
	)
	b.WithAutoSortex()
}
