package main

import (
	"log"
	"testing"
)

func TestCompanies(t *testing.T) {
	for _, company := range allCompanies {
		log.Printf("Company '%v'", company.Name)
		for _, companyFeature := range company.Features {
			log.Printf("- Feature '%v'", companyFeature)

			if companyFeature == hasPlanner {
				if company.hasFeature(hasPlannerInTestOnly) {
					t.Fatal("Can't have both hasPlanner & hasPlannerInTestOnly")
				}
			}

			if companyFeature == hasSkogfond {
				if !company.hasFeature(hasPlanner) {
					t.Fatal("Skogfond without planner?")
				}
			}
		}
	}
}

func TestCompanyFeatures(t *testing.T) {
	for _, companyFeature := range allCompanyFeatures {
		log.Printf("Company feature '%v'", companyFeature)
		var hasFeatureCompanyNames = []string{}
		var hasNotFeatureCompanyNames = []string{}
		for _, company := range allCompanies {
			if company.hasFeature(companyFeature) {
				hasFeatureCompanyNames = append(hasFeatureCompanyNames, company.Name)
			} else {
				hasNotFeatureCompanyNames = append(hasNotFeatureCompanyNames, company.Name)
			}

			if company.hasFeature(hasPlanner) && company.hasFeature(hasPlannerInTestOnly) {
				t.Fatal("Can't have both hasPlanner & hasPlannerInTestOnly")
			}
		}
		log.Printf("- Companies with feature: '%v'", hasFeatureCompanyNames)
		log.Printf("- Companies without feature: '%v'", hasNotFeatureCompanyNames)

		//test coorelations between features
		for _, otherCompanyFeature := range allCompanyFeatures {
			if companyFeature != otherCompanyFeature {
				var has = false
				var hasNot = false
				for _, company := range allCompanies {
					if company.hasFeature(companyFeature) {
						if company.hasFeature(otherCompanyFeature) {
							has = true
						} else {
							hasNot = true
						}
					}
				}
				if has && hasNot {
					log.Printf("- Some overlap with '%v'", otherCompanyFeature)
				} else if has {
					log.Printf("- All overlap with '%v'", otherCompanyFeature)
				} else if hasNot {
					log.Printf("- No overlap with '%v'", otherCompanyFeature)
				}
			}
		}
	}
}
