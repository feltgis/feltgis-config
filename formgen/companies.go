package main

import "slices"

type Company struct {
	Name         string           `json:"name"`
	OrgNum       string           `json:"orgNum"`
	Prefix       string           `json:"prefix"`
	ConfigName   string           `json:"configName"`
	ReportFolder string           `json:"reportFolder"`
	Features     []CompanyFeature `json:"features"`
	ExcludeFiles []string         `json:"excludeFiles,omitempty"` //Files currently not handled, that should be handled!
}

type CompanyFeature string

func (c *Company) hasFeature(feature CompanyFeature) bool {
	if c == nil {
		return false
	}
	return slices.Contains(c.Features, feature)
}
func (c *Company) isCompany(companies ...*Company) bool {
	for _, company := range companies {
		if c.Name == company.Name {
			return true
		}
	}
	return false
}

var (
	//apps
	hasPlanner           CompanyFeature = "planner"
	hasPlannerInTestOnly CompanyFeature = "plannerTestOnly"
	hasFeltlogg          CompanyFeature = "feltlogg"
	hasFeltkultur        CompanyFeature = "feltkultur"

	hasSkogfond CompanyFeature = "skogfond" //TODO: remove, should be given to all?

	//groups
	isGroupAlma    CompanyFeature = "alma"
	isGroupGlommen CompanyFeature = "glommen"

	//companyType
	isForestOwner CompanyFeature = "forestOwner"
	isContractor  CompanyFeature = "contractor"

	hasVsys CompanyFeature = "hasVsys"
)

var allCompanyFeatures = []CompanyFeature{
	hasPlanner,
	hasPlannerInTestOnly,
	hasFeltlogg,

	hasSkogfond,

	isGroupAlma,
	isGroupGlommen,
	isForestOwner,
	isContractor,
}

var (
	company_Altiskog = &Company{
		Name:         "Altiskog",
		OrgNum:       "923553029",
		Prefix:       "AI",
		ConfigName:   "altiskog",
		ReportFolder: "altiskog",
		Features:     []CompanyFeature{isContractor, hasPlanner, hasFeltlogg, hasFeltkultur},
	}
	company_Cappelen = &Company{
		Name:         "Cappelen",
		OrgNum:       "979200048",
		Prefix:       "CA",
		ConfigName:   "cappelen",
		ReportFolder: "cappelen",
		Features:     []CompanyFeature{isForestOwner, hasPlanner, hasFeltlogg, hasVsys, hasFeltkultur},
	}
	company_Fjordtømmer = &Company{
		Name:         "Fjordtømmer",
		OrgNum:       "922689016",
		Prefix:       "FT",
		ConfigName:   "fjordtommer",
		ReportFolder: "fjordtommer",
		Features:     []CompanyFeature{hasPlanner, hasFeltlogg, hasSkogfond, hasVsys, hasFeltkultur},
		ExcludeFiles: []string{
			"FTMarkberedningTelling", //TODO: exists, but is not up to date
			"FTPlantingTelling",      //TODO: exists, but is not up to date
			"FTUngskogpleieTelling",  //TODO: exists, but is not up to date
		},
	}
	company_Fritzøe = &Company{
		Name:         "Fritzøe",
		OrgNum:       "922965404",
		Prefix:       "FZ",
		ConfigName:   "fritzoe", //sub-set of fritzoeskoger
		ReportFolder: "fritzoe",
		Features:     []CompanyFeature{isForestOwner, hasPlanner, hasFeltlogg, hasSkogfond, hasVsys, hasFeltkultur},
	}
	company_FritzøeSkoger = &Company{
		Name:         "Fritzøe Skoger",
		OrgNum:       "928838188",
		Prefix:       "FS",
		ConfigName:   "fritzoeskoger",
		ReportFolder: "fritzoeskoger",
		Features:     []CompanyFeature{isForestOwner, hasPlanner, hasFeltlogg, hasSkogfond, hasVsys, hasFeltkultur},
	}
	company_GlommenMjosen = &Company{
		Name:         "Glommenmjosen",
		OrgNum:       "988983659",
		Prefix:       "GM",
		ConfigName:   "glommenmjosen",
		ReportFolder: "glommenmjosen",
		Features:     []CompanyFeature{isGroupAlma, isGroupGlommen, hasPlanner, hasFeltlogg, hasVsys, hasFeltkultur},
		ExcludeFiles: []string{ //TODO: Why are these files missing?
			"GMPCustomer",
		},
	}
	company_GlommenSkog = &Company{
		Name:         "Glommenskog",
		OrgNum:       "941306594",
		Prefix:       "GS",
		ConfigName:   "glommenskog",
		ReportFolder: "glommenskog",
		Features:     []CompanyFeature{isGroupAlma, isGroupGlommen, hasPlannerInTestOnly, hasFeltlogg, hasVsys, hasFeltkultur},
		ExcludeFiles: []string{ //TODO: Why are these files missing?
			"GSDoneWork",
			"GSMarkberedningTelling",
			"GSPCustomer",
			"GSPFeltkontrollPlanting",
			"GSPlantingTelling",
			"GSPSaaingGeom",
			"GSUngskogpleieTelling",
		},
	}
	company_LøvenskioldFossum = &Company{
		Name:         "Løvenskiold Fossum",
		OrgNum:       "987868821",
		Prefix:       "LF",
		ConfigName:   "lfossum",
		ReportFolder: "lfossum",
		Features:     []CompanyFeature{isForestOwner, hasPlanner, hasFeltlogg, hasVsys, hasFeltkultur},
	}
	company_MathiesenEidsvoldVærk = &Company{
		Name:         "Mathiesen Eidsvold Værk",
		OrgNum:       "931073737",
		Prefix:       "ME",
		ConfigName:   "mev",
		ReportFolder: "mev",
		Features:     []CompanyFeature{isForestOwner, hasPlanner, hasFeltlogg, hasFeltkultur},
	}
	company_Nortømmer = &Company{
		Name:         "Nortømmer",
		OrgNum:       "980018709",
		Prefix:       "NT",
		ConfigName:   "nortommer",
		ReportFolder: "nortommer",
		Features:     []CompanyFeature{hasPlanner, hasFeltlogg, hasSkogfond, hasVsys, hasFeltkultur},
	}
	company_StoraEnso = &Company{
		Name:         "Stora Enso",
		OrgNum:       "938124337",
		Prefix:       "SE",
		ConfigName:   "storaenso",
		ReportFolder: "storaenso",
		Features:     []CompanyFeature{hasPlanner, hasFeltlogg, hasVsys, hasFeltkultur},
		ExcludeFiles: []string{ //TODO: Why are these files missing?
			"SEMarkberedningTelling",
			"SEPlantingTelling",
			"SEUngskogpleieTelling",
			"SEEgenkontroll",
		},
	}
	company_StatSkog = &Company{
		Name:         "Stat Skog",
		OrgNum:       "966056258",
		Prefix:       "SS",
		ConfigName:   "statskog",
		ReportFolder: "statskog",
		Features:     []CompanyFeature{hasFeltlogg},
		ExcludeFiles: []string{
			"SSRegistreringerPunkt", //Stat Skog only use feltlogg for import of files!
			"SSRegistreringerLinjer",
			"SSRegistreringerFlater",
			"SSEgenkontroll",
			"SSEgenkontrollHogst",
			"SSEgenkontrollLass",
			"SSForeVar",
			"SSOrg",
			"SSSKOrgConfig",
		},
	}
	company_ValdresSkog = &Company{
		Name:         "Valdres Skog",
		OrgNum:       "912493350",
		Prefix:       "VS",
		ConfigName:   "valdresskog",
		ReportFolder: "valdresskog",
		Features:     []CompanyFeature{isContractor, hasPlanner, hasFeltlogg, hasFeltkultur},
	}

	/* //NO LONGER A CUSTOMER!
	company_SBSkog = Company{
		Name:         "Sbskog",
		OrgNum:       "",
		Prefix:       "SB",
		ConfigName:   "sbskog",
		ReportFolder: "sbskog",
		Features:     []CompanyFeature{hasPlannerInTestOnly},
	}
	*/
	/* //NO LONGER A CUSTOMER!
	company_Allskog = Company{
		Name:         "Allskog",
		OrgNum:       "989140108",
		Prefix:       "AS",
		ConfigName:   "allskog",
		ReportFolder: "allskog",
		Features:     []CompanyFeature{isGroupAlma, hasFeltlogg},
	}
	*/
)

var allCompanies = []*Company{
	company_Altiskog,
	company_ValdresSkog,
	company_Cappelen,
	company_Fjordtømmer,
	company_Fritzøe,
	company_FritzøeSkoger,
	company_GlommenMjosen,
	company_GlommenSkog,
	company_LøvenskioldFossum,
	company_MathiesenEidsvoldVærk,
	company_Nortømmer,
	company_StoraEnso,
	company_StatSkog,
	company_ValdresSkog,

	//company_Allskog, //NO LONGER A CUSTOMER!
	//company_SBSkog, //NO LONGER A CUSTOMER!
}

// https://register.stg.vsys.no/RegisterSrv/api/party/GetVirkeshandelApiEndpoint
var companiesWithVSYS = []*Company{
	company_Cappelen,
	company_Fjordtømmer,
	company_Fritzøe,
	company_FritzøeSkoger,
	company_GlommenMjosen,
	company_GlommenSkog,
	company_LøvenskioldFossum,
	company_Nortømmer,
	company_StoraEnso,
}
