package main

func PopulatePJobTypeGeomExtGmForm(b *FormBuilder) {
	//find form type
	formType := findFormType(b)

	//common...
	populatePJobTypeGeomFormCommon(b, formType)
	populatePJobTypeGeomFormDrawingInfo(b, formType)
	populatePJobTypeGeomFormTypes(b, formType)

	populatePJobTypeGeomFormHiddenFields(b, formType)

	// Visible fields, should we move this into a separate function?
	b.AddUneditableNullableStringField("bestand_str", "Bestand").
		WithAliasTranslation("en", "Stand")
	b.SetFieldIdSeries(100, "bestand_str")

	//dont think we need more, since we dont really use the geom, only the DoneWork for GmExt

	b.WithAutoSortex()
}
