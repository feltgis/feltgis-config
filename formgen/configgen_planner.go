package main

import (
	"strings"
)

func PopulatePlannerConfig(b *ConfigBuilder) {
	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//------------------------------------------------------------------------------------
	//common
	//------------------------------------------------------------------------------------
	b.config.Map.FreeHandPointDistance = intRef(4)
	if b.IsProd() {
		b.config.Name = "Planner " + prefix
	} else if b.IsBeta() {
		b.config.Name = "Planner " + prefix + " beta"
	} else if b.IsDev() {
		b.config.Name = "Planner " + prefix + " dev"
	}
	if b.IsNortømmer() {
		b.config.SellerContactEMailCopyMeasuringReceipt = boolRef(true)
	} else if b.IsFjordtømmer() {
		b.config.FillInSellerEmail = boolRef(true)
	}

	//------------------------------------------------------------------------------------
	//superuser
	//------------------------------------------------------------------------------------
	b.AddSuperUser("admin@feltgis.no")
	b.AddSuperUser("kristoffer@feltgis.no")
	if b.IsNortømmer() {
		b.AddSuperUser("ms@nortommer.no")
		b.AddSuperUser("frode.fjertoft@nortommer.no")
		b.AddSuperUser("nina.lilleeng@nortommer.no")
	} else if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddSuperUser("cs@fritzoeskoger.no")
		b.AddSuperUser("cm@fritzoeskoger.no")
		b.AddSuperUser("gb@fritzoeskoger.no")
		b.AddSuperUser("sb@fritzoeskoger.no")
		b.AddSuperUser("ks@fritzoeskoger.no")
	} else if b.IsLøvenskioldFossum() {
		b.AddSuperUser("knutv@l-fossum.no")
	} else if b.IsCappelen() {
		b.AddSuperUser("thor.wraa@ulefos.com")
	} else if b.IsMathiesen() {
		b.AddSuperUser("per.almqvist@mev.no")
		b.AddSuperUser("+4710000011")
	}

	//------------------------------------------------------------------------------------
	//featureNotifications
	//------------------------------------------------------------------------------------
	if b.IsNortømmer() {
		// Silvi culture started SMS / emails
		{
			receivers := MailReceivers{
				{
					Name:  "Kristoffer Stenersen",
					Email: "kristoffer@feltgis.no",
				},
				{
					Name:  "Martin Storberget",
					Email: "ms@nortommer.no",
				},
				{
					Name:  "Inger-Helene Onsager",
					Email: "iho@nortommer.no",
				},
			}

			template := "Hei {{forestOwnerName}}! Vi starter innen kort tid med skogkulturoppdraget {{jobType}} {{jobNr}} " +
				"hos deg ihht. avtale med din Virkeskjøper 🌲 NB: Har du låst bom på skogsvei inn til oppdraget må denne " +
				"stå åpen mens oppdraget pågår. Mvh Nortømmer."

			b.WithFeatureNotification(
				b.company.Prefix+"SluttrapportWorkStartedSMS",
				template,
				receivers,
				false,
			)
		}

		// Order with regulated origin activated
		{
			receivers := MailReceivers{
				{
					Name:  "Kristoffer Stenersen",
					Email: "kristoffer@feltgis.no",
				},
				{
					Name:  "Martin Storberget",
					Email: "ms@nortommer.no",
				},
				{
					Name:  "Narve Opsahl",
					Email: "narve.opsahl@nortommer.no",
				},
			}

			b.WithFeatureNotification(
				b.company.Prefix+"POrdreRegulatedOriginEmail",
				"IO {{head.OrderNum}} {{head.SellerName}} aktivert med virke fra omdisponert område",
				receivers,
				false,
			)
		}

	}

	//------------------------------------------------------------------------------------
	//environmentClearedEmails
	//------------------------------------------------------------------------------------
	if b.IsNortømmer() {
		b.AddEnvironmentClearedEmails(ConfigEnvironmentClearedEmails{
			To: []ConfigUserEmail{},
			CC: []ConfigUserEmail{
				{Name: "Nina Lilleeng", Email: "ntl@nortommer.no"},
			},
			BCC: []ConfigUserEmail{
				{Name: "Kristoffer Stenersen", Email: "kristoffer.stenersen@gmail.com"},
				{Name: "Martin Storberget", Email: "ms@nortommer.no"},
			},
			Subject:  "Utdatert miljøsjekk IO {{PurchaseOrderNum}}",
			Template: "Hei {{PlannerContactName}} og miljøansvarlig.<br><br>Miljøforholdene i oppdraget IO {{PurchaseOrderNum}} er utdatert (det er {{DaysSinceCheck}} dager siden de ble klarert, og grensen er på 60 dager). <br><br><br><b>Oppdraget ble først signert inn av:</b> <br>{{WorkOrderContractor}}<br>{{WorkOrderContractorSignDate}}<br><br>Åpne <b>FeltPlan</b>, gå til geografi-fanen og <b>bekreft på nytt</b> dersom alle miljøforhold fortsatt er ivaretatt.",
		})
	} else if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddEnvironmentClearedEmails(ConfigEnvironmentClearedEmails{
			To: []ConfigUserEmail{},
			CC: []ConfigUserEmail{
				{Name: "Christer Sandum", Email: "cs@fritzoeskoger.no"},
			},
			BCC: []ConfigUserEmail{
				{Name: "Kristoffer Stenersen", Email: "kristoffer.stenersen@gmail.com"},
			},
			Subject:  "Utdatert miljøsjekk IO {{PurchaseOrderNum}}",
			Template: "Hei {{PlannerContactName}} og miljøansvarlig.<br><br>Miljøforholdene i oppdraget IO {{PurchaseOrderNum}} er utdatert (det er {{DaysSinceCheck}} dager siden de ble klarert, og grensen er på 60 dager). <br><br><br><b>Oppdraget ble først signert inn av:</b> <br>{{WorkOrderContractor}}<br>{{WorkOrderContractorSignDate}}<br><br>Åpne <b>FeltPlan</b>, gå til geografi-fanen og <b>bekreft på nytt</b> dersom alle miljøforhold fortsatt er ivaretatt.",
		})
	} else if b.IsCappelen() {
		b.AddEnvironmentClearedEmails(ConfigEnvironmentClearedEmails{
			To: []ConfigUserEmail{},
			CC: []ConfigUserEmail{
				{Name: "Thor Wraa", Email: "thor.wraa@ulefos.com"},
			},
			BCC: []ConfigUserEmail{
				{Name: "Kristoffer Stenersen", Email: "kristoffer.stenersen@gmail.com"},
			},
			Subject:  "Utdatert miljøsjekk IO {{PurchaseOrderNum}}",
			Template: "Hei {{PlannerContactName}} og miljøansvarlig.<br><br>Miljøforholdene i oppdraget IO {{PurchaseOrderNum}} er utdatert (det er {{DaysSinceCheck}} dager siden de ble klarert, og grensen er på 60 dager). <br><br><br><b>Oppdraget ble først signert inn av:</b> <br>{{WorkOrderContractor}}<br>{{WorkOrderContractorSignDate}}<br><br>Åpne <b>FeltPlan</b>, gå til geografi-fanen og <b>bekreft på nytt</b> dersom alle miljøforhold fortsatt er ivaretatt.",
		})
	} else if b.IsLøvenskioldFossum() {
		b.AddEnvironmentClearedEmails(ConfigEnvironmentClearedEmails{
			To: []ConfigUserEmail{},
			CC: []ConfigUserEmail{
				{Name: "Knut Vale Kristoffersen", Email: "knutv@l-fossum.no"},
			},
			BCC: []ConfigUserEmail{
				{Name: "Kristoffer Stenersen", Email: "kristoffer.stenersen@gmail.com"},
			},
			Subject:  "Utdatert miljøsjekk IO {{PurchaseOrderNum}}",
			Template: "Hei {{PlannerContactName}} og miljøansvarlig.<br><br>Miljøforholdene i oppdraget IO {{PurchaseOrderNum}} er utdatert (det er {{DaysSinceCheck}} dager siden de ble klarert, og grensen er på 60 dager). <br><br><br><b>Oppdraget ble først signert inn av:</b> <br>{{WorkOrderContractor}}<br>{{WorkOrderContractorSignDate}}<br><br>Åpne <b>FeltPlan</b>, gå til geografi-fanen og <b>bekreft på nytt</b> dersom alle miljøforhold fortsatt er ivaretatt.",
		})
	} else if b.IsMathiesen() {
		b.AddEnvironmentClearedEmails(ConfigEnvironmentClearedEmails{
			To: []ConfigUserEmail{},
			CC: []ConfigUserEmail{
				{Name: "Per Almquist", Email: "per.almqvist@mev.no"},
			},
			BCC: []ConfigUserEmail{
				{Name: "Kristoffer Stenersen", Email: "kristoffer.stenersen@gmail.com"},
			},
			Subject:  "Utdatert miljøsjekk IO {{PurchaseOrderNum}}",
			Template: "Hei {{PlannerContactName}} og miljøansvarlig.<br><br>Miljøforholdene i oppdraget IO {{PurchaseOrderNum}} er utdatert (det er {{DaysSinceCheck}} dager siden de ble klarert, og grensen er på 60 dager). <br><br><br><b>Oppdraget ble først signert inn av:</b> <br>{{WorkOrderContractor}}<br>{{WorkOrderContractorSignDate}}<br><br>Åpne <b>FeltPlan</b>, gå til geografi-fanen og <b>bekreft på nytt</b> dersom alle miljøforhold fortsatt er ivaretatt.",
		})
	}

	//------------------------------------------------------------------------------------
	//customConfig
	//------------------------------------------------------------------------------------
	b.AddCustomConfig(ConfigCustomConfig{
		CustomerDbVersion: "3",
		SkogkulturUsers:   "*",
	})

	//------------------------------------------------------------------------------------
	//featureSwitches
	//------------------------------------------------------------------------------------
	if b.IsForestOwner() {

	} else {
		b.AddFeatureSwitch(ConfigFeatureSwitch{
			Enabled:  true,
			Feature:  "skogeieravtale",
			Username: "*",
		})
	}
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: true,
		Feature: "feltkontroll",
	})
	if b.hasVsysClient() {
		b.AddFeatureSwitch(ConfigFeatureSwitch{
			Enabled: true,
			Feature: "videresalg",
		})
	}
	if b.IsDev() || b.IsBeta() || !b.hasVsysClient() || !b.IsNortømmer() {
		b.AddFeatureSwitch(ConfigFeatureSwitch{
			Enabled: true,
			Feature: "querySyncOrders",
		})
	}
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: true,
		Feature: "hogstklasseIdentify",
	})
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: !b.IsNortømmer(),
		Feature: "copyOrderToDraft",
	})
	if b.IsForestOwner() && !b.IsProd() {
		b.AddFeatureSwitch(ConfigFeatureSwitch{
			Enabled: true,
			Feature: "multiSilviJobAssignments",
		})
	}
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: !b.IsContractor(),
		Feature: "forestOwnerContract",
	})
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: true,
		Feature: "workOrder",
	})
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: true,
		Feature: "fieldControl", // What is this? Cannot see any references to it in the code
	})
	b.AddFeatureSwitch(ConfigFeatureSwitch{
		Enabled: true,
		Feature: "silviculture",
	})

	//------------------------------------------------------------------------------------
	//newFeatureFromStand
	//------------------------------------------------------------------------------------

	if b.IsForestOwner() {
		b.AddNewFeatureFromStand(ConfigNewFeatureFromStand{
			Target:     b.company.Prefix + "PTommerdrift",
			VolumeGran: "volumeGran",
			VolumeFuru: "volumeFuru",
			VolumeLauv: "volumeLauv",
			StandNum:   "bestand_str",
			TeigNum:    "teignr",
			Title:      "bestand_str",
		})
		b.AddNewFeatureFromStand(ConfigNewFeatureFromStand{
			Target:   b.company.Prefix + "P\\w+Geom",
			Age:      "age",
			StandNum: "bestand_str",
			TeigNum:  "teignr",
			Title:    "bestand_str",
		})
	}

	if !b.IsForestOwner() {
		b.AddNewFeatureFromStand(ConfigNewFeatureFromStand{
			Target:     b.company.Prefix + "PTommerdrift",
			Bonitet:    "bonitet",
			VolumeGran: "volumeGran",
			VolumeFuru: "volumeFuru",
			VolumeLauv: "volumeLauv",
			StandNum:   "bestandnr",
			TeigNum:    "teignr",
			Title:      "name",
		})
		b.AddNewFeatureFromStand(ConfigNewFeatureFromStand{
			Target:   b.company.Prefix + "P\\w+Geom",
			Age:      "age",
			StandNum: "bestandnr",
			TeigNum:  "teignr",
			Title:    "bestandnr",
		})
	}

	//------------------------------------------------------------------------------------
	//newFeatureCopyOperations
	//------------------------------------------------------------------------------------
	if b.IsNortømmer() {
		b.AddNewFeatureCopyOperation(ConfigNewFeatureCopyOperation{
			Source: "ntbestand_samlet",
			Target: "NTPTommerdrift",
			CopyFields: map[string]string{
				"info":       "name",
				"Bestandsnr": "bestandnr",
				"Bonitet":    "bonitet",
				"Volum gran": "volumeGran",
				"Volum furu": "volumeFuru",
				"Volum lauv": "volumeLauv",
			},
		})
	}
	if b.IsForestOwner() {
		b.AddNewFeatureCopyOperation(ConfigNewFeatureCopyOperation{
			Source: "bestand_bestandsflater",
			Target: prefix + "PTommerdrift",
			CopyFields: map[string]string{
				"info":       "name",
				"Teig":       "teignr",
				"Bestandsnr": "bestand_str",
				"Volum gran": "volumeGran",
				"Volum furu": "volumeFuru",
				"Volum lauv": "volumeLauv",
			},
		})
		b.AddNewFeatureCopyOperation(ConfigNewFeatureCopyOperation{
			Source: "bestand_bestandsflater",
			Target: prefix + "P\\w+Geom",
			CopyFields: map[string]string{
				"Bestandsnr": "bestand_str",
				"Teig":       "teignr",
				"Alder":      "age",
			},
		})
	}

	//------------------------------------------------------------------------------------
	//newFeatureDefaultValues
	//------------------------------------------------------------------------------------
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddNewFeatureDefaultValues(ConfigNewFeatureDefaults{
			Collection: b.company.Prefix + "PSkogkultur",
			Values: map[string]interface{}{
				"plannerContactPhone": "TOKEN_PHONE_OR_+4791199456",
				"forestOwnerPhone":    "+4791199456",
				"forestOwnerEmail":    "cs@fritzoeskoger.no",
			},
		})
	}
	if b.IsFritzøeSkoger() {
		b.AddNewFeatureDefaultValues(ConfigNewFeatureDefaults{
			Collection: "FSPSkogkultur",
			Values: map[string]interface{}{
				"plannerContactPhone": "TOKEN_PHONE_OR_+4791199456",
				"forestOwnerPhone":    "+4791199456",
				"forestOwnerEmail":    "cs@fritzoeskoger.no",
			},
		})
	}
	//------------------------------------------------------------------------------------
	//map.addFeatures
	//------------------------------------------------------------------------------------
	b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerPunkt", Displayname: "Registreringer punkt"})
	b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerLinjer", Displayname: "Registreringer linjer"})
	b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerFlater", Displayname: "Registreringer flater"})

	//------------------------------------------------------------------------------------
	//map.background
	//------------------------------------------------------------------------------------
	if b.IsForestOwner() {
		b.AddRasterMapBackground("Kart", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
		b.AddRasterMapBackground("Foto", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
	} else {
		b.AddRasterMapBackground("Foto", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
		b.AddRasterMapBackground("Kart", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
	}

	//------------------------------------------------------------------------------------
	//map.layers
	//------------------------------------------------------------------------------------

	b.AddMapLayer(mapLayer_FGfugl).
		WithAlias("Fugl hensyn").
		WithDefaultOpacity(1.0)

	if b.IsForestOwner() {
		//not
	} else {
		b.AddMapLayer(mapLayer_Skyggerelieff).
			WithIdentityDisabled().
			WithDefaultOpacity(0.0).
			WithPreview("skyggerelieff.png")
	}

	if b.IsNortømmer() && !b.IsProd() {
		b.AddMapLayer(mapLayer_TraktorOgSkogsbilvegWMS).
			WithIdentityDisabled().
			WithDefaultOpacity(0.0).
			WithPreview("traktorogskogsbilveg.png")
	}

	if b.IsFjordtømmer() || b.IsNortømmer() || b.IsContractor() {
		b.AddMapLayer(mapLayer_BrattTerreng).
			WithIdentityDisabled().
			WithDefaultOpacity(0.0).
			WithPreview("bratterreng.png")
	}

	if b.IsFjordtømmer() {
		b.AddMapLayer(mapLayer_N5Raster2).
			WithIdentityDisabled().
			WithDefaultOpacity(0.0).
			WithPreview("n5raster.png")
	}

	if b.IsNortømmer() {
		b.AddMapLayer(mapLayer_NTBestand).
			WithAlias("Bestand (NT)").
			WithDefaultOpacity(0.5).
			WithPreview("ntbestand.png")
		b.AddMapLayer(mapLayer_NTBestandslinjer).
			WithAlias("Bestandslinjer (NT)").
			WithPreview("ntbestandslinjer.png").
			WithIdentityDisabled().
			WithDefaultOpacity(1.0)
	}

	b.AddMapLayer(mapLayer_VegBruksklasser).
		WithAlias("Veg - bruksklasser").
		WithPreview("vegdata.png").
		WithIdentityDisabled().
		WithDefaultOpacity(0.0)

	if b.IsNortømmer() {
		b.AddMapLayer(mapLayer_LivslopstrarNorgeNortømmer).
			WithAlias("Livsløpstrær Norge").
			WithDefaultOpacity(0.5).
			WithPreview("LLTNorgeNT.png")
	}
	if b.IsGlommen() {
		b.AddMapLayer(mapLayer_LivslopstrarNorgeGlommen).
			WithAlias("Livsløpstrær Norge").
			WithDefaultOpacity(0.5).
			WithPreview("LLTNorgeGM.png")
	}

	b.AddMapLayer(mapLayer_Misc).
		WithAlias("MiS (tidlig)").
		WithDefaultOpacity(0.8).
		WithMinScale(20000).
		WithPreview("mis.png")

	b.AddMapLayer(mapLayer_SR16FG).
		WithAlias("SR16").
		WithDefaultOpacity(0.0).
		WithPreview("sr16.png")

	b.AddMapLayer(mapLayer_Aldersklasseriskog).
		WithDefaultOpacity(0.0).
		WithPreview("Aldersklasseriskog.png")

	b.AddMapLayer(mapLayer_TurRuter).
		WithDefaultOpacity(0.0).
		WithPreview("turruter.png")
	b.AddMapLayer(mapLayer_BrukerMinner).
		WithDefaultOpacity(0.0).
		WithPreview("brukerminner.png")

	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		// They have their own wet map
	} else if b.IsAllma() {
		b.AddMapLayer(mapLayer_GMMarkfuktighetskart_Helelandet).
			WithIdentityDisabled().
			WithDefaultOpacity(0.6).
			WithPreview("markfuktighetskart.png")
	} else {
		b.AddMapLayer(mapLayer_Markfuktighetskart).
			WithIdentityDisabled().
			WithDefaultOpacity(0.6).
			WithPreview("markfuktighetskart.png")
	}

	//fra naturdirektoratet:
	if b.IsForestOwner() || b.IsNortømmer() || b.IsContractor() {
		//naturtyper_hb13_alle_a_b = kun de mest utsatte artene
		b.AddMapLayer(mapLayer_NaturtyperHb13AlleAB).
			WithAlias("Naturtyper HB13").
			WithDefaultOpacity(1.0).
			WithPreview("naturtyperhb13.png")
	} else {
		//naturtyper_hb13_naturtyper_hb13_alle = alle rader i datasettet
		b.AddMapLayer(mapLayer_NaturtyperHb13NaturtyperHb13Alle).
			WithAlias("Naturtyper HB13").
			WithDefaultOpacity(1.0).
			WithPreview("naturtyperhb13.png")
	}

	b.AddMapLayer(mapLayer_Narin).
		WithAlias("Narin").
		WithPreview("narin.png")

	b.AddMapLayer(mapLayer_NaturtyperUnfiltered).
		WithAlias("NiN").
		WithPreview("nin.png")

	b.AddMapLayer(mapLayer_FriluftslivStatligSikra).
		WithAlias("Friluftsliv").
		WithPreview("friluftsliv.png")

	if b.IsNortømmer() || b.IsStoraEnso() {
		b.AddMapLayer(mapLayer_Markagrensen).
			WithAlias("Markagrensen").
			WithPreview("markagrensen.png")
	}

	b.AddMapLayer(mapLayer_VernNaturvernOmrade).
		WithAlias("Vern").
		WithPreview("vern_naturvern_omrade.png")

	b.AddMapLayer(mapLayer_Vernskog).
		WithAlias("Vernskog").
		WithPreview("vernskog.png")

	if b.IsFjordtømmer() {
		b.AddMapLayer(mapLayer_Artsdata).
			WithAlias("Artsdata NIBIO").
			WithDefaultOpacity(0.0).
			WithPreview("artsdatanibio.png")
	} else {
		//Difference: Opacity!
		b.AddMapLayer(mapLayer_Artsdata).
			WithAlias("Artsdata NIBIO").
			WithDefaultOpacity(1.0).
			WithMinOpacity(1.0).
			WithPreview("artsdatanibio.png")
	}

	b.AddMapLayer(mapLayer_Arter).
		WithAlias("Arter").
		WithPreview("arter.png")

	if b.IsNortømmer() || b.IsAllma() {
		b.AddMapLayer(mapLayer_TiurleikerStjørdal).
			WithAlias("Tiurleiker Stjørdal").
			WithPreview("tiurstjordal.png")
	}

	if b.IsFjordtømmer() {
		//none
	} else {
		b.AddMapLayer(mapLayer_Skredkvikkleire2Kvikkleirefaregrad).
			WithAlias("Kvikkleirefaregrad").
			WithPreview("kvikkleirefaregrad.png")
	}

	if b.IsFjordtømmer() {
		//none
	} else {
		b.AddMapLayer(mapLayer_JordOgFlomskred).
			WithAlias("Jord og flomskred").
			WithPreview("jordogflom.png")
	}

	b.AddMapLayer(mapLayer_Reguleringsplaner).
		WithAlias("Reguleringsplaner").
		WithDefaultOpacity(0.0).
		WithPreview("reguleringsplaner.png")

	b.AddMapLayer(mapLayer_Teig).
		WithAlias("Eiendom").
		WithIdentityDisabled().
		WithPreview("teig.png")
	b.AddMapLayer(mapLayer_Grensepunkter).
		WithAlias("Grensepunkter").
		WithIdentityDisabled().
		WithPreview("grensepunkter.png")

	if b.IsForestOwner() {
		//none
	} else {
		b.AddMapLayer(mapLayer_Andre).
			WithAlias("Andre hensyn").
			WithPreview("andre.png")
	}

	if b.IsNortømmer() {
		b.AddMapLayer(mapLayer_NTMiljo).
			WithPreview("ntmiljo.png")
		b.AddMapLayer(mapLayer_NTMis).
			WithAlias("NTmis")
		//mis2 = mis with custom styling for NT
		b.AddMapLayer(mapLayer_Mis2).
			WithAlias("MiS").
			WithPreview("mis.png")
		b.AddMapLayer(mapLayer_MisTrysilPrevista).
			WithAlias("MiS Trysil Prevista")
	} else if b.IsForestOwner() || b.IsContractor() {
		b.AddMapLayer(mapLayer_NTMis).
			WithAlias("NTmis").
			WithPreview("ntmis.png")
		b.AddMapLayer(mapLayer_Mis).
			WithAlias("MiS").
			WithPreview("mis.png")
	} else if b.IsStoraEnso() {
		b.AddMapLayer(mapLayer_NTMiljo).
			WithPreview("ntmiljo.png").
			WithAlias("MiS")
		b.AddMapLayer(mapLayer_NTMis).
			WithPreview("ntmis.png").
			WithAlias("MiS (1)")
		b.AddMapLayer(mapLayer_Mis).
			WithPreview("mis.png").
			WithAlias("MiS (2)")
	} else if b.IsGlommen() || b.IsFjordtømmer() {
		b.AddMapLayer(mapLayer_Mis).
			WithPreview("mis.png").
			WithAlias("MiS")
	}

	if b.IsFjordtømmer() {
		b.AddMapLayer(mapLayer_FTMis).
			WithPreview("ftmis.png")
	}

	/*b.AddMapLayer(mapLayer_KulturminnerWMS).
	WithPreview("ra.png").
	WithMinOpacity(0.3)*/

	b.AddMapLayer(mapLayer_Kulturminner).
		WithPreview("ra.png").
		WithAlias("Kulturminner").
		WithMinOpacity(0.5)

	if b.IsNortømmer() {
		b.AddMapLayer(mapLayer_NtData).
			WithPreview("ntdata.png")

		b.AddMapLayer(mapLayer_NTEditor10Oppdragsflater).
			WithAlias("Oppdrag fra Feltapp").
			WithPreview("ntoppdragsflater.png")
	}

	b.AddMapLayer(mapLayer_Hoydekurver).
		WithAlias("Høydekurver").
		WithIdentityDisabled().
		WithDefaultOpacity(0.6).
		WithPreview("hoydekurver.png")

	if b.IsForestOwner() {

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZTrevolumRaster).
				WithAlias("Trevolum (raster)").
				WithDefaultOpacity(0.0).
				WithMinLevel(13).
				WithPreview("fztrevolum.png").
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("fzorto.png")
			b.AddMapLayer(mapLayer_FZMarkfuktighetskart).
				WithAlias("Markfuktighetskart").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("markfuktighetskart.png")
			b.AddMapLayer(mapLayer_FZTerrengGrå).
				WithAlias("Terreng grå").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("fzterrenggraa.png")
			b.AddMapLayer(mapLayer_FZTrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("fztrehoyde.png")
			b.AddMapLayer(mapLayer_FZSlope_30).
				WithMinLevel(10).
				WithAlias("slope_30").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("fzslope30.png")
			b.AddMapLayer(mapLayer_FZData).
				WithMinLevel(10).
				WithAlias("FZ: Data").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()

			b.AddMapLayer(mapLayer_Nibio_SO_Barkbilleskade_gradert).
				WithMinLevel(10).
				WithAlias("Skadeovervåking: Barkbilleskade gradert").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			/* Fritzoe has no need for these layers
			b.AddMapLayer(mapLayer_Nibio_SO_Prosjektavgrensing).
				WithMinLevel(4).
				WithAlias("Skadeovervåking: Prosjektavgrensing").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()

			b.AddMapLayer(mapLayer_Nibio_SO_Hogstflate).
				WithMinLevel(10).
				WithAlias("Skadeovervåking: Hogstflate").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()

			b.AddMapLayer(mapLayer_Nibio_SO_PBarkbilleskade).
				WithMinLevel(10).
				WithAlias("Skadeovervåking: Barkbilleskade").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()

			b.AddMapLayer(mapLayer_Nibio_SO_Oversiktskart).
				WithMinLevel(10).
				WithAlias("Skadeovervåking: Oversiktskart").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()*/
			b.AddMapLayer(mapLayer_FZExternal_gf_drone_ortophoto_cache).
				WithMinLevel(10).
				WithAlias("External: Drone orto photo").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			/* FZ does not need these layers
			b.AddMapLayer(mapLayer_FZExternal_gf_feature_lines_cache).
				WithMinLevel(10).
				WithAlias("External: Feature lines").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZExternal_gf_feature_points_cache).
				WithMinLevel(10).
				WithAlias("External: Feature points").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZExternal_gf_feature_polygons_cache).
				WithMinLevel(10).
				WithAlias("External: Feature polygons").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZExternal_gf_tracklog_lines_cache).
				WithMinLevel(10).
				WithAlias("External: Tracklog lines").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled() */
		}

		b.AddMapLayer(mapLayer_Andre).
			WithAlias("Brønn, gjødselfelt, forsøksfelt").
			WithMinOpacity(0.2)
		b.AddMapLayer(mapLayer_Losmasser).
			WithAlias("Løsmasser").
			WithDefaultOpacity(0.0).
			WithPreview("fzlosmasser.png")

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZTeig).
				WithPreview("teig.png")
			b.AddMapLayer(mapLayer_FZTreeVol).
				WithAlias("Trevolum").
				WithPreview("fztrevolum.png").
				WithMinLevel(13)

			b.AddMapLayer(mapLayer_FZBestandSkogtyper).
				WithAlias("Bestand skogtyper").
				WithPreview("bestandskogtyper.png").
				WithDefaultOpacity(0)
			b.AddMapLayer(mapLayer_FZGytebekker).
				WithAlias("Gytebekker")
		}

		if b.IsCappelen() {
			b.AddMapLayer(mapLayer_CATeiger).
				WithPreview("teig.png")

			// TODO: Consider removing since we now sync CAStand
			b.AddMapLayer(mapLayer_CABestandsflater).
				WithAlias("Bestandsflater").
				WithDefaultOpacity(0.0).
				WithPreview("cabestand.png")

			b.AddMapLayer(mapLayer_CABehandlingsareal).
				WithPreview("cabehandling.png")

			b.AddMapLayer(mapLayer_CASkoglinjer)

			b.AddMapLayer(mapLayer_CASkogpunkter).
				WithPreview("caskogpunkt.png")

			b.AddMapLayer(mapLayer_CABrønn)
			b.AddMapLayer(mapLayer_CABygninger)

			b.AddMapLayer(mapLayer_CAOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAGYL).
				WithAlias("GYL").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkfuktighet).
				WithAlias("Markfuktighet").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkstruktur).
				WithAlias("Markstruktur").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CA_RGB_orto).
				WithAlias("RGB orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CATrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()

		}

		if b.IsLøvenskioldFossum() {
			b.AddMapLayer(mapLayer_LFGrensemerker).
				WithDefaultOpacity(0.8)
			b.AddMapLayer(mapLayer_LFEiendomspolygoner).
				WithDefaultOpacity(0.8)
			// TODO: Consider removing since we now sync LFStand
			b.AddMapLayer(mapLayer_LFBestandsflater).
				WithDefaultOpacity(0.0)
			b.AddMapLayer(mapLayer_LFSamferdsel)
			b.AddMapLayer(mapLayer_LFBro)
			b.AddMapLayer(mapLayer_LFBom)
			b.AddMapLayer(mapLayer_LFStikkrenne)
			b.AddMapLayer(mapLayer_LFKjorespor)
			b.AddMapLayer(mapLayer_LFVeierBehandling)
			b.AddMapLayer(mapLayer_LFSnuplassVelteplassMM)
			b.AddMapLayer(mapLayer_LFBronn)
			b.AddMapLayer(mapLayer_LFSkogpunkter)
			b.AddMapLayer(mapLayer_LFSkoglinjer)
			b.AddMapLayer(mapLayer_LFEgneKulturminner)
			b.AddMapLayer(mapLayer_LFGytebekk)
			b.AddMapLayer(mapLayer_LFBygninger)
			b.AddMapLayer(mapLayer_LFFugl)
			b.AddMapLayer(mapLayer_LFLivslopstre)
			b.AddMapLayer(mapLayer_LFNokkelbiotop).
				WithDefaultOpacity(0.8)
			b.AddMapLayer(mapLayer_LFBehandlingsareal).
				WithDefaultOpacity(0.8)
		}

	}

	// These are basiccally "forest owners", but integration must be done so keep the list explicit
	if b.IsFritzøeSkoger() || b.IsCappelen() || b.IsLøvenskioldFossum() || b.IsMathiesen() {
		b.AddCollectionMapLayer("VTStand", mapLayerType_Polygon).
			WithAlias("Bestand").
			WithKeepInBackgroundMap().
			WithMinScale(100000).
			WithRendererIncludedFields("etikett").
			WithPreview("fzbestandsflater.png")
	}

	if b.IsForestOwner() || b.IsContractor() {
		b.AddCollectionMapLayer("Stem", mapLayerType_Point).
			WithAlias("Stammepunkt").
			WithPreview("stammepunkt.png")
	}

	if b.IsForestOwner() {

		layer := b.AddCollectionMapLayer("PTommerdrift", mapLayerType_Polygon).
			WithAlias("Driftsområde").
			WithKeepInBackgroundMap().
			WithMinScale(20000)
		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			layer.WithPreview("fzdriftsomraade.png")
		}
		if b.IsMathiesen() {
			layer.WithPreview("mevdriftsomraade.png")
		}
	} else if b.IsContractor() {
		b.AddCollectionMapLayer("PTommerdrift", mapLayerType_Polygon).
			WithAlias("Driftsområde").
			WithMinOpacity(0.3).
			WithKeepInBackgroundMap()
	} else {
		b.AddCollectionMapLayer("PTommerdrift", mapLayerType_Polygon).
			WithAlias("Driftsområde").
			WithMinOpacity(0.3)
	}

	b.AddCollectionMapLayer("MiljoFlater", mapLayerType_Polygon).
		WithAlias("Miljø flater").
		WithKeepInBackgroundMap()

	polygons := b.AddCollectionMapLayer("RegistreringerFlater", mapLayerType_Polygon).
		WithAlias("Registreringer flater").
		WithRendererIncludedFields("comment").
		WithMinOpacity(0.5)

	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		polygons.WithRendererIncludedFields("comment", "_headId").
			WithRendererDefinitionExpression("_headId='{{_headId}}'")
	}

	layer := b.AddCollectionMapLayer("RegistreringerLinjer", mapLayerType_Line).
		WithAlias("Registreringer linjer").
		WithRendererIncludedFields("comment")

	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		layer.WithPreview("fzreglinjer.png").
			WithRendererIncludedFields("comment", "_headId").
			WithRendererDefinitionExpression("_headId='{{_headId}}'")
	}

	b.AddCollectionMapLayer("MiljoLinjer", mapLayerType_Line).
		WithAlias("Miljø linjer").
		WithKeepInBackgroundMap()

	b.AddCollectionMapLayer("Sporlogg", mapLayerType_Line).
		WithAlias("Sporlogg").
		WithIdentityDisabled()

	b.AddMapLayer(mapLayer_Geocachenames).
		WithAlias("Stedsnavn").
		WithIdentityDisabled().
		WithDefaultOpacity(0.8).
		WithPreview("stedsnavn.png")

	b.AddCollectionMapLayer("MiljoPunkt", mapLayerType_Point).
		WithKeepInBackgroundMap()

	points := b.AddCollectionMapLayer("RegistreringerPunkt", mapLayerType_Point).
		WithAlias("Registreringer punkt").
		WithPreview("regpunkt.png").
		WithRendererIncludedFields("comment")
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		points.WithRendererIncludedFields("comment", "_headId").
			WithRendererDefinitionExpression("_headId='{{_headId}}'")
	}

	if b.company.hasFeature(hasVsys) {
		// When VSYS enabled, these data are backed by vsys and not synced to FG.
		b.AddPrefixedCollectionMapLayer("NT", "Skogdata", mapLayerType_Point) //TODO: local map layer, not in sync
	} else {
		// When VSYS disabled, these data are backed by FG and synced to FG.
		b.AddPrefixedCollectionMapLayer(prefix, "Skogdata", mapLayerType_Point) //TODO: local map layer, not in sync
	}

	if b.IsMathiesen() {
		if b.IsMathiesen() {
			b.AddMapLayer(mapLayer_MELinnea).
				WithAlias("Linnea").
				WithPreview("mevlinnea.png")
		}
	}
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddMapLayer(mapLayer_FZMiljø).
			WithPreview("fzmiljo.png")
		b.AddMapLayer(mapLayer_FZInfrastruktur).
			WithPreview("fzinfrastruktur.png")
	}

	if b.IsDev() {
		b.AddMapLayer(mapLayer_DebugIndexlayer).
			WithAlias("25833 mvt rutenett").
			WithIdentityDisabled()
	}

	//------------------------------------------------------------------------------------
	//hiddenFromLayerList
	//------------------------------------------------------------------------------------

	if b.IsForestOwner() || b.IsContractor() {

	} else {
		b.SetLayersHiddenFromLayerList(
			prefix+"RegistreringerFlater",
			prefix+"RegistreringerLinjer",
			prefix+"RegistreringerPunkt",
			prefix+"Sporlogg",
		)
	}
	if !b.IsContractor() {
		b.SetLayersHiddenFromLayerList(
			"kulturminner",
		)
	}
	b.SetLayersHiddenFromLayerList(
		prefix+"MiljoFlater",
		prefix+"MiljoLinjer",
		prefix+"MiljoPunkt",
	)

	if b.hasVsysClient() {
		b.SetLayersHiddenFromLayerList(
			"NTSkogdata",
		)
	} else {
		b.SetLayersHiddenFromLayerList(
			prefix + "Skogdata",
		)

	}

	if b.IsForestOwner() || b.IsContractor() || b.IsNortømmer() {

	} else {
		b.SetLayersHiddenFromLayerList(
			"naturtyper_hb13_naturtyper_hb13_alle",
		)
	}

	if b.IsFjordtømmer() || b.IsContractor() {

	} else {
		b.SetLayersHiddenFromLayerList(
			"narin",
			"naturtyper_unfiltered",
			"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
			"vern_naturvern_omrade",
			"vernskog",
			"arter",
			"teig",
			"misc",
			"artsdata",
		)
		if b.IsNortømmer() || b.IsStoraEnso() {
			b.SetLayersHiddenFromLayerList("markagrensen")
		}
		if b.IsNortømmer() || b.IsAllma() {
			b.SetLayersHiddenFromLayerList("tiurleiker_stjørdal")
		}

		if b.IsStoraEnso() {
			b.SetLayersHiddenFromLayerList(
				"NTmis",
			)
		} else if b.IsNortømmer() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_alle_a_b",
				"mis2",
				"NTmis",
				"NTPTommerdrift",
				"mis_trysil_prevista",
			)
		} else if b.IsForestOwner() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_alle_a_b",
				"mis",
				"NTmis",
			)
		} else if b.IsGlommen() {
			b.SetLayersHiddenFromLayerList(
				"skredkvikkleire2_kvikkleirefaregrad",
				"mis",
			)
		} else {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_alle_a_b",
				"skredkvikkleire2_kvikkleirefaregrad",
				"mis",
				"mis2",
			)
		}

		if b.IsForestOwner() || b.IsContractor() {
			//andre is not hidden 4 fritzoe
		} else {
			b.SetLayersHiddenFromLayerList(
				"Andre",
			)
		}
	}

	//------------------------------------------------------------------------------------
	//purchaseOrderPresets
	//------------------------------------------------------------------------------------
	if b.IsMathiesen() {
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Mathiesen Eidsvoll Værk",
			"head": map[string]interface{}{
				"BuyerAdress":          "Brustad",
				"BuyerCity":            "HURDAL",
				"BuyerContactEMail":    "per.almqvist@mev.no",
				"BuyerContactName":     "Per Almqvist",
				"BuyerContactPhone":    "+4793093635",
				"BuyerCountryCode":     "NO",
				"BuyerCountryName":     "Norge",
				"BuyerDepartmentName":  "Admin",
				"BuyerDepartmentNum":   1,
				"BuyerEMail":           "per.almqvist@mev.no",
				"BuyerName":            "MATHIESEN EIDSVOLD VÆRK ANS",
				"BuyerOrgNum":          "931073737",
				"BuyerPartyVSYSName":   "MATHIESEN EIDSVOLD VÆRK ANS",
				"BuyerPartyVSYSNum":    55585,
				"BuyerZipCode":         "2090",
				"SellerAddress":        "Brustad",
				"SellerCity":           "HURDAL",
				"SellerCountryCode":    "NO",
				"SellerCountryName":    "Norge",
				"SellerDepartmentName": "Skog",
				"SellerDepartmentNum":  2,
				"SellerPartyVSYSName":  "MATHIESEN EIDSVOLD VÆRK ANS",
				//"SellerPartyVSYSNum":       55585,
				"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
				"SellerOrgNum":             "931073737",
				"SellerZipCode":            "2090",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
				// Do not set muni num!
				// It is either set automatically when creating an order from a property in the map
				// - Or it is set, when the user selects one of the vsysnum options
				//"MuniNum":                  2039,
				//"MuniName":                 "Ullensaker",
			},
		})
		/*
			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Nes i Akershus",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        55586,
					"BuyerZipCode":             "2090",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       55586,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2090",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3229,
					"MuniName":                 "Nes",
				},
			})

			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Eidsvoll",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        55587,
					"BuyerZipCode":             "2090",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       55587,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2090",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3240,
					"MuniName":                 "Eidsvoll",
				},
			})

			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Nannestad",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        55588,
					"BuyerZipCode":             "2091",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       55588,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2091",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3238,
					"MuniName":                 "Nannestad",
				},
			})

			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Hurdal",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        55589,
					"BuyerZipCode":             "2090",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       55589,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2090",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3242,
					"MuniName":                 "Hurdal",
				},
			})

			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Østre Toten",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        55591,
					"BuyerZipCode":             "2091",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       55591,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2091",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3442,
					"MuniName":                 "Østre Toten",
				},
			})

			b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
				"alias": "Gran",
				"head": map[string]interface{}{
					"BuyerAdress":              "Brustad",
					"BuyerCity":                "HURDAL",
					"BuyerContactEMail":        "per.almqvist@mev.no",
					"BuyerContactName":         "Per Almqvist",
					"BuyerContactPhone":        "+4793093635",
					"BuyerCountryCode":         "NO",
					"BuyerCountryName":         "Norge",
					"BuyerDepartmentName":      "Admin",
					"BuyerDepartmentNum":       1,
					"BuyerEMail":               "per.almqvist@mev.no",
					"BuyerName":                "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerOrgNum":              "931073737",
					"BuyerPartyVSYSName":       "MATHIESEN EIDSVOLD VÆRK ANS",
					"BuyerPartyVSYSNum":        253567,
					"BuyerZipCode":             "2091",
					"SellerAddress":            "Brustad",
					"SellerCity":               "HURDAL",
					"SellerCountryCode":        "NO",
					"SellerCountryName":        "Norge",
					"SellerDepartmentName":     "Skog",
					"SellerDepartmentNum":      2,
					"SellerPartyVSYSName":      "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerPartyVSYSNum":       253567,
					"SellerName":               "MATHIESEN EIDSVOLD VÆRK ANS",
					"SellerOrgNum":             "931073737",
					"SellerZipCode":            "2091",
					"CertificationNum":         9,
					"CertificationDescription": "FSC/PEFC",
					"MuniNum":                  3446,
					"MuniName":                 "Gran",
				},
			})
		*/

	} else if b.IsStoraEnso() {

	} else if b.IsNortømmer() {

	} else if b.IsGlommen() {

	} else if b.IsFritzøe() {
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Fritzøe",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "922965404",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        503015,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Industrivegen 24",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "Skog",
				"SellerDepartmentNum":      2,
				"SellerName":               "FRITZØE SKOGER AS",
				"SellerOrgNum":             "922965404",
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Hellestvedt Bruk",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "922965404",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        503015,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Jernværksvegen 12",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "1",
				"SellerDepartmentNum":      1,
				"SellerName":               "HELLESTVEDT BRUK AS",
				"SellerOrgNum":             "932760649",
				"SellerPartyVSYSName":      "HELLESTVEDT BRUK AS",
				"SellerPartyVSYSNum":       827247,
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
				"MuniNum":                  4012,
				"MuniName":                 "Bamble",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Vold & Nenset brug",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "922965404",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        503015,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Jernværksvegen 12",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "1",
				"SellerDepartmentNum":      1,
				"SellerName":               "VOLD & NENSET BRUG AS",
				"SellerOrgNum":             "917224285",
				"SellerPartyVSYSName":      "VOLD OG NENSET BRUG AS",
				"SellerPartyVSYSNum":       875294,
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
				"MuniNum":                  4003,
				"MuniName":                 "Skien",
			},
		})
	} else if b.IsFritzøeSkoger() {
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Fritzøe",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "928838188",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        505232,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Industrivegen 24",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "Skog",
				"SellerDepartmentNum":      2,
				"SellerName":               "FRITZØE SKOGER AS",
				"SellerOrgNum":             "928838188",
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Hellestvedt Bruk",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "928838188",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        505232,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Jernværksvegen 12",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "1",
				"SellerDepartmentNum":      1,
				"SellerName":               "HELLESTVEDT BRUK AS",
				"SellerOrgNum":             "932760649",
				"SellerPartyVSYSName":      "HELLESTVEDT BRUK AS",
				"SellerPartyVSYSNum":       827247,
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
				"MuniNum":                  4012,
				"MuniName":                 "Bamble",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Vold & Nenset brug",
			"head": map[string]interface{}{
				"BuyerAdress":              "POSTBOKS 2051",
				"BuyerCity":                "SILJAN",
				"BuyerContactEMail":        "cs@fritzoeskoger.no",
				"BuyerContactName":         "Christer Sandum",
				"BuyerContactPhone":        "+4791199456",
				"BuyerCountryCode":         "NO",
				"BuyerCountryName":         "Norge",
				"BuyerDepartmentName":      "Admin",
				"BuyerDepartmentNum":       1,
				"BuyerEMail":               "cs@fritzoeskoger.no",
				"BuyerName":                "Fritzøe Skoger AS",
				"BuyerOrgNum":              "928838188",
				"BuyerPartyVSYSName":       "FRITZØE SKOGER",
				"BuyerPartyVSYSNum":        505232,
				"BuyerZipCode":             "3748",
				"SellerAddress":            "Jernværksvegen 12",
				"SellerCity":               "SILJAN",
				"SellerCountryCode":        "NO",
				"SellerCountryName":        "Norge",
				"SellerDepartmentName":     "1",
				"SellerDepartmentNum":      1,
				"SellerName":               "VOLD & NENSET BRUG AS",
				"SellerOrgNum":             "917224285",
				"SellerPartyVSYSName":      "VOLD OG NENSET BRUG AS",
				"SellerPartyVSYSNum":       875294,
				"SellerZipCode":            "3748",
				"CertificationNum":         9,
				"CertificationDescription": "FSC/PEFC",
				"MuniNum":                  4003,
				"MuniName":                 "Skien",
			},
		})
	} else if b.IsFjordtømmer() {
		b.AddPurchaseOrderPresetIssuingInstruction(map[string]interface{}{
			"alias":                 "Ingen",
			"issuingInstructionNum": 3,
		})
		b.AddPurchaseOrderPresetIssuingInstruction(map[string]interface{}{
			"alias":                   "Hogstmaskininnmåling",
			"issuingInstructionNum":   1,
			"mPartyNum":               43,
			"settlementMMetNum":       3,
			"trAdmNum":                43,
			"transportAccountableNum": 501786,
		})
		b.AddPurchaseOrderPresetIssuingInstruction(map[string]interface{}{
			"alias":                   "Mottaksinnmåling",
			"issuingInstructionNum":   1,
			"mPartyNum":               33,
			"settlementMMetNum":       1,
			"trAdmNum":                43,
			"transportAccountableNum": 501786,
		})

		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Fjordtømmer",
			"additionalFields": map[string]interface{}{
				"production": "Areal",
				//TODO: roadTrackDamageResponsible
				//TODO: roadTrackDamageBilling
				"terrainTrackDamageResponsible": "FJORDTØMMER",
				"terrainTrackDamageBilling":     "Inkludert i driftpris",
			},
		})
	} else if b.IsCappelen() {
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Cappelen Skien",
			"head": map[string]interface{}{
				"BuyerAdress":                           "Jernværksvegen 12",
				"BuyerCity":                             "ULEFOSS",
				"BuyerContactEMail":                     "thor.wraa@ulefos.com",
				"BuyerContactEMailCopyMeasuringReceipt": "thor.wraa@ulefos.com",
				"BuyerContactName":                      "Thor Wraa",
				"BuyerContactPhone":                     "+4795150018",
				"BuyerCountryCode":                      "NO",
				"BuyerCountryName":                      "Norge",
				"BuyerDepartmentName":                   "Admin",
				"BuyerDepartmentNum":                    1,
				"BuyerEMail":                            "thor.wraa@ulefos.com",
				"BuyerName":                             "S.D. CAPPELEN SKOGER",
				"BuyerOrgNum":                           "979200048",
				"BuyerPartyVSYSName":                    "S.D. CAPPELEN SKOGER",
				"BuyerPartyVSYSNum":                     504978,
				"BuyerZipCode":                          "3830",
				"SellerAddress":                         "Jernværksvegen 12",
				"SellerCity":                            "ULEFOSS",
				"SellerCountryCode":                     "NO",
				"SellerCountryName":                     "Norge",
				"SellerDepartmentName":                  "Skog",
				"SellerDepartmentNum":                   2,
				"SellerName":                            "S.D. CAPPELEN SKOGER",
				"SellerOrgNum":                          "979200048",
				"SellerPartyVSYSName":                   "CAPPELEN CARL DIDERIK/",
				"SellerPartyVSYSNum":                    809224,
				"SellerZipCode":                         "3830",
				"CertificationNum":                      9,
				"CertificationDescription":              "FSC/PEFC",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Cappelen Nome",
			"head": map[string]interface{}{
				"BuyerAdress":                           "Jernværksvegen 12",
				"BuyerCity":                             "ULEFOSS",
				"BuyerContactEMail":                     "thor.wraa@ulefos.com",
				"BuyerContactEMailCopyMeasuringReceipt": "thor.wraa@ulefos.com",
				"BuyerContactName":                      "Thor Wraa",
				"BuyerContactPhone":                     "+4795150018",
				"BuyerCountryCode":                      "NO",
				"BuyerCountryName":                      "Norge",
				"BuyerDepartmentName":                   "Admin",
				"BuyerDepartmentNum":                    1,
				"BuyerEMail":                            "thor.wraa@ulefos.com",
				"BuyerName":                             "S.D. CAPPELEN SKOGER",
				"BuyerOrgNum":                           "979200048",
				"BuyerPartyVSYSName":                    "S.D. CAPPELEN SKOGER",
				"BuyerPartyVSYSNum":                     504978,
				"BuyerZipCode":                          "3830",
				"SellerAddress":                         "Jernværksvegen 12",
				"SellerCity":                            "ULEFOSS",
				"SellerCountryCode":                     "NO",
				"SellerCountryName":                     "Norge",
				"SellerDepartmentName":                  "Skog",
				"SellerDepartmentNum":                   2,
				"SellerName":                            "S.D. CAPPELEN SKOGER",
				"SellerOrgNum":                          "979200048",
				"SellerPartyVSYSName":                   "CAPPELEN CARL DIDERIK/",
				"SellerPartyVSYSNum":                    809225,
				"SellerZipCode":                         "3830",
				"CertificationNum":                      9,
				"CertificationDescription":              "FSC/PEFC",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Silvi Montana Kviteseid",
			"head": map[string]interface{}{
				"BuyerAdress":                           "Jernværksvegen 12",
				"BuyerCity":                             "ULEFOSS",
				"BuyerContactEMail":                     "thor.wraa@ulefos.com",
				"BuyerContactEMailCopyMeasuringReceipt": "thor.wraa@ulefos.com",
				"BuyerContactName":                      "Thor Wraa",
				"BuyerContactPhone":                     "+4795150018",
				"BuyerCountryCode":                      "NO",
				"BuyerCountryName":                      "Norge",
				"BuyerDepartmentName":                   "Admin",
				"BuyerDepartmentNum":                    1,
				"BuyerEMail":                            "thor.wraa@ulefos.com",
				"BuyerName":                             "S.D. CAPPELEN SKOGER",
				"BuyerOrgNum":                           "979200048",
				"BuyerPartyVSYSName":                    "S.D. CAPPELEN SKOGER",
				"BuyerPartyVSYSNum":                     504978,
				"BuyerZipCode":                          "3830",
				"SellerAddress":                         "Jernværksvegen 12",
				"SellerCity":                            "ULEFOSS",
				"SellerCountryCode":                     "NO",
				"SellerCountryName":                     "Norge",
				"SellerDepartmentName":                  "Skog",
				"SellerDepartmentNum":                   2,
				"SellerName":                            "SILVI MONTANA V/CARL DIDERIK CAPPELEN",
				"SellerOrgNum":                          "989451987",
				"SellerPartyVSYSName":                   "CAPPELEN CARL DIDERIK",
				"SellerPartyVSYSNum":                    809236,
				"SellerZipCode":                         "3830",
				"CertificationNum":                      9,
				"CertificationDescription":              "FSC/PEFC",
			},
		})
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Silvi Montana Tokke",
			"head": map[string]interface{}{
				"BuyerAdress":                           "Jernværksvegen 12",
				"BuyerCity":                             "ULEFOSS",
				"BuyerContactEMail":                     "thor.wraa@ulefos.com",
				"BuyerContactEMailCopyMeasuringReceipt": "thor.wraa@ulefos.com",
				"BuyerContactName":                      "Thor Wraa",
				"BuyerContactPhone":                     "+4795150018",
				"BuyerCountryCode":                      "NO",
				"BuyerCountryName":                      "Norge",
				"BuyerDepartmentName":                   "Admin",
				"BuyerDepartmentNum":                    1,
				"BuyerEMail":                            "thor.wraa@ulefos.com",
				"BuyerName":                             "S.D. CAPPELEN SKOGER",
				"BuyerOrgNum":                           "979200048",
				"BuyerPartyVSYSName":                    "S.D. CAPPELEN SKOGER",
				"BuyerPartyVSYSNum":                     504978,
				"BuyerZipCode":                          "3830",
				"SellerAddress":                         "Jernværksvegen 12",
				"SellerCity":                            "ULEFOSS",
				"SellerCountryCode":                     "NO",
				"SellerCountryName":                     "Norge",
				"SellerDepartmentName":                  "Skog",
				"SellerDepartmentNum":                   2,
				"SellerName":                            "SILVI MONTANA V/CARL DIDERIK CAPPELEN",
				"SellerOrgNum":                          "989451987",
				"SellerPartyVSYSName":                   "CAPPELEN CARL DIDERIK",
				"SellerPartyVSYSNum":                    809227,
				"SellerZipCode":                         "3830",
				"CertificationNum":                      9,
				"CertificationDescription":              "FSC/PEFC",
			},
		})
		//map[string]interface{}
	} else if b.IsLøvenskioldFossum() {
		b.AddPurchaseOrderPresetDefaultOrders(map[string]interface{}{
			"alias": "Løvenskiold-Fossum",
			"head": map[string]interface{}{
				"BuyerAdress":                           "Jernverksvegen 52",
				"BuyerCity":                             "SKIEN",
				"BuyerContactEMail":                     "knutv@l-fossum.no",
				"BuyerContactEMailCopyMeasuringReceipt": "knutv@l-fossum.no",
				"BuyerContactName":                      "Knut Vale Kristoffersen",
				"BuyerContactPhone":                     "+4746826827",
				"BuyerCountryCode":                      "NO",
				"BuyerCountryName":                      "Norge",
				"BuyerDepartmentName":                   "Admin",
				"BuyerDepartmentNum":                    1,
				"BuyerEMail":                            "knutv@l-fossum.no",
				"BuyerName":                             "LØVENSKIOLD-FOSSUM SKOG ANS",
				"BuyerOrgNum":                           "987868821",
				"BuyerPartyVSYSName":                    "LØVENSKIOLD-FOSSUM SKOG ANS",
				"BuyerPartyVSYSNum":                     504979,
				"BuyerZipCode":                          "3721",
				"SellerAddress":                         "Jernverksvegen 52",
				"SellerCity":                            "SKIEN",
				"SellerCountryCode":                     "NO",
				"SellerCountryName":                     "Norge",
				"SellerDepartmentName":                  "Skog",
				"SellerDepartmentNum":                   2,
				"SellerName":                            "LØVENSKIOLD-FOSSUM SKOG ANS",
				"SellerOrgNum":                          "987868821",
				"SellerPartyVSYSName":                   "LØVENSKIOLD-FOSSUM SKOG ANS",
				"SellerPartyVSYSNum":                    843690,
				"SellerZipCode":                         "3721",
				"CertificationNum":                      9,
				"CertificationDescription":              "FSC/PEFC",
			},
		})
	}

	//------------------------------------------------------------------------------------
	//reports
	//------------------------------------------------------------------------------------
	var reportSuffix = "REPORT_SUFIX_NOT_SET" //prod = "", test = "-test" at end of filenames

	if b.IsProd() {
		reportSuffix = ""
	} else {
		reportSuffix = "-test"
	}

	var reportFolder = "REPORT_FOLDER_NOT_SET"
	if b.company != nil {
		reportFolder = b.company.ReportFolder
	}

	//TODO: USED FOR POPUP MENUES IN FeltPlan (and others?) BUT SHOULD NOT BE DEFINED HERE
	//TODO: NEW REPORT-GENERATORS DOES NOT USE THIS INFORMATION, SHOULD PROBABLY BE COLLECTED FROM FEATURES
	var reportLanguages []string = nil //translations
	if b.IsFritzøe() {
		reportLanguages = []string{"engelsk", "norsk"}
	} else if b.IsFritzøeSkoger() {
		reportLanguages = []string{"engelsk", "norsk"}
	}

	var reportBitBucketUrl = "https://bitbucket.org/feltgis/feltgis-config/raw/master/"
	reportUrl := reportBitBucketUrl + "rapporter/" + reportFolder

	b.AddGenericReportTemplates(
		reportUrl + "/generic.html",
	)
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-forhandsrydding-en", reportUrl+"/staticHtml/arbeidsinstruks-forhandsrydding-en.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-forhandsrydding", reportUrl+"/staticHtml/arbeidsinstruks-forhandsrydding.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-markberedning", reportUrl+"/staticHtml/arbeidsinstruks-markberedning.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-planting-en", reportUrl+"/staticHtml/arbeidsinstruks-planting-en.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-planting", reportUrl+"/staticHtml/arbeidsinstruks-planting.html")
		b.AddGenericReportStaticHtmlTemplates("arbeidsinstruks-ungskogpleie", reportUrl+"/staticHtml/arbeidsinstruks-ungskogpleie.html")
	}

	if b.IsNortømmer() || b.IsForestOwner() || b.IsContractor() { //TODO: rename to common filenames?
		//skogeieravtale vs skogeierkontrakt
		b.AddReportTemplate(prefix+"POrdre", reportUrl+"/skogeieravtale"+reportSuffix+".html", nil)
		b.AddReportTemplate(prefix+"PSkogkultur", reportUrl+"/skogeierkontrakt"+reportSuffix+".html", nil)
	} else {
		//skogeieravtale-io vs skogeieravtale-sk
		b.AddReportTemplate(prefix+"POrdre", reportUrl+"/skogeieravtale-io"+reportSuffix+".html", nil)
		b.AddReportTemplate(prefix+"PSkogkultur", reportUrl+"/skogeieravtale-sk"+reportSuffix+".html", nil)
	}

	b.AddReportTemplate(prefix+"Miljo", reportUrl+"/hogstinstruks"+reportSuffix+".html", nil)
	b.AddReportTemplate(prefix+"FeltkontrollDrift", reportUrl+"/feltkontrolldrift"+reportSuffix+".html", nil)
	b.AddReportTemplate(prefix+"FeltkontrollAvsluttet", reportUrl+"/feltkontrollavsluttet"+reportSuffix+".html", nil)
	b.AddReportTemplate(prefix+"PPlantingGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PPlantingGeom"+reportSuffix+".html", reportLanguages)
	b.AddReportTemplate(prefix+"PUngskogpleieGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PUngskogpleieGeom"+reportSuffix+".html", reportLanguages)
	b.AddReportTemplate(prefix+"PForhandsryddingGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PForhandsryddingGeom"+reportSuffix+".html", reportLanguages)
	b.AddReportTemplate(prefix+"PMarkberedningGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PMarkberedningGeom"+reportSuffix+".html", nil)
	b.AddReportTemplate(prefix+"PGrofterenskGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PGrofterenskGeom"+reportSuffix+".html", nil)
	b.AddReportTemplate(prefix+"PSaaingGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PSaaingGeom"+reportSuffix+".html", reportLanguages)
	b.AddReportTemplate(prefix+"PSkogskadeGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PSkogskadeGeom"+reportSuffix+".html", reportLanguages)
	b.AddReportTemplate(prefix+"PInfrastrukturGeom", reportUrl+"/arbeidsinstruks-"+prefix+"PInfrastrukturGeom"+reportSuffix+".html", reportLanguages)

	if b.IsNortømmer() {
		b.AddReportTemplate(prefix+"ForeVar", reportUrl+"/forevar"+reportSuffix+".html", nil)
	}

	b.VerifyReportTemplates(false)

	//------------------------------------------------------------------------------------
	//search
	//------------------------------------------------------------------------------------
	redColor := FormDrawingInfoColor(255, 0, 0, 255)
	solidRedLine := FormDrawingSolidLineSymbol(redColor, 2)

	b.AddSearch("Navn", "matrikkel", "Søk i matrikkelen").
		WithSymbol(Line, solidRedLine).
		WithField("", "Kommune", "muni").
		WithField("", "Fornavn", "text").
		WithField("", "Etternavn", "text")
	b.AddSearch("Gårdsnummer", "gardskart", "Søk i gårdskart").
		WithField("", "Kommune", "muni").
		WithField("", "Gårdsnummer", "int").
		WithField("", "Bruksnummer", "int")
	if b.IsFritzøe() || b.IsFritzøeSkoger() {
		b.AddSearch("Veinavn", "vtfront", "Søk i veinavn").
			WithSymbol(Line, solidRedLine).
			WithField("Navn", "navn", "text").
			WithURL("https://vtfront.feltgismaps.com/36e22217-cb47-cf2c-ae48-832fc31bbcb7").
			WithAutocompleteField("navn").
			WithMaxAutoCompleteResults(5).
			WithResultTitleTemplate("{{navn}}").
			WithResultSubTitleTemplate("{{veiklasse}}")
	}
	if b.IsForestOwner() {
		b.AddSearch("Bestand", "syncdb", "Søk i bestand (offline)").
			WithSymbol(Line, solidRedLine).
			WithField("bestandsnummer", "presentasj", "text").
			WithCollection(prefix + "VTStand").
			WithAutocompleteField("presentasj").
			WithMaxAutoCompleteResults(5).
			WithResultTitleTemplate("{{presentasj}} {{bontresl}}{{bontekst}}").
			WithResultSubTitleTemplate("{{navn}} prod {{arealprod}}daa")
	} else if b.IsNortømmer() {
		b.AddSearch("Bestand", "vtfront", "Søk i bestand").
			WithSymbol(Line, solidRedLine).
			WithField("Bestandsnummer", "best_nr", "text").
			WithURL("https://vtfront.feltgismaps.com/6f04efcc-9f8c-4ad0-6095-0084099e3b4a").
			WithAutocompleteField("best_nr").
			WithMaxAutoCompleteResults(5).
			WithResultTitleTemplate("{{best_nr}}").
			WithResultSubTitleTemplate("bestand {{best_nr}}, areal {{arealprod}}")
	}

	//------------------------------------------------------------------------------------
	//serverQueries
	//------------------------------------------------------------------------------------
	b.AddServerQuery(
		"matrikkel",
		[]string{
			"adresse",
			"bruksnavn",
			"bruksnr",
			"eierdatofra",
			"eieretternavn",
			"eierfodselsdato",
			"eierfornavn",
			"eiernavn",
			"eierorgnummer",
			"eierpostadresselinje1",
			"eierpostadresselinje2",
			"eierpostadresselinje3",
			"eierpostadresselinje4",
			"eierpostid",
			"eierpostlandkode",
			"eierpostnr",
			"eiertype",
			"fylkesnavn",
			"gardsnr",
			"hovedbruk_bruksnr",
			"hovedbruk_gardsnr",
			"hovedbruk_kommunenr",
			"kommunenavn",
			"kommunenr",
			"lagretareal",
			"matrikkeltype",
			"postnavn",
			"postnr",
			"teigid",
		},
	)

	//------------------------------------------------------------------------------------
	//sync
	//------------------------------------------------------------------------------------
	if b.IsProd() {
		b.AddSync(prefix, "planner-sync-dbs")
	} else {
		b.AddSync(prefix, "planner-sync-dbs-test")
	}

	var attachmentBucketSuffix string
	if b.IsProd() {
		attachmentBucketSuffix = ""
	} else {
		attachmentBucketSuffix = "-test"
	}

	b.AddSyncCollection(prefix + "Session").
		WithAlias("Sesjonsdata").
		WithAppFeature("Session").
		WithBatchSize(100).
		WithAttachmentsBucket(strings.ToLower(prefix) + "session" + attachmentBucketSuffix).
		WithDisablePull(true)

	b.AddSyncCollection(prefix + "MiljoPunkt").
		WithSyncFilterSyncEverything()
	b.AddSyncCollection(prefix + "MiljoLinjer").
		WithSyncFilterSyncEverything()
	b.AddSyncCollection(prefix + "MiljoFlater").
		WithSyncFilterSyncEverything()

	b.AddSyncCollection(prefix + "PDelteObjekter").
		WithAppFeature("SharedObjects").
		WithBatchSize(100).
		WithSyncFilterSyncEverything()

	if b.IsProd() {
		b.AddSyncCollection(prefix + "POrdre").
			WithAppFeature("Ordre").
			WithBatchSize(100).
			WithIndexFields("_headId").
			WithAttachmentsBucket(strings.ToLower(prefix) + "pordre")
	} else {
		b.AddSyncCollection(prefix + "POrdre").
			WithAppFeature("Ordre").
			WithBatchSize(100).
			WithIndexFields("_headId").
			WithAttachmentsBucket(strings.ToLower(prefix) + "pordre-test")
	}

	if b.IsFritzøeSkoger() || b.IsCappelen() || b.IsLøvenskioldFossum() || b.IsMathiesen() {
		b.AddSyncCollection(b.company.Prefix + "VTStand").
			WithAttachmentsBucket("planner-sync-dbs" + attachmentBucketSuffix).
			WithInitialDump(b.company.Prefix + "VTStand.sync.db").
			WithAlias("Bestand").
			WithAppFeature("Stand").
			WithBatchSize(1000).
			WithIndexFields("presentasj").
			WithFtsFields("presentasj").
			WithSyncFilterSyncEverything()
	}

	b.AddSyncCollection(prefix + "PTommerdrift").
		WithAlias("Driftsområde").
		WithAppFeature("Tømmerdrift").
		WithBatchSize(1000).
		WithIndexFields("_headId")

	if b.company.hasFeature(hasVsys) {
		// When VSYS enabled, these data are backed by vsys and not synced to FG.
		b.AddSyncCollection("NT" + "Skogdata").
			WithIndexFields("_headId").
			WithDisablePull(true).
			WithDisablePush(true).
			WithAppFeature("SupplyPoints")
	} else {
		// When VSYS disabled, these data are backed by FG and synced to FG.
		b.AddSyncCollection(prefix + "Skogdata").
			WithIndexFields("_headId").
			WithDisablePull(false).
			WithDisablePush(false).
			WithAppFeature("SupplyPoints")
	}

	if !b.company.hasFeature(hasVsys) {
		b.AddSyncCollection("FG" + "Register").
			WithBatchSize(10000).
			WithIndexFields("OrgNum").
			WithFtsFields("Fts").
			WithDisablePush(true).
			WithAppFeature("Register").
			WithSyncFilterSyncEverything()
	} else {
		b.AddSyncCollection("NT" + "Register").
			WithInitialDump("NT" + "Register.sync.db").
			WithBatchSize(10000).
			WithIndexFields("OrgNum").
			WithFtsFields("Fts").
			WithDisablePush(true).
			WithAppFeature("Register")
	}

	b.AddSyncCollection("Grunneiendom").
		WithInitialDump("Grunneiendom.sync.db").
		WithBatchSize(10000).
		WithIndexFields("komnr")
	b.AddSyncCollection("LandbruksForetak").
		WithInitialDump("LandbruksForetak.sync.db").
		WithBatchSize(10000).
		WithIndexFields("komnr", "orgnr")

	b.AddSyncCollection(prefix+"RegistreringerPunkt").
		WithAppFeature("RegistreringerPunkt").
		WithIndexFields("_headId", "_parentGlobalId", "_referenceCollection", "_visibilityMode")
	b.AddSyncCollection(prefix+"RegistreringerLinjer").
		WithIndexFields("_headId", "_parentGlobalId", "_visibilityMode")

	b.AddSyncCollection(prefix+"Sporlogg").
		WithAppFeature("Sporlogg").
		WithIndexFields("_headId").
		WithBatchSize(1000).
		WithIndexFields("_headId", "_parentGlobalId")

	b.AddSyncCollection(prefix+"RegistreringerFlater").
		WithIndexFields("_headId", "_parentGlobalId", "_visibilityMode")
	b.AddSyncCollection(prefix + "Miljo").
		WithAppFeature("Miljø").
		WithIndexFields("_headId")
	b.AddSyncCollection(prefix + "FeltkontrollDrift").
		WithAppFeature("FeltkontrollDrift").
		WithIndexFields("_headId")
	b.AddSyncCollection(prefix + "FeltkontrollAvsluttet").
		WithAppFeature("FeltkontrollAvsluttet").
		WithIndexFields("_headId")

	skogkulturCollection := b.AddSyncCollection(prefix+"PSkogkultur").
		WithAlias("Skogkultur").
		WithAppFeature("Skogkultur").
		WithIndexFields("_globalId", "_headId").
		WithSyncByLocalGlobalIds(true).
		WithChild(prefix+"PPlantingGeom", "_parentGlobalId").
		WithChild(prefix+"PMarkberedningGeom", "_parentGlobalId").
		WithChild(prefix+"PGrofterenskGeom", "_parentGlobalId").
		WithChild(prefix+"PUngskogpleieGeom", "_parentGlobalId").
		WithChild(prefix+"PForhandsryddingGeom", "_parentGlobalId").
		WithChild(prefix+"PSaaingGeom", "_parentGlobalId").
		WithChild(prefix+"PSkogskadeGeom", "_parentGlobalId").
		WithChild(prefix+"PInfrastrukturGeom", "_parentGlobalId").
		WithChild(prefix+"PGjodslingGeom", "_parentGlobalId").
		WithChild(prefix+"PSproytingGeom", "_parentGlobalId").
		WithChild(prefix+"PGravearbeidGeom", "_parentGlobalId").
		WithChild(prefix+"PSporskaderettingGeom", "_parentGlobalId").
		WithChild(prefix+"PStammekvistingGeom", "_parentGlobalId").
		WithChild(prefix+"RegistreringerPunkt", "_parentGlobalId").
		WithChild(prefix+"RegistreringerLinjer", "_parentGlobalId").
		WithChild(prefix+"RegistreringerFlater", "_parentGlobalId")

	if b.IsDev() || b.IsContractor() {
		skogkulturCollection.WithChild("FGPJobAssignment", "_parentGlobalId")
	}

	if b.IsProd() {
		skogkulturCollection.WithAttachmentsBucket(strings.ToLower(prefix) + "pskogkultur")
	} else {
		skogkulturCollection.WithAttachmentsBucket(strings.ToLower(prefix) + "pskogkultur-test")
	}

	skogkulturCollection.
		WithChild(prefix+"Sporlogg", "_parentGlobalId")

	if b.IsForestOwner() || b.IsContractor() {
		stems := b.AddSyncCollection(prefix + "Stem").
			WithAlias("Stammedata fra produksjonsfil").
			WithAppFeature("Stems").
			WithBatchSize(2000).
			WithIndexFields("_headId")

		if !b.IsContractor() {
			stems.WithAndFilterList(ConfigSyncFilterValue{
				FieldName: "_shareStemPosition_Boolean",
				Operator:  "!=",
				IntValue:  0,
			})
		}
	}

	if b.IsNortømmer() {
		//old system, SluttrapportPlanting shared with feltlogg
		skogkulturCollection.
			WithChild(prefix+"SluttrapportPlanting", "_parentGlobalId").
			WithChild(prefix+"SluttrapportPlantingTelling", "_parentGlobalId")
		b.AddSyncCollection(prefix + "SluttrapportPlanting").
			WithAlias("Planting feltkontroll").
			WithIndexFields("_parentGlobalId").
			WithAttachmentsBucket(strings.ToLower(prefix) + "_pfeltkontroll_planting" + attachmentBucketSuffix) //same as the others...
		b.AddSyncCollection(prefix+"SluttrapportPlantingTelling").
			WithAlias("Planting feltkontroll telling").
			WithIndexFields("_parentGlobalId", "_feltkontrollGlobalId")
	} else {
		//new system, dedicated PFeltkontrollPlanting file for planner feltkontroll planting
		skogkulturCollection.
			WithChild(prefix+"PFeltkontrollPlanting", "_parentGlobalId").
			WithChild(prefix+"PFeltkontrollPlantingTelling", "_parentGlobalId")
		b.AddSyncCollection(prefix + "PFeltkontrollPlanting").
			WithAlias("Planting feltkontroll").
			WithIndexFields("_parentGlobalId").
			WithAttachmentsBucket(strings.ToLower(prefix) + "_pfeltkontroll_planting" + attachmentBucketSuffix)
		b.AddSyncCollection(prefix+"PFeltkontrollPlantingTelling").
			WithAlias("Planting feltkontroll telling").
			WithIndexFields("_parentGlobalId", "_feltkontrollGlobalId")
	}
	skogkulturCollection.
		WithChild(prefix+"PFeltkontrollUngskogpleie", "_parentGlobalId").
		WithChild(prefix+"PFeltkontrollUngskogpleieTelling", "_parentGlobalId")
	b.AddSyncCollection(prefix + "PFeltkontrollUngskogpleie").
		WithAlias("Ungskogpleie feltkontroll").
		WithIndexFields("_parentGlobalId").
		WithAttachmentsBucket(strings.ToLower(prefix) + "_pfeltkontroll_ungskogpleie" + attachmentBucketSuffix)
	b.AddSyncCollection(prefix+"PFeltkontrollUngskogpleieTelling").
		WithAlias("Ungskogpleie feltkontroll telling").
		WithIndexFields("_parentGlobalId", "_feltkontrollGlobalId")

	b.AddSyncCollection(prefix+"PPlantingGeom").
		WithAlias("Planting").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PUngskogpleieGeom").
		WithAlias("Ungskogpleie").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PMarkberedningGeom").
		WithAlias("Markberedning").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PGrofterenskGeom").
		WithAlias("Grøfterensk").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PForhandsryddingGeom").
		WithAlias("Forhåndsrydding").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PSaaingGeom").
		WithAlias("Såing").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PSkogskadeGeom").
		WithAlias("Skogskade").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PInfrastrukturGeom").
		WithAlias("Infrastruktur").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PGjodslingGeom").
		WithAlias("Gjodsling").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PSproytingGeom").
		WithAlias("Sproyting").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PGravearbeidGeom").
		WithAlias("Gravearbeid").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PSporskaderettingGeom").
		WithAlias("Sporskaderetting").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")
	b.AddSyncCollection(prefix+"PStammekvistingGeom").
		WithAlias("Stammekvisting").
		WithAppFeature("SkogkulturGeom").
		WithIndexFields("_parentGlobalId", "_headId")

	b.AddSyncCollection(prefix + "ForeVar").
		WithAlias("Føre var").
		WithAppFeature("ForeVar").
		WithIndexFields("_headId")

	if !b.IsProd() {
		b.AddSyncCollection(prefix + "PCustomer").
			WithAlias("Kunderegister").
			WithAppFeature("CustomerDatabase").
			WithIndexFields("orgNum").
			WithFtsFields("name")
	}

	if b.IsDev() || b.IsContractor() {
		b.AddSyncCollection("FGPJobAssignment").
			WithAppFeature("JobAssignment").
			WithAlias("Tilknytta oppdrag").
			WithIndexFields("_parentGlobalId", "_headId").
			WithDisableMap()
	}

	b.VerifySync()
}
