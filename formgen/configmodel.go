package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type ConfigAdmin struct {
	SuperUsers []string `json:"superusers,omitempty"`
}

type ConfigUserEmail struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

type ConfigEnvironmentClearedEmails struct {
	To       []ConfigUserEmail `json:"to"`
	CC       []ConfigUserEmail `json:"cc"`
	BCC      []ConfigUserEmail `json:"bcc"`
	Subject  string            `json:"subject"`
	Template string            `json:"template"`
}

type ConfigCustomConfig struct {
	AllmaVectorDataURL     string `json:"allmaVectorDataURL,omitempty"`
	AllmaVectorIdentifyURL string `json:"allmaVectorIdentifyURL,omitempty"`
	AllmaVectorLoginURL    string `json:"allmaVectorLoginURL,omitempty"`
	AllmaVectorStyleURL    string `json:"allmaVectorStyleURL,omitempty"`
	CustomerDbVersion      string `json:"customerDbVersion"`
	SkogkulturUsers        string `json:"skogkulturUsers"`
}

type ConfigCustomConfigObjectsAllmaClient struct {
	DataURL          string `json:"dataURL,omitempty"`
	Default          bool   `json:"default,omitempty"`
	IdentifyURL      string `json:"identifyURL,omitempty"`
	ImageIdentifyURL string `json:"imageIdentifyURL,omitempty"`
	ImageURL         string `json:"imageURL,omitempty"`
	LoginURL         string `json:"loginURL,omitempty"`
	Name             string `json:"name,omitempty"`
	StyleURL         string `json:"styleURL,omitempty"`
}
type ConfigCustomConfigObjectsCountOperation struct {
	CountField       string `json:"countField,omitempty"`
	CountType        string `json:"countType,omitempty"`
	SearchCollection string `json:"searchCollection,omitempty"`
	SearchFieldName  string `json:"searchFieldName,omitempty"`
	SearchFieldValue string `json:"searchFieldValue,omitempty"`
	TargetCollection string `json:"targetCollection,omitempty"`
	WriteToField     string `json:"writeToField,omitempty"`
}

type ConfigCustomConfigObjectsUpdateReaction struct {
	ReactionCollection string `json:"reactionCollection"`
	SearchCollection   string `json:"searchCollection"`
	SearchKey          string `json:"searchKey"`
	SearchValue        string `json:"searchValue"`
}

type ConfigCustomConfigObjects struct {
	AllmaClients                    []ConfigCustomConfigObjectsAllmaClient    `json:"allmaClients,omitempty"`
	CountOperations                 []ConfigCustomConfigObjectsCountOperation `json:"countOperations,omitempty"`
	UpdateReactions                 []ConfigCustomConfigObjectsUpdateReaction `json:"updateReactions,omitempty"`
	UsersNotRequiredToSignWorkOrder []string                                  `json:"usersNotRequiredToSignWorkOrder,omitempty"`
}

type ConfigFeatureSwitch struct {
	Enabled  bool   `json:"enabled"`
	Feature  string `json:"feature,omitempty"`
	Username string `json:"username,omitempty"`
}

type ConfigNewFeatureFromStand struct {
	Target     string `json:"target"`
	Age        string `json:"age,omitempty"`
	Bonitet    string `json:"bonitet,omitempty"`
	VolumeGran string `json:"volumeGran,omitempty"`
	VolumeFuru string `json:"volumeFuru,omitempty"`
	VolumeLauv string `json:"volumeLauv,omitempty"`
	StandNum   string `json:"standNum,omitempty"`
	TeigNum    string `json:"teigNum,omitempty"`
	Title      string `json:"title"`
}

type ConfigNewFeatureCopyOperation struct {
	Source     string            `json:"source"`
	Target     string            `json:"target"`
	CopyFields map[string]string `json:"copyFields"`
}

type ConfigNewFeatureDefaults struct {
	Collection string                 `json:"collection"`
	Values     map[string]interface{} `json:"values"`
}

type ConfigMapAddFeature struct {
	Collection  string `json:"collection"`
	Displayname string `json:"displayname"`
}

type ConfigMapEditFeature struct {
	Collection      string `json:"collection"`
	DeleteMode      string `json:"deleteMode"`
	EditMode        string `json:"editMode"`
	GeometryEditing string `json:"geometryEditing,omitempty"`
}

type ConfigMapBackground struct {
	Hidden bool   `json:"hidden,omitempty"`
	Name   string `json:"name"`
	Type   string `json:"type"`
	Url    string `json:"url"`
}

type ConfigMapLayerBodyReplacement struct {
	Collection     string `json:"collection"`
	ContentType    string `json:"contenttype"`
	Replace        string `json:"replace"`
	Search         string `json:"search"`
	UrlContains    string `json:"urlcontains"`
	UrlNotContains string `json:"urlnotcontains"`
}

type ConfigMapLayerCacheWarmupUrls struct {
	Collection string   `json:"collection"`
	Urls       []string `json:"urls"`
}

type ConfigMapLayerRendering struct {
	DefinitionExpression string   `json:"definitionExpression,omitempty"`
	IncludeFields        []string `json:"includeFields"`
}

type ConfigMapLayer struct {
	Alias               string                          `json:"alias,omitempty"`
	BodyReplacements    []ConfigMapLayerBodyReplacement `json:"bodyreplacements,omitempty"`
	CacheWarmupUrls     []ConfigMapLayerCacheWarmupUrls `json:"cachewarmupurls,omitempty"`
	DefaultOpacity      *float64                        `json:"defaultopacity,omitempty"`
	Hidefromlayerlist   *bool                           `json:"hidefromlayerlist,omitempty"`
	KeepInBackgroundMap *bool                           `json:"keepInBackgroundMap,omitempty"`
	Minscale            int                             `json:"minscale,omitempty"`
	Minopacity          *float64                        `json:"minopacity,omitempty"`
	IdentifyDisabled    *bool                           `json:"identifyDisabled,omitempty"`
	LayerId             *string                         `json:"layerid,omitempty"`
	MinLevel            *int                            `json:"minLevel,omitempty"`
	Name                string                          `json:"name,omitempty"`
	Rendering           *ConfigMapLayerRendering        `json:"rendering,omitempty"`
	Type                string                          `json:"type,omitempty"`
	Preview             string                          `json:"preview,omitempty"`
	Url                 string                          `json:"url,omitempty"`
	StatusUrl           *string                         `json:"statusUrl,omitempty"`
}

type ConfigMap struct {
	TilInfo                string                 `json:"_til info,omitempty"`
	AddFeatures            []ConfigMapAddFeature  `json:"addFeatures,omitempty"`
	Background             []ConfigMapBackground  `json:"background,omitempty"`
	EditFeatures           []ConfigMapEditFeature `json:"editFeatures,omitempty"`
	FreeHandPointDistance  *int                   `json:"freeHandPointDistance,omitempty"`
	Layers                 []*ConfigMapLayer      `json:"layers"`
	PrioritizedExtentLayer string                 `json:"prioritizedExtentLayer,omitempty"`
	Sections               []ConfigSection        `json:"sections,omitempty"`
}

type ConfigSection struct {
	Id    int    `json:"id"`
	Title string `json:"title"`
}

type ConfigPurchaseOrderPresets struct {
	IssuingInstruction []map[string]interface{} `json:"issuingInstruction,omitempty"`
	DefaultOrders      []map[string]interface{} `json:"defaultOrders,omitempty"`
}

type ConfigReport struct {
	Collection string   `json:"collection"`
	Template   string   `json:"template"`
	Languages  []string `json:"languages,omitempty"`
}

type ConfigGenericReports struct {
	Template string `json:"template"`

	//key/url mapping
	StaticHtmlTemplates map[string]string `json:"staticHtmlTemplates,omitempty"`
}

type ConfigSearchField struct {
	Alias string `json:"alias,omitempty"`
	Title string `json:"title"`
	Type  string `json:"type"`
}

type ConfigSearch struct {
	AutocompleteField      string              `json:"autocompleteField,omitempty"`
	ButtonText             string              `json:"buttonText"`
	Fields                 []ConfigSearchField `json:"fields,omitempty"`
	Collection             string              `json:"collection,omitempty"`
	MaxAutoCompleteResults *int                `json:"maxAutoCompleteResults,omitempty"`
	ResultSubTitleTemplate string              `json:"resultSubTitleTemplate,omitempty"`
	ResultTitleTemplate    string              `json:"resultTitleTemplate,omitempty"`
	Symbol                 string              `json:"symbol,omitempty"`
	Title                  string              `json:"title"`
	Type                   string              `json:"type"`
	URL                    string              `json:"url,omitempty"`
}

type ConfigServerQuery struct {
	Name         string   `json:"name"`
	ReturnFields []string `json:"returnFields"`
}

type ConfigSyncFilterValue struct {
	FieldName     string `json:"attributeName"`
	Operator      string `json:"operator"`
	IntValue      int    `json:"intValue"`
	StringValue   string `json:"stringValue,omitempty"`
	ContainsValue string `json:"containsValue,omitempty"`
}

type ConfigSyncFilter struct {
	SyncEverything    *bool                   `json:"syncEverything,omitempty"`
	UseServiceOrderId *bool                   `json:"useServiceOrderId,omitempty"`
	AndFilters        []ConfigSyncFilterValue `json:"ands,omitempty"`
	OrFilters         []ConfigSyncFilterValue `json:"ors,omitempty"`
}

type ConfigSyncCollectionChild struct {
	CollectionName string `json:"collectionName"`
	ReferenceField string `json:"referenceField"`
}

type ConfigSyncCollection struct {
	Alias                    *string                     `json:"alias,omitempty"`
	AppFeature               *string                     `json:"appFeature,omitempty"`
	AttachmentFilenamesField *string                     `json:"attachmentFilenamesField,omitempty"`
	AttachmentsBucket        *string                     `json:"attachmentsBucket,omitempty"`
	BatchSize                *int                        `json:"batchSize,omitempty"`
	DisablePull              *bool                       `json:"disablePull,omitempty"`
	DisablePush              *bool                       `json:"disablePush,omitempty"`
	FtsFields                []string                    `json:"ftsFields,omitempty"`
	Children                 []ConfigSyncCollectionChild `json:"children,omitempty"`
	IndexFields              []string                    `json:"indexFields,omitempty"`
	InitialDump              *string                     `json:"initialDump,omitempty"`
	Name                     string                      `json:"name"`
	Rpi                      *bool                       `json:"rpi,omitempty"`
	SkipWaitOnAdd            *bool                       `json:"skipWaitOnAdd,omitempty"`
	SyncFilter               *ConfigSyncFilter           `json:"syncFilter,omitempty"`
	SyncByLocalGlobalIds     *bool                       `json:"syncByLocalGlobalIds,omitempty"`
	DisableMap               *bool                       `json:"disableMap,omitempty"`
}

type ConfigSync struct {
	Bucket          string                  `json:"bucket,omitempty"`
	Collections     []*ConfigSyncCollection `json:"collections"`
	IntervalMinutes *int                    `json:"intervalMinutes,omitempty"`
	OrgPrefix       string                  `json:"orgPrefix,omitempty"`
}

type ConfigVsysFeature struct {
	Feature string   `json:"feature,omitempty"`
	Users   []string `json:"users,omitempty"`
}

type ConfigUserAppSettingDefaultValue struct {
	BoolValue *bool `json:"boolValue,omitempty"`
}

type ConfigUserAppSetting struct {
	Title        string                            `json:"title"`
	SubTitle     string                            `json:"subTitle,omitempty"`
	ID           string                            `json:"id"`
	DefaultValue *ConfigUserAppSettingDefaultValue `json:"defaultValue,omitempty"`
}

type ConfigProximityAlarmFilter struct {
	FieldName   string `json:"fieldName"`
	StringValue string `json:"stringValue"`
}

const (
	ProximityAlarmSourceCollection = "collection"
	ProximityAlarmSourceVector     = "vector"
	ProximityAlarmSourceArcGIS     = "arcgis"
)

type ConfigProximityAlarmSource struct {
	Type             string                       `json:"type"`
	Identifier       string                       `json:"id"`
	WhereFilterAnyOf []ConfigProximityAlarmFilter `json:"whereFilterAnyOf,omitempty"`
}

type ConfigProximityAlarmSetup struct {
	//alarm gradient distance
	GradientCutoffDistanceMeters float64 `json:"gradientCutoffDistanceMeters,omitempty"`
	//default is that the alarm goes off inside the polygon, but for actions we want the alarm to go off outside
	AlarmOutside bool `json:"alarmOutside,omitempty"`
	//UsedToDetermineExtent is used to determine the extent of the alarm
	UsedToDetermineExtent bool `json:"usedToDetermineExtent,omitempty"`
}

type ConfigProximityAlarm struct {
	// User friendly name
	Name string `json:"name"`
	// 'collection' or 'url'
	// collection name or url
	Source ConfigProximityAlarmSource `json:"source"`

	Setup ConfigProximityAlarmSetup `json:"setup"`
}

type Config struct {
	Admin                                  *ConfigAdmin                    `json:"admin,omitempty"`
	EnvironmentClearedEmails               *ConfigEnvironmentClearedEmails `json:"environmentClearedEmails,omitempty"`
	Settings                               []ConfigUserAppSetting          `json:"settings,omitempty"`
	CustomConfig                           *ConfigCustomConfig             `json:"customConfig,omitempty"`
	CustomConfigObjects                    *ConfigCustomConfigObjects      `json:"customConfigObjects,omitempty"`
	EnabledFeatures                        []string                        `json:"enabledfeatures,omitempty"`
	FeatureSwitches                        []ConfigFeatureSwitch           `json:"featureSwitches,omitempty"`
	NewFeatureDefaults                     []ConfigNewFeatureDefaults      `json:"newFeatureDefaults,omitempty"`
	NewFeatureCopyOperations               []ConfigNewFeatureCopyOperation `json:"newFeatureCopyOperations,omitempty"` // TODO: legacy, will be removed when newFeatureFromStand is 100%
	Map                                    ConfigMap                       `json:"map"`
	Name                                   string                          `json:"name"`
	OrgNum                                 string                          `json:"orgNum,omitempty"`
	ProducedMessagesMustHaveCreatedBy      string                          `json:"producedMessagesMustHaveCreatedBy,omitempty"`
	PurchaseOrderPresets                   *ConfigPurchaseOrderPresets     `json:"purchaseOrderPresets,omitempty"`
	GenericReports                         *ConfigGenericReports           `json:"genericReports,omitempty"`
	Reports                                []ConfigReport                  `json:"reports,omitempty"` //TODO: legacy, will be removed when genericReports are 100%
	Search                                 []*ConfigSearch                 `json:"search,omitempty"`
	SellerContactEMailCopyMeasuringReceipt *bool                           `json:"sellerContactEMailCopyMeasuringReceipt,omitempty"`
	FillInSellerEmail                      *bool                           `json:"fillInSellerEmail,omitempty"`
	ServerQueries                          []ConfigServerQuery             `json:"serverQueries,omitempty"`
	Sync                                   ConfigSync                      `json:"sync"`
	VsysFeatures                           []ConfigVsysFeature             `json:"vsysFeatures,omitempty"`

	NewFeatureFromStand []ConfigNewFeatureFromStand `json:"newFeatureFromStand,omitempty"`

	ProximityAlarms []ConfigProximityAlarm `json:"proximityAlarms,omitempty"`

	FeatureNotifications []ConfigFeatureNotification `json:"featureNotifications,omitempty"`

	ServerRefreshURL string `json:"serverRefreshURL,omitempty"`
}

type MailReceiver struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type MailReceivers []*MailReceiver

type ConfigFeatureNotification struct {
	Collection           string        `json:"collection"`
	NotificationTemplate string        `json:"notificationTemplate"`
	EmailNotifications   MailReceivers `json:"emailNotifications,omitempty"`
	SmsDisabled          bool          `json:"smsDisabled,omitempty"`
}

func (f *Config) Json() string {

	jsonBytes := new(bytes.Buffer)
	e := json.NewEncoder(jsonBytes)
	e.SetEscapeHTML(false) //we need this to not get \u003c
	e.SetIndent("", "    ")
	err := e.Encode(f)

	// write our opened jsonFile as a byte array.
	if err != nil {
		fmt.Println(err)
	}

	jsonString := jsonBytes.String()

	return jsonString + "\n" //extra \n in old files
}
