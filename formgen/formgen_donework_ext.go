package main

func PopulateDoneWorkExtGmForm(b *FormBuilder) {

	//common
	populateDoneworkFormCommon(b)

	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//LOGIC
	/*
		subCollectionIsSaaing := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PSaaingGeom"),
		}
		subCollectionIsPlanting := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PPlantingGeom"),
		}
		subCollectionIsGrofteresk := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PGrofterenskGeom"),
		}
	*/
	subCollectionIsUngskogpleie := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PUngskogpleieGeom"),
	}
	subCollectionIsForhandsrydding := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PForhandsryddingGeom"),
	}
	subCollectionIsMarkberedning := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PMarkberedningGeom"),
	}
	subCollectionIsGjodsling := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PGjodslingGeom"),
	}
	subCollectionIsGravearbeid := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PGravearbeidGeom"),
	}
	subCollectionIsSproyting := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PSproytingGeom"),
	}
	subCollectionIsStammekvisting := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PStammekvistingGeom"),
	}

	//section 1
	b.AddSection("")

	b.AddUneditableNullableIntegerField("bestand", "Bestand").
		WithAliasTranslation("en", "Stand").
		WithLogic().
		WithVisibleIfAllOf(
			FormFieldLogicField{
				Operator:       "exists",
				OtherFieldName: "bestand",
			},
			FormFieldLogicField{
				Operator:       "missing",
				OtherFieldName: "bestand_str",
			},
		)
	b.AddUneditableNullableStringField("bestand_str", "Bestand").
		WithAliasTranslation("en", "Stand").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:       "exists",
			OtherFieldName: "bestand_str",
		})

	b.AddUneditableNullableDoubleField("area", "Areal").
		WithAliasTranslation("en", "Area")

	b.AddNullableBooleanField("contractor_done_Boolean", "Markér som ferdig (entrep)").
		WithAliasTranslation("en", "Mark done (by contractor)")

	//--------------------
	//Ungskogpleie

	//CostPrice
	b.AddRequiredIntegerField("CostPrice", "Pris").
		WithAliasTranslation("en", "Pris").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
			subCollectionIsForhandsrydding,
		)

	//Quantity (Count)
	b.AddUneditableNullableStringField("QuantityCount_Decimal1", "Antall daa planlagt").
		WithAliasTranslation("en", "Antall daa planlagt").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
			subCollectionIsForhandsrydding,
			subCollectionIsMarkberedning,
			subCollectionIsGjodsling,
			subCollectionIsSproyting,
			subCollectionIsStammekvisting,
		)

	//QuantityDelivered (Count)
	b.AddRequiredDecimalField("QuantityDeliveredCount_Decimal1", "Antall daa utført", 1).
		WithAliasTranslation("en", "Antall daa utført").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
			subCollectionIsForhandsrydding,
			subCollectionIsMarkberedning,
			//subCollectionIsGjodsling,
			subCollectionIsSproyting,
			subCollectionIsStammekvisting,
		)

	//PlantsPerAcreBeforeAdjustment
	b.AddRequiredIntegerField("PlantsPerAcreBeforeAdjustmentRydding", "Tetthet/daa før rydding").
		WithAliasTranslation("en", "Tetthet/daa før rydding").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
			subCollectionIsForhandsrydding,
		)

	//PlantsPerAcre
	b.AddRequiredIntegerField("PlantsPerAcreRydding", "Tetthet/daa etter rydding").
		WithAliasTranslation("en", "Tetthet/daa etter rydding").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
			subCollectionIsForhandsrydding,
		)

	//TreeHeightAfterAdjustment(Rydding)
	b.AddRequiredDecimalField("TreeHeightAfterAdjustmentRydding_Decimal1", "Middelhøyde ryddetre", 1).
		WithAliasTranslation("en", "Middelhøyde ryddetre").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
		)

	//ResidualTreeHeightAfterAdjustment(Rydding)
	b.AddRequiredDecimalField("ResidualTreeHeightAfterAdjustmentRydding_Decimal1", "Middelhøyde etter rydding", 1).
		WithAliasTranslation("en", "Middelhøyde etter rydding").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsUngskogpleie,
		)

	//--------------------
	//Forhandsrydding

	//CostPrice
	//Quantity (Count)
	//QuantityDelivered (Count)
	//PlantsPerAcreBeforeAdjustment (Rydding)
	//PlantsPerAcre (Rydding)

	//--------------------
	//Markberedning

	//Quantity (Count)
	//QuantityDelivered (Count)
	//PlantsPerAcre (Markberedning)
	b.AddRequiredIntegerField("PlantsPerAcreMarkberedning", "Tetthet/daa etter markberedning").
		WithAliasTranslation("en", "Tetthet/daa etter markberedning").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsMarkberedning,
		)

	//--------------------
	//Gjodsling

	//Quantity (Count)
	//QuantityDelivered (Count/Optional)
	b.AddNullableDecimalField("QuantityDeliveredCountOptional_Decimal1", "Antall daa utført", 1).
		WithAliasTranslation("en", "Antall daa utført").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsGjodsling,
		)

	//--------------------
	//Gravearbeid

	//Quantity (Meter)
	b.AddUneditableNullableIntegerField("QuantityMeter", "Meter planlagt").
		WithAliasTranslation("en", "Meter planlagt").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsGravearbeid,
		)

	//QuantityDelivered (Meter)
	b.AddRequiredIntegerField("QuantityDeliveredMeter", "Meter utført").
		WithAliasTranslation("en", "Meter utført").
		WithLogic().
		WithVisibleIfAnyOf(
			subCollectionIsGravearbeid,
		)

	//--------------------
	//Sproyting

	//Quantity (Count)
	//QuantityDelivered (Count)

	//--------------------
	//Stammekvisting

	//Quantity (Count)
	//QuantityDelivered (Count)

	//----------------------------------------
	//----------------------------------------
	// IDS!

	b.SetFieldIdSeries(
		6,
		"area",
	)
	b.SetFieldIdSeries(
		11,
		"contractor_done_Boolean",
	)
	b.SetFieldIdSeries(
		15,
		"bestand",
		"bestand_str",
	)

	b.SetFieldIdSeries(
		1000,
		"CostPrice",
	)
	b.SetFieldIdSeries(
		2000,
		"QuantityCount_Decimal1",
		"QuantityDeliveredCount_Decimal1",
		"QuantityDeliveredCountOptional_Decimal1",

		"QuantityMeter",
		"QuantityDeliveredMeter",
	)
	b.SetFieldIdSeries(
		3000,
		"PlantsPerAcreBeforeAdjustmentRydding",

		"PlantsPerAcreRydding",
		"PlantsPerAcreMarkberedning",
	)
	b.SetFieldIdSeries(
		4000,
		"TreeHeightAfterAdjustmentRydding_Decimal1",
		"ResidualTreeHeightAfterAdjustmentRydding_Decimal1",
	)

	//before we mingle up the file :-)
	b.WithAutoSortex()
}
