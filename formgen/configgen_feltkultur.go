package main

import (
	"fmt"
)

func PopulateFeltKulturConfig(b *ConfigBuilder) {
	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//------------------------------------------------------------------------------------
	//common
	//------------------------------------------------------------------------------------
	if b.IsAllma() {
		b.config.Map.PrioritizedExtentLayer = prefix + "Action"
	}
	var nameSuffix = ""
	if b.IsProd() {
		nameSuffix = " prod"
	} else if b.IsDev() {
		nameSuffix = " dev"
	} else if b.IsBeta() {
		nameSuffix = " beta"
	}

	if b.company != nil {
		b.config.Name = b.company.Name + nameSuffix
		b.config.OrgNum = b.company.OrgNum
	}

	//------------------------------------------------------------------------------------
	//enabledfeatures
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	//vsysFeatures
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	//map.addFeatures
	//------------------------------------------------------------------------------------

	if b.IsDefault() {
		//no addFeature
	} else if b.IsStatSkog() {
		//no addFeature
	} else {
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "ContractorPoint", Displayname: "Private entreprenørdata"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerPunkt", Displayname: "Registreringer punkt"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerLinjer", Displayname: "Registreringer linjer"})
		b.AddMapAddFeature(ConfigMapAddFeature{Collection: prefix + "RegistreringerFlater", Displayname: "Registreringer flater"})
	}

	//------------------------------------------------------------------------------------
	//map.editFeatures
	//------------------------------------------------------------------------------------
	if !b.IsDefault() {
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "ContractorPoint", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "authoredOnly"})
	}
	if b.IsDefault() {
		//no editFeatures
	} else if b.IsStatSkog() {
		//no editFeatures
	} else {
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerPunkt", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerLinjer", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "RegistreringerFlater", DeleteMode: "authoredOnly", EditMode: "authoredOnly", GeometryEditing: "authoredOnly"})

		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "MarkberedningTelling", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "disabled"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "PlantingTelling", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "disabled"})
		b.AddMapEditFeature(ConfigMapEditFeature{Collection: prefix + "UngskogpleieTelling", DeleteMode: "authoredOnly", EditMode: "everybody", GeometryEditing: "disabled"})

	}

	//------------------------------------------------------------------------------------
	//map.background
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//none
	} else if b.IsForestOwner() {
		b.AddRasterMapBackground("Kart", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
		b.AddRasterMapBackground("Foto", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
	} else {
		b.AddRasterMapBackground("Foto", false, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBilder/MapServer")
		b.AddRasterMapBackground("Kart", true, "http://services.geodataonline.no/arcgis/rest/services/Geocache_UTM33_EUREF89/GeocacheBasis/MapServer")
	}

	//------------------------------------------------------------------------------------
	//map.layers
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//no map layers
	} else {
		b.AddMapLayer(mapLayer_FGfugl).
			WithAlias("Fugl hensyn").
			SetHiddenFromLayerList()
		if b.IsStoraEnso() || b.IsNortømmer() || b.IsFjordtømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Skyggerelieff).
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("skyggerelieff.png")
		}

		if b.IsStoraEnso() || b.IsAllma() {
			b.AddMapLayer(mapLayer_Aldersklasseriskog).
				WithDefaultOpacity(0.0).
				WithPreview("Aldersklasseriskog.png")
		}

		if b.IsFjordtømmer() || b.IsNortømmer() {
			b.AddMapLayer(mapLayer_BrattTerreng).
				WithDefaultOpacity(0.0).
				WithIdentityDisabled().
				WithPreview("bratterreng.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NTBestand).
				WithAlias("Bestand (NT)").
				WithDefaultOpacity(0.5).
				WithPreview("ntbestand.png")
			b.AddMapLayer(mapLayer_NTBestandslinjer).
				WithAlias("Bestandslinjer (NT)").
				WithDefaultOpacity(1.0).
				WithIdentityDisabled().
				WithPreview("ntbestandslinjer.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_LivslopstrarNorgeNortømmer).
				WithAlias("Livsløpstrær Norge").
				WithDefaultOpacity(0.5).
				WithPreview("LLTNorgeNT.png")
		}
		if b.IsGlommen() {
			b.AddMapLayer(mapLayer_LivslopstrarNorgeGlommen).
				WithAlias("Livsløpstrær Norge").
				WithDefaultOpacity(0.5).
				WithPreview("LLTNorgeGM.png")
		}

		if b.IsNortømmer() || b.IsForestOwner() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Misc).
				WithAlias("MiS (tidlig)").
				WithDefaultOpacity(0.8).
				WithMinScale(20000)
		}

		if b.IsFjordtømmer() || b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_SR16FG).
				WithAlias("SR16").
				WithDefaultOpacity(0.0).
				WithPreview("sr16.png")
			b.AddMapLayer(mapLayer_Aldersklasseriskog).
				WithDefaultOpacity(0.0).
				WithPreview("Aldersklasseriskog.png")
		}

		if b.IsAllma() {
			b.AddMapLayer(mapLayer_Mis).
				WithAlias("MiS").
				WithMinOpacity(0.5).
				WithPreview("mis.png")
		}

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			// They have their own wet map
		} else if b.IsAllma() {
			b.AddMapLayer(mapLayer_GMMarkfuktighetskart_Helelandet).
				WithIdentityDisabled().
				WithDefaultOpacity(0.35).
				WithPreview("markfuktighetskart.png")
		} else {
			b.AddMapLayer(mapLayer_Markfuktighetskart).
				WithIdentityDisabled().
				WithDefaultOpacity(0.35).
				WithPreview("markfuktighetskart.png")
		}

		if b.IsForestOwner() || b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NaturtyperHb13AlleAB).
				WithAlias("Naturtyper HB13").
				WithDefaultOpacity(1.0).
				WithPreview("naturtyperhb13.png")
		} else {
			b.AddMapLayer(mapLayer_NaturtyperHb13NaturtyperHb13Alle).
				WithAlias("Naturtyper HB13").
				WithDefaultOpacity(1.0).
				WithPreview("naturtyperhb13.png")
		}

		if b.IsAllma() {
			//not
		} else {
			b.AddMapLayer(mapLayer_Narin).
				WithAlias("Narin")
			b.AddMapLayer(mapLayer_NaturtyperUnfiltered).
				WithAlias("NiN")
			b.AddMapLayer(mapLayer_FriluftslivStatligSikra).
				WithAlias("Friluftsliv")
		}

		if b.IsNortømmer() || b.IsStoraEnso() {
			b.AddMapLayer(mapLayer_Markagrensen).
				WithAlias("Markagrensen").
				WithPreview("markagrensen.png")
		}

		b.AddMapLayer(mapLayer_VernNaturvernOmrade).
			WithAlias("Vern").
			WithPreview("vern.png")
		b.AddMapLayer(mapLayer_Vernskog).
			WithAlias("Vernskog").
			WithPreview("vernskog.png")

		if b.IsAllma() {
			b.AddMapLayer(mapLayer_GMNaturtyper_NiN).
				WithAlias("Naturtype NiN").
				WithMinOpacity(0.5).
				WithPreview("gmnaturtypernin.png")
			b.AddMapLayer(mapLayer_GMNokkelbiotoper).
				WithAlias("Nøkkelbiotop").
				WithMinOpacity(0.5).
				WithPreview("gmnokkelbiotop.png")
		}

		if b.IsForestOwner() || b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Artsdata).
				WithAlias("Artsdata NIBIO").
				WithDefaultOpacity(1.0).
				WithMinOpacity(1.0)
		} else if b.IsFjordtømmer() {
			//only difference is opacity
			b.AddMapLayer(mapLayer_Artsdata).
				WithAlias("Artsdata NIBIO").
				WithDefaultOpacity(0.0)
		}

		if b.IsAllma() {
			//not
		} else {
			b.AddMapLayer(mapLayer_Arter).
				WithAlias("Arter")
		}

		if b.IsNortømmer() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_TiurleikerStjørdal).
				WithAlias("Tiurleiker Stjørdal")
		}
		if b.IsNortømmer() || b.IsStatSkog() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_Skredkvikkleire2Kvikkleirefaregrad).
				WithAlias("Kvikkleirefaregrad").
				WithPreview("kvikkleirefaregrad.png")
		}
		if b.IsNortømmer() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_JordOgFlomskred).
				WithAlias("Jord og flomskred").
				WithPreview("jordogflom.png")
		}

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZMarkfuktighetskart).
				WithAlias("Markfuktighetskart").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZTerrengGrå).
				WithAlias("Terreng grå").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZTrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_FZSlope_30).
				WithMinLevel(10).
				WithAlias("slope_30").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
		}

		if b.IsCappelen() {
			b.AddMapLayer(mapLayer_CAOrto).
				WithAlias("Orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAGYL).
				WithAlias("GYL").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkfuktighet).
				WithAlias("Markfuktighet").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CAMarkstruktur).
				WithAlias("Markstruktur").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CA_RGB_orto).
				WithAlias("RGB_orto").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
			b.AddMapLayer(mapLayer_CATrehøyde).
				WithAlias("Trehøyde").
				WithDefaultOpacity(0.0).
				WithIdentityDisabled()
		}

		if b.IsFjordtømmer() || b.IsNortømmer() || b.IsForestOwner() || b.IsStatSkog() {
			b.AddMapLayer(mapLayer_Reguleringsplaner).
				WithAlias("Reguleringsplaner").
				WithDefaultOpacity(0.0).
				WithPreview("reguleringsplaner.png")
			b.AddMapLayer(mapLayer_Teig).
				WithAlias("Eiendom").
				WithIdentityDisabled().
				WithPreview("teig.png")
			b.AddMapLayer(mapLayer_Grensepunkter).
				WithAlias("Grensepunkter").
				WithIdentityDisabled().
				WithPreview("grensepunkter.png")
			b.AddMapLayer(mapLayer_Andre).
				WithAlias("Andre hensyn").
				WithPreview("andre.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_NTMiljo).
				WithAlias("Miljø (NT)").
				WithPreview("ntmiljo.png")
		}
		if b.IsNortømmer() || b.IsForestOwner() {
			b.AddMapLayer(mapLayer_NTMis).
				WithAlias("NTmis").
				WithPreview("ntmis.png")
		}

		if b.IsNortømmer() {
			b.AddMapLayer(mapLayer_Mis2).
				WithAlias("MiS").
				WithPreview("mis.png")
		} else if !b.IsAllma() {
			b.AddMapLayer(mapLayer_Mis).
				WithAlias("MiS").
				WithPreview("mis.png")
		}

		if b.IsFjordtømmer() {
			b.AddMapLayer(mapLayer_FTMis).
				WithPreview("ftmis.png")
		}

		/* b.AddMapLayer(mapLayer_KulturminnerWMS).
		WithPreview("ra.png").
		WithMinOpacity(0.3)*/

		b.AddMapLayer(mapLayer_Kulturminner).
			WithAlias("Kulturminner").
			WithPreview("ra.png").
			WithMinOpacity(0.5)

		b.AddMapLayer(mapLayer_Hoydekurver).
			WithAlias("Høydekurver").
			WithIdentityDisabled().
			WithDefaultOpacity(0.6).
			WithPreview("hoydekurver.png")

		//---------
		//FLATER

		//---------
		//LINJER

		//Eiendomsgrenser må være øverst!
		if b.IsStoraEnso() || b.IsGlommen() {
			b.AddMapLayer(mapLayer_Teig).
				WithPreview("matrikkel.png")
		}

		//---------
		//PUNKTER

		if b.IsFritzøe() || b.IsFritzøeSkoger() {
			b.AddMapLayer(mapLayer_FZTeig)
			b.AddMapLayer(mapLayer_FZBestandsflaterMedEtiketter).
				WithDefaultOpacity(0.0).
				WithPreview("fzbestandsflater.png")
			b.AddMapLayer(mapLayer_FZBestandSkogtyper).
				WithAlias("Bestand skogtyper").
				WithPreview("fzbestandskogtyper.png")
			b.AddMapLayer(mapLayer_FZGytebekker).
				WithAlias("Gytebekker").
				SetHiddenFromLayerList()
			b.AddMapLayer(mapLayer_FZMiljø).
				SetHiddenFromLayerList()
			b.AddMapLayer(mapLayer_FZInfrastruktur)
		} else if b.IsLøvenskioldFossum() {
			b.AddMapLayer(mapLayer_LFBestandsflater).
				WithDefaultOpacity(0.5)
		} else if b.IsCappelen() {
			b.AddMapLayer(mapLayer_CABestandsflater).
				WithAlias("Bestandsflater").
				WithDefaultOpacity(0.5).
				WithPreview("cabestand.png")
		} else if b.IsMathiesen() {
			b.AddMapLayer(mapLayer_MEBestandsflater).
				WithAlias("Bestandsflater").
				WithDefaultOpacity(0.5).
				WithPreview("cabestand.png")
		}

		if b.IsStatSkog() {
			//none
		} else {

			b.AddMapLayer(mapLayer_Geocachenames).
				WithAlias("Stedsnavn").
				WithDefaultOpacity(0.8).
				WithIdentityDisabled().
				WithPreview("stedsnavn.png")
		}
	}

	//------------------------------------------------------------------------------------
	//hiddenFromLayerList
	//------------------------------------------------------------------------------------
	if b.IsDefault() {
		//ignore
	} else {

		if b.IsFjordtømmer() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"kulturminner",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		if b.IsForestOwner() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_alle_a_b",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"teig",
				"Andre",
				"NTmis",
				"mis",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		if b.IsAllma() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"vern_naturvern_omrade",
				"vernskog",
				"kulturminner",
			)
		}

		if b.IsNortømmer() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_alle_a_b",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"markagrensen",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"tiurleiker_stjørdal",
				"teig",
				"Andre",
				"NTmis",
				"mis2",
				"ntmiljo",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		if b.IsStatSkog() {
			b.SetLayersHiddenFromLayerList(
				"misc",
				"naturtyper_hb13_naturtyper_hb13_alle",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"artsdata",
				"arter",
				"tiurleiker_stjørdal",
				"teig",
				"Andre",
				"mis",
				"kulturminner",
			)
		}

		if b.IsStoraEnso() {
			b.SetLayersHiddenFromLayerList(
				"naturtyper_hb13_naturtyper_hb13_alle",
				"narin",
				"naturtyper_unfiltered",
				"friluftsliv_statlig_sikra_friluftsliv_statlig_sikra",
				"vern_naturvern_omrade",
				"vernskog",
				"arter",
				"mis",
				"kulturminner",
				prefix+"MiljoFlater",
				prefix+"MiljoLinjer",
				prefix+"MiljoPunkt",
			)
		}

		//DEBUG:
		if false {
			fmt.Println("<---- hidden debug")
			for _, layer := range b.config.Map.Layers {
				if layer.Hidefromlayerlist != nil && *layer.Hidefromlayerlist == true {
					fmt.Printf("%s\n", layer.Name)
				}
			}
			fmt.Println("hidden debug ---->")
		}
	}

	//------------------------------------------------------------------------------------
	//reports
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	//proximityAlarms
	//------------------------------------------------------------------------------------
	if b.IsGlommen() {
		b.WithGradientProximityAlarmOutsideCollection(
			"Driftsområde",
			prefix+"Action",
			10,
			true,
		)

	} else {
		b.WithGradientProximityAlarmOutsideCollection(
			"Driftsområde",
			prefix+"PTommerdrift",
			10,
			true,
		)
	}
	if b.IsGlommen() {
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynsflate",
			prefix+"HenPolygon",
			"Type",
			nil, // include all
			10,
		)
		b.WithGradientProximityAlarmInsideWhere(
			"HensynsPunkt",
			prefix+"HenPoint",
			"Type",
			[]string{"Livsløpstrær", "Artsreg (Rødliste,Henyn mm)", "Kulturminne", "Brønn", "Reir - Ørn", "Reir - Hønsehauk"},
			10,
		)
		//TODO(kristoffer): Remove port
		b.WithGradientProximityAlarmInsideArcGIS(
			"Naturtyper NiN",
			"http://35.228.152.51:8000/GM_Nokkelbiotoper/MapServer/identify?geometryType=esriGeometryEnvelope&sr=25833&layers=*&tolerance=0&returnGeometry=true",
			10,
		)

	}
	if b.IsNortømmer() {
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynspunkt",
			prefix+"RegistreringerPunkt",
			"_tema",
			[]string{"Reirtre", "Kulturminner", "Storfuglleik", "Balplass", "Ikke kryss", "Ledning", "Jordkabel", "Brønn", "Annen fare"},
			10,
		)
		b.WithGradientProximityAlarmInsideWhere(
			"Hensynsflate",
			prefix+"RegistreringerFlater",
			"_tema",
			nil, // include all
			10,
		)
		b.WithGradientProximityAlarmInsideVector(
			"MiS",
			"https://vtfront.feltgismaps.com/geometries?TYPENAME=nt_new:nortommer_nokkelbiotop&FORMAT=esrijson&ATTRIBUTEFLDS=skjotsel",
			10,
		)
	}

	//------------------------------------------------------------------------------------
	//sync (TODO: import from syncconfig.go)
	//------------------------------------------------------------------------------------

}
