package main

import "strings"

func PopulatePunktLinjerFlaterExtGroupAlmaForm(b *FormBuilder) {

	//file types
	isRegistreringOrHenFile := strings.Contains(b.file.Name, "Registreringer") || strings.Contains(b.file.Name, "Hen")
	isMisFile := strings.Contains(b.file.Name, "Mis") //Mis = GM "objekter kommer utenifra"
	isPunktFile := strings.Contains(b.file.Name, "Punkt") || strings.Contains(b.file.Name, "Point")
	isLinjeFile := strings.Contains(b.file.Name, "Linjer") || strings.Contains(b.file.Name, "Line")
	isFlateFile := strings.Contains(b.file.Name, "Flater") || strings.Contains(b.file.Name, "Polygon")

	//common
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.GlobalIdField = "_globalId"
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2

	b.form.MaxRecordCount = 100000
	if isPunktFile {
		b.form.MinScale = 100000
	} else {
		b.form.MinScale = 50000
	}
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"

	b.form.Relationships = []string{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"

	b.form.UseStandardizedQueries = true
	dib := b.AddDrawingInfo()

	var typeFieldName string //special field used to decide type!

	//group Registreringer/Miljo/Hen
	if isRegistreringOrHenFile {
		typeFieldName = "Type"
		populatePunktLinjerFlaterForm_descriptionLabelingInfo(b, dib)
	} else if isMisFile {
		if isPunktFile {
			typeFieldName = "Livsmiljo"
			dib.WithTransparency(0)
		} else if isFlateFile {
			typeFieldName = "Forvaltningskode"
			dib.WithTransparency(60)
		} else {
			b.reporter.Error("Unknown type of Mis Point/Polygon")
		}

	} else {
		b.reporter.Error("Unknown type Registreringer/Miljo/Hen")
	}

	b.form.DisplayField = typeFieldName
	b.form.TypeIdField = strRef(typeFieldName)

	//---------------------------
	//FIELDS!

	//common fields
	b.AddHiddenStringField("_globalId", "GlobalID")
	b.AddHiddenDateField("_created_Date", "CreatedDate")
	b.AddHiddenStringField("_createdBy", "CreatedBy")
	b.AddHiddenDateField("_modified_Date", "ModifiedDate")
	b.AddHiddenStringField("_modifiedBy", "ModifiedBy")

	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy", //4
	)

	b.AddHiddenStringField("_workTeamOrgNumUP", "Organisasjonsnummer arbeidslag ungskogpleie")
	b.AddHiddenStringField("_workTeamOrgNumFR", "Organisasjonsnummer arbeidslag forhåndsrydding")
	b.AddHiddenStringField("_workTeamOrgNumMB", "Organisasjonsnummer arbeidslag markberedning")
	b.AddHiddenStringField("_workTeamOrgNumPL", "Organisasjonsnummer arbeidslag planting")
	b.AddHiddenStringField("_workTeamOrgNumGR", "Organisasjonsnummer arbeidslag grøfterensk")
	b.AddHiddenStringField("_workTeamOrgNumSA", "Organisasjonsnummer arbeidslag såing")
	b.AddHiddenStringField("_workTeamOrgNumSS", "Organisasjonsnummer arbeidslag skogskade")
	b.AddHiddenStringField("_workTeamOrgNumIS", "Organisasjonsnummer arbeidslag infrastruktur")
	b.AddHiddenStringField("_workTeamOrgNumGJ", "Organisasjonsnummer arbeidslag gjødsling")
	b.AddHiddenStringField("_workTeamOrgNumSP", "Organisasjonsnummer arbeidslag sprøyting")
	b.AddHiddenStringField("_workTeamOrgNumGA", "Organisasjonsnummer arbeidslag gravearbeid")
	b.AddHiddenStringField("_workTeamOrgNumSR", "Organisasjonsnummer arbeidslag sporskaderetting")
	b.AddHiddenStringField("_workTeamOrgNumSK", "Organisasjonsnummer arbeidslag stammekvisting")
	b.SetFieldIdSeries(
		1000,
		"_workTeamOrgNumUP",
		"_workTeamOrgNumFR",
		"_workTeamOrgNumMB",
		"_workTeamOrgNumPL",
		"_workTeamOrgNumGR",
		"_workTeamOrgNumSA",
		"_workTeamOrgNumSS",
		"_workTeamOrgNumIS",
		"_workTeamOrgNumGJ",
		"_workTeamOrgNumSP",
		"_workTeamOrgNumGA",
		"_workTeamOrgNumSR",
		"_workTeamOrgNumSK",
	)

	if isMisFile {
		b.AddHiddenStringField("_headId", "HeadID")
		b.AddHiddenStringField("_orderId", "ContractorOrderID")
		b.AddHiddenStringField("_serviceOrderId", "ServiceOrderID")
		b.SetFieldIdSeries(5,
			"_headId",
			"_orderId",
			"_serviceOrderId", //7
		)

		b.AddUneditableRequiredStringField("orderNum", "PurchaseOrderNum")

		if isFlateFile {
			b.AddUneditableRequiredStringField("Livsmiljo", "Livsmiljo")
			b.AddUneditableRequiredIntegerField("MisFigurNumber", "MisFigurNumber")
			b.AddUneditableRequiredDoubleField("Area", "Area")
		} else if isPunktFile {
			b.AddUneditableRequiredStringField(typeFieldName, "Livsmiljo")
			b.AddUneditableRequiredIntegerField("MisPunktNumber", "MisPunktNumber")
			b.AddUneditableRequiredStringField("Comment", "Comment")
		}
		b.AddUneditableRequiredIntegerField("ObjectId", "ObjectID")
		b.AddUneditableRequiredDoubleField("Distance", "Distance")
		b.AddUneditableRequiredStringField("EndretDato", "EndretDato")

		if isFlateFile {
			b.SetFieldIdSeries(
				8,
				"orderNum",
				"Livsmiljo",
				"MisFigurNumber", //10
				"Area",
				"ObjectId",
				"Distance",
				"EndretDato",
			)
		} else if isPunktFile {
			b.SetFieldIdSeries(
				8,
				"orderNum",
				typeFieldName,
				"MisPunktNumber", //10
				"Comment",
				"ObjectId",
				"Distance",
				"EndretDato",
			)
		}

		if isFlateFile {
			b.AddUneditableRequiredIntegerField(typeFieldName, "Forvaltningskode")
			b.SetFieldIdSeries(
				15,
				typeFieldName,
			)
		}

	} else if isRegistreringOrHenFile {
		b.AddHiddenStringField("_headId", "HeadID")
		b.AddHiddenStringField("_orderId", "ContractorOrderID")
		b.AddHiddenStringField("_serviceOrderId", "ServiceOrderID")
		b.SetFieldIdSeries(5,
			"_headId",
			"_orderId",
			"_serviceOrderId", //7
		)

		b.AddUneditableRequiredStringField("orderNum", "PurchaseOrderNum")
		b.AddUneditableRequiredStringField(typeFieldName, "Type")
		b.SetFieldIdSeries(
			8,
			"orderNum",
			typeFieldName,
		)

		b.AddHiddenStringField("_parentGlobalId", "XXPSkogkultur globalID")
		b.SetFieldIdSeries(
			20,
			"_parentGlobalId",
		)

		if isPunktFile {
			b.AddUneditableRequiredStringField("Ivaretatt", "Ivaretatt")
			b.AddRequiredIntegerField("Count", "Antall")
			b.AddNullableStringField("Description", "Description")
			b.AddUneditableRequiredStringField("EndretDato", "EndretDato")
			b.AddUneditableRequiredIntegerField("ObjectId", "ObjectID")
			b.AddUneditableRequiredDoubleField("Distance", "Distance")

			b.SetFieldIdSeries(
				10,
				"Ivaretatt",
				"Count",
				"Description",
				"EndretDato",
				"ObjectId",
				"Distance", //15
			)
		} else if isLinjeFile {
			b.AddUneditableRequiredDoubleField("Length", "Length")
			b.AddNullableStringField("Description", "Description")
			b.AddUneditableRequiredIntegerField("ObjectId", "ObjectID")
			b.AddUneditableRequiredDoubleField("Geometry", "Geometry")
			b.AddUneditableRequiredDoubleField("Distance", "Distance")
			b.AddUneditableRequiredStringField("EndretDato", "EndretDato")

			b.SetFieldIdSeries(
				10,
				"Length",
				"Description",
				"ObjectId",
				"Geometry",
				"Distance",
				"EndretDato",
			)
		} else if isFlateFile {
			b.AddUneditableRequiredDoubleField("Area", "Area")
			b.SetFieldIdSeries(
				10,
				"Area",
			)

			b.AddNullableStringField("Description", "Antall/beskrivelse")
			b.AddUneditableRequiredIntegerField("ObjectId", "ObjectID")
			b.AddUneditableRequiredDoubleField("Geometry", "Geometry")
			b.AddUneditableRequiredDoubleField("Distance", "Distance")
			b.AddUneditableRequiredStringField("EndretDato", "EndretDato")

			b.SetFieldIdSeries(
				11,
				"Description",
				"ObjectId",
				"Geometry",
				"Distance",
				"EndretDato",
			)
		}
	}

	if isLinjeFile {
		if !b.IsGlommen() {
			b.AddUneditableRequiredDoubleField("length", "Lengde").
				WithAliasTranslation("en", "Length")
			b.SetFieldIdSeries(
				20,
				"length")
		}

	}

	b.WithAutoSortex()
	b.VerifyOrderAndSortex()

	//---------------------------
	//GEOMETRY!

	//geometry points/lines/polygons
	if isPunktFile {
		populatePunktLinjerFlaterExtGroupAlmaForm_punkt(b, dib)
	} else if isLinjeFile {
		populatePunktLinjerFlaterExtGroupAlmaForm_linjer(b, dib)
	} else if isFlateFile {
		populatePunktLinjerFlaterExtGroupAlmaForm_flater(b, dib)
	} else {
		b.reporter.Error("Unknown type Punkt/Linjer/Flater")
	}

	//---------------------------
	//TYPES! (we have created a types generator :-D )
	var typeDrawingTool string
	var typeIds []string //array so we can have the order!
	var typeNames map[string]string
	var typeVisibilityMode map[string]string

	if isRegistreringOrHenFile {
		if isPunktFile {
			typeDrawingTool = "esriFeatureEditToolPoint"
			typeIds = []string{
				"Livsløpstrær",
				"Friluftsanlegg",
				"Artsreg (Rødliste,Henyn mm)",
				"Kulturminne",
				"Oljeutslipp",
				"Brønn",
				"Reir - Ørn",
				"Reir - Hønsehauk",
				"Høgstubbe",
			}
			typeNames = map[string]string{}
		} else if isLinjeFile {
			typeDrawingTool = "esriFeatureEditToolLine"
			typeIds = []string{
				"Driftsvei",
				"Traktorvei- barmark",
				"Sporskader",
				"Kulturminne",
				"Bestandsgrense",
				"Traktorvei-vinter",
				"Lysløype",
				"Snøscooter",
				"Sti",
				"Vei",
				"Annet",
				"Driftsvei, markberedning",
				"Luftkabel",
				"Vannledning",
				"Bekk",
			}
			typeNames = map[string]string{}
		} else if isFlateFile {
			typeDrawingTool = "esriFeatureEditToolPolygon"
			typeIds = []string{
				"Døde trær",
				"Edellauvskog",
				"Flerbruksholt",
				"Jerpebiotop",
				"Kantsone",
				"Kulturminner",
				"Livsløpstrær",
				"Myr",
				"Rovfuglreir",
				"Stier og Friluftsanlegg",
				"Sump",
				"Tiurleiker",
				"FSC nøkkelbiotop (Gjennomhogst)",
				"FSC nøkkelbiotop (Friutvikling/urørt)",
			}
			typeNames = map[string]string{}
		}
	}

	for _, typeId := range typeIds {

		var typeName = typeId
		if typeNameMapLookup, ok := typeNames[typeId]; ok {
			typeName = typeNameMapLookup
		}
		var attributes = map[string]interface{}{
			typeFieldName: typeId,
		}
		if visibilityMode, ok := typeVisibilityMode[typeId]; ok {
			attributes["_visibilityMode"] = visibilityMode
		}

		b.AddType(FormType{
			Domains: FormTypeDomains{},
			Id:      typeId,
			Name:    typeName,
			Templates: []FormTypeTemplate{
				{
					Description: strRef(""),
					DrawingTool: &typeDrawingTool,
					Name:        typeName,
					Prototype: FormTypeTemplatePrototype{
						Attributes: attributes,
					},
				},
			},
		})
	}
}

func populatePunktLinjerFlaterForm_descriptionLabelingInfo(b *FormBuilder, dib *FormDrawingInfoBuilder) {

	isPunktFile := strings.Contains(b.file.Name, "Punkt") || strings.Contains(b.file.Name, "Point")
	isLinjeFile := strings.Contains(b.file.Name, "Linjer") || strings.Contains(b.file.Name, "Line")
	isFlateFile := strings.Contains(b.file.Name, "Flater") || strings.Contains(b.file.Name, "Polygon")

	//for "Hen"

	//labeling info builder
	lib := dib.WithLabelingInfo("Description")

	lib.WithNoMinScale()
	lib.WithUseCodedValues()

	lib.WithWhereClause("Type = 'Annet'")

	if isPunktFile {
		lib.WithAboveRightPointPlacement()
	} else if isLinjeFile {
		lib.WithCenteredPointPlacement()
	} else if isFlateFile {
		//lib.WithCenteredPointPlacement()
	} else {
		b.reporter.Error("Unknown type Point/Line/Polygon")
	}

	//symbol
	lib.WithLeftAlignedSymbol(
		FormDrawingInfoColor(235, 61, 194, 255), //black color
		12,
		FormDrawingInfoColor(255, 255, 255, 200), //white halo color
		2,
	)
}
