package main

import (
	"fmt"
	"log"
	"slices"
	"strconv"
	"strings"
)

type FormBuilder struct {
	file    *FormFile
	form    *Form
	company *Company

	currentSection *int

	reporter *Reporter
}

func NewFormBuilder(file *FormFile, company *Company, reporter *Reporter) *FormBuilder {
	p := new(FormBuilder)
	p.form = &Form{}
	p.file = file
	p.company = company
	p.reporter = reporter
	p.reporter.SetCurrentFile(file.Filename(), company)
	return p
}

// CUSTOMERS:
func (b *FormBuilder) IsNortømmer() bool {
	return b.company != nil && b.company.isCompany(company_Nortømmer)
}
func (b *FormBuilder) IsFritzøe() bool {
	return b.company != nil && b.company.isCompany(company_Fritzøe)
}
func (b *FormBuilder) IsFritzøeSkoger() bool {
	return b.company != nil && b.company.isCompany(company_FritzøeSkoger)
} // FritzøeSkoger = Fritzøe with new Org number
func (b *FormBuilder) IsGlommenMjosen() bool {
	return b.company != nil && b.company.isCompany(company_GlommenMjosen)
}
func (b *FormBuilder) IsGlommenSkog() bool {
	return b.company != nil && b.company.isCompany(company_GlommenSkog)
}
func (b *FormBuilder) IsStoraEnso() bool {
	return b.company != nil && b.company.isCompany(company_StoraEnso)
}
func (b *FormBuilder) IsFjordtømmer() bool {
	return b.company != nil && b.company.isCompany(company_Fjordtømmer)
}
func (b *FormBuilder) IsStatSkog() bool {
	return b.company != nil && b.company.isCompany(company_StatSkog)
}
func (b *FormBuilder) IsCappelen() bool {
	return b.company != nil && b.company.isCompany(company_Cappelen)
}
func (b *FormBuilder) IsLøvenskioldFossum() bool {
	return b.company != nil && b.company.isCompany(company_LøvenskioldFossum)
}
func (b *FormBuilder) IsMathiesenEidsvoldVærk() bool {
	return b.company != nil && b.company.isCompany(company_MathiesenEidsvoldVærk)
}
func (b *FormBuilder) IsValdresSkog() bool {
	return b.company != nil && b.company.isCompany(company_ValdresSkog)
}
func (b *FormBuilder) IsAltiskog() bool {
	return b.company != nil && b.company.isCompany(company_Altiskog)
}

// GROUPS:
func (b *FormBuilder) IsGlommen() bool {
	return b.company != nil && b.company.hasFeature(isGroupGlommen)
}
func (b *FormBuilder) IsAllma() bool { return b.company != nil && b.company.hasFeature(isGroupAlma) }

// CUSTOMER TYPES:
func (b *FormBuilder) IsForestOwner() bool {
	return b.company != nil && b.company.hasFeature(isForestOwner)
}
func (b *FormBuilder) IsContractor() bool {
	return b.company != nil && b.company.hasFeature(isContractor)
}

func (b *FormBuilder) Form() *Form {

	//verify no colliding fieldIds
	fieldIds := []string{}
	var fieldIdError = false
	for _, field := range b.form.Fields {
		fieldId := strconv.Itoa(field.FieldId)
		if slices.Contains(fieldIds, fieldId) == true {
			b.reporter.Error("Duplicate fieldID " + fieldId + " for field " + field.Name)
			fieldIdError = true
		} else {
			fieldIds = append(fieldIds, fieldId)
		}
	}
	if fieldIdError {
		for _, field := range b.form.Fields {
			fieldId := strconv.Itoa(field.FieldId)
			b.reporter.Error("" + field.Name + " -> " + fieldId)
		}
	}

	//verify no colliding sortexes
	sortexes := []string{}
	var sortexesError = false
	for _, field := range b.form.Fields {
		if field.Section != nil && field.Sortix != nil {
			sortex := strconv.Itoa(*field.Section) + strconv.Itoa(*field.Sortix)
			if slices.Contains(sortexes, sortex) == true {
				b.reporter.Error("Duplicate sortex " + sortex + " for field " + field.Name)
				sortexesError = true
			} else {
				fieldIds = append(sortexes, sortex)
			}
		}
	}
	if sortexesError {
		for _, field := range b.form.Fields {
			if field.Section != nil && field.Sortix != nil {
				sortex := strconv.Itoa(*field.Section) + strconv.Itoa(*field.Sortix)
				b.reporter.Error("" + field.Name + " -> " + sortex)
			}
		}
	}

	if b.reporter.CurrentFileHasBlockingIssues() {
		return nil
	} else {
		return b.form
	}
}

func (b *FormBuilder) AddType(formType FormType) {
	b.form.Types = append(
		b.form.Types,
		formType,
	)
}

func (b *FormBuilder) WithAutoSortex() *FormBuilder {

	var currentSection = -1
	var currentSortId = -1

	for _, field := range b.form.Fields {
		//ignore hidden fields
		if strings.HasPrefix(field.Name, "_") {
			continue
		}

		//decide what fieldSectionId is, tolerate nil
		var fieldSectionId = currentSection
		if field.Section != nil {
			fieldSectionId = *field.Section
		} else {
			fieldSectionId = 0
		}

		//did we change section?
		if fieldSectionId != currentSection {
			currentSection = fieldSectionId
			currentSortId = -1 //reset counter!
		}

		//set sortex if needed
		if field.FieldId > currentSortId {
			currentSortId = field.FieldId
		} else {
			currentSortId += 1
			var sortix int = currentSortId
			field.Sortix = &sortix
		}
	}

	return b
}

func (b *FormBuilder) SetFieldIdSeries(from int, fieldNames ...string) {
	var fieldIdCounter = from
	for _, fieldName := range fieldNames {
		var found = false
		for _, field := range b.form.Fields {
			if field.Name == fieldName {
				if found {
					b.reporter.Error("SetFieldIdSeries: Field " + fieldName + " found twice")
				}
				if field.FieldId != -1 {
					b.reporter.Error("SetFieldIdSeries: Field " + fieldName + " already set")
				}
				field.FieldId = fieldIdCounter
				found = true
			}
		}
		if !found {
			b.reporter.Error("SetFieldIdSeries: Field " + fieldName + " not found")
		}
		fieldIdCounter += 1
	}
}

func (b *FormBuilder) VerifyOrderAndSortex() {
	var sections []int = []int{}
	for _, field := range b.form.Fields {
		if field.Section != nil {
			if !slices.Contains(sections, *field.Section) {
				sections = append(sections, *field.Section)
			}
		}
	}
	slices.Sort(sections)

	for _, section := range sections {
		var issue = false
		var current = -1

		for _, field := range b.form.Fields {
			if field.Section != nil && *field.Section == section {
				var number = (field.FieldId * 10)
				if field.Sortix != nil {
					number = (*field.Sortix * 10) + 1
				}
				if number > current {
					current = number
				} else {
					issue = true
				}
			}
		}

		if issue {
			b.reporter.Error("#SORT SECTION ISSUES " + strconv.Itoa(section))

			fmt.Println("#SORT SECTION ISSUES " + strconv.Itoa(section))
			for _, field := range b.form.Fields {
				if field.Section != nil && *field.Section == section {
					if field.Sortix != nil {
						b.reporter.Error(
							fmt.Sprintf(" %-25s:%d: with sortix %d\n", field.Name, field.FieldId, *field.Sortix),
						)
					} else {
						b.reporter.Error(
							fmt.Sprintf(" %-25s:%d:\n", field.Name, field.FieldId),
						)

					}
				}
			}
			b.reporter.Error("WRONG ORDER OF FIELDID/SORTIX IN FILE!")
		}
	}
}

func (b *FormBuilder) VerifyFullOrNoTranslations() {

	//the idea is that if a part of something is translated, then you should probably translate it all!

	//find translated languages!
	languages := []string{}
	for _, translation := range b.form.Translations {
		if !slices.Contains(languages, translation.Language) {
			languages = append(languages, translation.Language)
		}
	}
	for _, field := range b.form.Fields {
		for _, translation := range field.Translations {
			if !slices.Contains(languages, translation.Language) {
				languages = append(languages, translation.Language)
			}
		}
	}

	missTranslation := func(key string, language string, translations []FormTranslation) bool {
		if key == "" {
			return false //no need to translate empty strings....
		}
		for _, translation := range translations {
			if translation.Language == language && translation.Key == key {
				return false
			}
		}
		return true
	}

	//check each language!
	for _, language := range languages {
		//alias
		if b.form.Alias != nil && missTranslation(*b.form.Alias, language, b.form.Translations) {
			b.reporter.Warning(fmt.Sprintf("Missing translation for form.name '%s' in language '%s'", *b.form.Alias, language))
		}

		//sections
		for _, section := range b.form.Sections {
			if missTranslation(section.Title, language, b.form.Translations) {
				b.reporter.Warning(fmt.Sprintf("Missing translation for section.title '%s' in language '%s'", section.Title, language))
			}
			if missTranslation(section.Subtitle, language, b.form.Translations) {
				b.reporter.Warning(fmt.Sprintf("Missing translation for section.subtitle '%s' in language '%s'", section.Subtitle, language))
			}
		}

		//fields
		for _, field := range b.form.Fields {
			if strings.HasPrefix(field.Name, "_") {
				continue
			}
			if missTranslation(field.Alias, language, field.Translations) {
				b.reporter.Warning(fmt.Sprintf("Missing translation for field.alias '%s' in language '%s'", field.Alias, language))
			}

			//TODO: OPTIONS = 10000000
			//TODO: NOTES = FORMATTED MARKUP TEXT
		}
	}
}

func (b *FormBuilder) Log() {

	log.Printf("==================================================")
	log.Printf(b.file.Name)
	log.Printf("==================================================")

	log.Printf("FIELDS")

	hiddenSection := "_hidden"
	hiddenSectionId := -1
	sections := map[int]string{hiddenSectionId: hiddenSection}
	fieldSection := map[string]int{}
	for _, section := range b.form.Sections {
		sections[section.Id] = section.Title
	}
	for _, field := range b.form.Fields {
		if strings.HasPrefix(field.Name, "_") {
			fieldSection[field.Name] = hiddenSectionId
		} else {
			fieldSection[field.Name] = *field.Section
		}
	}

	for sectionId, sectionName := range sections {
		log.Printf("    SECTION:(%v)%v", sectionId, sectionName)
		for _, field := range b.form.Fields {
			if fieldSection[field.Name] != sectionId {
				continue
			}
			log.Printf("        ('%v'/%v) '%v'", field.Name, field.FieldId, field.Alias)
		}
	}

	log.Printf("==================================================")
}

// Nice tool to reorder file when matching with current version
func (b *FormBuilder) DebugSetFieldsFileOrder(fieldNames ...string) {
	fieldsCopies := b.form.Fields
	for _, fieldName := range fieldNames {
		var found = false
		for _, fieldCopy := range fieldsCopies {
			if fieldCopy.Name == fieldName {
				found = true
			}
		}
		if !found {
			b.reporter.Error("SetFieldsFileOrder: Found " + fieldName + " in fieldNames, but not in fields")
		}
	}
	for _, fieldCopy := range fieldsCopies {

		var found = false
		for _, fieldName := range fieldNames {
			if fieldCopy.Name == fieldName {
				found = true
			}
		}
		if !found {
			b.reporter.Error("SetFieldsFileOrder: Found " + fieldCopy.Name + " in fields, but not in fieldNames")
		}
	}
	b.form.Fields = []*FormField{}
	for _, fieldName := range fieldNames {
		for _, fieldCopy := range fieldsCopies {
			if fieldCopy.Name == fieldName {
				b.form.Fields = append(b.form.Fields, fieldCopy)
				break
			}
		}
	}
}

func (b *FormBuilder) WithSyncEvent(config SyncEventConfig) *FormBuilder {
	b.form.SyncEvents = append(b.form.SyncEvents, config)
	return b
}

func (b *FormBuilder) WithDataSource(source *DataSource) *FormBuilder {
	b.form.DataSource = source
	return b
}
