package main

import (
	"fmt"
	"strings"
)

func PopulatePFeltkontrollForm(b *FormBuilder) {

	jobType := ""
	if strings.Contains(b.file.Name, "Planting") {
		jobType = "Planting"
	} else if strings.Contains(b.file.Name, "Ungskogpleie") {
		jobType = "Ungskogpleie"
	} else {
		panic("Unknown job type!")
	}

	//DEFAULT!
	b.form.Alias = strRef(fmt.Sprintf("Feltkontroll %v", strings.ToLower(jobType)))

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "dev_Boolean"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("dev_Boolean")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_job_nr", "Ordrenummer")
	b.AddHiddenNullableStringField("_type", "Type").
		WithComment("nullable because of backwards compability, null = 'Egenkontroll'")
	b.AddHiddenStringField("_parentGlobalId", "Tilhørende objekt")
	b.AddHiddenNullableStringField("_workTeamOrgNum", "Organisasjonsnummer arbeidslag")

	if strings.Contains(b.file.Name, "NTPFeltkontrollPlanting") {
		//fieldIds
		b.SetFieldIdSeries(0,
			"_globalId",
			"_created_Date",
			"_createdBy",
			"_modified_Date",
			"_modifiedBy",
			"_job_nr",
			"_type",
			"_parentGlobalId",
			"_workTeamOrgNum",
		)
		b.AddNullableStringField("comment", "This is just a file to make sync work while waiting for migration...")
		b.SetFieldIdSeries(10, "comment")
		return
	}

	//SECTION OPPDRAG
	b.AddSection("")

	//TODO: Hvilke bestand er kontrollen utført i? (mulig å legge inn flere)
	b.AddRequiredStringField("bestand_str", "Hvilke bestand er kontrollen utført i?")

	if (b.IsForestOwner() && !b.IsFritzøe()) || b.IsContractor() || b.IsStoraEnso() {
		//---------------------------------------
		// NEW PFeltkontroll, similar to feltkontroll for hogst etc.

		if jobType == "Planting" {
			//TODO: Hvilke treslag er plantet? Hvis mix, er de plantet godt i forhold til treslag?
			b.AddMultipleChoiceField("treslag", "Hvilke treslag er plantet?").
				WithMultipleChoiceSeparator("|").
				WithSuggestedOption("Gran").
				WithSuggestedOption("Furu").
				WithSuggestedOption("Løv")
			b.AddMultipleChoiceField("treslag_mix_planted_well", "Er de plantet godt i forhold til treslag?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("planted_deep_enough", "Er plantene satt dypt nok i jorda").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("planted_correctly", "Er plantene satt rett?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("planting_sites_used", "Er det benyttet gode planteplasser?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Delvis").
				WithSuggestedOption("Nei")

			//TODO: Kontrollmålinger: fylle rett inn i en tabell som er fordelt på antall gran, furu og totalt antall planter i sirkelen, eventuelt mulighet for kommentar (for å kunne skrive eventuelt hvorfor det er få/mange i akkurat den sirkelen)
			//TODO: Kontrollmålinger-feature

			//MILJØ:
			b.AddMultipleChoiceField("edge_zone", "Kantsone, er det avsatt kantsone i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddNullableStringField("edge_zone_average_width", "Kantsone gjennomsnittsbredde")
			b.AddMultipleChoiceField("path_buffer", "Sti, er det avsatt buffer i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("trench", "Grøft, er det avsatt buffer i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("key_biotope", "Nøkkelbiotop, er det plantet i henhold til merkebånd/instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")

			b.AddMultipleChoiceField("nature_type", "Naturtype, er det plantet i henhold til merkebånd/instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("cultural", "Kulturminner, er det plantet i henhold til merkebånd/instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("stored_well_sun", "Er plantene lagret godt i forhold til solen?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")

			b.SetFieldIdSeries(200,
				"treslag",
				"treslag_mix_planted_well",
				"planted_deep_enough",
				"planted_correctly",
				"planting_sites_used",
				"edge_zone",
				"edge_zone_average_width",
				"path_buffer",
				"trench",
				"key_biotope",
				"nature_type",
				"cultural",
				"stored_well_sun",
			)

		} else if jobType == "Ungskogpleie" {
			b.AddMultipleChoiceField("measurements_satisfactory", "Er målingene tilfredsstillende i forhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("futuretrunks_satisfactory", "Er valg av framtidsstammer tilfredsstillende?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("futuretrunks_spacing", "Er det god romlig fordeling mellom framtidstrærne?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("wildlife_retention", "Er det satt igjen ROS, toppkappa løvtrær, furu til viltet?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("stumpheight_satisfactory", "Er stubbehøyden tilfredsstillende?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")

			//MILJØ:
			b.AddMultipleChoiceField("edge_zone", "Kantsone intakt/avsatt i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddNullableStringField("edge_zone_average_width", "Kantsone gjennomsnittsbredde")
			b.AddMultipleChoiceField("path_buffer", "Er avstand til stier/skiløyper ivaretatt i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("trench", "Er grøfter holdt åpne i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("key_biotope", "Nøkkelbiotoper og naturtyper ivaretatt i henhold til instruks").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("cultural", "Kulturminner ivaretatt i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("operating_routes", "Er aktuelle driftsveier holdt åpne i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("wildlife", "Fugler og arter hensyntatt i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")
			b.AddMultipleChoiceField("read_dist", "Er avstand til skogsbilvei/offentligvei ivaretatt i henhold til instruks?").
				WithSuggestedOption("Ja").
				WithSuggestedOption("Nei").
				WithSuggestedOption("Ikke aktuelt")

			b.SetFieldIdSeries(
				200,
				"measurements_satisfactory",
				"futuretrunks_satisfactory",
				"futuretrunks_spacing",
				"wildlife_retention",
				"stumpheight_satisfactory",
				"edge_zone",
				"edge_zone_average_width",
				"path_buffer",
				"trench",
				"key_biotope",
				"cultural",
				"operating_routes",
				"wildlife",
				"read_dist",
			)
		}

		b.AddMultipleChoiceField("deviation", "Er det registrert avvik?").
			WithSuggestedOption("Ja").
			WithSuggestedOption("Nei").
			WithSuggestedOption("Ikke aktuelt")
		b.SetFieldIdSeries(
			300,
			"deviation",
		)

	} else {
		//---------------------------------------
		// OLD PFeltkontroll, boolean based (customers want more :-D)

		addMiljoYesNoAndCommentIfNo := func(prefix string, bool_alias string, comment_alias string) {
			bool_name := prefix + "_Boolean"
			comment_name := prefix + "_comment"
			b.AddRequiredBooleanField(bool_name, bool_alias)
			b.AddNullableStringField(comment_name, comment_alias).
				WithLogic().
				WithVisibleIfField(FormFieldLogicField{
					Operator:           "==",
					OtherFieldName:     bool_name,
					OtherFieldIntValue: intRef(0),
				})
		}

		if jobType == "Planting" {
			//TODO: Hvilke treslag er plantet? Hvis mix, er de plantet godt i forhold til treslag?
			b.AddMultipleChoiceField("treslag", "Hvilke treslag er plantet?").
				WithMultipleChoiceSeparator("|").
				WithSuggestedOption("Gran").
				WithSuggestedOption("Furu").
				WithSuggestedOption("Løv")
			b.AddRequiredBooleanField("treslag_mix_planted_well_Boolean", "Er de plantet godt i forhold til treslag?")

			b.AddRequiredBooleanField("planted_deep_enough_Boolean", "Er plantene satt dypt nok i jorda?")
			b.AddRequiredBooleanField("planted_correctly_Boolean", "Er plantene satt rett?")
			b.AddMultipleChoiceField("planting_sites_used", "Er det benyttet gode planteplasser?").
				WithMultipleChoiceSeparator("|").
				WithCodedOption("Ja").
				WithCodedOption("Delvis").
				WithCodedOption("Nei")
			b.AddNullableStringField("planting_sites_used_comment", "Kommentar planteplasser")

			//TODO: Kontrollmålinger: fylle rett inn i en tabell som er fordelt på antall gran, furu og totalt antall planter i sirkelen, eventuelt mulighet for kommentar (for å kunne skrive eventuelt hvorfor det er få/mange i akkurat den sirkelen)
			//TODO: Kontrollmålinger-feature

			//MILJØ:
			addMiljoYesNoAndCommentIfNo("edge_zone", "Kantsone, er det avsatt kantsone i henhold til instruks?", "Kommentar kantsone")
			b.AddNullableStringField("edge_zone_average_width", "Kantsone gjennomsnittsbredde")
			addMiljoYesNoAndCommentIfNo("path_buffer", "Sti, er det avsatt buffer i henhold til instruks?", "Kommentar sti")
			addMiljoYesNoAndCommentIfNo("trench", "Grøft, er det avsatt buffer i henhold til instruks?", "Kommentar grøft")

			b.AddMultipleChoiceField("key_biotope", "Nøkkelbiotop, er det plantet i henhold til merkebånd/instruks?").
				WithCodedOption("Ja").
				WithCodedOption("Nei").
				WithCodedOption("Ikke aktuelt")
			b.AddNullableStringField("key_biotope_comment", "Kommentar nøkkelbiotop").
				WithLogic().
				WithVisibleIfField(FormFieldLogicField{
					Operator:              "==",
					OtherFieldName:        "key_biotope",
					OtherFieldStringValue: strRef("Nei"),
				})

			addMiljoYesNoAndCommentIfNo("nature_type", "Naturtype, er det plantet i henhold til merkebånd/instruks?", "Kommentar naturtype")
			addMiljoYesNoAndCommentIfNo("cultural", "Kulturminner, er det plantet i henhold til merkebånd/instruks?", "Kommentar kulturminner")
			addMiljoYesNoAndCommentIfNo("operating_routes", "Driftsveier, er det avsatt buffer i henhold til instruks?", "Kommentar driftsveier")
			b.AddRequiredBooleanField("stored_well_sun_Boolean", "Er plantene lagret godt i forhold til solen?")

			b.SetFieldIdSeries(21,
				"treslag",
				"treslag_mix_planted_well_Boolean",
			)
			b.SetFieldIdSeries(30,
				"planted_deep_enough_Boolean",
				"planted_correctly_Boolean",
				"planting_sites_used",
				"planting_sites_used_comment",
			)
			b.SetFieldIdSeries(50,
				"edge_zone_Boolean",
				"edge_zone_comment",
				"edge_zone_average_width",
				"path_buffer_Boolean",
				"path_buffer_comment",
				"trench_Boolean",
				"trench_comment", //56
				//"key_biotope_Boolean", //57
			)
			b.SetFieldIdSeries(58,
				"key_biotope_comment", //58
				"nature_type_Boolean",
				"nature_type_comment", //60
				"cultural_Boolean",
				"cultural_comment",
				"operating_routes_Boolean",
				"operating_routes_comment", //64
				"stored_well_sun_Boolean",
				"key_biotope", //66
			)

		} else if jobType == "Ungskogpleie" {
			b.AddRequiredBooleanField("measurements_satisfactory_Boolean", "Er målingene tilfredsstillende i forhold til instruks?")
			b.AddRequiredBooleanField("futuretrunks_satisfactory_Boolean", "Er valg av framtidsstammer tilfredsstillende?")
			b.AddRequiredBooleanField("futuretrunks_spacing_Boolean", "Er det god romlig fordeling mellom framtidstrærne?")
			b.AddRequiredBooleanField("wildlife_retention_Boolean", "Er det satt igjen ROS, toppkappa løvtrær, furu til viltet?")
			b.AddRequiredBooleanField("stumpheight_satisfactory_Boolean", "Er stubbehøyden tilfredsstillende?")

			//MILJØ:
			addMiljoYesNoAndCommentIfNo("edge_zone", "Kantsone intakt/avsatt i henhold til instruks?", "Kantsone kommentar")
			b.AddNullableStringField("edge_zone_average_width", "Kantsone gjennomsnittsbredde")
			addMiljoYesNoAndCommentIfNo("path_buffer", "Er avstand til stier/skiløyper ivaretatt i henhold til instruks?", "Kommentar sti")
			addMiljoYesNoAndCommentIfNo("trench", "Er grøfter holdt åpne i henhold til instruks?", "Kommentar grøft")
			addMiljoYesNoAndCommentIfNo("key_biotope", "Nøkkelbiotoper og naturtyper ivaretatt i henhold til instruks", "Kommentar nøkkelbiotop")
			addMiljoYesNoAndCommentIfNo("cultural", "Kulturminner ivaretatt i henhold til instruks?", "Kulturminner kommentar")
			addMiljoYesNoAndCommentIfNo("operating_routes", "Er aktuelle driftsveier holdt åpne i henhold til instruks?", "Kommentar driftsveier")
			addMiljoYesNoAndCommentIfNo("wildlife", "Fugler og arter hensyntatt i henhold til instruks?", "Kommentar fugler og arter")
			addMiljoYesNoAndCommentIfNo("read_dist", "Er avstand til skogsbilvei/offentligvei ivaretatt i henhold til instruks?", "")

			b.SetFieldIdSeries(
				100,
				"measurements_satisfactory_Boolean",
				"futuretrunks_satisfactory_Boolean",
				"futuretrunks_spacing_Boolean",
				"wildlife_retention_Boolean",
				"stumpheight_satisfactory_Boolean",
			)
			b.SetFieldIdSeries(50,
				"edge_zone_Boolean",
				"edge_zone_comment",
				"edge_zone_average_width",
				"path_buffer_Boolean",
				"path_buffer_comment",
				"trench_Boolean",
				"trench_comment",      //56
				"key_biotope_Boolean", //57
				"key_biotope_comment", //58
				//"nature_type_Boolean", //59
				//"nature_type_comment", //60
			)
			b.SetFieldIdSeries(61,
				"cultural_Boolean", //61
				"cultural_comment",
				"operating_routes_Boolean",
				"operating_routes_comment", //64
			)

			b.SetFieldIdSeries(80,
				"wildlife_Boolean",
				"wildlife_comment",
				"read_dist_Boolean",
				"read_dist_comment",
			)
		}

		b.AddRequiredBooleanField("deviation_Boolean", "Er det registrert avvik?")
		b.AddNullableStringField("deviation_comment", "Kommentar avvik").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "deviation_Boolean",
				OtherFieldIntValue: intRef(1),
			})
		b.SetFieldIdSeries(70,
			"deviation_Boolean",
			"deviation_comment",
		)
	}

	b.AddNullableStringField("comment", "Generell kommentar")

	//type
	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      "Feltkontroll",
		Name:    "Feltkontroll",
		Templates: []FormTypeTemplate{
			{
				Name: "FeltkontrollOngoing",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{"_type": "FeltkontrollOngoing"},
				},
			},
			{
				Name: "FeltkontrollCompleted",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{"_type": "FeltkontrollCompleted"},
				},
			},
		},
	})

	//fieldIds
	b.SetFieldIdSeries(0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_job_nr",
		"_type",
		"_parentGlobalId",
		"_workTeamOrgNum",
	)

	b.SetFieldIdSeries(20,
		"bestand_str",
	)

	b.SetFieldIdSeries(72,
		"comment",
	)

	b.WithAutoSortex()
}
