package main

import (
	"fmt"
	"log"
	"slices"
	"strconv"
	"strings"
)

func (r *Reporter) AnalyzeGeomAndDoneWorkFields() {

	log.Printf("=========================================================")
	log.Printf("AnalyzeGeomAndDoneWorkFields:")
	log.Printf("=========================================================")

	r.AnalyzeFields(
		[]string{
			"ForhandsryddingGeom",
			"GrofterenskGeom",
			"MarkberedningGeom",
			"PlantingGeom",
			"SaaingGeom",
			"UngskogpleieGeom",
			"DoneWork",
		},
		[]analyzeFieldsGroup{
			{
				Name: "f-owner",
				F:    func(file *ReporterFile) bool { return file.Company != nil && file.Company.hasFeature(isForestOwner) },
			},
			{
				Name: "!f-owner",
				F:    func(file *ReporterFile) bool { return file.Company != nil && !file.Company.hasFeature(isForestOwner) },
			},
			{
				Name: "contractor",
				F:    func(file *ReporterFile) bool { return file.Company != nil && file.Company.hasFeature(isContractor) },
			},
			{
				Name: "!contractor",
				F:    func(file *ReporterFile) bool { return file.Company != nil && !file.Company.hasFeature(isContractor) },
			},

			{
				Name: "forhandsrydding",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "ForhandsryddingGeom") },
			},
			{
				Name: "grofterensk",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "GrofterenskGeom") },
			},
			{
				Name: "markberedning",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "MarkberedningGeom") },
			},
			{
				Name: "planting",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "PlantingGeom") },
			},
			{
				Name: "såing",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "SaaingGeom") },
			},
			{
				Name: "ungskogpleie",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "UngskogpleieGeom") },
			},
			{
				Name: "donework",
				F:    func(file *ReporterFile) bool { return strings.Contains(file.Filename, "DoneWork") },
			},
		},
	)
}

type analyzeFieldsGroup struct {
	Name string
	F    func(file *ReporterFile) bool
}

//This is a little hard to explain :-) I want the 'FieldsOrder' to contain every field in every order.
//so for example if 'a' is before 'b' in one file, but 'b' is before 'a' in another, this can either end
//up as 'a','b','a' or 'b','a','b', guarainteeing that every file will find all it fields in order in the
//set.

//Why? Well this makes it easy to understand order-differences between a set of files, for example skogkultur,
//where all the fields were in different order in all the files.

//If a field is placed differently in two files, they will show up as two rows, and all connected files/groups
//will be part of it

type analyzeFieldsOrder struct {
	Fields        []string
	Index         int
	PendingFields []string
}

func (o *analyzeFieldsOrder) beginNewFile() {
	//reset index to 0
	o.Index = 0
}
func (o *analyzeFieldsOrder) pushNewFileField(field string) {
	for o.Index < len(o.Fields) {
		//log.Printf("field %v from index %v", field, o.Index)
		var iterIndex = o.Index
		for iterIndex < len(o.Fields) {
			//log.Printf("- field %v at index %v", field, iterIndex)
			if o.Fields[iterIndex] == field { //found it!
				//log.Printf("- found it at %v", iterIndex)

				//insert pending before!
				//log.Printf("- insert %v pending at %v of %v", len(o.PendingFields), iterIndex, len(o.Fields))
				o.Fields = slices.Insert(o.Fields, iterIndex, o.PendingFields...)
				//update index!
				o.Index = iterIndex + len(o.PendingFields) + 1
				//log.Printf("- new index %v and a total of %v", o.Index, len(o.Fields))
				//clear pending!
				o.PendingFields = []string{}
				return //done with field!
			}
			//step
			iterIndex += 1
		}
		//log.Printf("- did not find %v, add to pending", field)
		//did not find it? add to pending
		o.PendingFields = append(o.PendingFields, field)
		return
	}
	//end of list
	o.PendingFields = append(o.PendingFields, field)
}
func (o *analyzeFieldsOrder) endNewFile() {
	//add pending to fields, and clear pending!
	//log.Printf("- add %v pending at end, to existing %v", len(o.PendingFields), len(o.Fields))
	o.Fields = append(o.Fields, o.PendingFields...)
	//log.Printf("- ending up with %v elements", len(o.Fields))
	o.PendingFields = []string{}
}
func (o *analyzeFieldsOrder) done() []string {
	return o.Fields
}

func (r *Reporter) AnalyzeFields(filenames []string, groups []analyzeFieldsGroup) {

	//dont run if errors!
	for _, file := range r.files {
		if len(file.Errors) > 0 {
			return
		}
	}

	log.Printf("=========================================================")
	log.Printf("AnalyzeFields:")
	log.Printf("=========================================================")

	//filter files!
	var files []*ReporterFile
	for _, file := range r.files {
		for _, filename := range filenames {
			if strings.Contains(file.Filename, filename) {
				files = append(files, file)
			}
		}
	}

	//find fields
	var fields []string

	for _, file := range files {
		for _, fileField := range file.FinalForm.Fields {
			if !slices.Contains(fields, fileField.Name) {
				fields = append(fields, fileField.Name)
			}
		}
	}

	fieldsOrder := analyzeFieldsOrder{}
	for _, file := range files {
		fieldsOrder.beginNewFile()
		for _, fileField := range file.FinalForm.Fields {
			fieldsOrder.pushNewFileField(fileField.Name)
		}
		fieldsOrder.endNewFile()
		//verify
		for _, fileField := range file.FinalForm.Fields {
			var found = false
			for _, field := range fieldsOrder.Fields {
				if field == fileField.Name {
					found = true
					break
				}
			}
			if !found {
				log.Fatal("WHAT?")
			}
		}
	}
	//verify
	for _, file := range files {
		for _, fileField := range file.FinalForm.Fields {
			var found = false
			for _, field := range fieldsOrder.Fields {
				if field == fileField.Name {
					found = true
					break
				}
			}
			if !found {
				log.Fatal("WHAT?")
			}
		}
	}

	//-----------------------------------

	//clear all
	fieldIndexGroupDidFind := map[string]int{}
	fieldIndexGroupDidNotFind := map[string]int{}
	for fieldsOrderIndex, _ := range fieldsOrder.Fields {
		for _, group := range groups {
			fieldIndexGroupDidFind[group.Name+strconv.Itoa(fieldsOrderIndex)] = 0
			fieldIndexGroupDidNotFind[group.Name+strconv.Itoa(fieldsOrderIndex)] = 0
		}
	}
	//count them! (since field names can be multiple of the same, we use fieldsOrderIndex from now on!)
	for _, file := range files {
		for _, group := range groups {
			if group.F(file) {
				//for each file in each group:
				var fieldsOrderIndex = 0
				for _, fileField := range file.FinalForm.Fields {
					for fieldsOrder.Fields[fieldsOrderIndex] != fileField.Name {
						fieldIndexGroupDidNotFind[group.Name+strconv.Itoa(fieldsOrderIndex)] += 1
						fieldsOrderIndex += 1
						if fieldsOrderIndex >= len(fieldsOrder.Fields) {
							log.Fatal("WHAT?")
						}
					}
					if fieldsOrderIndex > len(fieldsOrder.Fields) {
						log.Fatal("HOW?")
					}
					fieldIndexGroupDidFind[group.Name+strconv.Itoa(fieldsOrderIndex)] += 1
					fieldsOrderIndex += 1
				}
			}
		}
	}

	//-----------------------------------
	//grid!

	//header!
	fieldsGrid := [][]string{}
	fieldsGridHeader := []string{}
	fieldsGridHeader = append(fieldsGridHeader, "FIELD")
	for _, group := range groups {
		fieldsGridHeader = append(fieldsGridHeader, group.Name)
	}
	fieldsGrid = append(fieldsGrid, fieldsGridHeader)
	for fieldsOrderIndex, fieldsOrderField := range fieldsOrder.Fields {
		fieldsGridRow := []string{fieldsOrderField}
		for _, group := range groups {
			didFind := fieldIndexGroupDidFind[group.Name+strconv.Itoa(fieldsOrderIndex)]
			didNotFind := fieldIndexGroupDidNotFind[group.Name+strconv.Itoa(fieldsOrderIndex)]
			if didFind == 0 {
				fieldsGridRow = append(fieldsGridRow, "")
			} else if didNotFind == 0 {
				fieldsGridRow = append(fieldsGridRow, "all")
			} else {
				fieldsGridRow = append(fieldsGridRow, "some")
			}
		}
		fieldsGrid = append(fieldsGrid, fieldsGridRow)
	}

	//render grid
	for _, row := range fieldsGrid {
		var rowString = "|"
		for colIndex, col := range row {
			if colIndex == 0 {
				rowString += fmt.Sprintf("%-25.25s", col) + "|"
			} else {
				rowString += fmt.Sprintf("%-7.7s", col) + "|"
			}
		}
		log.Printf(rowString)
	}

	//-----------------------------------
	//print duplicate fields
	for index, field := range fieldsOrder.Fields {
		var count = 1
		for index2, field2 := range fieldsOrder.Fields {
			if index != index2 && field == field2 {
				if index < index2 { //we only care about the first duplicate :-)
					count += 1
				} else {
					break
				}
			}
		}
		if count > 1 {
			log.Printf("FOUND %v INSTANCES OF %v", count, field)
		}
	}
}
