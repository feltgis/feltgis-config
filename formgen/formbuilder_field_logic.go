package main

type FormFieldLogicBuilder struct {
	logic *FormFieldLogic
}

func (b *FormFieldBuilder) WithLogic() *FormFieldLogicBuilder {
	b.field.Logic = &FormFieldLogic{}
	return &FormFieldLogicBuilder{
		logic: b.field.Logic,
	}
}

func (b *FormFieldLogicBuilder) WithSetValue(setValue FormFieldLogicSetValue) *FormFieldLogicBuilder {
	b.logic.SetValue = &setValue
	return b
}

func (b *FormFieldLogicBuilder) WithVisibleIfField(field FormFieldLogicField) *FormFieldLogicBuilder {
	b.logic.VisibleIf = &FormFieldLogicIfValueIs{
		Field: &field,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithRequiredIfField(field FormFieldLogicField) *FormFieldLogicBuilder {
	b.logic.RequiredIf = &FormFieldLogicIfValueIs{
		Field: &field,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithVisibleIfAllOf(fields ...FormFieldLogicField) *FormFieldLogicBuilder {
	var allOf = []FormFieldLogicFieldInArray{}
	for _, field := range fields {
		allOf = append(
			allOf,
			FormFieldLogicFieldInArray{
				Field: field,
			},
		)
	}
	b.logic.VisibleIf = &FormFieldLogicIfValueIs{
		AllOf: allOf,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithRequiredIfAllOf(fields ...FormFieldLogicField) *FormFieldLogicBuilder {
	var allOf = []FormFieldLogicFieldInArray{}
	for _, field := range fields {
		allOf = append(
			allOf,
			FormFieldLogicFieldInArray{
				Field: field,
			},
		)
	}
	b.logic.RequiredIf = &FormFieldLogicIfValueIs{
		AllOf: allOf,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithVisibleIfAnyOf(fields ...FormFieldLogicField) *FormFieldLogicBuilder {
	var anyOf = []FormFieldLogicFieldInArray{}
	for _, field := range fields {
		anyOf = append(
			anyOf,
			FormFieldLogicFieldInArray{
				Field: field,
			},
		)
	}
	b.logic.VisibleIf = &FormFieldLogicIfValueIs{
		AnyOf: anyOf,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithRequiredIfAnyOf(fields ...FormFieldLogicField) *FormFieldLogicBuilder {
	var anyOf = []FormFieldLogicFieldInArray{}
	for _, field := range fields {
		anyOf = append(
			anyOf,
			FormFieldLogicFieldInArray{
				Field: field,
			},
		)
	}
	b.logic.RequiredIf = &FormFieldLogicIfValueIs{
		AnyOf: anyOf,
	}
	return b
}

func (b *FormFieldLogicBuilder) WithVisibleIf(visibleIf FormFieldLogicIfValueIs) *FormFieldLogicBuilder {
	b.logic.VisibleIf = &visibleIf
	return b
}

func (b *FormFieldLogicBuilder) WithRequiredIf(requiredIf FormFieldLogicIfValueIs) *FormFieldLogicBuilder {
	b.logic.RequiredIf = &requiredIf
	return b
}

func (b *FormFieldLogicBuilder) WithResetValueOnChange(expectedIntValue int) *FormFieldLogicBuilder {
	b.logic.ResetValueOnChange = true
	b.logic.Validation = &FormFieldLogicValidation{ExpectedIntValue: &expectedIntValue}
	return b
}
func (b *FormFieldLogicBuilder) WithMaxIntValueValidation(maxIntValue int) *FormFieldLogicBuilder {
	if b.logic.Validation == nil {
		b.logic.Validation = &FormFieldLogicValidation{}
	}
	b.logic.Validation.MaxIntValue = &maxIntValue
	return b
}
