package main

func PopulatePlantelagerPlanterForm(b *FormBuilder) {
	//DEFAULT!
	b.form.Alias = strRef("{{tree_kind}} {{proveniens}} {{plant_type}}")
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      "{{tree_kind}} {{proveniens}} {{plant_type}}",
			Language: "en",
			Value:    "{{tree_kind}} {{proveniens}} {{plant_type}}",
		},
	)
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_type"

	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_type")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	/*
		ORIGINAL SETUP, PRETTY MESSY SINCE HIDDEN NUMBERS HAD SORTEXES, WAS OUT OF ORDER,
		AND THAT THE no-section WAS USED AS THE FIRST SECTION

		_globalId		0
		_created_Date	1
		_createdBy		2
		_modified_Date	3
		_modifiedBy		4
		_type			5


		comment			20	  (section -)

		_deltasum		21/1

		proveniens		22/14 (section 1)
		plant_type		23    (section 1)

		plant_treatment_comment 24/16 (section 2)

		tree_kind		25    (section 1)
		plant_age		26    (section 1)

		_locationGlobalId

		referencenumber 28
	*/

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")

	b.AddHiddenStringField("_type", "Type")
	b.AddHiddenIntegerField("_deltasum", "Antall planter totalt")
	b.AddHiddenStringField("_locationGlobalId", "Plantelager _globalId")

	b.AddSection("")

	b.AddRequiredStringField("comment", "Merknad").
		WithAliasTranslation("en", "Comment")
	b.AddRequiredStringField("referencenumber", "Referansenummer").
		WithAliasTranslation("en", "Reference number")

	b.AddSection("")

	b.AddNullableStringField("proveniens", "Proveniens").
		WithAliasTranslation("en", "")

	b.AddNullableMultipleChoiceField("plant_type", "Plantetype").
		WithAliasTranslation("en", "Plant type").
		WithSuggestedOption("M60").
		WithSuggestedOption("M95")

	b.AddMultipleChoiceField("tree_kind", "Treslag").
		WithAliasTranslation("en", "Tree kind").
		WithCodedOption("Gran").
		WithCodedOption("Furu").
		WithCodedOption("Bjørk").
		WithCodedOption("Svartor").
		WithCodedOption("Lerk").
		WithCodedOption("Sitka")

	b.AddNullableMultipleChoiceField("plant_age", "Alder").
		WithAliasTranslation("en", "Age").
		WithSuggestedOption("1-åring").
		WithSuggestedOption("2-åring")

	b.AddSection("")

	b.AddNullableStringField("plant_treatment_comment", "Behandling, kommentar").
		WithAliasTranslation("en", "Treatment, comment")

	//fieldIds
	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)

	b.SetFieldIdSeries(5, "_type")
	b.SetFieldIdSeries(21, "_deltasum")
	b.SetFieldIdSeries(27, "_locationGlobalId")

	b.SetFieldIdSeries(20, "comment")
	b.SetFieldIdSeries(28, "referencenumber")

	b.SetFieldIdSeries(22, "proveniens")
	b.SetFieldIdSeries(23, "plant_type")
	b.SetFieldIdSeries(25, "tree_kind")
	b.SetFieldIdSeries(26, "plant_age")

	b.SetFieldIdSeries(24, "plant_treatment_comment")

	//template
	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      "PlantelagerPlanter",
		Name:    "PlantelagerPlanter",
		Templates: []FormTypeTemplate{
			{
				Description: strRef(""),
				DrawingTool: strRef("esriFeatureEditToolPoint"),
				Name:        "PlantelagerPlanter",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{
						"_type": "PlantelagerPlanter",
					},
				},
			},
		},
	})

	b.WithAutoSortex()

	/*
		//For diffing with original file
		b.DebugSetFieldsFileOrder(
			"_globalId",
			"_created_Date",
			"_createdBy",
			"_modified_Date",
			"_modifiedBy",
			"_type",
			"comment",
			"_deltasum",
			"proveniens",
			"plant_type",
			"plant_treatment_comment",
			"tree_kind",
			"plant_age",
			"_locationGlobalId",
			"referencenumber",
		)
	*/
}
