package main

import (
	"encoding/json"
	"strconv"
	"strings"
)

func strRef(s string) *string {
	return &s
}
func intRef(i int) *int {
	return &i
}
func f64Ref(f float64) *float64 { return &f }
func boolRef(b bool) *bool      { return &b }

type StringOrInt struct {
	StringValue *string
	IntValue    *int
}

// Used so that StringOrInt mappes correctly to String or Int
func (v *StringOrInt) MarshalJSON() ([]byte, error) {
	if v.StringValue != nil {
		return json.Marshal(v.StringValue)
	} else if v.IntValue != nil {
		return json.Marshal(v.IntValue)
	} else {
		return json.Marshal(nil)
	}
}

// Used so that StringOrInt mappes correctly from String or Int
func (v *StringOrInt) UnmarshalJSON(data []byte) error {
	myString := string(data)
	if strings.HasPrefix(myString, "\"") && strings.HasSuffix(myString, "\"") {
		myString = strings.TrimPrefix(myString, "\"")
		myString = strings.TrimSuffix(myString, "\"")
		v.StringValue = &myString
	} else {
		if myInt, err := strconv.Atoi(myString); err != nil {
			return err
		} else {
			v.IntValue = &myInt
		}
	}
	return nil
}
