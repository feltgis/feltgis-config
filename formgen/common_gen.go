package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	fmt.Println("------------------------")
	fmt.Println("<FORMGEN>")
	fmt.Println("------------------------")

	reporter := Reporter{}

	//-------------
	//test and prod
	for _, prod := range []bool{false, true} {

		//-------------
		//company independent
		if prod {
			ConfigRunner("feltlogg-skogdata-default", nil, &reporter, PopulateFeltloggConfig)
		} else {
			ConfigRunner("feltlogg-skogdata-default"+"-dev", nil, &reporter, PopulateFeltloggConfig)
			ConfigRunner("feltlogg-skogdata-default"+"-beta", nil, &reporter, PopulateFeltloggConfig)
		}
		FormRunner("FGPJobAssignment", prod, nil, &reporter, PopulatePJobAssignmentForm)

		//-------------
		//companies
		for _, company := range allCompanies {

			FormRunner(company.Prefix+"Stem", prod, company, &reporter, PopulateStemForm)

			//-------------
			//feltlogg
			if company.hasFeature(hasFeltlogg) {

				if prod {
					// currently, all companies use this suffix in prod
					// the ide is to empty old files to disable the old app once all users are on this version
					ConfigRunner("feltlogg-skogdata-"+company.ConfigName+"-3427", company, &reporter, PopulateFeltloggConfig)

					// semi-older apps could be disabled soon by removing this file
					ConfigRunner("feltlogg-skogdata-"+company.ConfigName+"-2347", company, &reporter, PopulateFeltloggConfig)

					// older apps from Nortømmer and GlommenMjøsen use an older suffix
					if company.isCompany(company_Nortømmer, company_GlommenMjosen) {
						ConfigRunner("feltlogg-skogdata-"+company.ConfigName+"-347", company, &reporter, PopulateFeltloggConfig)
					} else {
						ConfigRunner("feltlogg-skogdata-"+company.ConfigName, company, &reporter, PopulateFeltloggConfig)
					}

				} else {
					ConfigRunner("feltlogg-skogdata-"+company.ConfigName+"-dev", company, &reporter, PopulateFeltloggConfig)
					ConfigRunner("feltlogg-skogdata-"+company.ConfigName+"-beta", company, &reporter, PopulateFeltloggConfig)
				}

				//egenkontroll
				if company.hasFeature(isGroupAlma) {
					//currently only one, but should be split in two, actually only half is enabled, because of api bug.
					//alma has some backend integration here.
					FormRunner(company.Prefix+"WorkReport", prod, company, &reporter, PopulateEgenkontrollAlmaForm)
				} else if company.isCompany(company_Fjordtømmer) {
					//fjordtommer wants a single report!
					FormRunner(company.Prefix+"Egenkontroll", prod, company, &reporter, PopulateEgenkontrollForm)

				} else {

					//LEGACY: not needed for new customers, will be removed after a while
					FormRunner(company.Prefix+"Egenkontroll", prod, company, &reporter, PopulateEgenkontrollLegacyForm)

					//this is the "correct" solution
					FormRunner(company.Prefix+"EgenkontrollHogst", prod, company, &reporter, PopulateEgenkontrollForm)
					FormRunner(company.Prefix+"EgenkontrollLass", prod, company, &reporter, PopulateEgenkontrollForm)
				}
			}

			//feltkultur
			if company.hasFeature(hasFeltkultur) {
				ConfigRunner("feltkultur-"+company.ConfigName, company, &reporter, PopulateFeltKulturConfig)
				ConfigRunner("feltkultur-"+company.ConfigName+"-dev", company, &reporter, PopulateFeltKulturConfig)
				ConfigRunner("feltkultur-"+company.ConfigName+"-beta", company, &reporter, PopulateFeltKulturConfig)
			}

			//-------------
			//planner
			FormRunner(company.Prefix+"Session", prod, company, &reporter, PopulateSessionForm)
			if company.hasFeature(hasPlanner) || (!prod && company.hasFeature(hasPlannerInTestOnly)) {
				if prod {
					ConfigRunner("planner-"+company.ConfigName+"", company, &reporter, PopulatePlannerConfig)
				} else {
					ConfigRunner("planner-"+company.ConfigName+"-dev", company, &reporter, PopulatePlannerConfig)
					ConfigRunner("planner-"+company.ConfigName+"-beta", company, &reporter, PopulatePlannerConfig)
				}

				//org
				FormRunner(company.Prefix+"Org", prod, company, &reporter, PopulateOrgForm)
				FormRunner(company.Prefix+"SKOrgConfig", prod, company, &reporter, PopulateSKOrgConfigForm)

				//ordre
				FormRunner(company.Prefix+"POrdre", prod, company, &reporter, PopulatePOrdreForm)

				//div
				FormRunner(company.Prefix+"PCustomer", prod, company, &reporter, PopulatePCustomerForm)

				if company.hasFeature(hasVsys) {
					FormRunner("NT"+"Register", prod, company, &reporter, PopulateRegisterForm)
				} else {
					FormRunner("FG"+"Register", prod, company, &reporter, PopulateRegisterForm)
				}

				//tommerdrift
				FormRunner(company.Prefix+"PTommerdrift", prod, company, &reporter, PopulatePTommerdriftForm)
				FormRunner(company.Prefix+"Miljo", prod, company, &reporter, PopulateMiljoForm)

				if company.isCompany(company_FritzøeSkoger, company_Cappelen, company_LøvenskioldFossum, company_MathiesenEidsvoldVærk) {
					FormRunner(company.Prefix+"Stand", prod, company, &reporter, PopulateStandForm)
				}
				if company.isCompany(company_FritzøeSkoger, company_Cappelen, company_LøvenskioldFossum, company_MathiesenEidsvoldVærk) {
					FormRunner(company.Prefix+"VTStand", prod, company, &reporter, PopulateVTStandForm)
				}
				//skogkultur
				FormRunner(company.Prefix+"PSkogkultur", prod, company, &reporter, PopulatePSkogkulturForm)

				FormRunner(company.Prefix+"PForhandsryddingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PGrofterenskGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PMarkberedningGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PPlantingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PSaaingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PUngskogpleieGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PSkogskadeGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PInfrastrukturGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PGjodslingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PSproytingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PGravearbeidGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PSporskaderettingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)
				FormRunner(company.Prefix+"PStammekvistingGeom", prod, company, &reporter, PopulatePJobTypeGeomForm)

				FormRunner(company.Prefix+"DoneWork", prod, company, &reporter, PopulateDoneWorkForm)
				FormRunner(company.Prefix+"Fakturalinje", prod, company, &reporter, PopulateFakturalinjeForm)

				FormRunner(company.Prefix+"PlantingTelling", prod, company, &reporter, PopulateJobTypeTellingForm)
				FormRunner(company.Prefix+"UngskogpleieTelling", prod, company, &reporter, PopulateJobTypeTellingForm)
				FormRunner(company.Prefix+"MarkberedningTelling", prod, company, &reporter, PopulateJobTypeTellingForm)

				FormRunner(company.Prefix+"PFeltkontrollPlanting", prod, company, &reporter, PopulatePFeltkontrollForm)
				FormRunner(company.Prefix+"PFeltkontrollPlantingTelling", prod, company, &reporter, PopulateJobTypeTellingForm)
				FormRunner(company.Prefix+"PFeltkontrollUngskogpleie", prod, company, &reporter, PopulatePFeltkontrollForm)
				FormRunner(company.Prefix+"PFeltkontrollUngskogpleieTelling", prod, company, &reporter, PopulateJobTypeTellingForm)

				FormRunner(company.Prefix+"Sluttrapport", prod, company, &reporter, PopulateSluttrapportForm)

				FormRunner(company.Prefix+"SluttrapportPlanting", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				FormRunner(company.Prefix+"SluttrapportUngskogpleie", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				FormRunner(company.Prefix+"SluttrapportMarkberedning", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				FormRunner(company.Prefix+"SluttrapportGrofterensk", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				FormRunner(company.Prefix+"SluttrapportForhandsrydding", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				//TODO: what about the rest? GM uses these for GM integration
				if company.isCompany(company_GlommenMjosen) {
					FormRunner(company.Prefix+"SluttrapportSaaing", prod, company, &reporter, PopulateSluttrapportJobtypeForm)

					FormRunner(company.Prefix+"SluttrapportGjodsling", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
					FormRunner(company.Prefix+"SluttrapportSproyting", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
					FormRunner(company.Prefix+"SluttrapportGravearbeid", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
					FormRunner(company.Prefix+"SluttrapportSporskaderetting", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
					FormRunner(company.Prefix+"SluttrapportStammekvisting", prod, company, &reporter, PopulateSluttrapportJobtypeForm)
				}

				//if company.hasFeature(hasSluttrapportPlanting) { //TODO: Only some?
				//FormRunner(company.Prefix+"SluttrapportPlanting", prod, company, &reporter, PopulateSluttrapportPlantingForm)
				//}
				if company.isCompany(company_Nortømmer) { //TODO: Special NT-case, will be removed later!
					FormRunner(company.Prefix+"SluttrapportPlantingTelling", prod, company, &reporter, PopulateJobTypeTellingForm)
				}

				//forevar
				FormRunner(company.Prefix+"ForeVar", prod, company, &reporter, PopulateForevarForm)

				//delte objekter
				FormRunner(company.Prefix+"PDelteObjekter", prod, company, &reporter, PopulatePDelteObjekterForm)

				//plantelager
				FormRunner(company.Prefix+"Plantelager", prod, company, &reporter, PopulatePlantelagerForm)
				FormRunner(company.Prefix+"PlantelagerPlanter", prod, company, &reporter, PopulatePlantelagerPlanterForm)
				FormRunner(company.Prefix+"PlantelagerTelling", prod, company, &reporter, PopulatePlantelagerTellingForm)

				//planting mottakskontroll?
				FormRunner(company.Prefix+"PlantingMottakskontroll", prod, company, &reporter, PopulatePlantingMottakskontrollForm)
			}

			//-------------
			//skogfond
			if company.hasFeature(hasSkogfond) {
				FormRunner(company.Prefix+"Skogfond", prod, company, &reporter, PopulateSkogfondForm)
				for _, colName := range skogfondColNames {
					FormRunner(company.Prefix+"Skogfond"+colName, prod, company, &reporter, PopulateSkogfondChildForm)
				}
			}

			//-------------
			//punkt-linjer-flater
			FormRunner(company.Prefix+"RegistreringerPunkt", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
			FormRunner(company.Prefix+"RegistreringerLinjer", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
			FormRunner(company.Prefix+"RegistreringerFlater", prod, company, &reporter, PopulatePunktLinjerFlaterForm)

			//-------------
			//sporlogg
			FormRunner(company.Prefix+"Sporlogg", prod, company, &reporter, PopulateSporloggForm)

			if company.hasFeature(isGroupAlma) {
				FormRunner(company.Prefix+"HenPoint", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
				FormRunner(company.Prefix+"HenLine", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
				FormRunner(company.Prefix+"HenPolygon", prod, company, &reporter, PopulatePunktLinjerFlaterForm)

				FormRunner(company.Prefix+"MisPoint", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
				FormRunner(company.Prefix+"MisPolygon", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
			} else {
				if company.hasFeature(hasPlanner) || (company.hasFeature(hasPlannerInTestOnly) && !prod) {
					FormRunner(company.Prefix+"MiljoPunkt", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
					FormRunner(company.Prefix+"MiljoLinjer", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
					FormRunner(company.Prefix+"MiljoFlater", prod, company, &reporter, PopulatePunktLinjerFlaterForm)
				}
			}
		}
	}

	reporter.PrintReport()

	fmt.Println("------------------------")
	fmt.Println("</FORMGEN>")
	fmt.Println("------------------------")

	if false {
		reporter.AnalyzeGeomAndDoneWorkFields()
	}

	if err := writeCompanies(); err != nil {
		fmt.Errorf("Unable to write companies: %v", err)
	}
}

func writeCompanies() error {
	js, err := json.MarshalIndent(allCompanies, "", " ")
	if err != nil {
		return err
	}
	return os.WriteFile("../companies.json", js, 0655)
}
