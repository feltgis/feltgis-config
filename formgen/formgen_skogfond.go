package main

import (
	"strings"
)

type skogfondCode string

const (
	suppleringsgrofting  skogfondCode = "002"
	groftebekkerensk     skogfondCode = "003"
	gjodslingmyr         skogfondCode = "010"
	gjodslingfastmark    skogfondCode = "011"
	flaterydding         skogfondCode = "100"
	markberedning        skogfondCode = "101"
	kjembehforarbeid     skogfondCode = "102"
	inngjerdning         skogfondCode = "103"
	fellingnyttblauv     skogfondCode = "104"
	nyplanting           skogfondCode = "120"
	saaing               skogfondCode = "122"
	etabljuletreprod     skogfondCode = "125"
	pyntegrøntproduksjon skogfondCode = "126"
	suppleringsplanting  skogfondCode = "130"
	kjembehetterarb      skogfondCode = "132"
	ungskogpleie         skogfondCode = "133"
	underskuddtynning    skogfondCode = "140"
	kunstigkvisting      skogfondCode = "141"
	tiltakmotskogskade   skogfondCode = "142"
	forhandsryddtynning  skogfondCode = "143"
)

func codeForColName(colName string) skogfondCode {
	for k, v := range skogfondColNames {
		if strings.HasSuffix(colName, v) {
			return k
		}
	}
	panic("unknown colname " + colName)
}

var skogfondNames = map[skogfondCode]string{
	suppleringsgrofting:  "SUPPLERINGSGRØFTING",
	groftebekkerensk:     "GRØFTE-/BEKKERENSK",
	gjodslingmyr:         "GJØDSLING MYR",
	gjodslingfastmark:    "GJØDSLING FASTMARK",
	flaterydding:         "FLATERYDDING",
	markberedning:        "MARKBEREDNING",
	kjembehforarbeid:     "KJEM.BEH. FORARBEID",
	inngjerdning:         "INNGJERDING",
	fellingnyttblauv:     "FELLING NYTTB. LAUV",
	nyplanting:           "NYPLANTING",
	saaing:               "SÅING",
	etabljuletreprod:     "ETABL.JULETREPROD.",
	pyntegrøntproduksjon: "PYNTEGRØNTPRODUKSJON",
	suppleringsplanting:  "SUPPLERINGSPLANTING",
	kjembehetterarb:      "KJEM.BEH. ETTERARB.",
	ungskogpleie:         "UNGSKOGPLEIE",
	underskuddtynning:    "UNDERSKUDD TYNNING",
	kunstigkvisting:      "KUNSTIG KVISTING",
	tiltakmotskogskade:   "TILTAK MOT SKOGSKADE",
	forhandsryddtynning:  "FORHÅNDSRYD. TYNNING",
}

var skogfondColNames = map[skogfondCode]string{
	suppleringsgrofting:  "Suppleringsgrofting",
	groftebekkerensk:     "GrofteBekkerensk",
	gjodslingmyr:         "GjodslingMyr",
	gjodslingfastmark:    "GjodslingFastmark",
	flaterydding:         "Flaterydding",
	markberedning:        "Markberedning",
	kjembehforarbeid:     "KjemBehForarbeid",
	inngjerdning:         "Inngjerdning",
	fellingnyttblauv:     "FellingNyttbLauv",
	nyplanting:           "Nyplanting",
	saaing:               "Saaing",
	etabljuletreprod:     "EtablJuletreProd",
	pyntegrøntproduksjon: "PynteGrontproduksjon",
	suppleringsplanting:  "Suppleringsplanting",
	kjembehetterarb:      "KjemBehEtterarb",
	ungskogpleie:         "Ungskogpleie",
	underskuddtynning:    "UnderskuddTynning",
	kunstigkvisting:      "KunstigKvisting",
	tiltakmotskogskade:   "Skogskade",
	forhandsryddtynning:  "ForhandsryddTynning",
}

func PopulateSkogfondForm(b *FormBuilder) {

	//DEFAULT!
	b.form.Alias = strRef("Skogfond")

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_subCollection"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_subCollection")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "Skogkultur id + subcollection")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_subCollection", "Tiløhrende skogkulturtype")
	b.AddHiddenStringField("_parentGlobalId", "Tilhørende skogkulturobjekt")

	b.AddRequiredStringField("invoice", "Fakturanummer")
	b.AddRequiredStringField("forestowneremail", "Skogeiers e-post")
	b.AddNullableStringField("copyemail", "Kopi e-post")

	b.AddRequiredStringField("komnr", "Kommunenummer")
	b.AddRequiredIntegerField("gardsnr", "Gårdsnummer")
	b.AddRequiredIntegerField("bruksnr", "Bruksnummer")
	b.AddRequiredIntegerField("festenr", "Festenummer")
	b.AddRequiredStringField("skogeiernavn", "Skogeiernavn")
	b.AddNullableStringField("skogfondskonto", "Skogfondskonto")

	b.SetFieldIdSeries(
		0,
		"_globalId", //0
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_subCollection", //5
		"_parentGlobalId",
	)

	b.SetFieldIdSeries(
		10,
		"invoice", //10
		"forestowneremail",
		"copyemail",
	)

	b.SetFieldIdSeries(
		20,
		"komnr", //20
		"gardsnr",
		"bruksnr",
		"festenr",
		"skogeiernavn",
		"skogfondskonto",
	)

	b.VerifyOrderAndSortex()
}

func PopulateSkogfondChildForm(b *FormBuilder) {
	hovedkode := codeForColName(b.file.Name)

	//DEFAULT!
	b.form.Alias = strRef(skogfondNames[hovedkode])

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_hovedkode"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_hovedkode")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "Basert på geom-guid")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_parentGlobalId", "Tilhørende skogkulturobjekt")

	b.AddRequiredDateField("tiltaksdato_Date", "Tiltaksdato")
	b.AddRequiredStringField("beskrivelse", "Beskrivelse")
	b.AddHiddenStringField("_hovedkode", "Hovedkode")

	b.SetFieldIdSeries(
		1,
		"_globalId", //0
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy", //6
		"_parentGlobalId",
		"_hovedkode", //8
	)

	b.SetFieldIdSeries(
		10,
		"tiltaksdato_Date", //10
		"beskrivelse",
	)

	switch hovedkode {
	case suppleringsgrofting,
		groftebekkerensk,
		gjodslingmyr,
		gjodslingfastmark,
		nyplanting,
		saaing,
		etabljuletreprod,
		pyntegrøntproduksjon,
		suppleringsplanting:
		b.AddRequiredIntegerField("antall", skogfondAntallAlias(hovedkode))
		b.SetFieldIdSeries(
			12,
			"antall",
		)
	}

	b.AddMultipleChoiceField("hoh", "Høyde over havet").
		WithCodedOptionWithCustomName("1", "0-149").
		WithCodedOptionWithCustomName("2", "150-249").
		WithCodedOptionWithCustomName("3", "250-349").
		WithCodedOptionWithCustomName("4", "350-449").
		WithCodedOptionWithCustomName("5", "450-549").
		WithCodedOptionWithCustomName("6", "550-649").
		WithCodedOptionWithCustomName("7", "650-749").
		WithCodedOptionWithCustomName("8", "750-849").
		WithCodedOptionWithCustomName("9", "850 og over")
	b.SetFieldIdSeries(
		13,
		"hoh",
	)

	switch hovedkode {
	case gjodslingfastmark,
		flaterydding,
		markberedning,
		kjembehforarbeid,
		inngjerdning,
		fellingnyttblauv,
		nyplanting,
		saaing,
		etabljuletreprod,
		pyntegrøntproduksjon,
		suppleringsplanting,
		kjembehetterarb,
		ungskogpleie,
		underskuddtynning,
		kunstigkvisting,
		tiltakmotskogskade,
		forhandsryddtynning:
		b.AddMultipleChoiceField("bonitet", "Bonitet").
			WithCodedOptionWithCustomName("1", "26 og høyere").
			WithCodedOptionWithCustomName("2", "23").
			WithCodedOptionWithCustomName("3", "20").
			WithCodedOptionWithCustomName("4", "17").
			WithCodedOptionWithCustomName("5", "14").
			WithCodedOptionWithCustomName("6", "11").
			WithCodedOptionWithCustomName("7", "8 og lavere")
		b.SetFieldIdSeries(
			14,
			"bonitet",
		)
	}

	switch hovedkode {
	case suppleringsgrofting,
		groftebekkerensk:
		b.AddMultipleChoiceField("groftetype", "Grøftetype").
			WithCodedOptionWithCustomName("1", "Grøfter").
			WithCodedOptionWithCustomName("2", "Kanaler")
		b.SetFieldIdSeries(
			15,
			"groftetype",
		)
	}

	switch hovedkode {
	case suppleringsgrofting,
		groftebekkerensk,
		flaterydding,
		markberedning,
		kjembehforarbeid,
		inngjerdning,
		fellingnyttblauv,
		nyplanting,
		saaing,
		etabljuletreprod,
		pyntegrøntproduksjon,
		suppleringsplanting,
		kjembehetterarb,
		ungskogpleie,
		underskuddtynning,
		kunstigkvisting,
		tiltakmotskogskade,
		forhandsryddtynning:
		b.AddMultipleChoiceField("arbeidskraft", "Arbeidskraft").
			WithCodedOptionWithCustomName("1", "Eget arb./egne ansat").
			WithCodedOptionWithCustomName("2", "Entreprenør/andelsl.")
		b.SetFieldIdSeries(
			16,
			"arbeidskraft",
		)
	}

	switch hovedkode {
	case gjodslingfastmark:
		b.AddMultipleChoiceField("gjodslingsmetode", "Gjodslingsmetode").
			WithCodedOptionWithCustomName("Helikopter", "1").
			WithCodedOptionWithCustomName("Traktor", "2").
			WithCodedOptionWithCustomName("Manuelt", "3")
		b.SetFieldIdSeries(
			17,
			"gjodslingsmetode",
		)
	}
	switch hovedkode {
	case gjodslingmyr,
		gjodslingfastmark:
		b.AddMultipleChoiceField("vegetasjonstype", "Vegetasjonstype").
			WithCodedOptionWithCustomName("1", "Lavskog").
			WithCodedOptionWithCustomName("2", "Blokkebærskog").
			WithCodedOptionWithCustomName("3", "Bærlyngskog").
			WithCodedOptionWithCustomName("4", "Blåbærskog").
			WithCodedOptionWithCustomName("5", "Småbregneskog").
			WithCodedOptionWithCustomName("6", "Storbregneskog").
			WithCodedOptionWithCustomName("7", "Lågurtskog").
			WithCodedOptionWithCustomName("8", "Høgstaudeskog").
			WithCodedOptionWithCustomName("9", "Furumyrskog").
			WithCodedOptionWithCustomName("A", "Lauvskoger").
			WithCodedOptionWithCustomName("B", "Sumpskoger").
			WithCodedOptionWithCustomName("C", "Myrskog").
			WithCodedOptionWithCustomName("D", "Røsslynghei")
		b.SetFieldIdSeries(
			18,
			"vegetasjonstype",
		)
	}

	switch hovedkode {
	case markberedning:
		b.AddMultipleChoiceField("metode_markberedning", "Metode").
			WithCodedOptionWithCustomName("1", "Såing/naturlig foryngelse").
			WithCodedOptionWithCustomName("2", "Planting").
			WithCodedOptionWithCustomName("3", "Kombinert")
		b.SetFieldIdSeries(
			19,
			"metode_markberedning",
		)
	}

	switch hovedkode {
	case nyplanting, saaing, etabljuletreprod, suppleringsplanting:
		b.AddMultipleChoiceField("markslag", "Markslag").
			WithCodedOptionWithCustomName("1", "Barskog").
			WithCodedOptionWithCustomName("2", "Lauvskog").
			WithCodedOptionWithCustomName("3", "Snaumark").
			WithCodedOptionWithCustomName("4", "Fulldyrket jord").
			WithCodedOptionWithCustomName("5", "Innmarksbeite").
			WithCodedOptionWithCustomName("6", "Overflatedyrket jord").
			WithCodedOptionWithCustomName("7", "Blandingsskog").
			WithCodedOptionWithCustomName("8", "Myr")
		b.SetFieldIdSeries(
			20,
			"markslag",
		)
	}

	switch hovedkode {
	case suppleringsgrofting,
		groftebekkerensk,
		gjodslingmyr:
		b.AddMultipleChoiceField("torvmarkstype", "Torvmarkstype").
			WithCodedOptionWithCustomName("1", "Vannsyk skogsmark").
			WithCodedOptionWithCustomName("2", "Næringsrik tilsigmyr").
			WithCodedOptionWithCustomName("3", "Svak tilsigsmyr").
			WithCodedOptionWithCustomName("4", "Ren nedbørsmyr")
		b.SetFieldIdSeries(
			21,
			"torvmarkstype",
		)
	}

	switch hovedkode {
	case suppleringsgrofting,
		groftebekkerensk:
		b.AddRequiredIntegerField("areal", "Areal")
		b.SetFieldIdSeries(
			22,
			"areal",
		)
	}

	switch hovedkode {
	case kjembehforarbeid:
		b.AddMultipleChoiceField("sproytemetode", "Sprøytemetode").
			WithCodedOptionWithCustomName("1", "Helikopter").
			WithCodedOptionWithCustomName("2", "Traktor").
			WithCodedOptionWithCustomName("3", "Ryggtåkesprøyte").
			WithCodedOptionWithCustomName("4", "Hoggsprøyting").
			WithCodedOptionWithCustomName("5", "Stubbebehandling")
		b.SetFieldIdSeries(
			23,
			"sproytemetode",
		)
	}
	switch hovedkode {
	case gjodslingmyr,
		gjodslingfastmark,
		nyplanting,
		saaing,
		etabljuletreprod,
		pyntegrøntproduksjon,
		suppleringsplanting:
		b.AddMultipleChoiceField("treslag", "Treslag").
			WithCodedOptionWithCustomName("1", "Gran").
			WithCodedOptionWithCustomName("2", "Furu").
			WithCodedOptionWithCustomName("3", "Sitka").
			WithCodedOptionWithCustomName("4", "Contorta").
			WithCodedOptionWithCustomName("5", "Lutzii").
			WithCodedOptionWithCustomName("6", "Bjørk").
			WithCodedOptionWithCustomName("7", "Flere treslag").
			WithCodedOptionWithCustomName("8", "Andre Bartre").
			WithCodedOptionWithCustomName("9", "Andre Lauvtre")
		b.SetFieldIdSeries(
			24,
			"treslag",
		)
	}

	b.AddRequiredDoubleField("kostnad", "Kostnad")
	b.SetFieldIdSeries(
		25,
		"kostnad",
	)

	switch hovedkode {
	case suppleringsplanting:
		b.AddMultipleChoiceField("tetthet_etter_supplering", "Tetthet etter supplering").
			WithCodedOptionWithCustomName("1", "Tilfredsstiller min").
			WithCodedOptionWithCustomName("2", "Ikke tilstrekkelig")
		b.SetFieldIdSeries(
			26,
			"tetthet_etter_supplering",
		)
	}

	switch hovedkode {
	case nyplanting, saaing, suppleringsplanting:
		b.AddRequiredStringField("froparti_id", "Frøparti-ID")
		b.SetFieldIdSeries(
			27,
			"froparti_id",
		)
	}

	switch hovedkode {
	case nyplanting:
		b.AddMultipleChoiceField("planting_etter_markberedning", "Planting etter markberedning").
			WithCodedOptionWithCustomName("1", "Med markberedning").
			WithCodedOptionWithCustomName("2", "Uten markberedning").
			WithCodedOptionWithCustomName("9", "Ikke oppgitt")
		b.SetFieldIdSeries(
			28,
			"planting_etter_markberedning",
		)
	}

	switch hovedkode {
	case gjodslingmyr,
		gjodslingfastmark,
		nyplanting,
		saaing,
		etabljuletreprod,
		pyntegrøntproduksjon,
		suppleringsplanting:
		visibleIfTreslagMultiple := "7"
		b.AddRequiredIntegerField("antall_gran", "Antall gran").
			WithLogic().
			WithVisibleIf(FormFieldLogicIfValueIs{
				Field: &FormFieldLogicField{
					Operator:              "==",
					OtherFieldStringValue: &visibleIfTreslagMultiple,
					OtherFieldName:        "treslag",
				},
			})
		b.AddRequiredIntegerField("antall_furu", "Antall furu").
			WithLogic().
			WithVisibleIf(FormFieldLogicIfValueIs{
				Field: &FormFieldLogicField{
					Operator:              "==",
					OtherFieldStringValue: &visibleIfTreslagMultiple,
					OtherFieldName:        "treslag",
				},
			})
		b.AddRequiredIntegerField("antall_lauv", "Antall lauv").
			WithLogic().
			WithVisibleIf(FormFieldLogicIfValueIs{
				Field: &FormFieldLogicField{
					Operator:              "==",
					OtherFieldStringValue: &visibleIfTreslagMultiple,
					OtherFieldName:        "treslag",
				},
			})
		b.SetFieldIdSeries(
			29,
			"antall_gran", //29
			"antall_furu",
			"antall_lauv", //31
		)
	}

	b.AddHiddenNullableStringField("_title", "Tittel")
	b.SetFieldIdSeries(32, "_title")

	b.form.Types = []FormType{
		{
			Comment: nil,
			Domains: FormTypeDomains{},
			Id:      "hovedkode",
			Name:    "hovedkode",
			Templates: []FormTypeTemplate{
				{
					Name: "hovedkode",
					Prototype: FormTypeTemplatePrototype{
						map[string]interface{}{
							"hovedkode": string(hovedkode),
						},
					},
				},
			},
		},
	}

	b.WithAutoSortex()
}

func skogfondAntallAlias(code skogfondCode) string {
	switch code {
	case suppleringsgrofting, groftebekkerensk:
		return "Antall meter"
	case gjodslingfastmark, gjodslingmyr:
		return "Antall kilo"
	case nyplanting, etabljuletreprod, suppleringsplanting:
		return "Antall planter"
	case pyntegrøntproduksjon:
		return "Antall produsert"
	case saaing:
		return "Antall gram"
	}
	return ""
}
