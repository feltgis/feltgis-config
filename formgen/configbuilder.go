package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
)

type ConfigBuilder struct {
	file    *ConfigFile
	config  *Config
	company *Company //nil = "default"-files

	reporter *Reporter
}

func NewConfigBuilder(file *ConfigFile, company *Company, reporter *Reporter) *ConfigBuilder {
	p := new(ConfigBuilder)
	p.config = &Config{}
	p.file = file
	p.company = company
	p.reporter = reporter
	p.reporter.SetCurrentFile(file.Filename(), company)
	return p
}

func (b *ConfigBuilder) Config() *Config {

	/*
		//verify no colliding fieldIds
		fieldIds := []string{}
		var fieldIdError = false
		for _, field := range b.form.Fields {
			fieldId := strconv.Itoa(field.FieldId)
			if slices.Contains(fieldIds, fieldId) == true {
				fmt.Println("Duplicate fieldID " + fieldId + " for field " + field.Name)
				fieldIdError = true
			} else {
				fieldIds = append(fieldIds, fieldId)
			}
		}
		if fieldIdError {
			for _, field := range b.form.Fields {
				fieldId := strconv.Itoa(field.FieldId)
				fmt.Println("" + field.Name + " -> " + fieldId)
			}
		}

		//verify no colliding sortexes
		sortexes := []string{}
		var sortexesError = false
		for _, field := range b.form.Fields {
			if field.Section != nil && field.Sortix != nil {
				sortex := strconv.Itoa(*field.Section) + strconv.Itoa(*field.Sortix)
				if slices.Contains(sortexes, sortex) == true {
					fmt.Println("Duplicate sortex " + sortex + " for field " + field.Name)
					sortexesError = true
				} else {
					fieldIds = append(sortexes, sortex)
				}
			}
		}
		if sortexesError {
			for _, field := range b.form.Fields {
				if field.Section != nil && field.Sortix != nil {
					sortex := strconv.Itoa(*field.Section) + strconv.Itoa(*field.Sortix)
					fmt.Println("" + field.Name + " -> " + sortex)
				}
			}
		}
	*/

	return b.config
}

// CUSTOMERS:
func (b *ConfigBuilder) IsDefault() bool { return b.company == nil }
func (b *ConfigBuilder) IsNortømmer() bool {
	return b.company != nil && b.company.isCompany(company_Nortømmer)
}
func (b *ConfigBuilder) IsFritzøe() bool {
	return b.company != nil && b.company.isCompany(company_Fritzøe)
}
func (b *ConfigBuilder) IsFritzøeSkoger() bool {
	return b.company != nil && b.company.isCompany(company_FritzøeSkoger)
} // FritzøeSkoger = Fritzøe with new Org number
func (b *ConfigBuilder) IsGlommenMjosen() bool {
	return b.company != nil && b.company.isCompany(company_GlommenMjosen)
}
func (b *ConfigBuilder) IsGlommenSkog() bool {
	return b.company != nil && b.company.isCompany(company_GlommenSkog)
}
func (b *ConfigBuilder) IsStoraEnso() bool {
	return b.company != nil && b.company.isCompany(company_StoraEnso)
}
func (b *ConfigBuilder) IsFjordtømmer() bool {
	return b.company != nil && b.company.isCompany(company_Fjordtømmer)
}
func (b *ConfigBuilder) IsStatSkog() bool {
	return b.company != nil && b.company.isCompany(company_StatSkog)
}
func (b *ConfigBuilder) IsCappelen() bool {
	return b.company != nil && b.company.isCompany(company_Cappelen)
}
func (b *ConfigBuilder) IsLøvenskioldFossum() bool {
	return b.company != nil && b.company.isCompany(company_LøvenskioldFossum)
}
func (b *ConfigBuilder) IsMathiesen() bool {
	return b.company != nil && b.company.isCompany(company_MathiesenEidsvoldVærk)
}
func (b *ConfigBuilder) IsValdresSkog() bool {
	return b.company != nil && b.company.isCompany(company_ValdresSkog)
}
func (b *ConfigBuilder) IsAltiskog() bool {
	return b.company != nil && b.company.isCompany(company_Altiskog)
}

// GROUPS:
func (b *ConfigBuilder) IsAllma() bool { return b.company != nil && b.company.hasFeature(isGroupAlma) }
func (b *ConfigBuilder) IsGlommen() bool {
	return b.company != nil && b.company.hasFeature(isGroupGlommen)
}

// CUSTOMER TYPES:
func (b *ConfigBuilder) IsForestOwner() bool {
	return b.company != nil && b.company.hasFeature(isForestOwner)
}
func (b *ConfigBuilder) IsContractor() bool {
	return b.company != nil && b.company.hasFeature(isContractor)
}
func (b *ConfigBuilder) hasVsysClient() bool {
	for _, company := range companiesWithVSYS {
		if company.isCompany(b.company) {
			return true
		}
	}
	return false
}

func (b *ConfigBuilder) IsProd() bool {
	return !b.IsBeta() && !b.IsDev()
}
func (b *ConfigBuilder) IsBeta() bool {
	return strings.Contains(b.file.Name, "beta")
}
func (b *ConfigBuilder) IsDev() bool {
	return strings.Contains(b.file.Name, "dev")
}

func (b *ConfigBuilder) AddSuperUser(email string) *ConfigBuilder {
	if b.config.Admin == nil {
		b.config.Admin = &ConfigAdmin{}
	}
	b.config.Admin.SuperUsers = append(
		b.config.Admin.SuperUsers,
		email,
	)
	return b
}

func (b *ConfigBuilder) AddEnabledFeature(feature string) *ConfigBuilder {
	b.config.EnabledFeatures = append(
		b.config.EnabledFeatures,
		feature,
	)
	return b
}

func (b *ConfigBuilder) AddSetting(title, subTitle, identifier string, defaultValue ...any) *ConfigBuilder {
	var defaultValueRef *ConfigUserAppSettingDefaultValue
	if len(defaultValue) > 0 {
		switch t := defaultValue[0].(type) {
		case bool:
			defaultValueRef = &ConfigUserAppSettingDefaultValue{BoolValue: boolRef(defaultValue[0].(bool))}
		default:
			panic(fmt.Sprintf("unknown settings switch type %T", t))
		}
	}
	b.config.Settings = append(
		b.config.Settings,
		ConfigUserAppSetting{
			Title:        title,
			SubTitle:     subTitle,
			DefaultValue: defaultValueRef,
			ID:           identifier,
		},
	)
	return b
}

func (b *ConfigBuilder) AddEnvironmentClearedEmails(emails ConfigEnvironmentClearedEmails) *ConfigBuilder {
	b.config.EnvironmentClearedEmails = &emails
	return b
}

func (b *ConfigBuilder) AddCustomConfig(customConfig ConfigCustomConfig) *ConfigBuilder {
	b.config.CustomConfig = &customConfig
	return b
}

func (b *ConfigBuilder) AddCustomConfigObjectsAllmaClient(client ConfigCustomConfigObjectsAllmaClient) *ConfigBuilder {
	if b.config.CustomConfigObjects == nil {
		b.config.CustomConfigObjects = &ConfigCustomConfigObjects{}
	}
	b.config.CustomConfigObjects.AllmaClients = append(
		b.config.CustomConfigObjects.AllmaClients,
		client,
	)
	return b
}
func (b *ConfigBuilder) AddCustomConfigObjectsCountOperation(countOperation ConfigCustomConfigObjectsCountOperation) *ConfigBuilder {
	if b.config.CustomConfigObjects == nil {
		b.config.CustomConfigObjects = &ConfigCustomConfigObjects{}
	}
	b.config.CustomConfigObjects.CountOperations = append(
		b.config.CustomConfigObjects.CountOperations,
		countOperation,
	)
	return b
}
func (b *ConfigBuilder) AddCustomConfigObjectsUpdateReaction(updateReaction ConfigCustomConfigObjectsUpdateReaction) *ConfigBuilder {
	if b.config.CustomConfigObjects == nil {
		b.config.CustomConfigObjects = &ConfigCustomConfigObjects{}
	}
	b.config.CustomConfigObjects.UpdateReactions = append(
		b.config.CustomConfigObjects.UpdateReactions,
		updateReaction,
	)
	return b
}
func (b *ConfigBuilder) AddCustomConfigUserNotRequiredToSignWorkOrder(email string) *ConfigBuilder {
	if b.config.CustomConfigObjects == nil {
		b.config.CustomConfigObjects = &ConfigCustomConfigObjects{}
	}
	b.config.CustomConfigObjects.UsersNotRequiredToSignWorkOrder = append(
		b.config.CustomConfigObjects.UsersNotRequiredToSignWorkOrder,
		email,
	)
	return b
}

func (b *ConfigBuilder) AddFeatureSwitch(featureSwitch ConfigFeatureSwitch) *ConfigBuilder {
	b.config.FeatureSwitches = append(
		b.config.FeatureSwitches,
		featureSwitch,
	)
	return b
}

func (b *ConfigBuilder) AddNewFeatureCopyOperation(copyOperation ConfigNewFeatureCopyOperation) *ConfigBuilder {
	b.config.NewFeatureCopyOperations = append(
		b.config.NewFeatureCopyOperations,
		copyOperation,
	)
	return b
}

func (b *ConfigBuilder) AddNewFeatureFromStand(values ConfigNewFeatureFromStand) *ConfigBuilder {
	b.config.NewFeatureFromStand = append(
		b.config.NewFeatureFromStand,
		values,
	)
	return b
}

func (b *ConfigBuilder) AddNewFeatureDefaultValues(defaultValues ConfigNewFeatureDefaults) *ConfigBuilder {
	b.config.NewFeatureDefaults = append(
		b.config.NewFeatureDefaults,
		defaultValues,
	)
	return b
}

func (b *ConfigBuilder) AddMapAddFeature(feature ConfigMapAddFeature) *ConfigBuilder {
	b.config.Map.AddFeatures = append(
		b.config.Map.AddFeatures,
		feature,
	)
	return b
}
func (b *ConfigBuilder) AddMapEditFeature(feature ConfigMapEditFeature) *ConfigBuilder {
	b.config.Map.EditFeatures = append(
		b.config.Map.EditFeatures,
		feature,
	)
	return b
}
func (b *ConfigBuilder) AddRasterMapBackground(name string, hidden bool, url string) *ConfigBuilder {
	background := ConfigMapBackground{
		Name: name,
		Type: "raster",
		Url:  url,
	}
	if hidden {
		background.Hidden = true
	}
	b.config.Map.Background = append(
		b.config.Map.Background,
		background,
	)
	return b
}
func (b *ConfigBuilder) AddPurchaseOrderPresetIssuingInstruction(issuingInstruction map[string]interface{}) *ConfigBuilder {
	if b.config.PurchaseOrderPresets == nil {
		b.config.PurchaseOrderPresets = &ConfigPurchaseOrderPresets{}
	}
	b.config.PurchaseOrderPresets.IssuingInstruction = append(
		b.config.PurchaseOrderPresets.IssuingInstruction,
		issuingInstruction,
	)
	return b
}
func (b *ConfigBuilder) AddPurchaseOrderPresetDefaultOrders(defaultOrder map[string]interface{}) *ConfigBuilder {
	if b.config.PurchaseOrderPresets == nil {
		b.config.PurchaseOrderPresets = &ConfigPurchaseOrderPresets{}
	}
	b.config.PurchaseOrderPresets.DefaultOrders = append(
		b.config.PurchaseOrderPresets.DefaultOrders,
		defaultOrder,
	)
	return b
}
func (b *ConfigBuilder) AddGenericReportTemplates(template string) *ConfigBuilder {
	b.config.GenericReports = &ConfigGenericReports{
		Template: template,
	}
	return b
}

func (b *ConfigBuilder) AddGenericReportStaticHtmlTemplates(key string, url string) *ConfigBuilder {
	if b.config.GenericReports == nil {
		log.Fatal("Adding StaticHtmlTemplates without a generic template? Call AddGenericReportTemplates first")
	}
	if b.config.GenericReports.StaticHtmlTemplates == nil {
		b.config.GenericReports.StaticHtmlTemplates = map[string]string{}
	}
	b.config.GenericReports.StaticHtmlTemplates[key] = url
	return b
}

func (b *ConfigBuilder) AddReportTemplate(collection string, template string, languages []string) *ConfigBuilder {
	b.config.Reports = append(
		b.config.Reports,
		ConfigReport{
			Collection: collection,
			Template:   template,
			Languages:  languages,
		},
	)
	return b
}
func (b *ConfigBuilder) AddServerQuery(name string, returnFields []string) *ConfigBuilder {
	b.config.ServerQueries = append(
		b.config.ServerQueries,
		ConfigServerQuery{
			Name:         name,
			ReturnFields: returnFields,
		},
	)
	return b
}

/*------------
Map layer
------------*/

type ConfigMapLayerBuilder struct {
	layer *ConfigMapLayer
}

func (b *ConfigBuilder) AddMapLayer(mapLayer MapLayer) *ConfigMapLayerBuilder {
	layer := ConfigMapLayer{}
	layer.Name = mapLayer.Name
	layer.Type = string(mapLayer.Type)

	if b.IsDev() || b.IsBeta() {
		layer.Url = mapLayer.DevUrl
		if len(mapLayer.DevStatusUrl) > 0 {
			layer.StatusUrl = &mapLayer.DevStatusUrl
		}
		layer.BodyReplacements = mapLayer.DevBodyReplacements
	} else if b.IsProd() {
		layer.Url = mapLayer.ProdUrl
		if len(mapLayer.ProdStatusUrl) > 0 {
			layer.StatusUrl = &mapLayer.ProdStatusUrl
		}
		layer.BodyReplacements = mapLayer.ProdBodyReplacements
	} else {
		log.Fatal("Neither prod or dev?")
	}
	if len(mapLayer.LayerId) > 0 {
		layer.LayerId = &mapLayer.LayerId
	}

	layer.CacheWarmupUrls = mapLayer.CacheWarmupUrls

	b.config.Map.Layers = append(
		b.config.Map.Layers,
		&layer,
	)

	return &ConfigMapLayerBuilder{
		layer: &layer,
	}
}

func (b *ConfigBuilder) AddCollectionMapLayer(collection string, mapLayerType MapLayerType) *ConfigMapLayerBuilder {
	return b.AddPrefixedCollectionMapLayer(b.company.Prefix, collection, mapLayerType)
}
func (b *ConfigBuilder) AddPrefixedCollectionMapLayer(prefix string, collection string, mapLayerType MapLayerType) *ConfigMapLayerBuilder {
	layer := ConfigMapLayer{}
	layer.Name = prefix + collection
	layer.Type = string(mapLayerType)
	b.config.Map.Layers = append(
		b.config.Map.Layers,
		&layer,
	)
	return &ConfigMapLayerBuilder{
		layer: &layer,
	}
}

func (b *ConfigMapLayerBuilder) WithAlias(alias string) *ConfigMapLayerBuilder {
	b.layer.Alias = alias
	return b
}
func (b *ConfigMapLayerBuilder) SetHiddenFromLayerList() *ConfigMapLayerBuilder {
	b.layer.Hidefromlayerlist = boolRef(true)
	return b
}
func (b *ConfigMapLayerBuilder) WithKeepInBackgroundMap() *ConfigMapLayerBuilder {
	b.layer.KeepInBackgroundMap = boolRef(true)
	return b
}
func (b *ConfigMapLayerBuilder) WithIdentityDisabled() *ConfigMapLayerBuilder {
	b.layer.IdentifyDisabled = boolRef(true)
	return b
}
func (b *ConfigMapLayerBuilder) WithDefaultOpacity(opacity float64) *ConfigMapLayerBuilder {
	b.layer.DefaultOpacity = &opacity
	return b
}
func (b *ConfigMapLayerBuilder) WithPreview(preview string) *ConfigMapLayerBuilder {
	b.layer.Preview = "https://map-thumbs.feltgis.no/" + preview
	return b
}
func (b *ConfigMapLayerBuilder) WithMinOpacity(opacity float64) *ConfigMapLayerBuilder {
	b.layer.Minopacity = &opacity
	return b
}
func (b *ConfigMapLayerBuilder) WithMinScale(scale int) *ConfigMapLayerBuilder {
	b.layer.Minscale = scale
	return b
}
func (b *ConfigMapLayerBuilder) WithRendererIncludedFields(fields ...string) *ConfigMapLayerBuilder {
	if b.layer.Rendering == nil {
		b.layer.Rendering = &ConfigMapLayerRendering{}
	}
	b.layer.Rendering.IncludeFields = fields
	return b
}
func (b *ConfigMapLayerBuilder) WithRendererDefinitionExpression(definitionExpression string) *ConfigMapLayerBuilder {
	if b.layer.Rendering == nil {
		b.layer.Rendering = &ConfigMapLayerRendering{}
	}
	b.layer.Rendering.DefinitionExpression = definitionExpression
	return b
}
func (b *ConfigMapLayerBuilder) WithMinLevel(minLevel int) *ConfigMapLayerBuilder {
	b.layer.MinLevel = &minLevel
	return b
}
func (b *ConfigBuilder) SetLayersHiddenFromLayerList(layernames ...string) {
	for _, layername := range layernames {
		var found = false
		for _, layer := range b.config.Map.Layers {
			if layer.Name == layername {
				found = true
				layer.Hidefromlayerlist = boolRef(true)
				if layer.Minopacity == nil {
					value := 0.75
					layer.Minopacity = &value
				}
			}
		}
		if !found {
			b.reporter.Warning("layername '" + layername + "' not found in for SetLayersHiddenFromLayerList")
		}
	}
}

/*------------
Search
------------*/

type ConfigSearchBuilder struct {
	search *ConfigSearch
}

func (b *ConfigBuilder) AddSearch(title string, searchtype string, buttonText string) *ConfigSearchBuilder {
	for _, existing := range b.config.Search {
		if strings.EqualFold(existing.Title, title) {
			b.reporter.Error("Map search '" + title + "' already defined for AddSearch(). Title must be unique")
		}
	}
	search := ConfigSearch{
		ButtonText: buttonText,
		Title:      title,
		Type:       searchtype,
	}
	b.config.Search = append(
		b.config.Search,
		&search,
	)
	return &ConfigSearchBuilder{
		search: &search,
	}
}
func (b *ConfigSearchBuilder) WithField(alias string, title string, searchtype string) *ConfigSearchBuilder {
	b.search.Fields = append(
		b.search.Fields,
		ConfigSearchField{
			Alias: alias,
			Title: title,
			Type:  searchtype,
		},
	)
	return b
}
func (b *ConfigSearchBuilder) WithSymbol(symbolType CIMSymbolType, layers ...CIMSymbolLayer) *ConfigSearchBuilder {
	symbol := CimSymbol(symbolType, layers...)
	symbolJson, _ := json.Marshal(symbol)
	b.search.Symbol = string(symbolJson)
	return b
}
func (b *ConfigSearchBuilder) WithCollection(collection string) *ConfigSearchBuilder {
	b.search.Collection = collection
	return b
}
func (b *ConfigSearchBuilder) WithURL(url string) *ConfigSearchBuilder {
	b.search.URL = url
	return b
}
func (b *ConfigSearchBuilder) WithAutocompleteField(autocompleteField string) *ConfigSearchBuilder {
	b.search.AutocompleteField = autocompleteField
	return b
}
func (b *ConfigSearchBuilder) WithMaxAutoCompleteResults(maxAutoCompleteResults int) *ConfigSearchBuilder {
	b.search.MaxAutoCompleteResults = &maxAutoCompleteResults
	return b
}
func (b *ConfigSearchBuilder) WithResultSubTitleTemplate(resultSubTitleTemplate string) *ConfigSearchBuilder {
	b.search.ResultSubTitleTemplate = resultSubTitleTemplate
	return b
}
func (b *ConfigSearchBuilder) WithResultTitleTemplate(resultTitleTemplate string) *ConfigSearchBuilder {
	b.search.ResultTitleTemplate = resultTitleTemplate
	return b
}

/*------------
Sync
------------*/

func (b *ConfigBuilder) AddSync(prefix string, bucket string) *ConfigBuilder {
	if b.config.Sync.Bucket != "" {
		panic("sync added twice?")
	}
	b.config.Sync.Bucket = bucket
	b.config.Sync.IntervalMinutes = intRef(2)
	b.config.Sync.OrgPrefix = prefix
	return b
}

type ConfigSyncCollectionBuilder struct {
	collection *ConfigSyncCollection
}

func (b *ConfigBuilder) AddSyncCollection(name string) *ConfigSyncCollectionBuilder {
	collection := ConfigSyncCollection{}
	collection.Name = name

	b.config.Sync.Collections = append(
		b.config.Sync.Collections,
		&collection,
	)
	return &ConfigSyncCollectionBuilder{
		collection: &collection,
	}
}

func (b *ConfigSyncCollectionBuilder) WithAlias(alias string) *ConfigSyncCollectionBuilder {
	b.collection.Alias = &alias
	return b
}
func (b *ConfigSyncCollectionBuilder) WithInitialDump(initialDump string) *ConfigSyncCollectionBuilder {
	b.collection.InitialDump = &initialDump
	return b
}
func (b *ConfigSyncCollectionBuilder) WithAppFeature(appFeature string) *ConfigSyncCollectionBuilder {
	b.collection.AppFeature = &appFeature
	return b
}
func (b *ConfigSyncCollectionBuilder) WithBatchSize(batchSize int) *ConfigSyncCollectionBuilder {
	b.collection.BatchSize = &batchSize
	return b
}
func (b *ConfigSyncCollectionBuilder) WithIndexFields(indexFields ...string) *ConfigSyncCollectionBuilder {
	b.collection.IndexFields = indexFields
	return b
}
func (b *ConfigSyncCollectionBuilder) WithFtsFields(ftsFields ...string) *ConfigSyncCollectionBuilder {
	b.collection.FtsFields = ftsFields
	return b
}
func (b *ConfigSyncCollectionBuilder) WithAttachmentsBucket(attachmentsBucket string) *ConfigSyncCollectionBuilder {
	b.collection.AttachmentsBucket = &attachmentsBucket
	return b
}
func (b *ConfigSyncCollectionBuilder) WithAttachmentFilenamesField(field string) *ConfigSyncCollectionBuilder {
	b.collection.AttachmentFilenamesField = &field
	return b
}
func (b *ConfigSyncCollectionBuilder) WithDisablePush(disablePush bool) *ConfigSyncCollectionBuilder {
	b.collection.DisablePush = &disablePush
	return b
}
func (b *ConfigSyncCollectionBuilder) WithDisablePull(disablePull bool) *ConfigSyncCollectionBuilder {
	b.collection.DisablePull = &disablePull
	return b
}
func (b *ConfigSyncCollectionBuilder) WithRpi() *ConfigSyncCollectionBuilder {
	b.collection.Rpi = boolRef(true)
	return b
}
func (b *ConfigSyncCollectionBuilder) WithSyncFilterSyncEverything() *ConfigSyncCollectionBuilder {
	if b.collection.SyncFilter == nil {
		b.collection.SyncFilter = &ConfigSyncFilter{}
	}
	b.collection.SyncFilter.SyncEverything = boolRef(true)
	return b
}
func (b *ConfigSyncCollectionBuilder) WithSyncFilterUseServiceOrderId() *ConfigSyncCollectionBuilder {
	if b.collection.SyncFilter == nil {
		b.collection.SyncFilter = &ConfigSyncFilter{}
	}
	b.collection.SyncFilter.UseServiceOrderId = boolRef(true)
	return b
}
func (b *ConfigSyncCollectionBuilder) WithSyncByLocalGlobalIds(syncByLocalGlobalIds bool) *ConfigSyncCollectionBuilder {
	b.collection.SyncByLocalGlobalIds = &syncByLocalGlobalIds
	return b
}
func (b *ConfigSyncCollectionBuilder) WithAndFilterList(andFilters ...ConfigSyncFilterValue) *ConfigSyncCollectionBuilder {
	if b.collection.SyncFilter == nil {
		b.collection.SyncFilter = &ConfigSyncFilter{}
	}
	b.collection.SyncFilter.AndFilters = andFilters
	return b
}
func (b *ConfigSyncCollectionBuilder) WithDisableMap() *ConfigSyncCollectionBuilder {
	b.collection.DisableMap = boolRef(true)
	return b
}
func (b *ConfigSyncCollectionBuilder) WithSkipWaitOnAdd() *ConfigSyncCollectionBuilder {
	b.collection.SkipWaitOnAdd = boolRef(true)
	return b
}
func (b *ConfigSyncCollectionBuilder) WithChild(collectionName string, referenceField string) *ConfigSyncCollectionBuilder {
	b.collection.Children = append(
		b.collection.Children,
		ConfigSyncCollectionChild{
			collectionName,
			referenceField,
		},
	)
	return b
}

/*
------------
Silviculture job started SMS
------------
*/
func (b *ConfigBuilder) WithFeatureNotification(collection,
	notificationTemplate string,
	emailNotifications MailReceivers,
	smsDisabled bool) *ConfigBuilder {
	b.config.FeatureNotifications = append(
		b.config.FeatureNotifications,
		ConfigFeatureNotification{
			Collection:           collection,
			NotificationTemplate: notificationTemplate,
			EmailNotifications:   emailNotifications,
			SmsDisabled:          smsDisabled,
		},
	)
	return b
}

/*
------------
Proximity alarms
------------
*/
func (b *ConfigBuilder) WithGradientProximityAlarmInsideWhere(name, collectionName, key string, values []string, gradientCutoffMeters float64) *ConfigBuilder {
	var where []ConfigProximityAlarmFilter
	for _, value := range values {
		where = append(where, ConfigProximityAlarmFilter{
			FieldName:   key,
			StringValue: value,
		})
	}
	b.config.ProximityAlarms = append(b.config.ProximityAlarms, ConfigProximityAlarm{
		Name: name,
		Source: ConfigProximityAlarmSource{
			Type:             ProximityAlarmSourceCollection,
			Identifier:       collectionName,
			WhereFilterAnyOf: where,
		},
		Setup: ConfigProximityAlarmSetup{
			GradientCutoffDistanceMeters: gradientCutoffMeters,
		},
	})
	return b
}

func (b *ConfigBuilder) WithGradientProximityAlarmOutsideCollection(name, collectionName string, gradientCutoffMeters float64, forExtent bool) *ConfigBuilder {
	b.config.ProximityAlarms = append(b.config.ProximityAlarms, ConfigProximityAlarm{
		Name: name,
		Source: ConfigProximityAlarmSource{
			Type:       ProximityAlarmSourceCollection,
			Identifier: collectionName,
		},
		Setup: ConfigProximityAlarmSetup{
			GradientCutoffDistanceMeters: gradientCutoffMeters,
			AlarmOutside:                 true,
			UsedToDetermineExtent:        forExtent,
		},
	})
	return b
}

func (b *ConfigBuilder) WithGradientProximityAlarmInsideVector(name, URL string, gradientCutoffMeters float64) *ConfigBuilder {
	b.config.ProximityAlarms = append(b.config.ProximityAlarms, ConfigProximityAlarm{
		Name: name,
		Source: ConfigProximityAlarmSource{
			Type:       ProximityAlarmSourceVector,
			Identifier: URL,
		},
		Setup: ConfigProximityAlarmSetup{
			GradientCutoffDistanceMeters: gradientCutoffMeters,
		},
	})
	return b
}

func (b *ConfigBuilder) WithGradientProximityAlarmInsideArcGIS(name, URL string, gradientCutoffMeters float64) *ConfigBuilder {
	b.config.ProximityAlarms = append(b.config.ProximityAlarms, ConfigProximityAlarm{
		Name: name,
		Source: ConfigProximityAlarmSource{
			Type:       ProximityAlarmSourceArcGIS,
			Identifier: URL,
		},
		Setup: ConfigProximityAlarmSetup{
			GradientCutoffDistanceMeters: gradientCutoffMeters,
		},
	})
	return b
}

/*------------
VsysFeatures
------------*/

func (b *ConfigBuilder) AddVsysFeatureForAllUsers(feature string) *ConfigBuilder {
	b.config.VsysFeatures = append(
		b.config.VsysFeatures,
		ConfigVsysFeature{
			feature,
			[]string{"*"},
		},
	)
	return b
}

/*------------
Verification
------------*/

func (b *ConfigBuilder) VerifyReportTemplates(allowProdInTest bool) {
	for _, report := range b.config.Reports {
		//test vs prod?
		if b.IsProd() {
			if strings.Contains(report.Template, "-test") {
				b.reporter.Warning("prod config points to test report: " + report.Template)
			}
		} else {
			if !strings.Contains(report.Template, "-test") && !allowProdInTest {
				b.reporter.Warning("test config does not point to test report: " + report.Template)
			}
		}

		var segments = strings.Split(report.Template, "/")
		for segments[0] != "rapporter" {
			segments = segments[1:]
		}

		if len(segments) < 3 { //should be rapporter/{firma}/{filnavn}, not rapporter/{filnavn}
			b.reporter.Warning("template not in separate folder: " + report.Template)
		}

		var shortPath = strings.Join(segments, "/")
		if _, err := os.Stat("./../" + shortPath); err != nil {
			if os.IsNotExist(err) {
				b.reporter.Warning("template does not exist: " + report.Template)
			}
		}
	}
}

func (b *ConfigBuilder) VerifySync() {

	//TODO: TODO! connection between sync and layers!!!

	for _, layer := range b.config.Map.Layers {
		var layerFoundInSync bool = false
		for _, sync := range b.config.Sync.Collections {
			if strings.ToLower(layer.Name) == strings.ToLower(sync.Name) {
				layerFoundInSync = true
			}
		}
		if !layerFoundInSync {
			//fmt.Println("⚠️ map.layer.name not found in sync: " + layer.Name)
		}
	}

	for _, sync := range b.config.Sync.Collections {
		var syncFoundInLayers bool = false
		for _, layer := range b.config.Map.Layers {
			if strings.ToLower(layer.Name) == strings.ToLower(sync.Name) {
				syncFoundInLayers = true
			}
		}
		if !syncFoundInLayers {
			//fmt.Println("⚠️ sync not found in map.layers: " + sync.Name)
		}
	}
}
