package main

func PopulatePlantingMottakskontrollForm(b *FormBuilder) {

	//DEFAULT!
	b.form.Alias = strRef("Mottakskontroll planting")
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      "Mottakskontroll planting",
			Language: "en",
			Value:    "Plants reception control",
		},
	)

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_type"
	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_type")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	di := b.AddDrawingInfo().
		WithTransparency(0)
	renderer := di.WithRenderer("_type")
	renderer.WithUniqueValueInfo("", "Mottakskontroll").
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAANsAAADmCAMAAABruQABAAAAkFBMVEX///8atzoAsRoTtjYAtCwAsiEAtC0AsycAsR0PtjQAsygAsRfb8d4HtTH2/Pfi9OVHwFu/5sTo9urQ7dQhuT+q3rGY2KF8z4ik3Kxexm6S1pzy+vPK6s89vlNYxGmA0Iy14rtwy34yvEuI05NoyXdjx3JPwmJBv1a75MCd2qau4LVuynyM1JbW79osu0fE6MlRKMR3AAAH3UlEQVR4nO1daVciSwyt3hfpFlABZRFlU8Tx//+71zI6ytjLDXA6dee8+11Piq5KbpJbKWP+h/W4nGwH98tZdzrdzB4Gb9rmnAmd+e7WiYMoDbMk8QokWRjFO22zTkZ/tI6DNEw8528E99q2nYZ51/WTH6v6De9R27oTMN5FadXCCmQP2gYejf7QDasXViDqaZt4JPKhm9WuzPEybRuPxCBuWJnj+CNtI49Cz/ObVuZ4obaVx2C8DhpXVpy2rbadR2AU1/jGr89GGADG3QhYmeO4HW1LxegFyEdznJCPkwxjaGUFtC2VIt80u8ePHXmlbasQfacxprHuyL7/k+qXw7vWtlWIcYYuzXEvtY0VYohuSCdgI1tjhIvskc20bZWii8W1Aom2qVLsUnRpF2yE5MpFl0Z32PoYhywQLrVtleIajmy/tE2Voou7/762rUI8oCzSuZho2yrECI5swbO2rUJMLtCl+S/atgrRx/lIV9tWKRzYRdKlo2vURXoRm4u8h6kWXe1nDpdH6Lx/B2aR7pO2rULkOB250bZVig2asqVDbVOlGKJUK1trmyrFFg3afIGtA1OtmC2w4X7Epev8wqWfiE5FAvMRvoLdE8pH6Krj5hKmWnR+BC/98PmRW9RHpgttU6V4RouRfHzkFQ3aXpJr2ypEDncQ6bJRPGjHdNqYBRq0fToB4RuaaSdTbVOlwJuj0VjbVike0cPmspV+8Ew7GmibKgXMkPnIfx8m/562qWL8goM2mzTGDOtF418I6II2fNhCuqCNHzY+rS4c2ei69Xhk48u0e+iO5Mu0YRqZbLRNFWMKHjYvZcu08ZyNTmFtJmjOFt1pmypFnoJci6+sZdZoZKPTs5oBWo28eNU2VQq4hUinZ8UlTdmttqViLMHSP19P28xRQsJXQ4YTGz5tDJzY8InHzQJMbPgav/h9Br7aT47e+eKr/cCd34xNYY3LtQgPG+z++Q6befx3D9sOdP8h32FD3T/ftShj4BYiXaEVZv+Ehw1l/4SRDa20EkY2WB1DGNlQKRrhYbsEiz+EORva1WY8bGg+SnjYUEJCWCBBCUm20jZUjhdMZ0FYjYRbv3zVSJODPcSI7Qp6gRlGkfmUaHCFhHFsJEqR+fpsMEVO6WSfxowwipzQDUXAa3Z8ChJYHsOnsTZmgIU2n06uVSRtGEXmu9Bg4CpyQHehwZg7bEfGbAMfDKwh4RMiG7SMwFhFQPsahFUEdEcyVhHAHUmoaYJ3JJ+AEN6RLmFiA+7IlE6ta9AdyZjYoDuS7/KoQXdkTHcPxaA8MqSbQGXQzIaSa4GZDSPXAnNtQv24MX1oRzIWkcFpdl5IWNcC65GMdS0zhuqRjHUt8AYR4Z1fg8qa+KZrFcihw0aojimwQrqIlKk21tdWfcPsajRcX0dB+rgaCHOQECFbau7//WHNyM/2zs5L/Fh0QfcBUVpouf95Nz58WNOLBYQW0v7ouP98kP58WFMiioauIqq4/7ugdEfh1ZoFsiM12P88qyreuKA/gUokCux/vA4q9xMqjIYS0va72vXv/WH5MZSQxq2/P72o928ZIrKFEtL2G22rJg6IXLJABDLtKwhnjRXgrLnWdoNw5NYfw1oCxe24yQNAkq3Wa//PiFVhk9TqFtiRressMNF+k/rvCfmBWick4AsLUb1IAplC23rr9woUktX3kl6AhDRpfXbMAzqIOq5xcRPkB4paJyToxdbaMACx//aVPxv4VdSg8n8gIxYhanNmzOD3Nf0qV/AKkC0VlTXiBT7Mq8pRkbH4sUaFZIeO7Cw2ZXnlBLkepdMfHcEPGlYcGeThKKVLNm/wS2QV+TfSs1caZteBR22Xk0pERaL2XDj89keps4N8pFrH/hpfWwmpRHyknmBrCQe44gv8naQgUVuxiTjAg0ARpg4/ATI+UlND8ga/s1wgOewtIY0NTQkh1p3+Y+n3ah5yYU/NR+6RwGzZOYzfyGO/yldjcUb5jq8PB1XIlUfQCliX8y17zhH/qn2nQcJMCgQfikekzqLa+91DtrYPqdIKakepKz/x9HSPfVVoiVDsRF+NMJJE7wLZdNKF/iTW1yK/yjbl+2xx6EtHNsgjJcxE8AtYodiS0GUcdkx92IoiHAigrdUG4KGJErRfRi4H/PQOjtSWO22CQh4Ie6YQXkmjQCOC1uUIlRAU8iDYdDsK7lSBsGkQSe+8H86u+3pnjQLtd0hrgcgMYNgS2j4wP+OmbF+yVY/8fJtSu47wE8IEtQb2vYi4PdemtPBxpXNtSt1aawXOsyntHEN4Hk9p6QCBc2zK0NJLbbLaeSmsvdMsalaVwyaKfIiTE1SLr1kuTtyU1u7IAp0TvUnrQmsJYL1hKfydtv11uDmlTmlP9acUJ/Euq3ekOalsYlcZoQTijs4f2NHYqMXRBWaCSSTHehOKcdbHbUqbSq3VuD/Km1jQ/AUATmc9BMtwhCPSb5rhCBPxiSOajSMmlUSzcZ6EKapvY2GrCrKlWU6R/8KNqOBFQEi+QyIWZXH/nxB8OL7HbODv5tnX12jCHH7H3vJ8tAzo7HgbhHZSYJeHQ7rDtgfymqXN5cg65M2sMiF8gPQ3ni7S2je2Q3dN6Ec+0Rt6cfn6kjBw7lg/2if68/tpHER+mCWJVyDJMj8Kgtkz8Sc7wOVkezdcrTfTaXf2shi9sX+wfwL/ATagdgzu5sUuAAAAAElFTkSuQmCC",
			"02118a46198679b7824f4459330dfde3",
			24.0,
			24.0,
		).
		WithStringValue("Mottakskontroll")

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")

	b.AddHiddenStringField("_type", "Type")
	b.AddHiddenStringField("_workTeamOrgNum", "Orgnum")
	b.AddHiddenStringField("_parentGlobalId", "Skogkultur global id")

	b.AddNullableStringField("comment", "Kommentar").
		WithAliasTranslation("en", "Comment")

	b.AddRequiredStringField("refNr", "Referansenummer").
		WithAliasTranslation("en", "Reference number")

	b.AddRequiredBooleanField("moist_Boolean", "Er plantene fuktige?").
		WithAliasTranslation("en", "The plants are moist?")

	b.AddRequiredBooleanField("green_Boolean", "Er plantene grønne?").
		WithAliasTranslation("en", "The plants are green?")

	b.AddRequiredBooleanField("notdamaged_Boolean", "Er plantene hele og ikke mekanisk skadde?").
		WithAliasTranslation("en", "The plants are whole and not mechanically damaged?")

	b.AddRequiredIntegerField("num_plants", "Antall planter").
		WithAliasTranslation("en", "Number of plants")

	b.AddRequiredStringField("location", "Sted").
		WithAliasTranslation("en", "Location")

	//fieldIds
	b.SetFieldIdSeries(0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)
	b.SetFieldIdSeries(
		12,
		"_workTeamOrgNum",
		"_parentGlobalId",
	)
	b.SetFieldIdSeries(
		5,
		"_type",
		"comment",
		"refNr",
		"moist_Boolean",
		"green_Boolean",
		"notdamaged_Boolean",
		"num_plants",
	)
	b.SetFieldIdSeries(
		14,
		"location",
	)

	//template
	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      "Mottakskontroll",
		Name:    "Mottakskontroll",
		Templates: []FormTypeTemplate{
			{
				Description: strRef(""),
				DrawingTool: strRef("esriFeatureEditToolPoint"),
				Name:        "Mottakskontroll",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{
						"_type": "Mottakskontroll",
					},
				},
			},
		},
	})

	b.WithAutoSortex()
}
