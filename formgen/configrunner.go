package main

import (
	"fmt"
	"slices"
)

func ConfigRunner(filename string, company *Company, reporter *Reporter, populator func(b *ConfigBuilder)) {
	file := NewConfigFile(filename)

	if company != nil && slices.Contains(company.ExcludeFiles, filename) {
		fmt.Println("-- Exclude file " + file.Filename())
		return
	}

	fmt.Println("-- Parse and write file " + file.Filename())

	//parse
	var file1 = file
	var config1 = file1.ParseConfig()
	var json1 = config1.Json()

	//generate
	var builder = NewConfigBuilder(file1, company, reporter)
	populator(builder)

	//compare
	var configGen = builder.Config()

	//compare
	if configGen != nil {
		//set the final form in the reporter
		reporter.SetFinalConfig(configGen)

		var jsonGen = configGen.Json()
		if json1 != jsonGen {
			reporter.Diff("Generated json not equal to content in file")
			reporter.DiffDebug(json1, jsonGen)
		}

		//write back to file1? = the real output!
		if true {
			file1.WriteConfig(configGen)
			var jsonGenWritten = file1.ParseConfig().Json()
			if json1 != jsonGenWritten {
				reporter.Diff("Generated json written to file not equal to existing json")
				reporter.DiffDebug(json1, jsonGenWritten)
			}
		}
	}
}
