package main

func PopulatePlantelagerTellingForm(b *FormBuilder) {
	//DEFAULT!
	b.form.Alias = strRef("PlantelagerTelling")
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      "PlantelagerTelling",
			Language: "en",
			Value:    "Plant storage count",
		},
	)

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "_type"

	b.form.EditFieldsInfo = nil
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 450493.12090000045,
		XMin: -305588.6796000004,
		YMax: 7210241.434,
		YMin: 6469673.3945,
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("_type")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenStringField("_globalId", "GlobalId")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")

	b.AddHiddenStringField("_type", "Type")
	b.AddHiddenStringField("_workTeamOrgNum", "Orgnum")
	b.AddHiddenStringField("_parentGlobalId", "Skogkultur oppdragsID")
	b.AddHiddenStringField("_currentLocation", "Aktuell posisjon")

	b.AddSection("Bruk minus (-) for å ta ut planter, positivt tall for å legge til").
		WithTranslation("en", "Use negative (-) values to remove plants. Positive number is adding plants.")

	b.AddRequiredIntegerField("delta", "Endring i antall planter").
		WithAliasTranslation("en", "Change in number of plants").
		WithNegativeNumbersAllowed()

	b.AddRequiredStringField("comment", "Merknad").
		WithAliasTranslation("en", "Comment")

	b.AddRequiredBooleanField("poor_plants_Boolean", "Dårlige planter").
		WithAliasTranslation("en", "Poor plants")

	b.AddHiddenStringField("_locationGlobalId", "Plantelager _globalId")
	b.AddHiddenStringField("_plantsGlobalId", "Plantelager planter _globalId")
	b.AddHiddenStringField("_modifiedByName", "Endret av (navn)")

	//fieldIds
	b.SetFieldIdSeries(
		0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
	)
	b.SetFieldIdSeries(5, "_type")
	b.SetFieldIdSeries(
		10,
		"_workTeamOrgNum",
		"_parentGlobalId",
	)
	b.SetFieldIdSeries(
		13,
		"_currentLocation",
	)
	b.SetFieldIdSeries(15, "delta")
	b.SetFieldIdSeries(20, "comment")
	b.SetFieldIdSeries(22, "poor_plants_Boolean")
	b.SetFieldIdSeries(23,
		"_locationGlobalId",
		"_plantsGlobalId",
		"_modifiedByName",
	)

	//template
	b.AddType(FormType{
		Domains: FormTypeDomains{},
		Id:      "PlantelagerTelling",
		Name:    "PlantelagerTelling",
		Templates: []FormTypeTemplate{
			{
				Description: strRef(""),
				DrawingTool: strRef("esriFeatureEditToolPoint"),
				Name:        "PlantelagerTelling",
				Prototype: FormTypeTemplatePrototype{
					Attributes: map[string]interface{}{
						"_type": "PlantelagerTelling",
					},
				},
			},
		},
	})

	b.WithAutoSortex()
}
