package main

func PopulateRegisterForm(b *FormBuilder) {
	//DEFAULT!
	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "Name"

	//TODO: do this with builder...
	b.form.EditFieldsInfo = nil
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "PartyId"
	b.form.HasAttachments = false
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = nil
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddUneditableRequiredStringField("PartyId", "PartyId")
	b.AddUneditableRequiredStringField("OrgNum", "OrgNum")
	b.AddUneditableRequiredIntegerField("OrgTypeNum", "OrgTypeNum")
	b.AddUneditableRequiredStringField("Name", "Name")
	b.AddUneditableRequiredStringField("Name2", "Name2")
	b.AddUneditableRequiredStringField("Address", "Address")
	b.AddUneditableRequiredStringField("ZipCode", "ZipCode")
	b.AddUneditableRequiredStringField("City", "City")
	b.AddUneditableRequiredStringField("EMail", "EMail")
	b.AddUneditableRequiredStringField("Fts", "Full text search")
	b.AddUneditableNullableStringField("CountryCode", "Country code")

	b.SetFieldIdSeries(0, "PartyId", "OrgNum", "OrgTypeNum", "Name", "Name2", "Address", "ZipCode", "City", "EMail", "Fts", "CountryCode")
	b.WithAutoSortex()
}
