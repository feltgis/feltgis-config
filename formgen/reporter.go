package main

import (
	"cmp"
	"fmt"
	"slices"
	"strings"
)

//Nice helper class that records success, warnings, errors, and diff messages, and prints out a nice
//repport after a run. Will also be used to "stop" stuff :-)

// USAGE:
func (r *Reporter) Success(message string) {
	r.currentFile.Successes = append(r.currentFile.Successes, message)
}
func (r *Reporter) Warning(message string) {
	r.currentFile.Warnings = append(r.currentFile.Warnings, message)
}
func (r *Reporter) Error(message string) {
	r.currentFile.Errors = append(r.currentFile.Errors, message)
}
func (r *Reporter) Diff(message string) {
	r.currentFile.Diff = append(r.currentFile.Diff, message)
}
func (r *Reporter) DiffDebug(a, b string) {
	aLines := strings.Split(a, "\n")
	bLines := strings.Split(b, "\n")
	for i, aLine := range aLines {
		if len(bLines) > i {
			bLine := bLines[i]
			if strings.Compare(aLine, bLine) != 0 {
				r.currentFile.Diff = append(r.currentFile.Diff, "-diff at line '"+aLine+"' vs '"+bLine+"'")
				return
			}
		} else {
			r.currentFile.Diff = append(r.currentFile.Diff, "-diff at line '"+aLine+"'")
			return
		}
	}
	if len(bLines) > len(aLines) {
		r.currentFile.Diff = append(r.currentFile.Diff, "-diff at line '"+bLines[len(aLines)]+"'")
		return
	}
}

// STUFF:
type Reporter struct {
	currentFile *ReporterFile
	files       []*ReporterFile
}

type ReporterFile struct {
	Filename string
	Company  *Company

	Successes []string //nice stuff will get ✅
	Warnings  []string //non-blocking stuff, will get ⚠️
	Errors    []string //blocking stuff, will get 🛑
	Diff      []string //stuff that has changed, will get 🟣

	//to run analytics at the end :-)
	FinalForm   *Form
	FinalConfig *Config
}

func (r *Reporter) SetCurrentFile(filename string, company *Company) {
	r.currentFile = &ReporterFile{
		Filename: filename,
		Company:  company,
	}
	r.files = append(r.files, r.currentFile)
}

func (r *Reporter) SetFinalForm(form *Form) {
	r.currentFile.FinalForm = form
}
func (r *Reporter) SetFinalConfig(config *Config) {
	r.currentFile.FinalConfig = config
}

func (r *Reporter) CurrentFileHasBlockingIssues() bool {
	return len(r.currentFile.Errors) > 0
}

func (r *Reporter) PrintReport() {

	reportCmp := func(a, b *ReporterFile) int {
		if len(a.Errors) != len(b.Errors) {
			return cmp.Compare(len(a.Errors), len(b.Errors))
		} else if len(a.Warnings) != len(b.Warnings) {
			return cmp.Compare(len(a.Warnings), len(b.Warnings))
		} else if len(a.Diff) != len(b.Diff) {
			return cmp.Compare(len(a.Diff), len(b.Diff))
		} else {
			return cmp.Compare(strings.ToLower(a.Filename), strings.ToLower(b.Filename))
		}
	}

	slices.SortFunc(r.files, reportCmp)

	inset := "  "

	for _, file := range r.files {
		if len(file.Errors) == 0 {
			fmt.Println("📄:" + file.Filename + ":✅ ")
		} else {
			fmt.Println("📄:" + file.Filename + ":🛑 ")
		}
		if len(file.Errors) == 0 {
			for _, row := range file.Successes {
				fmt.Println(inset + "✅ " + row)
			}
		}
		if len(file.Diff) > 0 {
			for _, row := range file.Diff {
				fmt.Println(inset + "🟣" + row)
			}
		}
		if len(file.Warnings) > 0 {
			for _, row := range file.Warnings {
				fmt.Println(inset + "⚠️" + row)
			}
		}
		if len(file.Errors) > 0 {
			for _, row := range file.Errors {
				fmt.Println(inset + "🛑" + row)
			}
		}
	}

	/*
		//all success!!!!
		for _, file := range r.files {
			if len(file.Errors) == 0 {
				fmt.Println("✅  " + file.Filename)
				for _, row := range file.Successes {
					fmt.Println("✅     " + row)
				}
			}
		}

		//all warnings!!!!
		for _, file := range r.files {
			if len(file.Warnings) > 0 {
				fmt.Println("⚠️ " + file.Filename)
				for _, row := range file.Warnings {
					fmt.Println("⚠️     " + row)
				}
			}
		}

		//all diff!!!!
		for _, file := range r.files {
			if len(file.Diff) > 0 {
				fmt.Println("🟣 " + file.Filename)
				for _, row := range file.Diff {
					fmt.Println("🟣     " + row)
				}
			}
		}

		//all errors!!!!
		for _, file := range r.files {
			if len(file.Errors) > 0 {
				fmt.Println("🛑 " + file.Filename)
				for _, row := range file.Errors {
					fmt.Println("🛑     " + row)
				}
			}
		}
	*/
}
