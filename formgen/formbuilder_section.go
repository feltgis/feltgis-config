package main

type FormSectionBuilder struct {
	section *FormSection
	form    *Form
}

func (b *FormBuilder) AddSection(name string) *FormSectionBuilder {
	if b.currentSection == nil {
		one := 1
		b.currentSection = &one
	} else {
		next := *b.currentSection + 1
		b.currentSection = &next
	}
	section := FormSection{
		Id:    *b.currentSection,
		Title: name,
	}
	b.form.Sections = append(
		b.form.Sections,
		&section,
	)
	return &FormSectionBuilder{
		&section,
		b.form,
	}
}

func (b *FormSectionBuilder) WithSubtitle(subtitle string) *FormSectionBuilder {
	b.section.Subtitle = subtitle
	return b
}

func (b *FormSectionBuilder) WithTranslation(language string, text string) *FormSectionBuilder {
	if language != "en" {
		panic("Only supported language is 'en', not " + language)
	}
	b.form.Translations = append(b.form.Translations,
		FormTranslation{
			Key:      b.section.Title,
			Language: language,
			Value:    text,
		},
	)
	return b
}
