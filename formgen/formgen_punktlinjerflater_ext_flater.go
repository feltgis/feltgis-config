package main

import "strings"

func populatePunktLinjerFlaterExtGroupAlmaForm_flater(b *FormBuilder, dib *FormDrawingInfoBuilder) {

	//file types
	isRegistreringFile := strings.Contains(b.file.Name, "Registreringer")
	isHenFile := strings.Contains(b.file.Name, "Hen")
	isMisFile := strings.Contains(b.file.Name, "Mis")

	//common
	b.form.GeometryType = "esriGeometryPolygon"
	b.form.Extent = FormExtent{
		SpatialReference: FormExtentSpatialReference{
			LatestWkid: 25833,
			Wkid:       25833,
		},
		XMax: 449078.5729,
		XMin: 22145.20600000024,
		YMax: 7182411.7896,
		YMin: 6488287.4629999995,
	}

	//renderer
	renderer := dib.WithRenderer(b.form.DisplayField)

	//default symbol
	if isMisFile {
		renderer.WithDefaultLabel("<all other values>")
		renderer.WithSolidOutlineDefaultSymbol(
			FormDrawingInfoColor(255, 255, 0, 255),
			FormDrawingInfoColor(197, 0, 255, 255),
			3,
			true,
		)
	}

	//uniqueValueInfos
	if isRegistreringFile || isHenFile {
		renderer.WithUniqueValueInfo("", "Annet").
			WithStringValue("Annet").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(168, 112, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "Døde trær").
			WithStringValue("Døde trær").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(0, 0, 0, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA1ZJREFUeJztW9ty7DAIIzP5/1/e89KczXp9QRJ24w566aQ12FEwNlBOe+NlZof58Pr56R1fyjGyl/ysOUvdrrnOyiTeCRgcQXq8KN+rN3dJnkf3cQ6HfU6gWpAKZk4vMeiHPczsdRq2HVdbkArEqhgD+W+BKwlZORdlVYgMsoWfANhCCBkIDIGjk8wzjhmPHAh3THU7Z0Ux+8XQF2QIQU9KBhAfpQWyX7k2gecFUSeP6I7AkI/aFmYXOpO8awy7DZXt211nSaDyldEXZE/I+7MCj64hH7UtrHxl5AWpe5f519jT7XVVw7lqFqh8ZfTEm3VCegjy7LYhHy0fuFO00UKPICZCqfKx20XazL/VL9QICjOQGQQy277E7LRX2IHUIhCJCp4ScdzhIQhxVc0xjAXuEnEs8eXsFv5rEQcNNZnwtIgDhTwPu4V3iTimfwjWAp8UcXgxJS+o+MAnRBxeRJzsVex4kb7AWHVNVkIvH/jboVzk1Yglb8jHnUBlUR5EfxCEFLUg39QRmQ9cCeYqxewuKB+4C3lmWngXRp7Z9xZekeWNkI/OTrfWMtRbWqCyKPWqwPhgpejlGQsdIuyiSvkLjBt4mhsZ8hF5D/wN8tRcouwGIglUfWi0Dx7pDIlORnVhBFEFqZ58SCkSHAtV5VRE+NBeibF8ZiptzNgmdouFI4tFIQmF1QSqW7ympxyz9C6JEoj6oJbs9dwq9rTko3OJqsuB/sU3qlrW0ueRXZEtgvhAQzmVAFb2riM6MatY/HH/B8sVp9bsgnlNt3fOSzfER/aJvIEayMss+0Ss+BucbMg+kc+xcJ0l+0Q+ARvIbpEIW19GZCBkn4iI7BPpz1HO87W+7BPpY8hH9olgsl/rzD6Rvq4hH9kn8n5uJXGhjHRUWt4rN+uEjMpeD/nIPhF/hFLlY7eLtJmWjbn/LsRAsk+En8PMsk+EyS9+IPtERGSfiIjsExGRfSIisk9ERPaJiNjxIn2BseqarITsE+ljyEf2ifRlhjqyT0ScJ/tE+uPv81WRfSL9tQz1Zp/IWH9XNvtE+hjykX0i/rFVZJ+IOH/2iYzHQlU5FRE+tFdiLJ+ZShsztondYuHIYlFIQiH7RPj1mFn2iaguJ/tEKoD4yD4RzeKPfz5Sg64bvDHmAAAAAElFTkSuQmCC",
				"6206a384e15dc0369234300bbdd1ad81",
				60,
				60,
			)
		renderer.WithUniqueValueInfo("", "Edellauvskog").
			WithStringValue("Edellauvskog").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(255, 170, 0, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA2ZJREFUeJztW1GS5SAIxKrcMWd8p8z+rFOuT0nTECdu0V+TGQTTQRQZDvmL6yNXOaUIgOsjl4gIKt+PY8bW8U/Z7HWjto7eCGqAQTmlROhB0b+XZrsnD9FdTikHOpn68iODq8DYRImxftjKx2FZjqs9yAuLVzEO8uOBKwlZaYv1KssYeAm/AYyHPB12zATe7WSIHCNv2RBaPB12jl4x+8WsL8gQYt0pGVj5OGbC9dkaQ2a6vPIryOuB8PG1hNmJPklelWGXoWf53s3zsAhrsL4gu0O2z5b5jezf6UL4+FrCnq9seUH23IXOUdONhirE1pcHer6ydcd7aodECEK8C+FjGAN3yjZm0AhiMpQZH1sdpEXwpV5/HhEU6SDhBDLLvv/d09dekRvSkEBLVvCWjKMFQpAlVGkyZg/cJeNYFcupJfy/ZRweuC4T3pZxWBFhh1rCu2QcKz4E5YFvyjhQPHUvSMfAN2QcKCJ29hm2O0hXMF49GuvF9D7wt1O5yKMRSx7CxzEStk4KQfQHsZDiLchrOsLuA1eCOUoxq8t0H7gLeSK+9C6SPJFuCa+45Y0YH307PZsLovcfD/RMyntUYGKwp+iFyJo2EXZSswkyYeBtYQThI+wc+Bvkee8SI8JAGIHeGBodg+90RmUnal3YgqiClDY+qhRpkTVV5byIiKFaibF/ZiptjKyGrXLhyGJR1IXCUgK9S3ykp5dZfZY0EWiNQbOx9XlW7JmNj75L9Iac62P4F9+oatlMHzJ2xW2RlQ9TKuclgB3b6oi+mPV4fDml/PyD5Ypd6+mC+Ug3arPqtvKRfSLN3GbjZrrLmX0i0v6NuWzIPpFGlqmzZJ9IA8ZBtspE2PqyZYwV2SfiRPaJKDZ6O6P5ZZ+IAoSP7BMxjB3NM/tEFF0IH9knIvP3Rmxln8hAvp/faExF9omAGcqMj60O0iL4Uq8/e6/+75B9IqSNiuwTMXp0j+wTcSL7RJzIPhEnsk/EiewTcSL7RJzY7iBdwXj1aKwX2SeiAOEj+0SUMYiO7BNx2sk+EUW+tTeTyz4RZS6I3uwTudF/Nzb7RBQgfGSfCCg7Q/aJOO1nn8iNrKkq50VEDNVKjP0zU2ljZDVslQtHFouiLhSyT4ScT0X2iTg88fpkn8gXrHxkn4jD48sp5Q+c/6grfjtDnAAAAABJRU5ErkJggg==",
				"815ddb447dbc533326f2ff120528b0e2",
				60,
				60,
			)
		renderer.WithUniqueValueInfo("", "Flerbruksholt").
			WithStringValue("Flerbruksholt").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(230, 230, 0, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA2dJREFUeJztW2Fy7iAIJDM5hfc/W67R/nl2fH5KloXY2GF/NS2C2SCKlFP+4brkqxQ5BMB1yZeICCrfj2PG1vFP2ex1o7bO3ghqgEEpckToQdG/l2a7Jw/RXYocJzqZ+vIjg6vA2ESJsX7YysdpWY6rPcgLi1cxDvLjgSsJWWmL9SrLGHgJvwGMhzwddswE3u1kiBwjb9kQWjwdds5eMfvFrC/IEGLdKRlY+ThnwvXZGkNmurzyK8jrgfDxsYTZiT5JXpVhl6Fn+d7N87QIa7C+ILtDts+W+Y3s3+lC+PhYwp6vbHlB9tyFzlHTjYYqxNaHB3q+snXHe2qHRAhCvAvhYxgDd8o2ZtAIYjKUGR9bHaRF8KVefx4RFOkg4QQyy77/3dPXXpEb0pBAS1bwloyjBUKQJVRpMmYP3CXjWBXLqSX81zIOD1yXCW/LOKyIsEMt4V0yjhUfgvLAN2UcKJ66F6Rj4BsyDhQRO/sM2x2kKxivHo31Ynof+NupXOTRiCUP4eMcCVsnhSD6g1hI8RbkNR1h94ErwRylmNVlug/chTwRX3oXSZ5It4RX3PJGjI++nZ7NBdH7nwd6JuU9KjAx2FP0QmRNmwg7qdkEmTDwtjCC8BF2DvwN8rx3iRFhIIxAbwyNjsF3OqOyE7UubEFUQUobH1WKtMiaqnJeRMRQrcTYPzOVNkZWw1a5cGSxKOpCYSmB3iU+0tPLrD5Lmgi0xqDZ2Po8K/bMxkffJXpDznUZ/sU3qlo204eMXXFbZOXDlMp5CWDHtjqiL2Y9Hl+KHD//YLli13q6YD7Sjdqsuq18ZJ9IM7fZuJnuUrJPRNq/MZcN2SfSyDJ1luwTacA4yFaZCFtftoyxIvtEnMg+EcVGb2c0v+wTUYDwkX0ihrGjeWafiKIL4SP7RGT+3oit7BMZyPfzG42pyD4RMEOZ8bHVQVoEX+r1Z+/V/x2yT4S0UZF9IkaP7pF9Ik5kn4gT2SfiRPaJOJF9Ik5kn4gT2x2kKxivHo31IvtEFCB8ZJ+IMgbRkX0iTjvZJ6LIt/ZmctknoswF0Zt9Ijf678Zmn4gChI/sEwFlZ8g+Eaf97BO5kTVV5byIiKFaibF/ZiptjKyGrXLhyGJR1IVC9omQ86nIPhGHJ15X9ol8wMpH9ok4PL4UOb4B0Gaa8whlZ1oAAAAASUVORK5CYII=",
				"89a724671a8fd3e960597d6075612f6f",
				60,
				60,
			)
		renderer.WithUniqueValueInfo("", "Jerpebiotop").
			WithStringValue("Jerpebiotop").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(0, 0, 0, 255),
				1,
			)
		renderer.WithUniqueValueInfo("", "Kantsone").
			WithStringValue("Kantsone").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 230, 169, 255),
				FormDrawingInfoColor(0, 230, 169, 255),
				1,
			)
		renderer.WithUniqueValueInfo("", "Kulturminner").
			WithStringValue("Kulturminner").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA9FJREFUaIG1mtuV0DAMRM05qYLWqI/WaAN+liXrSDN35OCvxA9JlkYPO7nWWmv9+PV7/fz+bU1atfbel9A+kSOgca211pdJ3aJuI9Xce5+j1a27z1NK3N/Vhj/mXi1jRYxYQwmnkFHJong7WQq+F9b6TsBBuHsnc990t4LOZaHdWah7pvBTG+vWk+eO5m3OV3hTi6tG4UcDHVE0kfE25yoZK0FIoOvmpdCewBysf0ZvJ4gLTn/7pqhRFkzh3ax/Rm8niOtPg1JizRN43+b92zSFeAo5R/MkaA1d6mrhSHOsYpAUDvv4JIBBl7qQ1WhQo4rb32nQSwKseL/ahQTeU5jv785COxqTYqh47/N0xyBJZW8cICqeSTEkfdoxnFRaCinJ6YxWb+79M2XRCE2ChurfIeog3T2foKf06YrJzmjK9A2Y73RocNzmPTftrPw/i4+0NqByPnx6JxwGhXKc+vX+TGoDokRj8b44qQSmxUWS6khsCI+OzuIskL2RjkhMSC409vmBLCyQqUJBCVuNkbTnaHZyVmsLl+sPHF0frZ72MUqL0Jzc8HyJ3tSnT5Ry7zdWGNcIEbypr04Y0fRHIZ7Ell2h7YFjuhGywUm+nli4q/xKeDsB3EZcdFbw7mh1Mqh+oNj+PK18XTFw1jU51PJJlYPg7YSYaJ1CO60AO5krxXyBd7qhCbxpcEo2mNDZ2oUmJ/2ppab+e6Agdkc2EeouSHJISBXf0RfG1Pfep8VHtaaDpRI6KV6U8j5PWZMN0jWT8UpoWrxUMhcWv9rJpLk1k3SYyEEgX8zRNyeTsq+as+dKSjNpLtDd2vOrpVtIAk0HudMgpRRL0+LqvloqxqTfjSV0J75tssj7Xy3J2El6UvNdRfcIZM6XO0JurKI78Wkin+Pz0eronUCnYtr1JZZVfJ3SzLiH91v+TjMBOd2lMrYVWXKuntTrNBOcBkzgKjW8k3NwwKyd59YeROpqHbsCJo1aglg3OTa6oFa8s5RFC4TilmKUBYhrqDHjDvV/ZIpwWnmd3oO5DbmUWCCN/wWcCnNjUj7va5QM9HYFBkb+F7BrE8W9Ae+k2TKUQJRYd5qvjeB2rchA7/n0IIpGdBJ6huZ7Pp3SSFFCEAZr8vyzDmlEoNTnyVpH81GckFSgnt2YEuj05iS0fH9HRrScpo9J4OsaSXcNnecHvLe1XglBq7EJcgCinpf9pxBPovU0a4gNyf6HTydMJzBWJ7c3qsCOn/TpU1jfW2rdfYwGS6VwsRd/np5EbtWn1lSbocVIcP/mz9OTyO0EpOOuTW5w1lp/AMHnscBjAqUZAAAAAElFTkSuQmCC",
				"9a5572bcb4e2254906bf823650d47c4b",
				45.75,
				45.75,
			)
		renderer.WithUniqueValueInfo("", "Livsløpstrær").
			WithStringValue("Livsløpstrær").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(58, 172, 106, 255),
				FormDrawingInfoColor(56, 168, 0, 255),
				1,
			)
		renderer.WithUniqueValueInfo("", "Myr").
			WithStringValue("Myr").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(178, 178, 178, 255),
				1.5,
				"iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAcZJREFUeJztmktuAjEQBY00B8khOUYOmZvANkgD7vGn3EZV22T6DbVwC/yO+9/jUT7w+3O7vftb7dkRz0cZmXM2q9fT8ekfWl6IfJ7M6fV09L6AxFA0hKIhTkWPWFLUEux9l1Lal/qVz3EqevaSopZglNb3ufKcRweEoiEUDfEiundJXV06sxbm6m+JZ7yIzrQEZ73Lqm+JHh0QioZQNMSRZelkyh61AP/P6/qZtDZ8xtxV2b0zPTogFA2haIiq6NYlsONdYWtOJKsqOuNiyZYTyfLogFA0hKIhhtwZjlhI9LfMq3np7wxXl1dm5nlnmBBFQygaovozqW3S2AzbpINybJNugqIhFA1hm7TYJsWwTfpFKBpC0RC2SRtm2SaF5tomTYyiIRQNYZs0ONs2KZRtm3QTFA2haAjbpANyIlm2SaEsjw4IRUMoGsI2aTAv/Z3h6vLKzDzvDBOiaAhFQ9gmDc6yTQrl2CbdBEVDKBrCNmmxTYphm/SLUDSEoiFskzbMsk0KzbVNmhhFQygawjZpcLZtUijbNukmKBpC0RC2SQfkRLJsk0JZHh0QioZQNIRt0mBe+jvD1eWVmXneGSZE0RCKhngCTYaAXs0QchYAAAAASUVORK5CYII=",
				"6a24bfd452db0cccc3c14b39c1a7e1b2",
				67.5,
				67.5,
			)
		renderer.WithUniqueValueInfo("", "Rovfuglreir").
			WithStringValue("Rovfuglreir").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 255, 197, 255),
				FormDrawingInfoColor(230, 76, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "Stier og Friluftsanlegg").
			WithStringValue("Stier og Friluftsanlegg").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(230, 0, 169, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAA81JREFUaIG1mtmR3DAMROkqReHEHKMTcxr2105pKaD7NSjzS7wAEGgc5My11lp/1u+/P9evH2vQqr33sYT2iRwJjWutte6Luk3dQaq19zFHq9t3X6eUuPfVgb/WXh1jRYxYQwmnkFHJong7WSq+F9X6TsBBuOuTtW+6W0XnctDuLNR9U/ipg3X7yXdH877mG7ypxVWj8KOBjiiayHhfc1WMlSAk0HXrUmhPYE72P6K3E8QFp6+xKWqUBVN4d/sf0dsJ4sbToJRY8wTe93WfQ1OIp5BzNE+C1tSlrg6ONMcqBknhsM9PAhh1qYtYjQY1qri9Ty2UBFjVv7qNBN5TmO99Z6EdjUkxVPXbPN0xSFLZGxeIimdSDEmfdgwnlZZCSnI7o9Wb639SFo3QJGio8R2iDtLd9wl6Sp/uBD+tkiqa0zYJjvu6x6Gdlf9n8ZHWBlTOh0/vhNOgUM1Tv96/SW1AlOgs3hYnlcC0uEhSHYkN6dXRWRwFsjfSEYkJyYPGvj6RBQUyVSgoYas5kvYczU7Oam/lcu2Foxuj1dM+R2kRmpMXnm/Rm/r0iVLu484K0xohgjf11Qkjmv4oxJPYsiu0vXBMD0IOOMnXEwt3lV8JbyeAO4iLzgreHa1OBjVOFNvep5WvKwbOui6HOj6pchC8nRATrVNopxVgJ3OlmG/wTg80gTcNTskBEzp7u8jiZDy11NR/TxSE3sgmQt0FSS4JqeI7+sqY8t37tPio9nSwVEInxYtS3ueWNTkg3TOZr4SmxUslc2Xxq1tMmtszSYeJHATy1Rr5cjIp+6o1e66kNJPmAt29PX61dBtJoOkgdxqklGJpWvwceuK7atzNJXQnvu2yyOu/WpK5k/Sk1jsLPwKZ8+WOkJur6E58msjn+Hy1Mnon0KmYdmOJZRVfpzQ3b+H9lr/TTEBud6mMbUWW3Ksn9TrNBKcBk7hKCe/kHpww69a5vSeRutqHnoBJo5Yg1k2ujS6oVX2UsmiBUL1STLIAcQ0159yh/B+ZIpxWXqfvYO5ALiVWSMP/Ak6FuTOpvvc9Sgb6ukIDI/4XsGsTxb0B76TZMpRAlFh3mq+d4G6vykCv+fQkiiZ0EnqO5ms+ndJIUUIQRmvy+Gcd0ohAqc+TvY7mozghqUB9uzkl0OnLSWr59o2MaDlNH5PA1zWS7jo6jx/w3tZ6JQStxibIIYh6PPafQjyJ1tOsoQ6kxh8+nTCdwFjlzTeqwI6f9OlTWN9bat19jgZLpXB1FnufnkRuNab2VIehxUjy/mbv05PI7QSk865NXnDWWusfre88JDFf6IwAAAAASUVORK5CYII=",
				"f4c49b0edf023cdc856e242fe1839ecf",
				45.75,
				45.75,
			)
		renderer.WithUniqueValueInfo("", "Sump").
			WithStringValue("Sump").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(115, 223, 255, 255),
				0.4,
				"iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAl9JREFUaIHNWlGywyAITGZykB6yx+ghe5O8L9+YVIwLu+j+dZogC0QBOTYH3t/zrH9/XvuufLd+B1mrhQN9IZssusYTYMJeRAzFhJuwV+FZRAsgwncvIZhNtGCIcFY4lnWU8h8JR7yKKOJ5Ft3ht+3Bwy1F2NZXRY9lRJPwap6NoDbi0Des+KYY0dP65p8ixiScRZKxFhIp0xIPLz6vfR+V1TLkwcxTLbA9W97zfBZUDyNeZBjXI+OfsHfDQDE74zqyFJhNtMAV0k/eXYVcC+F6eNv0R9i0engGWTZSzmHEW0yyLVkQ4frQ91Qq7Oc9MmAPj4YwK/wj67WQllr2FI8aB8nkfghHug6q4sADa80L4Vrh9/c8GVb2ADUQsrdQQjpCltXxGH1PWg+vmHFRPNyqUVcg2wrz4/5n7+EeWJscuncgsrft1gCwXsz2lrIp8RPSvYYYEyrZUKZ1t2Z2YdBr3XhkdxMPddiOEPXqgmRqaallC5nt2QKoxRO916mhiqhwpmUp7c25lXdHI7JlIa3sjkRkp00AKL9XRDbc8Rh5Tt2mQfWp8UhYVYh75ERkFkiPpRWbCDLCzGNH2peOJO4rha4Fs8WjQtaYg7Xm9AkA64JblYmZDQA2epVMtEpC1pXu0igBZJzBi5+ORySUVrtGHbpb8hpgNbKWDlC1NKsy8mBoEu+uYKuTaZFQdhpZMi7DpS2FlbvnXZEsOdNGDwuYI4iWnEviscJmUkOxScLDpUpkFxnps5aqzoe8xeOF95hj5dthwrOGU9M7How8eeSijn0k0kYPRyzOKg4ikfQHCmoR0Q1u4KMAAAAASUVORK5CYII=",
				"a7a10dfd8575e27a49659632a3fb4cb0",
				45,
				45,
			)
		renderer.WithUniqueValueInfo("", "Tiurleiker").
			WithStringValue("Tiurleiker").
			WithOutlinedImageSymbol(
				FormDrawingInfoColor(110, 110, 110, 255),
				2,
				"iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAu5JREFUSInNkVFIk2sYx3+vX9LYxIaSB5QWfNNkwscuJE5woiKHnCCCIVhhXhgku5A4mRheHBjixYZ0o7ALERERCddh6s1YuwphpSA7x+ibUjffhcGG6RJOE3J7u2nSsc3YYUH/u/f/f5/39zzPe4ISlc1mLwCXs9msFUBRlEVFUV4eV3OiVMj/UcmQdDrdFQwGb2xvb1sBuru7d4HyTrKzs/Orx+Ox5c+6rvuj0WjA5XJlhRCZskCqqqpeA+fz57m5Obq6uh5KKV8DT8sC2d3dPXPUy2QyfwghHhSr+bk+XkpZKYT4tL6+3nY029jYsEgpT3/t7e/vW00mU7okSC6XuyKlfOfz+b7J7Hb7J6Ahl8tN7u3tfQAwDONUKpX6q66uLnwsREp5fnZ2FoDR0VH74OBgbaF7iUTCPDMzY7dYLNra2tpZAJ/Px8LCwm3AUhBycHBwLplMEovFnoXDYSvA5uYm7e3taU3Tvrk/PDwMcB2go6MDgLGxsVR1dfVJKLIuRVHqV1dXH7ndbuvX/tDQkLWlpSUNWAvVtba2pv1+fwJAVdUxKWWyKKTcKgiRUjoCgcDvR/1IJEIkEik4BcDU1NSeqqpvACoqKp4cC8lkMiej0WjJHY+Pj9s6OzsPvjRqBhBCfCy2rrdut/tjKBQyFwrj8XjC4/E4AFZWVg79yclJtra27gI4HI50fX39EvC8IMRsNr8fGBh4nEwm/4zFYv/J+vv7/3U6nfMTExMXAZxO5wXAks/D4XAeeMPr9b4qCim3CkKEEC+AF7quX/V6vU6A+fn5KoDa2toMsKJp2t8AhmGYFhcXfwkGg43Ly8uHb0xPTzf29PTcB6aPnaSpqelmIBC4AzAyMmKPx+O4XC5DCPEe+AfAZrPFent7f2toaLgG3MuDDMPAMIzGopPkVVlZuQX4v7ONFBACQrqu3+rr6zsMampqnnwXUqpUVb3T1tZ2CUDTNKO5uXmj7JBiKivEZDItAUs/FFJMnwHJbRUba+BXKgAAAABJRU5ErkJggg==",
				"ae497240dcdbd1189fd90e8f69294785",
				19.35,
				19.35,
			)
		renderer.WithUniqueValueInfo("", "FSC nøkkelbiotop (Gjennomhogst)").
			WithStringValue("FSC nøkkelbiotop (Gjennomhogst)").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 170, 0, 255),
				FormDrawingInfoColor(115, 76, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "FSC nøkkelbiotop (Friutvikling/urørt)").
			WithStringValue("FSC nøkkelbiotop (Friutvikling/urørt)").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 0, 0, 255),
				FormDrawingInfoColor(115, 76, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "Rovfuglbuffer - automatisk").
			WithStringValue("Rovfuglbuffer - automatisk").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(255, 170, 0, 255),
				1.5,
			)
		renderer.WithUniqueValueInfo("", "Rovfuglbuffer").
			WithStringValue("Rovfuglbuffer").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(255, 170, 0, 255),
				1.5,
			)
		renderer.WithUniqueValueInfo("", "SensitiveArter - hensynsområde").
			WithStringValue("SensitiveArter - hensynsområde").
			WithSolidOutlineSolidSymbol(
				FormDrawingInfoColor(0, 0, 0, 0),
				FormDrawingInfoColor(168, 0, 132, 255),
				1.5,
			)
		renderer.WithUniqueValueInfo("", "Naturtype").
			WithStringValue("Naturtype").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(56, 168, 0, 255),
				FormDrawingInfoColor(56, 168, 0, 255),
				2,
			)
		renderer.WithUniqueValueInfo("", "Hensynsområde oppdrag").
			WithStringValue("Hensynsområde oppdrag").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(56, 168, 0, 255),
				FormDrawingInfoColor(56, 168, 0, 255),
				2,
			)
	}

	// Types from Allma Mis Points, added in the GM-Silviculture integration
	// to avoid adding yet another sync db for GMMisPoint
	if isMisFile || isRegistreringFile {
		renderer.WithUniqueValueInfo("", "Forvaltningsfigur").
			WithStringValue("0").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(255, 255, 0, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Urørt").
			WithStringValue("1").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(230, 0, 0, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("10").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("11").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("12").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Urørt").
			WithStringValue("13").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(230, 0, 0, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("2").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("3").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("4").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("5").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("6").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("7").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("8").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
		renderer.WithUniqueValueInfo("", "Gjennomhogst").
			WithStringValue("9").
			WithSolidOutlineBackwardDiagonalSymbol(
				FormDrawingInfoColor(0, 92, 230, 255),
				FormDrawingInfoColor(197, 0, 255, 255),
				3,
			)
	}

}
