package main

import (
	"fmt"
	"slices"
	"strconv"
)

func FormRunner(filename string, prod bool, company *Company, reporter *Reporter, populator func(b *FormBuilder)) {

	file := NewFormFile(filename, prod)

	if company != nil && slices.Contains(company.ExcludeFiles, filename) {
		fmt.Println("-- Exclude file " + file.Filename())
		return
	}

	fmt.Println("-- Parse and write file " + file.Filename())

	//parse
	var file1 = file
	var form1 = file1.ParseForm()
	var json1 = form1.Json()

	//generate
	var builder = NewFormBuilder(file1, company, reporter)
	populator(builder)

	//do verification here!
	builder.VerifyOrderAndSortex()
	builder.VerifyFullOrNoTranslations()

	//compare
	var formGen = builder.Form()
	if formGen != nil {
		//set the final form in the reporter
		reporter.SetFinalForm(formGen)

		//make sure fieldIds are kept!
		if !form1.VerifyFieldIdsAreKept(formGen) {
			reporter.Error("Field ids are not consistant with previous version")
			for _, field1 := range form1.Fields {
				for _, field2 := range formGen.Fields {
					if field1.Name == field2.Name {
						if field1.FieldId != field2.FieldId {
							reporter.Error(field1.Name + " " + strconv.Itoa(field1.FieldId) + " -> " + strconv.Itoa(field2.FieldId))
						}
					}
				}
			}
		}
		if !formGen.VerifyFieldIdsAreSet() {
			reporter.Error("Not all field ids are set in " + formGen.Name)
			for _, field := range formGen.Fields {
				if field.FieldId == -1 {
					reporter.Error("-" + field.Name + " " + strconv.Itoa(field.FieldId))
				}
			}
		}

		//compare
		var jsonGen = formGen.Json()
		if json1 != jsonGen {
			reporter.Diff("Generated json not equal to content in file")
			reporter.DiffDebug(json1, jsonGen)
		}

		//write back to file1? = the real output!
		if true {
			file1.WriteForm(formGen)
			var jsonGenWritten = file1.ParseForm().Json()
			if json1 != jsonGenWritten {
				reporter.Diff("Generated json written to file not equal to existing json")
				reporter.DiffDebug(json1, jsonGenWritten)
			}
		}
	}
}
