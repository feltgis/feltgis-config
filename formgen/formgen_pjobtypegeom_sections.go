package main

import "log"

func populatePJobTypeGeomFormInfoSection(b *FormBuilder, formType geomFormTypeType) {
	switch formType {
	case geomFormTypeForhandsrydding:
	case geomFormTypeGrofterensk:
	case geomFormTypeMarkberedning:
		b.AddSection("[Info om markberedning](https://skogkurs.no/wp-content/uploads/markberedning.pdf)\\n\\n").
			WithTranslation("en", "[Soil Scarification Information](https://skogkurs.no/wp-content/uploads/markberedning.pdf)\\n\\n")
	case geomFormTypePlanting:
		b.AddSection("[Info om planting](https://skogkurs.no/wp-content/uploads/planting.pdf)\n\n").
			WithTranslation("en", "[Information on Planting](https://skogkurs.no/wp-content/uploads/planting.pdf)\n\n")
	case geomFormTypeSaaing:
	case geomFormTypeUngskogpleie:
		b.AddSection("[Info om ungskogpleie](https://skogkurs.no/wp-content/uploads/ungskogpleie.pdf)\n\n").
			WithTranslation("en", "[Information on Young Forest Tending](https://skogkurs.no/wp-content/uploads/ungskogpleie.pdf)\n\n")
	case geomFormTypeGjodsling:
	case geomFormTypeSproyting:
	case geomFormTypeGravearbeid:
	case geomFormTypeSporskaderetting:
	case geomFormTypeStammekvisting:
	}
}

func populatePJobTypeGeomFormGeneralSection(b *FormBuilder, formType geomFormTypeType) {

	//logic: Plantesalg/PlantesalgPlanting not for roest owners
	typeIsPlantesalgPlantingLogic := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("PlantesalgPlanting"),
	}
	typeNotPlantesalgLogic := FormFieldLogicField{
		Operator:              "!=",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("Plantesalg"),
	}
	jobTypeIsSuppleringsplanting := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "job_type",
		OtherFieldStringValue: strRef("Suppleringsplanting"),
	}
	hiddenField := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "job_type",
		OtherFieldStringValue: strRef("never this value"),
	}

	//--------------------------------
	//General
	switch formType {
	case geomFormTypeSaaing, geomFormTypeSkogskade:
		b.AddSection("") //TODO: remove!
	default:
		b.AddSection("Generelt").
			WithTranslation("en", "General")
	}

	//jobtype
	if formType == geomFormTypePlanting {
		if b.IsForestOwner() {
			b.AddMultipleChoiceField("job_type", "Oppdragstype").
				WithAliasTranslation("en", "Assignment type").
				WithCodedOption("Nyplanting").
				WithCodedOption("Suppleringsplanting")
		} else {
			b.AddMultipleChoiceField("job_type", "Oppdragstype").
				WithAliasTranslation("en", "Assignment type").
				WithCodedOption("Nyplanting").
				WithCodedOption("Suppleringsplanting").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}
		b.SetFieldIdSeries(
			46,
			"job_type",
		)
	}

	//bestand_str, teignavn,  plannr, teignr, area, elevation, bonitet
	if formType != geomFormTypePlanting || b.IsForestOwner() {

		if b.IsForestOwner() {
			b.AddNullableStringField("bestand_str", "Bestandsnummer").
				WithAliasTranslation("en", "Stand number")
		} else {
			b.AddNullableIntegerField("bestand", "Bestandsnummer").
				WithAliasTranslation("en", "Stand number")
		}

		if !b.IsForestOwner() {
			b.AddNullableStringField("teignavn", "Teignavn").
				WithAliasTranslation("en", "Lot name")
		}

		b.AddNullableIntegerField("plannr", "Plannummer").
			WithAliasTranslation("en", "Plan number")

		b.AddNullableIntegerField("teignr", "Teignummer").
			WithAliasTranslation("en", "Lot number")
		b.AddUneditableNullableDoubleField("area", "Areal").
			WithAliasTranslation("en", "Area")
		b.AddUneditableNullableIntegerField("elevation", "Høyde over havet").
			WithAliasTranslation("en", "Height above sea level")

		switch formType {
		case geomFormTypeGrofterensk:
			//none
		default:
			if b.IsNortømmer() && formType == geomFormTypeUngskogpleie {
				b.AddMultipleChoiceField("bonitet", "Bonitet").
					WithAliasTranslation("en", "Site quality").
					WithCodedOption("6").
					WithCodedOption("8").
					WithCodedOption("11").
					WithCodedOption("14").
					WithCodedOption("17").
					WithCodedOption("20").
					WithCodedOption("23").
					WithCodedOptionWithCustomName("26", "26 eller høyere")
			} else {
				b.AddNullableMultipleChoiceField("bonitet", "Bonitet").
					WithAliasTranslation("en", "Site quality").
					WithCodedOption("6").
					WithCodedOption("8").
					WithCodedOption("11").
					WithCodedOption("14").
					WithCodedOption("17").
					WithCodedOption("20").
					WithCodedOption("23").
					WithCodedOptionWithCustomName("26", "26 eller høyere")
			}
		}

	} else {
		b.AddNullableIntegerField("bestand", "Bestandsnummer").
			WithAliasTranslation("en", "Stand number").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
		b.AddNullableStringField("teignavn", "Teignavn").
			WithAliasTranslation("en", "Lot name").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)

		b.AddNullableIntegerField("plannr", "Plannummer").
			WithAliasTranslation("en", "Plan number").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)

		b.AddNullableIntegerField("teignr", "Teignummer").
			WithAliasTranslation("en", "Lot number").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
		b.AddUneditableNullableDoubleField("area", "Areal").
			WithAliasTranslation("en", "Area").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
		b.AddUneditableNullableIntegerField("elevation", "Høyde over havet").
			WithAliasTranslation("en", "Height above sea level").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
		b.AddMultipleChoiceField("bonitet", "Bonitet").
			WithAliasTranslation("en", "Site quality").
			WithCodedOption("6").
			WithCodedOption("8").
			WithCodedOption("11").
			WithCodedOption("14").
			WithCodedOption("17").
			WithCodedOption("20").
			WithCodedOption("23").
			WithCodedOptionWithCustomName("26", "26 eller høyere").
			WithOptionTranslation("en", "26 or higher").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
	}

	switch formType {
	case geomFormTypeForhandsrydding:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(9, "teignr", "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "bestand", "teignavn", "teignr")
		}
		b.SetFieldIdSeries(
			13,
			"area", //13
			"elevation",
			"bonitet",
		)
		b.SetFieldIdSeries(56, "plannr")
	case geomFormTypeGrofterensk:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(9, "teignr", "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "bestand", "teignavn", "teignr")
		}
		b.SetFieldIdSeries(11, "area")
		b.SetFieldIdSeries(14, "elevation")
		b.SetFieldIdSeries(56, "plannr")
	case geomFormTypeMarkberedning:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(9, "teignr", "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "bestand", "teignavn", "teignr")
		}
		b.SetFieldIdSeries(11, "area")
		b.SetFieldIdSeries(12, "bonitet")
		b.SetFieldIdSeries(14, "elevation")
		b.SetFieldIdSeries(46, "plannr")
	case geomFormTypePlanting:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(9, "teignr")
			b.SetFieldIdSeries(15, "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "bestand", "teignavn", "teignr")
		}
		b.SetFieldIdSeries(
			11,
			"area",
			"bonitet",
		)
		b.SetFieldIdSeries(14, "elevation")
		b.SetFieldIdSeries(56, "plannr")
	case geomFormTypeSaaing, geomFormTypeSkogskade:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(8, "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "teignavn")
			b.SetFieldIdSeries(15, "bestand")
		}
		b.SetFieldIdSeries(9, "bonitet")
		b.SetFieldIdSeries(
			12,
			"plannr",
			"teignr",
		)
		b.SetFieldIdSeries(
			301,
			"area",
			"elevation",
		)
	case
		geomFormTypeUngskogpleie,
		geomFormTypeInfrastruktur,
		geomFormTypeGjodsling,
		geomFormTypeSproyting,
		geomFormTypeGravearbeid,
		geomFormTypeSporskaderetting,
		geomFormTypeStammekvisting:
		if b.IsForestOwner() {
			b.SetFieldIdSeries(9, "teignr", "bestand_str")
		} else {
			b.SetFieldIdSeries(7, "bestand", "teignavn", "teignr")
		}
		b.SetFieldIdSeries(56, "plannr")
		b.SetFieldIdSeries(11, "area")
		b.SetFieldIdSeries(14, "elevation")
		b.SetFieldIdSeries(57, "bonitet")
	default:
		log.Fatal("Unknown formType")
	}

	//estimated_area
	switch formType {
	case geomFormTypePlanting:
		if b.IsForestOwner() {
			b.AddRequiredIntegerField("estimated_area", "Anslått pantbart areal").
				WithAliasTranslation("en", "Estimated mortgageable area")
			b.SetFieldIdSeries(79, "estimated_area")
		}
	}

	//plant specific
	if formType == geomFormTypePlanting {
		if b.IsForestOwner() {
			b.AddRequiredBooleanField("soil_preparation_Boolean", "Markberedt").
				WithAliasTranslation("en", "Soil prepared")
			b.AddRequiredBooleanField("plants_denser_Boolean", "Tettere planting").
				WithAliasTranslation("en", "Denser planting")
			b.AddRequiredIntegerField("plants_per_da", "Antall planter pr daa").
				WithAliasTranslation("en", "Number of plants per decare").
				WithNotes("Du kan søke om tilskudd til tettere skogplanting som klimatiltak ved nyplanting, nyplanting etter markberedning og suppleringsplaning\n\n[Vilkår og detaljer for støtteordningen på Landbruksdirektoratets nettsider](https://www.landbruksdirektoratet.no/nb/skogbruk/ordninger-for-skogbruk/tilskudd-til-tettere-skogplanting-som-klimatiltak?openStep=a2051e73-7f33-475c-84c6-933b57ac52d5-0)")
		} else {
			b.AddRequiredBooleanField("soil_preparation_Boolean", "Markberedt").
				WithAliasTranslation("en", "Soil prepared").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
			b.AddRequiredBooleanField("plants_denser_Boolean", "Tettere planting").
				WithAliasTranslation("en", "Denser planting").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
			b.AddRequiredIntegerField("plants_per_da", "Antall planter pr daa").
				WithAliasTranslation("en", "Number of plants per decare").
				WithNotes("Du kan søke om tilskudd til tettere skogplanting som klimatiltak ved nyplanting, nyplanting etter markberedning og suppleringsplaning\n\n[Vilkår og detaljer for støtteordningen på Landbruksdirektoratets nettsider](https://www.landbruksdirektoratet.no/nb/skogbruk/ordninger-for-skogbruk/tilskudd-til-tettere-skogplanting-som-klimatiltak?openStep=a2051e73-7f33-475c-84c6-933b57ac52d5-0)").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}

		b.SetFieldIdSeries(
			17,
			"soil_preparation_Boolean",
			"plants_denser_Boolean",
			"plants_per_da",
		)

		if b.IsForestOwner() {
			//todo
		} else {
			b.AddNullableIntegerField("dead_plants_perc", "Prosent døde planter som skal suppleres (feks 20%)").
				WithAliasTranslation("en", "Percentage of dead plants to be replaced (e.g., 20%)").
				WithLogic().
				WithSetValue(FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						Field: &FormFieldLogicField{
							Operator:              "!=",
							OtherFieldName:        "job_type",
							OtherFieldStringValue: strRef("Suppleringsplanting"),
						},
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						IntValue: intRef(0),
					},
				}).
				WithMaxIntValueValidation(100).
				WithVisibleIfField(jobTypeIsSuppleringsplanting)

			b.AddMultipleChoiceField("hoydelag", "Høydelag").
				WithAliasTranslation("en", "Elevation level").
				WithCodedOptionWithCustomName("1", "Høydelag 1: 0-149 moh").
				WithCodedOptionWithCustomName("2", "Høydelag 2: 150-249 moh").
				WithCodedOptionWithCustomName("3", "Høydelag 3: 250-349 moh").
				WithCodedOptionWithCustomName("4", "Høydelag 4: 350-449 moh").
				WithCodedOptionWithCustomName("5", "Høydelag 5: 450-549 moh").
				WithCodedOptionWithCustomName("6", "Høydelag 6: 550-649 moh").
				WithCodedOptionWithCustomName("7", "Høydelag 7: 650-749 moh").
				WithCodedOptionWithCustomName("8", "Høydelag 8: 750-849 moh").
				WithCodedOptionWithCustomName("9", "Høydelag 9: 850-949 moh").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)

			b.SetFieldIdSeries(43, "hoydelag")
			b.SetFieldIdSeries(51, "dead_plants_perc")
		}
	}

	//planned_area / gram_per_daa
	switch formType {
	case geomFormTypeSaaing:
		b.AddRequiredIntegerField("planned_area", "Planlagt såareal i daa.").
			WithAliasTranslation("en", "Planned sowing area in decares")
		b.AddRequiredIntegerField("gram_per_daa", "Gram pr. daa").
			WithAliasTranslation("en", "Grams per decare")
		b.SetFieldIdSeries(
			10,
			"planned_area",
			"gram_per_daa",
		)
	case geomFormTypeSkogskade:
		b.AddRequiredIntegerField("planned_area", "Planlagt utbedringsareal i daa.").
			WithAliasTranslation("en", "Planned improvement area in decares")
		b.AddRequiredIntegerField("gram_per_daa", "Gram pr. daa").
			WithAliasTranslation("en", "Grams per decare")
		b.SetFieldIdSeries(
			10,
			"planned_area",
			"gram_per_daa",
		)
	}

	//markslag
	switch formType {
	case geomFormTypePlanting:
		b.AddMultipleChoiceField("markslag", "Markslag").
			WithAliasTranslation("en", "Soil type").
			WithCodedOption("Barskog").
			WithCodedOption("Lauvskog").
			WithCodedOption("Snaumark").
			WithCodedOption("Fulldyrket jord").
			WithCodedOption("Innmarksbeite").
			WithCodedOption("Overflatedyrket jord").
			WithCodedOption("Blandingsskog").
			WithCodedOption("Myr").
			WithLogic().
			WithVisibleIfField(typeNotPlantesalgLogic)
		b.SetFieldIdSeries(
			49,
			"markslag",
		)
	case geomFormTypeSaaing, geomFormTypeSkogskade:
		b.AddMultipleChoiceField("markslag", "Markslag").
			WithAliasTranslation("en", "Soil type").
			WithCodedOption("Barskog").
			WithCodedOption("Lauvskog").
			WithCodedOption("Snaumark").
			WithCodedOption("Fulldyrket jord").
			WithCodedOption("Innmarksbeite").
			WithCodedOption("Overflatedyrket jord").
			WithCodedOption("Blandingsskog").
			WithCodedOption("Myr")
		b.SetFieldIdSeries(
			14,
			"markslag",
		)
	}

	//price model + price
	if b.IsForestOwner() {
		// Forst owners does not use price model, but add the field since it is part of structs used for reporting
		switch formType {
		case geomFormTypePlanting:
			b.AddMultipleChoiceField("price_model", "Prismodell for arbeid").
				WithAliasTranslation("en", "Work pricing model").
				WithCodedOption("Per plante").
				WithCodedOption("Timespris").
				WithLogic().
				WithSetValue(FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						Field: &FormFieldLogicField{
							Operator:              "==",
							OtherFieldName:        "job_type",
							OtherFieldStringValue: strRef("Nyplanting"),
						},
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						StringValue: strRef("Per plante"),
					},
				}).
				WithVisibleIfField(hiddenField)
			b.SetFieldIdSeries(47, "price_model")
		}
	} else {
		switch formType {
		case geomFormTypeForhandsrydding:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per dekar").
				WithCodedOption("Timespris")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(11, "price_model")
			b.SetFieldIdSeries(12, "price")
		case geomFormTypeGrofterensk:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per meter").
				WithCodedOption("Timespris")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(15, "price_model")
			b.SetFieldIdSeries(16, "price")
		case geomFormTypeMarkberedning:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per dekar").
				WithCodedOption("Timespris")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(15, "price_model")
			b.SetFieldIdSeries(16, "price")
		case geomFormTypePlanting:
			b.AddMultipleChoiceField("price_model", "Prismodell for arbeid").
				WithAliasTranslation("en", "Work pricing model").
				WithCodedOption("Per plante").
				WithCodedOption("Timespris").
				WithLogic().
				WithSetValue(FormFieldLogicSetValue{
					IfValueIs: FormFieldLogicIfValueIs{
						Field: &FormFieldLogicField{
							Operator:              "==",
							OtherFieldName:        "job_type",
							OtherFieldStringValue: strRef("Nyplanting"),
						},
					},
					ThenSetValue: FormFieldLogicSetValueThenSetValue{
						StringValue: strRef("Per plante"),
					},
				}).
				WithVisibleIfField(jobTypeIsSuppleringsplanting)
			b.SetFieldIdSeries(47, "price_model")
		case geomFormTypeUngskogpleie:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per dekar").
				WithCodedOption("Timespris")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(
				21,
				"price_model",
				"price",
			)
		case geomFormTypeSaaing, geomFormTypeSkogskade, geomFormTypeInfrastruktur:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per dekar").
				WithCodedOption("Timespris")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(
				400,
				"price_model",
				"price",
			)
		case geomFormTypeGjodsling:
			b.AddMultipleChoiceField("price_model", "Prismodell").
				WithAliasTranslation("en", "Price model").
				WithCodedOption("Per dekar").
				WithCodedOption("Per tonn")
			b.AddRequiredIntegerField("price", "Pris").
				WithAliasTranslation("en", "Price")
			b.SetFieldIdSeries(
				21,
				"price_model",
				"price",
			)
		case geomFormTypeSproyting, geomFormTypeGravearbeid, geomFormTypeSporskaderetting, geomFormTypeStammekvisting:
			//TODO:
		default:
			log.Fatal("Unknown formType")
		}
	}

	//machine_type,machine_type_comment,type_mb
	switch formType {
	case geomFormTypeMarkberedning:
		b.AddMultipleChoiceField("machine_type", "Maskintype").
			WithAliasTranslation("en", "Machine type").
			WithSuggestedOption("Gravemaskin").
			WithSuggestedOption("Lassbærer med skålharv").
			WithSuggestedOption("Lassbærer med hauglegger").
			WithSuggestedOption("Annen")
		b.AddNullableStringField("machine_type_comment", "Maskintype, kommentar").
			WithAliasTranslation("en", "Machine type, comment")
		b.AddMultipleChoiceField("type_mb", "Type markberedning").
			WithAliasTranslation("en", "Type of soil preparation").
			WithCodedOption("Planting").                  // was "Til planting"
			WithCodedOption("Såing/naturlig foryngelse"). // was "Til naturlig foryngelse"
			WithCodedOption("Kombinert").                 // was "Annen"
			// WithCodedOption("Med såing").
			WithMultipleChoiceSeparator(",")
		b.SetFieldIdSeries(
			36,
			"machine_type",
			"type_mb",
		)
		b.SetFieldIdSeries(
			44,
			"machine_type_comment",
		)
	}

	//forestowner_covers_cost_f, hour_price_to_carry_plant, work_price_Decimal2
	switch formType {
	case geomFormTypePlanting:
		if b.IsForestOwner() {

		} else {
			b.AddRequiredBooleanField("forestowner_covers_cost_for_300_m_Boolean", "Kostnad for bæring av planter over 300 meter dekkes av skogeier").
				WithAliasTranslation("en", "Cost for carrying plants over 300 meters is covered by the forest owner").
				WithLogic().
				WithVisibleIfField(typeIsPlantesalgPlantingLogic)
			b.AddRequiredIntegerField("hour_price_to_carry_plants", "Timespris bæring av planter").
				WithAliasTranslation("en", "Hourly rate for carrying plants").
				WithLogic().
				WithVisibleIf(FormFieldLogicIfValueIs{
					Field: &FormFieldLogicField{
						Operator:           "==",
						OtherFieldIntValue: intRef(1),
						OtherFieldName:     "forestowner_covers_cost_for_300_m_Boolean",
					},
				})
			b.AddRequiredDecimalField("work_price_Decimal2", "Enhetspris, arbeid (eks. mva)", 2).
				WithAliasTranslation("en", "Unit price, work (excluding VAT)").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
			b.SetFieldIdSeries(600, "forestowner_covers_cost_for_300_m_Boolean", "hour_price_to_carry_plants")
			b.SetFieldIdSeries(48, "work_price_Decimal2")
		}
	}

	//age, desired_plants
	switch formType {
	case geomFormTypeUngskogpleie:
		if b.IsForestOwner() {
			b.AddNullableIntegerField("age", "Alder").
				WithAliasTranslation("en", "Age")
			b.AddNullableIntegerField("desired_plants", "Ønsket treantall").
				WithAliasTranslation("en", "Desired number of trees")
			b.SetFieldIdSeries(59, "age")
			b.SetFieldIdSeries(60, "desired_plants")
		}
	}

	//tree_kind, trees_before, height_before_Decimal1,
	switch formType {
	case geomFormTypeUngskogpleie:
		if b.IsForestOwner() {

		} else {
			b.AddMultipleChoiceField("tree_kind", "Nåværende treslag").
				WithAliasTranslation("en", "Current tree species").
				WithCodedOption("Gran").
				WithCodedOption("Furu").
				WithCodedOption("Lauv").
				WithMultipleChoiceSeparator(",")
			b.AddRequiredIntegerField("trees_before", "Antall trær per dekar før ungskogpleie").
				WithAliasTranslation("en", "Number of trees per decare before young forest tending")
			b.AddRequiredDecimalField("height_before_Decimal1", "Gjennomsnittlig trehøyde før ungskogpleie", 1).
				WithAliasTranslation("en", "Average tree height before young forest tending")

			b.SetFieldIdSeries(10, "tree_kind")
			b.SetFieldIdSeries(
				12,
				"trees_before",
				"height_before_Decimal1",
			)
		}
	}

	//tiltak_skogskade
	switch formType {
	case geomFormTypeSkogskade:
		b.AddMultipleChoiceField("tiltak_skogskade", "Tiltak skogskade").
			WithAliasTranslation("en", "Forest damage measure").
			WithCodedOption("Barkbillefeller").
			WithCodedOption("Råtebehandl. Stubber").
			WithCodedOption("Andre Tiltak")
		b.SetFieldIdSeries(200, "tiltak_skogskade")
	}

	//tiltak_infrastruktur
	switch formType {
	case geomFormTypeInfrastruktur:
		b.AddMultipleChoiceField("tiltak_infrastruktur", "Tiltak infrastruktur").
			WithAliasTranslation("en", "Infrastructure Measures").
			WithCodedOption("Sporpuss").
			WithCodedOption("Vedlikehold vei").
			WithCodedOption("Skilting").
			WithCodedOption("Kantklipp").
			WithCodedOption("Transport")
		b.SetFieldIdSeries(200, "tiltak_infrastruktur")
	}
}
func populatePJobTypeGeomFormØnsketTreslagsfordelingSection(b *FormBuilder, formType geomFormTypeType) {

	//--------------------------------
	//Section Ønsket treslagsfordeling
	switch formType {
	case geomFormTypePlanting:
		if b.IsForestOwner() {
			b.AddSection("Ønsket treslagsfordeling").
				WithTranslation("en", "Desired tree species distribution")

			b.AddRequiredIntegerField("perc_gran", "Andel gran").
				WithAliasTranslation("en", "Proportion of spruce")
			b.AddRequiredIntegerField("perc_furu", "Andel furu").
				WithAliasTranslation("en", "Proportion of pine")
			b.AddRequiredIntegerField("perc_lauv", "Andel lauv").
				WithAliasTranslation("en", "Proportion of deciduous trees")
			b.AddUneditableNullableIntegerField("num_plants", "Beregnet antall planter").
				WithAliasTranslation("en", "Estimated number of plants")

			b.SetFieldIdSeries(80, "perc_gran", "perc_furu", "perc_lauv")
			b.SetFieldIdSeries(42, "num_plants")
		}
	case geomFormTypeSaaing:
		b.AddSection("Ønsket treslagsfordeling").
			WithTranslation("en", "Desired tree species distribution")

		b.AddRequiredIntegerField("precent_spruce", "Prosent gran").
			WithAliasTranslation("en", "Proportion of spruce")
		b.AddRequiredIntegerField("precent_pine", "Prosent furu").
			WithAliasTranslation("en", "Proportion of pine")
		b.AddRequiredIntegerField("precent_leav", "Prosent lauv").
			WithAliasTranslation("en", "Proportion of deciduous trees")
		b.SetFieldIdSeries(
			100,
			"precent_spruce",
			"precent_pine",
		)
		b.SetFieldIdSeries(
			103,
			"precent_leav",
		)
	}

}

func populatePJobTypeGeomFormArbeidsinstruksSection(b *FormBuilder, formType geomFormTypeType) {

	//TODO: no arbeidsinstruks for sååing? : Såing and sporutbedring are new types, so let us rather add these if needed later
	switch formType {
	case geomFormTypeSaaing, geomFormTypeSkogskade:
		return
	}

	//logic (not for forestowner)
	typeIsPlantesalgLogic := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("Plantesalg"),
	}

	//--------------------------------
	//Section Arbeidsinstruks
	b.AddSection("Arbeidsinstruks").
		WithTranslation("en", "Work order")

	//trees_after, lauv_dist
	switch formType {
	case geomFormTypeUngskogpleie:
		if b.IsForestOwner() {

		} else {
			b.AddRequiredIntegerField("trees_after", "Antall trær per dekar etter ungskogpleie").
				WithAliasTranslation("en", "Number of trees per decare after young forest tending")
			b.AddNullableStringField("lauv_dist", "Skal løv stå spredt i bestand eller i klynger").
				WithAliasTranslation("en", "Should deciduous trees be spread out in the stand or in clusters")

			b.AddNullableStringField("beitetraer", "Behandling av beitetrær").
				WithAliasTranslation("en", "Treatment of browse trees")

			b.AddMultipleChoiceField("plant_pri", "Prioritert treslag etter ungskogpleie").
				WithAliasTranslation("en", "Prioritized tree species after young forest tending").
				WithCodedOption("Gran").
				WithCodedOption("Lauv").
				WithCodedOption("Furu").
				WithMultipleChoiceSeparator(",")

			b.AddNullableStringField("prio_comment", "Kommentar prioritert treslag etter ungskogpleie").
				WithAliasTranslation("en", "Comment on prioritized tree species after young forest tending")

			b.SetFieldIdSeries(
				15,
				"trees_after",
				"prio_comment",
				"lauv_dist",
				"beitetraer",
			)
			b.SetFieldIdSeries(
				20,
				"plant_pri",
			)
		}
	}

	//delivery_place
	switch formType {
	case geomFormTypePlanting:
		if b.IsForestOwner() {
		} else {
			b.AddNullableStringField("delivery_place", "Hentested/leveringssted planter").
				WithAliasTranslation("en", "Pickup/delivery location for plants").
				WithLogic().
				WithVisibleIfField(typeIsPlantesalgLogic)
			b.SetFieldIdSeries(22, "delivery_place")
		}
	}

	//start
	switch formType {
	case geomFormTypeForhandsrydding, geomFormTypeGrofterensk, geomFormTypeMarkberedning, geomFormTypePlanting, geomFormTypeUngskogpleie, geomFormTypeInfrastruktur, geomFormTypeGjodsling, geomFormTypeSproyting, geomFormTypeGravearbeid, geomFormTypeSporskaderetting, geomFormTypeStammekvisting:
		if b.IsForestOwner() {

		} else {
			b.AddMultipleChoiceField("start", "Ønsket starttidspunkt").
				WithAliasTranslation("en", "Desired start time").
				WithCodedOption("Høsten 2024").
				WithCodedOption("Våren 2025").
				WithCodedOption("Høsten 2025").
				WithCodedOption("Våren 2026").
				WithCodedOption("Høsten 2026").
				WithCodedOption("Våren 2027").
				WithCodedOption("Høsten 2027").
				WithCodedOption("Våren 2028")
			b.SetFieldIdSeries(
				23,
				"start",
			)
		}
	default:
		log.Fatal("Unknown formType")
	}

	//road dist
	if b.IsForestOwner() {

	} else {
		switch formType {
		case geomFormTypeMarkberedning:
			b.AddNullableIntegerField("road_dist", "Avstand inn til hogstfelt fra avlastingsplass").
				WithAliasTranslation("en", "Distance to the clear-cut area from unloading point")
			b.SetFieldIdSeries(24, "road_dist")
		case geomFormTypeGjodsling:
		default:
			b.AddRequiredIntegerField("road_dist", "Avstand fra parkeringsplass").
				WithAliasTranslation("en", "Distance from parking lot")
			b.SetFieldIdSeries(24, "road_dist")
		}
		b.AddNullableStringField("keys", "Bomveg kommentar").
			WithAliasTranslation("en", "Toll road comment")

		b.SetFieldIdSeries(25, "keys")
	}

	//num_plants (non-forest owner?)
	switch formType {
	case geomFormTypePlanting:
		if b.IsForestOwner() {

		} else {
			b.AddRequiredIntegerField("num_plants", "Antall planter").
				WithAliasTranslation("en", "Number of plants").
				WithLogic().
				WithVisibleIfField(typeIsPlantesalgLogic)
			b.SetFieldIdSeries(42, "num_plants")
		}
	}

	//comment
	if b.IsForestOwner() {
		b.AddNullableStringField("comment", "Beskrivelse av tiltaket").
			WithAliasTranslation("en", "Description of the measure").
			WithLength(2048)
	} else {
		b.AddNullableStringField("comment", "Kommentarer til tiltaket").
			WithAliasTranslation("en", "Comments on the measure").
			WithLength(2048)
	}
	switch formType {
	case geomFormTypeForhandsrydding, geomFormTypeGrofterensk, geomFormTypeMarkberedning, geomFormTypePlanting, geomFormTypeUngskogpleie:
		b.SetFieldIdSeries(26, "comment")
	case geomFormTypeSaaing, geomFormTypeSkogskade, geomFormTypeInfrastruktur, geomFormTypeGjodsling, geomFormTypeSproyting, geomFormTypeGravearbeid, geomFormTypeSporskaderetting, geomFormTypeStammekvisting:
		b.SetFieldIdSeries(300, "comment")
	default:
		log.Fatal("Unknown formType")
	}

	//dist
	switch formType {
	case geomFormTypeGrofterensk:
		b.AddRequiredIntegerField("dist", "Avstand inn til hogstfelt fra avlastingsplass").
			WithAliasTranslation("en", "Distance to the clear-cut area from unloading point")
		b.SetFieldIdSeries(36, "dist")
	}

	//grofterensk, type_mb_comment
	switch formType {
	case geomFormTypeMarkberedning:
		b.AddMultipleChoiceField("grofterensk", "Grøfterensk utføres sammen med markberedning").
			WithAliasTranslation("en", "Ditch cleaning performed together with soil preparation").
			WithSuggestedOption("Ja").
			WithSuggestedOption("Nei").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:              "==",
				OtherFieldName:        "machine_type",
				OtherFieldStringValue: strRef("Gravemaskin"),
			})
		b.AddNullableStringField("type_mb_comment", "Type markberedning, kommentar").
			WithAliasTranslation("en", "Type of soil preparation, comment")
		b.SetFieldIdSeries(
			42,
			"grofterensk",
			"type_mb_comment",
		)
	}
}

func populatePJobTypeGeomFormMiljoSection(b *FormBuilder, formType geomFormTypeType) {
	if formType == geomFormTypeSkogskade {
		return //for now :-)
	}

	b.AddSection("Miljø").
		WithTranslation("en", "Environment")

	switch formType {
	case geomFormTypePlanting:

		//not for forest owners
		typeNotPlantesalgLogic := FormFieldLogicField{
			Operator:              "!=",
			OtherFieldName:        "_type",
			OtherFieldStringValue: strRef("Plantesalg"),
		}

		if b.IsForestOwner() {
			b.AddRequiredBooleanField("bio_Boolean", "Berører tiltaket biologisk viktig område?").
				WithAliasTranslation("en", "Does the measure affect a biologically important area?")
		} else {
			b.AddRequiredBooleanField("bio_Boolean", "Berører tiltaket biologisk viktig område?").
				WithAliasTranslation("en", "Does the measure affect a biologically important area?").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}
		b.AddNullableStringField("bioComment", "Biologisk, kommentar").
			WithAliasTranslation("en", "Biological, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "bio_Boolean",
				OtherFieldIntValue: intRef(1),
			})
		if b.IsForestOwner() {
			b.AddRequiredBooleanField("herit_Boolean", "Berører tiltaket kjent kulturminne?").
				WithAliasTranslation("en", "Does the measure affect a known cultural heritage site?")
		} else {
			b.AddRequiredBooleanField("herit_Boolean", "Berører tiltaket kjent kulturminne?").
				WithAliasTranslation("en", "Does the measure affect a known cultural heritage site?").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}
		b.AddNullableStringField("heritComment", "Kulturminne, kommentar").
			WithAliasTranslation("en", "Cultural heritage, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "herit_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		if b.IsForestOwner() {
			b.AddRequiredBooleanField("recreational_Boolean", "Berører tiltaket viktige friluftsverdier?").
				WithAliasTranslation("en", "Does the measure affect important outdoor recreation values?")
		} else {
			b.AddRequiredBooleanField("recreational_Boolean", "Berører tiltaket viktige friluftsverdier?").
				WithAliasTranslation("en", "Does the measure affect important outdoor recreation values?").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}

		b.AddNullableStringField("recreationalComment", "Friluftsverdier, kommentar").
			WithAliasTranslation("en", "Outdoor recreation values, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "recreational_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		if b.IsForestOwner() {
			b.AddRequiredBooleanField("bufferZone_Boolean", "Kantsone?").
				WithAliasTranslation("en", "Riparian zone?")
		} else {
			b.AddRequiredBooleanField("bufferZone_Boolean", "Kantsone?").
				WithAliasTranslation("en", "Riparian zone?").
				WithLogic().
				WithVisibleIfField(typeNotPlantesalgLogic)
		}
		b.AddNullableStringField("bufferZoneComment", "Kantsone, kommentar").
			WithAliasTranslation("en", "Riparian zone, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "bufferZone_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		b.SetFieldIdSeries(
			27,
			"bio_Boolean",
			"bioComment",
			"herit_Boolean",
			"heritComment", //30
			"recreational_Boolean",
			"recreationalComment",
			"bufferZone_Boolean",
			"bufferZoneComment",
		)
	default:
		b.AddRequiredBooleanField("bio_Boolean", "Berører tiltaket biologisk viktig område?").
			WithAliasTranslation("en", "Does the measure affect a biologically important area?")
		b.AddNullableStringField("bioComment", "Biologisk, kommentar").
			WithAliasTranslation("en", "Biological, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "bio_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		b.AddRequiredBooleanField("herit_Boolean", "Berører tiltaket kjent kulturminne?").
			WithAliasTranslation("en", "Does the measure affect a known cultural heritage site?")
		b.AddNullableStringField("heritComment", "Kulturminne, kommentar").
			WithAliasTranslation("en", "Cultural heritage, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "herit_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		b.AddRequiredBooleanField("recreational_Boolean", "Berører tiltaket viktige friluftsverdier?").
			WithAliasTranslation("en", "Does the measure affect important outdoor recreation values?")
		b.AddNullableStringField("recreationalComment", "Friluftsverdier, kommentar").
			WithAliasTranslation("en", "Outdoor recreation values, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "recreational_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		b.AddRequiredBooleanField("bufferZone_Boolean", "Kantsone?").
			WithAliasTranslation("en", "Riparian zone?")
		b.AddNullableStringField("bufferZoneComment", "Kantsone, kommentar").
			WithAliasTranslation("en", "Riparian zone, comment").
			WithLogic().
			WithVisibleIfField(FormFieldLogicField{
				Operator:           "==",
				OtherFieldName:     "bufferZone_Boolean",
				OtherFieldIntValue: intRef(1),
			})

		//switch in switch, only small part different between all but planting
		switch formType {
		case geomFormTypeSaaing:
			b.SetFieldIdSeries(
				200,
				"bio_Boolean",
				"bioComment",
				"herit_Boolean",
				"heritComment",
				"recreational_Boolean",
				"recreationalComment",
				"bufferZone_Boolean",
				"bufferZoneComment",
			)
		default:
			b.SetFieldIdSeries(
				27,
				"bio_Boolean",
				"bioComment",
				"herit_Boolean",
				"heritComment",
				"recreational_Boolean",
				"recreationalComment",
				"bufferZone_Boolean",
				"bufferZoneComment",
			)
		}
	}
}

func populatePJobTypeGeomFormForestOwnerCommentSection(b *FormBuilder, formType geomFormTypeType) {

	if b.IsForestOwner() {

	} else {
		b.AddSection("")
		b.AddNullableStringField("forestOwnerComment", "Kommentar til avtale med skogeier").
			WithAliasTranslation("en", "Comment on agreement with forest owner")
		b.SetFieldIdSeries(
			55,
			"forestOwnerComment",
		)
	}
}
