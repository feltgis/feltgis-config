package main

func PopulatePCustomerForm(b *FormBuilder) {
	b.form.Alias = strRef("Kunderegister")
	b.form.AllowGeometryUpdates = true
	b.form.GeometryType = "esriGeometryPoint"
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "name"
	b.form.EditFieldsInfo = nil
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GlobalIdField = "orgNum"
	b.form.HasAttachments = false
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//FIELDS
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")

	b.AddSection("Kunde")
	b.AddRequiredStringField("orgNum", "Organisasjonsnummer")
	b.AddRequiredStringField("name", "Navn")
	b.AddNullableStringField("bankAccount", "Kontonummer")

	b.AddSection("Kontaktinformasjon")
	b.AddNullableStringField("email", "Epost-adresse")
	b.AddNullableStringField("phone", "Telefonnummer")
	b.AddNullableStringField("address1", "Adresse")
	b.AddNullableStringField("address2", "Adresse linje 2")
	b.AddNullableStringField("postCode", "Postnummer")
	b.AddNullableStringField("postOffice", "Poststed")

	b.AddSection("Eiendom")
	b.AddNullableIntegerField("muniNum", "Kommunenummer")
	b.AddNullableIntegerField("gridNum", "Gårdsnummer")
	b.AddNullableIntegerField("holdingNum", "Bruksnummer")

	//fieldIds
	b.SetFieldIdSeries(0,
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"orgNum",
		"name",
		"bankAccount",
		"email",
		"phone",
		"address1",
		"address2",
		"postCode",
		"postOffice",
		"muniNum",
		"gridNum",
		"holdingNum",
	)
	b.WithAutoSortex()
}
