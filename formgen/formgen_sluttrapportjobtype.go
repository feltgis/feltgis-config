package main

import (
	"log"
	"strconv"
	"strings"
)

type sluttrapportFormTypeType string

const (
	sluttrapportFormTypeForhandsrydding  sluttrapportFormTypeType = "Forhåndsrydding"
	sluttrapportFormTypeGrofterensk      sluttrapportFormTypeType = "Grøfterensk"
	sluttrapportFormTypeMarkberedning    sluttrapportFormTypeType = "Markberedning"
	sluttrapportFormTypePlanting         sluttrapportFormTypeType = "Planting"
	sluttrapportFormTypeSaaing           sluttrapportFormTypeType = "Såing"
	sluttrapportFormTypeUngskogpleie     sluttrapportFormTypeType = "Ungskogpleie"
	sluttrapportFormTypeSkogskade        sluttrapportFormTypeType = "Skogskade"
	sluttrapportFormTypeInfrastruktur    sluttrapportFormTypeType = "Infrastruktur"
	sluttrapportFormTypeGjodsling        sluttrapportFormTypeType = "Gjødsling"
	sluttrapportFormTypeSproyting        sluttrapportFormTypeType = "Sprøyting"
	sluttrapportFormTypeGravearbeid      sluttrapportFormTypeType = "Gravearbeid"
	sluttrapportFormTypeSporskaderetting sluttrapportFormTypeType = "Sporskaderetting"
	sluttrapportFormTypeStammekvisting   sluttrapportFormTypeType = "Stammekvisting"
)

func PopulateSluttrapportJobtypeForm(b *FormBuilder) {

	//GM is imported from integration, so it works differently
	if b.IsGlommenMjosen() {
		PopulateSluttrapportJobtypeExtGmForm(b)
		return
	}

	//find form type
	var formType sluttrapportFormTypeType
	if strings.Contains(b.file.Name, "Forhandsrydding") {
		formType = sluttrapportFormTypeForhandsrydding
	} else if strings.Contains(b.file.Name, "Grofterensk") {
		formType = sluttrapportFormTypeGrofterensk
	} else if strings.Contains(b.file.Name, "Markberedning") {
		formType = sluttrapportFormTypeMarkberedning
	} else if strings.Contains(b.file.Name, "Planting") {
		formType = sluttrapportFormTypePlanting
	} else if strings.Contains(b.file.Name, "Saaing") {
		formType = sluttrapportFormTypeSaaing
	} else if strings.Contains(b.file.Name, "Ungskogpleie") {
		formType = sluttrapportFormTypeUngskogpleie
	} else if strings.Contains(b.file.Name, "Skogskade") {
		formType = sluttrapportFormTypeSkogskade
	} else if strings.Contains(b.file.Name, "Infrastruktur") {
		formType = sluttrapportFormTypeInfrastruktur
	} else if strings.Contains(b.file.Name, "Gjodsling") {
		formType = sluttrapportFormTypeGjodsling
	} else if strings.Contains(b.file.Name, "Sproyting") {
		formType = sluttrapportFormTypeSproyting
	} else if strings.Contains(b.file.Name, "Gravearbeid") {
		formType = sluttrapportFormTypeGravearbeid
	} else if strings.Contains(b.file.Name, "Sporskaderetting") {
		formType = sluttrapportFormTypeSporskaderetting
	} else if strings.Contains(b.file.Name, "Stammekvisting") {
		formType = sluttrapportFormTypeStammekvisting
	} else {
		log.Fatalf("Unknown file %v", b.file.Name)
	}

	//DEFAULT!
	var alias string
	var aliasEnTranslation string
	switch formType {
	case sluttrapportFormTypeForhandsrydding:
		alias = "Sluttrapport forhåndsrydding"
		aliasEnTranslation = "Final report for pre-clearance"
	case sluttrapportFormTypeGrofterensk:
		alias = "Sluttrapport grøfterensk"
		aliasEnTranslation = "Final report for ditch cleaning"
	case sluttrapportFormTypeMarkberedning:
		alias = "Sluttrapport markberedning"
		aliasEnTranslation = "Final report for soil preparation"
	case sluttrapportFormTypePlanting:
		alias = "Sluttrapport planting"
		aliasEnTranslation = "Final report for planting"
	case sluttrapportFormTypeSaaing:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeUngskogpleie:
		alias = "Sluttrapport ungskogpleie"
		aliasEnTranslation = "Final report for tree nursery"
	case sluttrapportFormTypeSkogskade:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeInfrastruktur:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeGjodsling:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeSproyting:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeGravearbeid:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeSporskaderetting:
		log.Fatalf("Did not expect file %v", b.file.Name)
	case sluttrapportFormTypeStammekvisting:
		log.Fatalf("Did not expect file %v", b.file.Name)
	}

	b.form.Alias = strRef(alias)
	b.form.Translations = append(
		b.form.Translations,
		FormTranslation{
			Key:      alias,
			Language: "en",
			Value:    aliasEnTranslation,
		},
	)

	b.form.AllowGeometryUpdates = true
	b.form.Capabilities = "Create,Delete,Query,Sync,Update,Uploads,Editing"
	b.form.CopyrightText = ""
	b.form.CurrentVersion = 10.22
	b.form.DefaultVisibility = true
	b.form.Description = ""
	b.form.DisplayField = "dev_Boolean"
	b.form.EditFieldsInfo = nil
	if formType == sluttrapportFormTypePlanting {
		b.form.Extent = FormExtent{
			SpatialReference: FormExtentSpatialReference{
				LatestWkid: 25833,
				Wkid:       25833,
			},
			XMax: 450493.12090000045,
			XMin: -305588.6796000004,
			YMax: 7210241.434,
			YMin: 6469673.3945,
		}
	} else {
		b.form.Extent = FormExtent{
			SpatialReference: FormExtentSpatialReference{
				LatestWkid: 32633,
				Wkid:       32633,
			},
			XMax: 450493.12090000045,
			XMin: -305588.6796000004,
			YMax: 7210241.434,
			YMin: 6469673.3945,
		}
	}
	b.form.FieldOrder = []string{}
	b.form.Fields = []*FormField{}
	b.form.GeometryType = "esriGeometryPoint"
	b.form.GlobalIdField = "_globalId"
	b.form.HasAttachments = false
	b.form.HasM = boolRef(false)
	b.form.HasZ = boolRef(false)
	b.form.HtmlPopupType = "esriServerHTMLPopupTypeAsHTMLText"
	b.form.Id = 2
	b.form.IsDataVersioned = false
	b.form.MaxRecordCount = 100000
	b.form.MaxScale = 0
	b.form.MinScale = 100000
	b.form.Name = b.file.Name
	b.form.ObjectIdField = "OBJECTID"
	b.form.OwnershipBasedAccessControlForFeatures = nil
	b.form.Relationships = []string{}
	b.form.Sections = []*FormSection{}
	b.form.SupportedQueryFormats = "JSON, AMF"
	b.form.SupportsAdvancedQueries = true
	b.form.SupportsRollbackOnFailureParameter = true
	b.form.SupportsStatistics = true
	b.form.SyncCanReturnChanges = true
	b.form.Templates = []FormTemplate{}
	b.form.Type = "Feature Layer"
	b.form.TypeIdField = strRef("dev_Boolean")
	b.form.Types = []FormType{}
	b.form.UseStandardizedQueries = true

	//DRAWING INFO
	di := b.AddDrawingInfo().
		WithTransparency(0)
	renderer := di.WithRenderer("dev_Boolean")
	renderer.WithUniqueValueInfo("", "Avvik registrert").
		WithIntValue(1).
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgpJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIRozCxR1IntORZmrhOL033nlM0R8eXN75n98958A9Qf6zgj6xgFKq+4BIcjs0hSH0HZam4WtG68fGmJmagVrrtmFjzvWJNG+FWMAVK8Dya2N678HsTbgAzPkCU3B0xJjRbthpu6weQu3Q2gotcYie13rcGPPKCkwolRwTGeuCeNVB/AHXnYLOoONMpOHIaqVuYGhQ5GItMLeOi3R8hquX4bAH2A9D/bD7b2ClSjkEeya1ThhjZsrANuhphIgt6QuQBWLAekt8F0T7HGfwOoyUgRtgow2WARKAAF3AIwu0HoiJRMHV8jposAHflmAAz4FZoMPii5Tyy8AiFG3AZtdZAZtsJqAABQ/QUSqHyBrjXmAa+AC0Y5/LLyAHOQ9wQeRdHlrCFeYAsK/0VVMWijl44AHOw5lncDAFjT65Vk0p9fGSyH0PcAIWtil1JysyunmlsJr0En6IyDgrnXufXlrk9G2IJ6FnSw2wN7CcUerakMiT1X9rlsMJ2H8XJt/DQCeEK2cKYIApMHmtzx4z5pY7Zl1fA5B+Clcewo0G2BGBpjoI/YSlRfiWhxd5GD5pTL4yt+qCTcEnoNenY6v8NvY/6Tfiz5iwRteyOAAAAABJRU5ErkJggg==",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		)
	renderer.WithUniqueValueInfo("", "Ingen avvik registrert").
		WithIntValue(0).
		WithImageSymbol(
			"iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IB2cksfwAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAgZJREFUOI2tlE1IVFEUx3+jw9yGwUVXoU1Ug1AMUzSFaQqJMGjY0KZNpAhZ0WAISogR9LGKiMA2FRitqlVU1KJtlpvoY+EicKKIJorCxR1IntORZmrh2Lyn9z2m6A8XLvd/zo9zDtwT5j8rHOCFlFLdsk/SbKWJKBG+s8A0X3VOPzTGzNQM1Fq3mWFzRvqkmc0kCLnMEovmpenlDrNc5xzwKRiY4bAZMaN0s91adz0R2mmllRaSxPVZPW6MeWEFqpRKy5iM0UXSbw4ucB0n6HTCzgRZDi1X6gZGZFDO1wRzSY5KBx+5zEUOeoH9DNHPzr+BVSqFA+zSkzpljJmpAtvooZGYNekLkAcSwFqLv4O40+cMcpWRKnAd662wHJACBOgCHlig9SAJiYO75TU0WIGvKzCAp8As0GGJiy3lV4FlylZgs+uugA3WKChR8gCVowrypxSXdgPTwDugHayD+QUUKHiAMidvKNJCdEVwCNhTOX7KU6bAPQ+Qz5ziCfvJ0BiQapWaUu/lgtz1AieYU1vULcnLKBs9vzdYz/khIuMsNe79epKVk9wkSZoeNtUAe8WiyqkrMiSPlp9WL4dj7OU2k7xlgE6iq2YKYIApjC7q0+aIueG27PtwgCyPucR9rtHANmI0UUeEnywwzzeKPKPIsDluiitT/Rdshg9Ar6/vo6CN/U/6DbwMmLDH65GJAAAAAElFTkSuQmCC",
			"02118a46198679b7824f4459330dfde3",
			16,
			16,
		)

	//LOGIC
	//TODO: legacy since NT uses NTSluttrapportPlanting as PFeltkontroll
	logicIsFeltkontrollOngoing := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("FeltkontrollOngoing"),
	}
	logicIsNotFeltkontrollOngoing := FormFieldLogicField{
		Operator:              "!=",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("FeltkontrollOngoing"),
	}
	logicIsFeltkontrollCompleted := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("FeltkontrollCompleted"),
	}
	logicIsNotFeltkontrollCompleted := FormFieldLogicField{
		Operator:              "!=",
		OtherFieldName:        "_type",
		OtherFieldStringValue: strRef("FeltkontrollCompleted"),
	}
	logicVisibleIfEgenkontroll := FormFieldLogicIfValueIs{
		AllOf: []FormFieldLogicFieldInArray{
			{Field: logicIsNotFeltkontrollOngoing},
			{Field: logicIsNotFeltkontrollCompleted},
		},
	}

	logicIsDeviation := FormFieldLogicField{
		Operator:           "==",
		OtherFieldName:     "dev_Boolean",
		OtherFieldIntValue: intRef(1),
	}

	//FIELDS
	b.AddHiddenStringField("_globalId", "Entreprenørordre GUID")
	b.AddHiddenDateField("_created_Date", "Opprettet")
	b.AddHiddenStringField("_createdBy", "Opprettet av")
	b.AddHiddenDateField("_modified_Date", "Endret")
	b.AddHiddenStringField("_modifiedBy", "Endret av")
	b.AddHiddenStringField("_job_nr", "Ordrenummer")
	if formType == sluttrapportFormTypePlanting && b.IsNortømmer() {
		b.AddHiddenNullableStringField("_type", "Type").
			WithComment("nullable because of backwards compability, null = 'Egenkontroll'")
	}
	b.AddHiddenStringField("_parentGlobalId", "Tilhørende objekt")
	b.AddHiddenNullableStringField("_workTeamOrgNum", "Organisasjonsnummer arbeidslag")

	//fieldIds
	b.SetFieldIdSeries(0,
		"_globalId",
		"_created_Date",
		"_createdBy",
		"_modified_Date",
		"_modifiedBy",
		"_job_nr",
	)
	if formType == sluttrapportFormTypePlanting && b.IsNortømmer() {
		b.SetFieldIdSeries(20,
			"_type",
		)
	}
	b.SetFieldIdSeries(
		21,
		"_parentGlobalId",
	)
	b.SetFieldIdSeries(8,
		"_workTeamOrgNum",
	)

	//SECTION OPPDRAG
	if formType == sluttrapportFormTypePlanting {
		//planting seems special, handled here, the other ones are more similar

		b.AddSection("Oppdrag").WithTranslation("en", "Job") //1
		b.AddRequiredStringField("responsible", "Ansvarlig planting").
			WithAliasTranslation("en", "Responsible for planting")

		if b.IsNortømmer() {
			b.AddRequiredBooleanField("proveniens_Boolean", "Er riktig proveniens benyttet?").
				WithAliasTranslation("en", "Has the correct provenance been used?").
				WithLogic().
				WithVisibleIfAnyOf(logicIsFeltkontrollOngoing, logicIsFeltkontrollCompleted)
			b.AddRequiredBooleanField("cleaned_Boolean", "Er avfallet ryddet etter planting?").
				WithAliasTranslation("en", "Has the debris been cleared after planting?").
				WithLogic().
				WithVisibleIfField(logicIsNotFeltkontrollOngoing)

			b.SetFieldIdSeries(9,
				"proveniens_Boolean",
				"cleaned_Boolean",
			)
		} else if b.IsFjordtømmer() {
			b.AddRequiredBooleanField("proveniens_Boolean", "Er riktig proveniens benyttet?").
				WithAliasTranslation("en", "Has the correct provenance been used?")
			b.AddRequiredBooleanField("cleaned_Boolean", "Er avfallet ryddet etter planting?").
				WithAliasTranslation("en", "Has the debris been cleared after planting?")
			b.SetFieldIdSeries(9,
				"proveniens_Boolean",
				"cleaned_Boolean",
			)
		} else if b.IsForestOwner() {
			b.AddRequiredBooleanField("according_instructions_Boolean", "Er det plantet i henhold til instruks?").
				WithAliasTranslation("en", "Is it planted according to instructions?")
			b.AddRequiredStringField("according_instructions_comment", "Kommentar").
				WithAliasTranslation("en", "Comment").
				WithLogic().
				WithVisibleIfField(FormFieldLogicField{
					Operator:           "==",
					OtherFieldName:     "according_instructions_Boolean",
					OtherFieldIntValue: intRef(0),
				})

			b.SetFieldIdSeries(
				70,
				"according_instructions_Boolean",
				"according_instructions_comment",
			)
		}

		b.SetFieldIdSeries(6,
			"responsible",
		)

	} else {
		switch formType {
		case sluttrapportFormTypePlanting:
			panic("should be handled outside this switch")
		case sluttrapportFormTypeUngskogpleie:
			b.AddRequiredStringField("responsible", "Ansvarlig ungskogpleie").
				WithAliasTranslation("en", "Responsible young forest management")
		case sluttrapportFormTypeForhandsrydding:
			b.AddRequiredStringField("responsible", "Ansvarlig forhåndsrydding").
				WithAliasTranslation("en", "Responsible for pre-clearance")
		case sluttrapportFormTypeGrofterensk:
			b.AddRequiredStringField("responsible", "Ansvarlig grøfterensk").
				WithAliasTranslation("en", "Responsible for ditch cleaning")
		case sluttrapportFormTypeMarkberedning:
			b.AddRequiredStringField("responsible", "Ansvarlig markberedning").
				WithAliasTranslation("en", "Responsible soil preparation")
		default:
			panic("unhandled form type " + formType)
		}

		b.AddMultipleChoiceField("edges_ok", "Er det ryddet i kantsoner/vegetasjonsbelter?").
			WithAliasTranslation("en", "Has the edge/vegetation belts been cleared?").
			WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
			WithCodedOption("Nei").WithOptionTranslation("en", "No").
			WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

		b.AddMultipleChoiceField("trails_ok", "Er merkede og mye brukte stier ryddet og fremkommelige?").
			WithAliasTranslation("en", "Are marked and heavily used trails cleared and passable?").
			WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
			WithCodedOption("Nei").WithOptionTranslation("en", "No").
			WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

		b.AddMultipleChoiceField("bio_ok", "Er det ryddet i nøkkelbiotop, reservat eller naturtype A eller B?").
			WithAliasTranslation("en", "Has clearance been carried out in key biotopes, reserves, or nature types A or B?").
			WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
			WithCodedOption("Nei").WithOptionTranslation("en", "No").
			WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

		b.AddMultipleChoiceField("nest_ok", "Er rovfuglreir/tiurleik hensyntatt under arbeidet?").
			WithAliasTranslation("en", "Have bird of prey nests/capercaillie leks been taken into account during the work?").
			WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
			WithCodedOption("Nei").WithOptionTranslation("en", "No").
			WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

		b.AddMultipleChoiceField("other_ok", "Er evt. andre spesielle hensyn ved arbeidet godt gjennomført? (eks. sette igjen lauv til elgbeite)").
			WithAliasTranslation("en", "Have any other special considerations during the work been well executed? (e.g. leaving leaves for moose grazing)").
			WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
			WithCodedOption("Nei").WithOptionTranslation("en", "No").
			WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

		b.AddNullableStringField("other_notes", "Merknad, andre hensyn:").
			WithAliasTranslation("en", "Note, other considerations:").
			WithLength(1000)

		//ids
		b.SetFieldIdSeries(6,
			"responsible",
		)
		b.SetFieldIdSeries(9,
			"edges_ok",
			"trails_ok",
			"bio_ok",
			"nest_ok",
			"other_ok",
			"other_notes",
		)

		switch formType {
		case sluttrapportFormTypePlanting:
			//none
		case sluttrapportFormTypeMarkberedning:
			//none
		default:
			b.AddRequiredBooleanField("treecount_ok_Boolean", "Er treantall i tråd med gitt arbeidsordre?").
				WithAliasTranslation("en", "Is the number of trees in accordance with the given work order?")

			b.AddRequiredBooleanField("selection_ok_Boolean", "Er det gjort et godt valg av gjenstående trær?").
				WithAliasTranslation("en", "Has a good selection of remaining trees been made?")

			b.SetFieldIdSeries(
				15,
				"treecount_ok_Boolean",
				"selection_ok_Boolean",
			)
		}
	}

	//SECTION MILJØ
	switch formType {
	case sluttrapportFormTypePlanting:

		if b.IsNortømmer() {
			b.AddSection("Miljø").WithTranslation("en", "Environment") //2
			b.AddMultipleChoiceField("cleaned_edges", "Er det plantet i kantsoner/vegetasjonsbelter eller stier?").
				WithAliasTranslation("en", "Has planting been done in edge/vegetation belts or trails?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant").
				WithLogic().
				WithVisibleIfField(logicIsNotFeltkontrollOngoing)
			b.AddMultipleChoiceField("planted_near_ch", "Er det plantet nærmere enn 5m fra kjent kulturminne?").
				WithAliasTranslation("en", "Has planting been done closer than 5m from a known cultural heritage site?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant").
				WithLogic().
				WithVisibleIfField(logicIsNotFeltkontrollOngoing)
			b.AddMultipleChoiceField("planted_near_bio", "Er det plantet i nøkkelbiotop, reservat eller naturtype A eller B?").
				WithAliasTranslation("en", "Has planting been done in key biotopes, reserves, or nature types A or B?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant").
				WithLogic().
				WithVisibleIfField(logicIsFeltkontrollCompleted)

			b.SetFieldIdSeries(11,
				"cleaned_edges",
				"planted_near_ch",
				"planted_near_bio",
			)
		} else if b.IsFjordtømmer() {
			b.AddSection("Miljø").WithTranslation("en", "Environment") //2
			b.AddMultipleChoiceField("cleaned_edges", "Er det plantet i kantsoner/vegetasjonsbelter eller stier?").
				WithAliasTranslation("en", "Has planting been done in edge/vegetation belts or trails?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")
			b.AddMultipleChoiceField("planted_near_ch", "Er det plantet nærmere enn 5m fra kjent kulturminne?").
				WithAliasTranslation("en", "Has planting been done closer than 5m from a known cultural heritage site?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")
			b.AddMultipleChoiceField("planted_near_bio", "Er det plantet i nøkkelbiotop, reservat eller naturtype A eller B?").
				WithAliasTranslation("en", "Has planting been done in key biotopes, reserves, or nature types A or B?").
				WithCodedOption("Ja").WithOptionTranslation("en", "Yes").
				WithCodedOption("Nei").WithOptionTranslation("en", "No").
				WithCodedOption("Ikke aktuelt").WithOptionTranslation("en", "Not relevant")

			b.SetFieldIdSeries(11,
				"cleaned_edges",
				"planted_near_ch",
				"planted_near_bio",
			)
		} else if b.IsForestOwner() {
			//none
		}
	default:
		//nothing
	}

	//SECTION GENERELT OM PLATINGEN
	switch formType {
	case sluttrapportFormTypePlanting:
		if b.IsNortømmer() {
			b.AddSection("Generelt om plantingen").WithTranslation("en", "About the planting in general") //3
			b.AddRequiredBooleanField("plant_count_ok_Boolean", "Er planteantall i tråd med gitt arbeidsordre?").
				WithAliasTranslation("en", "Is the number of plants in accordance with the given work order?")
			b.AddRequiredBooleanField("plant_place_Boolean", "Er riktig planteplass benyttet?").
				WithAliasTranslation("en", "Has the correct planting location been used?").
				WithLogic().
				WithVisibleIfAnyOf(logicIsFeltkontrollOngoing, logicIsFeltkontrollCompleted)

			b.SetFieldIdSeries(
				14,
				"plant_count_ok_Boolean",
			)

			b.SetFieldIdSeries(
				16,
				"plant_place_Boolean",
			)
		} else if b.IsFjordtømmer() {
			b.AddSection("Generelt om plantingen").WithTranslation("en", "About the planting in general") //3
			b.AddRequiredBooleanField("plant_count_ok_Boolean", "Er planteantall i tråd med gitt arbeidsordre?").
				WithAliasTranslation("en", "Is the number of plants in accordance with the given work order?")

			b.AddRequiredBooleanField("legal_ok_Boolean", "Er antall planter iht. lovens minstekrav?").
				WithAliasTranslation("en", "Is the number of plants in accordance with the legal minimum requirement?")

			b.AddRequiredBooleanField("plant_place_Boolean", "Er riktig planteplass benyttet?").
				WithAliasTranslation("en", "Has the correct planting location been used?")

			b.SetFieldIdSeries(
				14,
				"plant_count_ok_Boolean",
				"legal_ok_Boolean",
				"plant_place_Boolean",
			)
		} else if b.IsForestOwner() {
			//none
		}
	default:
		//nothing
	}

	//PLANTETYPER!
	switch formType {
	case sluttrapportFormTypePlanting:
		for i := 1; i <= 4; i++ {
			iString := strconv.Itoa(i)

			b.AddHiddenNullableStringField("_planttype"+iString+"globalId", "Plantetype globalID")

			if i == 1 {
				b.AddSection("Antall plantede planter").WithTranslation("en", "Number of planted plants") //4 //since key is the same on all, we only translate this once
			} else {
				b.AddSection("Antall plantede planter") //5,6,7
			}

			if b.IsNortømmer() {
				b.AddRequiredStringField("planttype"+iString, "Plantetype").
					WithAliasTranslation("en", "Plant type").
					WithLogic().WithVisibleIf(logicVisibleIfEgenkontroll)
				b.AddRequiredIntegerField("planttype"+iString+"count", "Antall plantede planter").
					WithAliasTranslation("en", "Number of planted plants").
					WithRequiredIfField(FormFieldLogicField{
						Operator:              "!=",
						OtherFieldName:        "planttype" + iString,
						OtherFieldStringValue: strRef(""),
					}).
					WithLogic().WithVisibleIf(logicVisibleIfEgenkontroll)
				//TODO(kristoffer): Should all of these be required?
			} else {
				if i == 1 {
					b.AddRequiredStringField("planttype"+iString, "Plantetype").
						WithAliasTranslation("en", "Plant type")
					b.AddRequiredIntegerField("planttype"+iString+"count", "Antall plantede planter").
						WithAliasTranslation("en", "Number of planted plants").
						WithRequiredIfField(FormFieldLogicField{
							Operator:              "!=",
							OtherFieldName:        "planttype" + iString,
							OtherFieldStringValue: strRef(""),
						})
				} else {
					b.AddNullableStringField("planttype"+iString, "Plantetype").
						WithAliasTranslation("en", "Plant type")
					b.AddNullableIntegerField("planttype"+iString+"count", "Antall plantede planter").
						WithAliasTranslation("en", "Number of planted plants").
						WithRequiredIfField(FormFieldLogicField{
							Operator:              "!=",
							OtherFieldName:        "planttype" + iString,
							OtherFieldStringValue: strRef(""),
						})
				}
			}

			b.SetFieldIdSeries(
				20+(i*10), //30,40,50,60
				"planttype"+iString,
				"planttype"+iString+"count",
				"_planttype"+iString+"globalId",
			)
		}
	default:
		//nothing
	}

	//AVVIK!
	if b.IsNortømmer() && formType == sluttrapportFormTypePlanting {
		b.AddSection("") //8

		b.AddRequiredBooleanField("dev_Boolean", "Er det under denne kontrollen registrert avvik?").
			WithAliasTranslation("en", "Have any deviations been recorded during this inspection?").
			WithLogic().
			WithVisibleIfField(logicIsFeltkontrollCompleted)
		b.AddRequiredStringField("dev_responsibility", "Hvem er ansvarlig for avviket?").
			WithAliasTranslation("en", "Who is responsible for the deviation?").
			WithLogic().
			WithVisibleIfAllOf(logicIsDeviation, logicIsFeltkontrollCompleted)
		b.AddRequiredStringField("dev_description", "Kort beskrivelse av avviket.").
			WithAliasTranslation("en", "Brief description of the deviation.").
			WithLogic().
			WithVisibleIfAllOf(logicIsDeviation, logicIsFeltkontrollCompleted)

		b.SetFieldIdSeries(
			17,
			"dev_Boolean",
		)

		b.SetFieldIdSeries(
			25,
			"dev_responsibility",
			"dev_description",
		)

		b.AddRequiredBooleanField("edgezones_keybiotopes_cultural_Boolean", "Er det holdt tilstrekkelig avstand til kantsoner, nøkkelbiotoper og kulturminner?").
			WithAliasTranslation("en", "Has a sufficient distance been kept to edge zones, key biotopes and cultural monuments?").
			WithLogic().
			WithVisibleIfField(logicIsFeltkontrollOngoing)
		b.AddRequiredBooleanField("need_for_followup_check_Boolean", "Er det behov for en etterkontroll på dette oppdraget?").
			WithAliasTranslation("en", "Is there a need for a follow-up check on this assignment?").
			WithLogic().
			WithVisibleIfField(logicIsFeltkontrollOngoing)
		b.AddRequiredBooleanField("accordance_to_work_order_Boolean", "Er oppdraget utført i henhold til arbeidsordre?").
			WithAliasTranslation("en", "Has the assignment been carried out in accordance with the work order?").
			WithLogic().
			WithVisibleIfAllOf(logicIsNotFeltkontrollOngoing, logicIsNotFeltkontrollCompleted)
		b.AddNullableStringField("accordance_to_work_order_comment", "Merknad").
			WithAliasTranslation("en", "Comment").
			WithLogic().
			WithVisibleIfAllOf(
				FormFieldLogicField{
					Operator:           "==",
					OtherFieldName:     "accordance_to_work_order_Boolean",
					OtherFieldIntValue: intRef(0),
				},
				logicIsNotFeltkontrollOngoing,
				logicIsNotFeltkontrollCompleted,
			)

		b.SetFieldIdSeries(
			22,
			"edgezones_keybiotopes_cultural_Boolean",
		)
		b.SetFieldIdSeries(
			24,
			"need_for_followup_check_Boolean",
		)
		b.SetFieldIdSeries(
			27,
			"accordance_to_work_order_Boolean",
			"accordance_to_work_order_comment",
		)

	} else {
		if formType == sluttrapportFormTypePlanting {
			b.AddSection("") //8
		}

		b.AddRequiredBooleanField("dev_Boolean", "Er det under denne kontrollen registrert avvik?").
			WithAliasTranslation("en", "Have any deviations been recorded during this inspection?")
		if formType == sluttrapportFormTypePlanting {
			b.AddNullableStringField("dev_comment", "Merknad").
				WithAliasTranslation("en", "Comments").
				WithRequiredIfField(FormFieldLogicField{
					Operator:           "==",
					OtherFieldName:     "dev_Boolean",
					OtherFieldIntValue: intRef(1),
				})
		} else {
			b.AddNullableStringField("dev_comment", "Merknad til avvik").
				WithAliasTranslation("en", "Remark on deviations").
				WithRequiredIfField(FormFieldLogicField{
					Operator:           "==",
					OtherFieldName:     "dev_Boolean",
					OtherFieldIntValue: intRef(1),
				})
		}
		b.AddRequiredBooleanField("followup_Boolean", "Er det behov for oppfølging?").
			WithAliasTranslation("en", "Is there a need for follow-up?")

		b.SetFieldIdSeries(
			17,
			"dev_Boolean",
		)

		b.SetFieldIdSeries(
			18,
			"dev_comment",
			"followup_Boolean",
		)
	}

	//type
	if formType == sluttrapportFormTypePlanting && b.IsNortømmer() {
		b.AddType(FormType{
			Comment: strRef("_type = null = egenkontroll feltlogg"),
			Domains: FormTypeDomains{},
			Id:      "Sluttrapport",
			Name:    "Sluttrapport",
			Templates: []FormTypeTemplate{
				{
					Name: "FeltkontrollOngoing",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_type": "FeltkontrollOngoing"},
					},
				},
				{
					Name: "FeltkontrollCompleted",
					Prototype: FormTypeTemplatePrototype{
						Attributes: map[string]interface{}{"_type": "FeltkontrollCompleted"},
					},
				},
			},
		})
	}

	b.WithAutoSortex()

	//DEBUG FILE-ORDER
	//TODO: for better diff with older version
	/*
		switch formType {
		case sluttrapportFormTypePlanting:
			if b.IsFjordtømmer() {
				b.DebugSetFieldsFileOrder(
					"_globalId",
					"_created_Date",
					"_createdBy",
					"_modified_Date",
					"_modifiedBy",
					"_job_nr",
					"responsible",
					"_workTeamOrgNum",
					"proveniens_Boolean",
					"cleaned_Boolean",
					"cleaned_edges",
					"planted_near_ch",
					"planted_near_bio",
					"plant_count_ok_Boolean",
					"legal_ok_Boolean",
					"plant_place_Boolean",
					"dev_Boolean",
					"dev_comment",
					"followup_Boolean",
					"_parentGlobalId",

					"planttype1",
					"planttype1count",
					"_planttype1globalId",

					"planttype2",
					"planttype2count",
					"_planttype2globalId",

					"planttype3",
					"planttype3count",
					"_planttype3globalId",

					"planttype4",
					"planttype4count",
					"_planttype4globalId",
				)
			}
		default:
			if formType == sluttrapportFormTypeMarkberedning {
				b.DebugSetFieldsFileOrder(
					"_globalId",
					"_created_Date",
					"_createdBy",
					"_modified_Date",
					"_modifiedBy",
					"_job_nr",
					"responsible",
					"_workTeamOrgNum",
					"edges_ok",
					"trails_ok",
					"bio_ok",
					"nest_ok",
					"other_ok",
					"other_notes",
					"dev_Boolean",
					"dev_comment",
					"followup_Boolean",
					"_parentGlobalId",
				)
			} else {
				b.DebugSetFieldsFileOrder(
					"_globalId",
					"_created_Date",
					"_createdBy",
					"_modified_Date",
					"_modifiedBy",
					"_job_nr",
					"responsible",
					"_workTeamOrgNum",
					"edges_ok",
					"trails_ok",
					"bio_ok",
					"nest_ok",
					"other_ok",
					"other_notes",
					"treecount_ok_Boolean",
					"selection_ok_Boolean",
					"dev_Boolean",
					"dev_comment",
					"followup_Boolean",
					"_parentGlobalId",
				)
			}
		}
	*/
}
