package main

func PopulateDoneWorkForm(b *FormBuilder) {

	//GM is imported from integration, so it works differently
	if b.IsGlommenMjosen() {
		PopulateDoneWorkExtGmForm(b)
		return
	}

	//common
	populateDoneworkFormCommon(b)

	//prefix!
	var prefix = "PREFIX"
	if b.company != nil {
		prefix = b.company.Prefix
	}

	//LOGIC
	subCollectionIsSaaing := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PSaaingGeom"),
	}
	subCollectionIsPlanting := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PPlantingGeom"),
	}
	subCollectionIsGrofteresk := FormFieldLogicField{
		Operator:              "==",
		OtherFieldName:        "_subCollection",
		OtherFieldStringValue: strRef(prefix + "PGrofterenskGeom"),
	}
	/*
		subCollectionIsUngskogpleie := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PUngskogpleieGeom"),
		}
		subCollectionIsForhandsrydding := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PForhandsryddingGeom"),
		}
		subCollectionIsMarkberedning := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PMarkberedningGeom"),
		}
		subCollectionIsGjodsling := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PGjodslingGeom"),
		}
		subCollectionIsGravearbeid := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PGravearbeidGeom"),
		}
		subCollectionIsSproyting := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PSproytingGeom"),
		}
		subCollectionIsSporskaderetting := FormFieldLogicField{
			Operator:              "==",
			OtherFieldName:        "_subCollection",
			OtherFieldStringValue: strRef(prefix + "PSporskaderettingGeom"),
		}
	*/

	//section 1
	b.AddSection("")

	b.AddUneditableNullableIntegerField("bestand", "Bestand").
		WithAliasTranslation("en", "Stand").
		WithLogic().
		WithVisibleIfAllOf(
			FormFieldLogicField{
				Operator:       "exists",
				OtherFieldName: "bestand",
			},
			FormFieldLogicField{
				Operator:       "missing",
				OtherFieldName: "bestand_str",
			},
		)
	b.AddUneditableNullableStringField("bestand_str", "Bestand").
		WithAliasTranslation("en", "Stand").
		WithLogic().
		WithVisibleIfField(FormFieldLogicField{
			Operator:       "exists",
			OtherFieldName: "bestand_str",
		})

	b.AddUneditableNullableDoubleField("area", "Areal").
		WithAliasTranslation("en", "Area")

	b.AddNullableBooleanField("contractor_done_Boolean", "Markér som ferdig (entrep)").
		WithAliasTranslation("en", "Mark done (by contractor)")

	//-------------------------------
	//GROFTERENSK

	b.AddRequiredIntegerField("meters_grofterensk", "Antall meter grøfterensk").
		WithAliasTranslation("en", "Number of meters of ditch cleaning").
		WithLogic().
		WithVisibleIfField(subCollectionIsGrofteresk)

	b.AddMultipleChoiceField("grofterensk_type", "Type grøfterensk").
		WithAliasTranslation("en", "Ditch cleaning type").
		WithCodedOptionWithCustomName("1", "Grøfter").WithOptionTranslation("en", "Ditches").
		WithCodedOptionWithCustomName("2", "Kanaler").WithOptionTranslation("en", "Channels").
		WithLogic().
		WithVisibleIfField(subCollectionIsGrofteresk)

	//-------------------------------
	//SAAING

	b.AddRequiredIntegerField("seeds_grams_total", "Antall gram frø totalt").
		WithAliasTranslation("en", "Sown grams totally").
		WithLogic().
		WithVisibleIfField(subCollectionIsSaaing)

	//-------------------------------
	//HOURS

	//phasing out old field "hours", but keep it around if set...
	//new field is "hours_Decimal2"
	hoursExists := FormFieldLogicField{
		Operator:       "exists",
		OtherFieldName: "hours",
	}
	hoursMissing := FormFieldLogicField{
		Operator:       "missing",
		OtherFieldName: "hours",
	}
	priceModelHours := FormFieldLogicField{
		OtherFieldName:        "price_model",
		Operator:              "==",
		OtherFieldStringValue: strRef("Timespris"),
	}
	priceModelIsSet := FormFieldLogicField{
		OtherFieldName:        "price_model",
		Operator:              "!=",
		OtherFieldStringValue: strRef(""),
	}

	if b.IsForestOwner() {
		//They still want to write hours :-D, even if they dont have a "price model"
		b.AddNullableIntegerField("hours", "Timer").
			WithAliasTranslation("en", "Hours").
			WithLogic().
			WithVisibleIfField(hoursExists)
		b.AddNullableDecimalField("hours_Decimal2", "Timer", 2).
			WithAliasTranslation("en", "Hours").
			WithLogic().
			WithVisibleIfField(hoursMissing)
	} else {
		b.AddNullableIntegerField("hours", "Timer").
			WithAliasTranslation("en", "Hours").
			WithLogic().
			WithVisibleIfAllOf(hoursExists, priceModelHours)
		b.AddNullableDecimalField("hours_Decimal2", "Timer", 2).
			WithAliasTranslation("en", "Hours").
			WithLogic().
			WithVisibleIfAllOf(hoursMissing, priceModelHours)
	}

	b.AddUneditableNullableStringField("price_model", "Prismodell").
		WithAliasTranslation("en", "Price model").
		WithLogic().
		WithVisibleIfField(priceModelIsSet)

	//-------------------------------
	//SAAING

	if b.IsForestOwner() {
		b.AddRequiredIntegerField("natural_plants", "Antall naturlig kommet per daa").
			WithAliasTranslation("en", "Number of natural plants per daa").
			WithLogic().
			WithVisibleIfField(subCollectionIsSaaing)
	}

	b.AddRequiredIntegerField("seeds_grams_per_daa", "Antall gram frø per daa.").
		WithAliasTranslation("en", "Sown grams per decar").
		WithLogic().
		WithVisibleIfField(subCollectionIsSaaing)

	if b.IsForestOwner() {
		b.AddRequiredIntegerField("seeds_area_daa", "Sådd areal i daa.").
			WithAliasTranslation("en", "Sown area in decares.").
			WithLogic().
			WithVisibleIfField(subCollectionIsSaaing)
	} else {
		b.AddRequiredIntegerField("seeds_area_daa", "Antall naturlig kommet per daa").
			WithAliasTranslation("en", "Number of natural plants per daa").
			WithLogic().
			WithVisibleIfField(subCollectionIsSaaing)
	}

	b.AddRequiredStringField("seeds_ref_nr", "Frøparti (sertifikat nr)").
		WithAliasTranslation("en", "Seed reference (certificate number)").
		WithLogic().
		WithVisibleIfField(subCollectionIsSaaing)
	b.AddRequiredStringField("seeds_Date", "Såtidspunkt").
		WithAliasTranslation("en", "Seed date").
		WithLogic().
		WithVisibleIfField(subCollectionIsSaaing)

	//-------------------------------
	//SAAING + PLANTING

	if b.IsForestOwner() {
		/*
			b.AddNullableStringField("refnr_plants", "Referansenummer planter").
				WithAliasTranslation("en", "Reference number plants").
				WithLogic().
				WithVisibleIfField(subCollectionIsPlanting)*/
		b.AddUneditableNullableIntegerField("perc_gran", "Andel gran").
			WithAliasTranslation("en", "Proportion spruce").
			WithLogic().
			WithVisibleIfAnyOf(
				subCollectionIsPlanting,
				subCollectionIsSaaing,
			)
		b.AddUneditableNullableIntegerField("perc_furu", "Andel furu").
			WithAliasTranslation("en", "Proportion pine").
			WithLogic().
			WithVisibleIfAnyOf(
				subCollectionIsPlanting,
				subCollectionIsSaaing,
			)
		b.AddUneditableNullableIntegerField("perc_lauv", "Andel lauv").
			WithAliasTranslation("en", "Proportion deciduous").
			WithLogic().
			WithVisibleIfAnyOf(
				subCollectionIsPlanting,
				subCollectionIsSaaing,
			)
	}

	//-------------------------------
	//PLANTING

	//section 2,3,4....
	PopulatePlantTypeSections(b, PlantTypeSectionsSource_DoneWork)

	// section comment
	if b.IsForestOwner() {
		b.AddSection("")
		b.AddNullableStringField("comment", "Kommentar").
			WithAliasTranslation("en", "Comment")
	}

	// section forestover covers cost
	if !b.IsForestOwner() {
		b.AddSection("")
		b.AddUneditableNullableBooleanField("forestowner_covers_cost_for_300_m_Boolean", "Kostnad for bæring av planter over 300 meter dekkes av skogeier").
			WithAliasTranslation("en", "The cost of carrying plants over 300 meters is covered by the forest owner").
			WithLogic().
			WithVisibleIfField(subCollectionIsPlanting)
		b.AddRequiredIntegerField("hours_carried_plants", "Timer brukt på bæring av planter").
			WithAliasTranslation("en", "Hours used carrying plants").
			WithLogic().
			WithVisibleIfField(subCollectionIsPlanting)
	}

	//-------------------------------
	//IDS!

	//fieldIds
	b.SetFieldIdSeries(
		6,
		"area",
	)

	b.SetFieldIdSeries(
		11,
		"contractor_done_Boolean",
	)

	b.SetFieldIdSeries(
		15,
		"bestand",
		"bestand_str",
	)

	if b.IsForestOwner() {
		b.SetFieldIdSeries(71,
			"perc_gran",
			"perc_furu",
			"perc_lauv")
	}

	/*b.SetFieldIdSeries(
		25,
		"num_planted",
	)*/

	b.SetFieldIdSeries(
		51,
		"meters_grofterensk",
		"grofterensk_type",
	)

	if b.IsForestOwner() || b.IsAltiskog() || b.IsValdresSkog() {
		b.SetFieldIdSeries(
			56,
			"seeds_area_daa",
		)
	}
	b.SetFieldIdSeries(
		57,
		"seeds_grams_total",
		"seeds_grams_per_daa",
	)
	if b.IsForestOwner() {
		b.SetFieldIdSeries(
			59,
			"natural_plants",
		)
	} else if b.IsNortømmer() || b.IsStoraEnso() || b.IsFjordtømmer() || b.IsGlommenMjosen() {
		b.SetFieldIdSeries(
			59,
			"seeds_area_daa",
		)
	}
	b.SetFieldIdSeries(
		60,
		"seeds_ref_nr",
		"seeds_Date",
		"hours",
	)
	b.SetFieldIdSeries(
		63,
		"hours_Decimal2",
		"price_model",
	)

	if b.IsForestOwner() {
		b.SetFieldIdSeries(
			70,
			"comment",
		)
	}

	if !b.IsForestOwner() {
		b.SetFieldIdSeries(80,
			"forestowner_covers_cost_for_300_m_Boolean",
			"hours_carried_plants")
	}

	//before we mingle up the file :-)
	b.WithAutoSortex()
}
