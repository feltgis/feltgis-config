#!/bin/bash

# Sjekk om minst ett bøttenavn er oppgitt
if [ "$#" -lt 1 ]; then
    echo "Usage: $0 bucket_name1 [bucket_name2 ... bucket_nameN]"
    exit 1
fi

PROJECT="feltgis-prod"
LOCATION="europe-north1"

# Opprett bøtter for alle argumenter
for BUCKET in "$@"; do
    echo "Creating bucket: gs://$BUCKET/"
    gsutil mb -p "$PROJECT" -l "$LOCATION" "gs://$BUCKET/"
    gsutil bucketpolicyonly set off "gs://$BUCKET/"
    echo "Bucket $BUCKET created successfully."
done



