# Automatic windows configuration

The script automates the manual configuration steps described in [DSS-Feltboks-Installasjon.pdf](./DSS-Feltboks-Installasjon.pdf) and logs system information about the windows system.
