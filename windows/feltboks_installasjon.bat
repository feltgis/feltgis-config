@echo off
setlocal enabledelayedexpansion
for /f "tokens=2 delims==." %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~0,4%"
set "MM=%dt:~4,2%"
set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%"
set "Min=%dt:~10,2%"
set "Sec=%dt:~12,2%"
set "timestamp=%YY%%MM%%DD%_%HH%%Min%%Sec%"
set "logFile=feltboks_setup_%timestamp%.log"
set "sharedFolderName=mpc"
set "everyoneGroupSID=S-1-1-0"
set "guestAccountName="
set "guestAccountSID="

:: Ensure that the script is run with administrative permissions
net session >nul 2>&1
if not %errorlevel% == 0 (
    echo Please run this script as an Administrator.
    pause
    exit
)

goto:main

:get_interfacename
set "_adapter="
set "_ip="
for /f "tokens=1* delims=:" %%g in ('ipconfig /all') do (
    set "_tmp=%%~g"
    if "!_tmp:adapter=!"=="!_tmp!" (
        if not "!_tmp:IPv4 Address=!"=="!_tmp!" set foundIP="T"
        if not "!_tmp:IP Address=!"=="!_tmp!" set foundIP="T"
        if !foundIP! == "T" (
            for %%i in (%%~h) do (
                if not "%%~i"=="" set "_ip=%%~i"
            )
            set "_ip=!_ip:(Preferred)=!"
            if "!_ip!"=="%~1" (
                @echo !_adapter!
            )
        )
    ) else (
        set "_ip="
        set "_adapter=!_tmp:*adapter =!"
    )
)

exit /b 0

:main
echo --- System info --- >> %logFile%
systeminfo >> %logFile%
echo --- System info END --- >> %logFile%
echo.

for %%i in ("c:\TIMBERMATIC FILES", "c:\prd", "c:\feltgis") do (
    if exist %%i (
         set "folderPath=%%i"
         goto :folderPath
    )
)

:folderPath
if not defined folderPath ( 
	if not defined inputFolderPath (
            echo "Did not find any production files, please enter the path to the production files (e.g. c:\prodfiler), and press ENTER..."
            set /p "inputFolderPath="
	)
)

if not defined folderPath (
    if not exist "%inputFolderPath%" (
            echo "Did not find the folder, please try again"
            goto :folderPath
    )
)

echo.
:retryNetwork
ping -n 3 192.168.20.1 >> %logFile% 2>&1
arp -a | findstr /i "B8-27-EB" > nul
if %errorlevel% neq 0 (
    echo "Did not find the Feltboks on the network based on MAC address prefix" >> %logFile%
    echo "Did not find the Feltboks on the network. Try connecting the Feltboks to another network port"
    set /p "input=Press ENTER when you have connected the Feltboks to another network port..."
    echo "Wating for the Feltboks to become visible on the network..."
    ping 127.0.0.1 -n 15 > nul
    goto :retryNetwork
) else (
    for /f "tokens=2" %%a in ('arp -a ^| findstr /c:"Interface:"') do set "interface_ip=%%a"
    echo "Finding interface name for IP: !interface_ip!" >> %logFile%
    call:get_interfacename !interface_ip! > temp.txt
    set /p interface_name=<temp.txt
    del temp.txt
    echo "Found interface name: !interface_name!" >> %logFile%
)

echo.
echo Set the IP address, subnet mask of the local network interface >> %logFile%
netsh interface ip set address name="!interface_name!" static 192.168.20.10 255.255.255.0 >> %logFile% 2>&1
echo Set 'Automatic metric' to a high value to prevent windows from connecting to the Internet through the RPI >> %logFile%
netsh interface ip set interface "!interface_name!" metric=100 >> %logFile% 2>&1

if defined folderPath (
    set "folderPath=%folderPath%"
) else (
    set "folderPath=%inputFolderPath%"
)

echo.
echo Detecting the Guest account name... >> %logFile%
for /f "tokens=1 delims=" %%A in ('wmic useraccount where "SID like 'S-1-5-%%-501'" get Name /value ^| findstr "="') do (
    set "guestAccountName=%%A"
    set "guestAccountName=!guestAccountName:~5!"
)
for /f "tokens=1,2 delims==" %%A in ('wmic useraccount where "SID like 'S-1-5-%%-501'" get Name^,SID /value 2^>^&1 ^| findstr /i "="') do (
    if /i "%%A"=="Name" (
        set "guestAccountName=%%B"
        for /f "tokens=* delims= " %%C in ("%%B") do set "guestAccountName=%%C"
    )
    if /i "%%A"=="SID" (
        set "guestAccountSID=%%B"
        for /f "tokens=* delims= " %%C in ("%%B") do set "guestAccountSID=%%C"
    )
)
echo Detected Guest Account: %guestAccountName% >> %logFile%
:: Enable Guest Account
echo Enabling %guestAccountName%... >> %logFile%
net user %guestAccountName% /active:yes >> %logFile% 2>&1

:: Add Guest to Necessary Groups
echo Ensuring %guestAccountName% is in Guests and Users groups... >> %logFile%

:: Get Localized Group Name
for /f "tokens=2 delims==, " %%I in ('wmic group where sid^="S-1-5-32-546" get name /value') do set "localizedGroupName=%%I"
echo Localized Guests Group Name: %localizedGroupName% >> %logFile%

:: Get Localized Users Group Name
for /f "tokens=2 delims==, " %%I in ('wmic group where sid^="S-1-5-32-545" get name /value') do set "localizedUsersGroupName=%%I"
echo Localized Users Group Name: %localizedUsersGroupName% >> %logFile%

net localgroup %localizedGroupName% | findstr /i "\<*%guestAccountName%\>" >nul
if %errorlevel% neq 0 (
    net localgroup %localizedGroupName% %guestAccountName% /add >> %logFile% 2>&1
    echo %guestAccountName% added to Guests group. >> %logFile%
) else (
    echo %guestAccountName% is already in the Guests group. >> %logFile%
)

net localgroup %localizedUsersGroupName% | findstr /i "\<%guestAccountName%\>" >nul
if %errorlevel% neq 0 (
    net localgroup %localizedUsersGroupName% %guestAccountName% /add >> %logFile% 2>&1
    echo %guestAccountName% added to Users group. >> %logFile%
) else (
    echo %guestAccountName% is already in the Users group. >> %logFile%
)
echo Guest setup completed. >> %logFile%


echo.
echo Enable network discovery and file sharing >> %logFile%
netsh advfirewall firewall set rule group="@FirewallAPI.dll,-28502" new enable=Yes >> %logFile% 2>&1
netsh advfirewall firewall set rule group="@FirewallAPI.dll,-32752" new enable=Yes >> %logFile% 2>&1
echo Enables Object inheritance, Container inheritance and set Full control for Everyone & Guest for the folder %folderPath% >> %logFile%
icacls %folderPath% /grant *%everyoneGroupSID%:(OI)(CI)(F) >> %logFile% 2>&1
icacls %folderPath% /grant %guestAccountSID%:(OI)(CI)(F) >> %logFile% 2>&1

echo Get the group name for Everyone >> %logFile%
for /f "tokens=1" %%a in ('whoami /groups ^| findstr %everyoneGroupSID%') do set "allGroupName=%%a"
if %allGroupName%=="" (
    echo Current user is not a member of the group Everyone
)

echo.
echo Sharing the folder %folderPath% as %sharedFolderName% >> %logFile%
net share %sharedFolderName%=%folderPath% /grant:%allGroupName%,Full >> %logFile% 2>&1
net share %sharedFolderName%=%folderPath% /grant:%guestAccountName%,Full >> %logFile% 2>&1

echo.
echo Disabling Password Protected Sharing...>> %logFile%
netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes
netsh advfirewall firewall set rule group="File and Printer Sharing" new enable=Yes
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v LimitBlankPasswordUse /t REG_DWORD /d 0 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v everyoneincludesanonymous /t REG_DWORD /d 1 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v restrictanonymous /t REG_DWORD /d 0 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" /v restrictnullsessaccess /t REG_DWORD /d 0 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" /v  everyoneincludeanonymous /t REG_DWORD /d 1 /f
:: Restart the service to apply changes
net stop lanmanserver /y
net start lanmanserver
echo Password Protected Sharing is now disabled. >> %logFile%

echo Copying the log file to %folderPath% >> %logFile%
copy %logFile% %folderPath% >> %logFile% 2>&1


pause
endlocal