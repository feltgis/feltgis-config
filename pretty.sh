#!/bin/bash

COMMAND=$1
if [ -z $COMMAND ]; then
    COMMAND="default"
fi

# Copy fields from one file to another
# Mostly used for updating prod from test or transfering fields to new tenants.
if [ $COMMAND = "copy-fields" ]; then
    ORIGIN_FILE=$2
    OUTPUT_FILE=$3
    if [ ! -f $ORIGIN_FILE ]; then
        echo "source file does not exist"
    fi
    if [ ! -f $OUTPUT_FILE ]; then
        echo "target file does not exist"
    fi
    echo "Copying fields from $ORIGIN_FILE to $OUTPUT_FILE"

    jq --slurpfile origin "$ORIGIN_FILE" \
        '.fields=$origin[0].fields' "$OUTPUT_FILE" | sponge "$OUTPUT_FILE"
    # Run the formatter on the output file
    $SHELL $0 "$OUTPUT_FILE"

elif [ $COMMAND = "copy-single-field" ]; then
    ORIGIN_FILE=$2
    OUTPUT_FILE=$3
    FIELDNAME=$4
    ORIGIN_FILE_FIELDID=$( jq '.fields[] | select(.name=="'$FIELDNAME'") | .fieldid' $ORIGIN_FILE )
    if [ -z $ORIGIN_FILE_FIELDID ]; then
        echo "Input file does not have field with field name '$FIELDNAME'"
        exit 1
    fi

    OUTPUT_FILE_EXISTING_FIELDID=$( jq '.fields[] | select(.name=="'$FIELDNAME'") | .fieldid' $OUTPUT_FILE )
    if [ -z $OUTPUT_FILE_EXISTING_FIELDID ]; then
        NEW_ID=$(jq '(.fields | map(.fieldid) | max + 1)' $OUTPUT_FILE)
        # The field does not already exist in the output file, so we can do a full copy,
        # but we set the fieldid to 
        jq --indent 4 --sort-keys --slurpfile origin "$ORIGIN_FILE" \
            '.fields += [ ($origin[0].fields[] | (select(.name=="'$FIELDNAME'")) | .fieldid = '$NEW_ID')]' "$OUTPUT_FILE" | sponge $OUTPUT_FILE
    else
        # The field does exist in the output file, so we ensure it keeps its field ID
        jq --indent 4 --sort-keys --slurpfile origin "$ORIGIN_FILE" \
            '.fields |= (map(select(.name!="'$FIELDNAME'")) + [($origin[0].fields[] | (select(.name=="'$FIELDNAME'") | .fieldid = '$OUTPUT_FILE_EXISTING_FIELDID'))])' "$OUTPUT_FILE" \
           | sponge "$OUTPUT_FILE"
    fi
    # Run the formatter on the output file
    $SHELL $0 "$OUTPUT_FILE"

elif [ $COMMAND = "diff-fields" ]; then
    ORIGIN_FILE=$2
    DIFF_FILE=$3
    diff -p <(jq --sort-keys '.fields | sort_by(.name)' "$ORIGIN_FILE") <(jq --sort-keys '.fields | sort_by(.name)' "$DIFF_FILE")
    # diff -p <(jq --sort-keys 'reduce .fields[] as $field ({}; .[$field.name] = $field)' "$ORIGIN_FILE") <(jq --sort-keys 'reduce .fields[] as $field ({}; .[$field.name] = $field)' "$DIFF_FILE")
    # jq --sort-keys 'reduce .fields[] as $field ({}; .[$field.name] = $field)' layerdefinitions/FZPPlantingGeom.test.json > fzp.json

elif [ $COMMAND = "add-field" ]; then
    FILE=$2
    FIELD_NAME=$3
    jq --indent 4 --sort-keys '.fields += [{"fieldid": (.fields | map(.fieldid) | max + 1), "name": "'$FIELD_NAME'"}]' "$FILE" | sponge "$FILE"
    # Run the formatter on the output file
    $SHELL $0 "$OUTPUT_FILE"

# If argument is a file, run formatter on it
elif [ -f $COMMAND ]; then
    file=$COMMAND
     # If the file has fields (is a form), also sort the fields by context
        if jq -e 'has("fields")' "$file" &> /dev/null; then
            if ! jq '.fields |= sort_by(.section, .sortix, .fieldid)' --indent 4 --sort-keys "$file" | sponge "$file"; then
                echo "Feil ved behandling av filen: $file"
            fi
        else
             if ! jq --indent 4 --sort-keys . "$file" | sponge "$file"; then
                echo "Feil ved behandling av filen: $file"
            fi
        fi

else
    # Without any arguments, call the old formatter
    for file in $( git ls-files | grep json ); do
        $SHELL $0 $file
    done

fi
