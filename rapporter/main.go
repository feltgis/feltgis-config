package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Regex to match Mustache section with a two-letter prefix
var sectionPattern = regexp.MustCompile(`{{#(..PPlantingGeom\w+)}}`)

// Content to insert before {{comment}} row
var insertContent = `  
  {{#forestowner_covers_cost_for_300_m_Boolean}}
  <tr><td>Kostnad for bæring av planter over 300 meter dekkes av skogeier</td><td>{{forestowner_covers_cost_for_300_m_Boolean}}</td></tr>  
  {{/forestowner_covers_cost_for_300_m_Boolean}}
  {{#hour_price_to_carry_plants}}
  <tr><td>Timespris bæring av planter</td><td>{{hour_price_to_carry_plants}}</td></tr>  
  {{/hour_price_to_carry_plants}}
`

func processFile(filePath string) error {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	text := string(content)
	matches := sectionPattern.FindAllStringSubmatchIndex(text, -1)
	modified := false

	// Iterate in reverse order to avoid index shifting
	for i := len(matches) - 1; i >= 0; i-- {
		match := matches[i]
		sectionStart := match[0]
		sectionName := text[match[2]:match[3]]      // Extract section name (e.g., XXPPlantingGeomYY)
		sectionEndTag := "{{/" + sectionName + "}}" // Proper closing tag

		// Find the correct closing tag
		sectionEnd := strings.Index(text[sectionStart:], sectionEndTag)
		if sectionEnd == -1 {
			continue
		}
		sectionEnd += sectionStart + len(sectionEndTag) // Move to end of closing tag

		// Find the comment line inside the section
		commentLine := `<tr><td>Kommentar til tiltaket</td><td>{{comment}}</td></tr>`
		commentIndex := strings.Index(text[sectionStart:sectionEnd], commentLine)
		if commentIndex == -1 {
			continue
		}
		commentIndex += sectionStart // Adjust to absolute position

		// Insert content before the comment line
		text = text[:commentIndex] + insertContent + text[commentIndex:]
		modified = true
	}

	if modified {
		err = os.WriteFile(filePath, []byte(text), 0644)
		if err != nil {
			return err
		}
		fmt.Println("Updated:", filePath)
	}

	return nil
}

func main() {
	rootDir := "." // Start from current directory

	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.HasPrefix(info.Name(), "skogeieravtale") && strings.HasSuffix(info.Name(), ".html") {
			return processFile(path)
		}
		return nil
	})

	if err != nil {
		fmt.Println("Error:", err)
	}
}
