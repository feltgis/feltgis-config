#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Step 0: Verify that the current branch is master
echo "Checking if the current branch is master..."
CURRENT_BRANCH=$(git branch --show-current)
if [[ "$CURRENT_BRANCH" != "master" ]]; then
  echo "Error: You are not on the master branch. Aborting deployment."
  exit 1
fi

# Check for dry-run flag
DRY_RUN=false
if [[ "$1" == "--dry-run" ]]; then
  DRY_RUN=true
fi

# Step 1: Sync files to the server using rsync
echo "Syncing files to the server using rsync..."
if $DRY_RUN; then
  echo "Running in dry-run mode. No changes will be made."
  rsync -av --dry-run --exclude=".git" ./ feltgis.no@ssh.feltgis.no:/www/config/
else
  rsync -av --exclude=".git" ./ feltgis.no@ssh.feltgis.no:/www/config/
fi

if [[ $? -ne 0 ]]; then
  echo "Error: Failed to sync files to the server. Aborting deployment."
  exit 1
fi

echo "Deployment completed successfully!"
